<?php


// Set output to mail() function.
$new = [
  'outBound_option' => 3,  // 3 is for mail()
];
$mailingBackend = \Civi::settings()->get('mailing_backend');
$mailingBackend = array_merge($mailingBackend, $new);
\Civi::settings()->set('mailing_backend', $mailingBackend);
\Civi::settings()->set('environment', 'Development');


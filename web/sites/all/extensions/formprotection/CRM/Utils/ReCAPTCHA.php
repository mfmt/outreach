<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

use Civi\Formprotection\Recaptcha;
use CRM_Formprotection_ExtensionUtil as E;

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC https://civicrm.org/licensing
 */
class CRM_Utils_ReCAPTCHA {

  protected $_captcha = NULL;

  protected $_name = NULL;

  protected $_url = NULL;

  protected $_phrase = NULL;

  /**
   * Singleton.
   *
   * We only need one instance of this object. So we use the singleton
   * pattern and cache the instance in this variable
   *
   * @var CRM_Utils_ReCAPTCHA
   */
  private static $_singleton = NULL;

  /**
   * Singleton function used to manage this object.
   *
   * @return object
   */
  public static function &singleton() {
    if (self::$_singleton === NULL) {
      self::$_singleton = new CRM_Utils_ReCAPTCHA();
    }
    return self::$_singleton;
  }

  /**
   * Add element to form.
   *
   * @param CRM_Core_Form $form
   */
  public static function add(&$form) {
    // If we already added reCAPTCHA then don't add it again.
    if (!empty($form->get_template_vars('recaptchaHTML'))) {
      return;
    }

    if (\Civi::settings()->get('formprotection_enable_recaptcha') === 'v3') {
        $html = Recaptcha::getV3HTML(\Civi::settings()->get('formprotection_recaptcha_publickey'));
        $form->assign('recaptchaVersion', 3);
        $form->assign('recaptchaHTML', $html);
    }
    else if (\Civi::settings()->get('formprotection_enable_recaptcha') == 'v2checkbox') {
        $html = Recaptcha::getV2HTML(\Civi::settings()->get('formprotection_recaptcha_publickey'));
        $form->assign('recaptchaVersion', 2);
        $form->assign('recaptchaHTML', $html);
        $form->add(
        'text',
        'g-recaptcha-response',
        'reCaptcha',
        NULL,
        TRUE
        );
        if ($form->isSubmitted() && empty($form->_submitValues['g-recaptcha-response'])) {
           $form->setElementError(
               'g-recaptcha-response',
               E::ts('Please go back and complete the CAPTCHA at the bottom of this form.')
           );
        }
    }
  }

  /**
   * Enable ReCAPTCHA on Contribution form
   * THIS FUNCTION IS CALLED FROM CIVICRM CORE
   *
   * @param CRM_Core_Form $form
   * @param bool $checkStandardConditions Check the standard conditions before adding?
   */
  public static function enableCaptchaOnForm(&$form, bool $checkStandardConditions = TRUE) {
    if ($checkStandardConditions && !Recaptcha::checkAreStandardConditionsMetForEnableReCAPTCHA()) {
      return;
    }
    $captcha = CRM_Utils_ReCAPTCHA::singleton();
    if (Recaptcha::hasSettingsAvailable()) {
      $captcha->add($form);
      $form->assign('isCaptcha', TRUE);
      CRM_Core_Region::instance('form-bottom')->add(['template' => 'CRM/common/ReCAPTCHA.tpl']);
    }
  }

}

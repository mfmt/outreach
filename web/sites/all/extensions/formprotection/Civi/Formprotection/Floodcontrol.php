<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

namespace Civi\Formprotection;

use CRM_Formprotection_ExtensionUtil as E;

class Floodcontrol {

  /**
   * @var Forms
   */
  private $forms;

  /**
   * @var string
   */
  private $clientIP;

  public function __construct() {
    $this->forms = new Forms();

    $this->clientIP = Utils::getIPAddress();
  }

  /**
   * Generates the cache 'path' (key) for storing the information
   * on the form request. Currently uses a combination of the formName,
   * form ID and IP address.
   *
   * @param \CRM_Core_Form $form
   *
   * @return string
   */
  public function getFormCachePath(\CRM_Core_Form $form): string {
    $id = $form->getVar('_id');
    $cache_path = 'floodcontrol_contribution_' . $form->getName() . '_' . $id . '_' . $this->clientIP;
    return $cache_path;
  }

  /**
   * If enabled in the settings, adds reCaptcha on the form.
   */
  public function addCaptchaOnErrors(&$form) {
    if (!\Civi::settings()->get('formprotection_floodcontrol_recaptcha')) {
      return;
    }

    if (!\CRM_Core_Session::getLoggedInContactID() && is_callable(['CRM_Utils_ReCAPTCHA', 'enableCaptchaOnForm'])) {
      \CRM_Utils_ReCAPTCHA::enableCaptchaOnForm($form);
    }
  }

  /**
   * Store the timestamp/user in the cache during form load.
   *
   * @param \CRM_Core_Form $form
   */
  public function buildForm(&$form) {
    $context = $this->forms::getContextFromQuickform($form);
    if (!$this->forms->isFormProtectionActive('floodcontrol', $context)) {
      return;
    }

    $cache_path = $this->getFormCachePath($form);
    $data = \Civi::cache('long')->get($cache_path);
    $time = time();

    if (empty($data)) {
      $data = [
        'time' => $time,
        'fail' => 0,
        'success' => 0,
      ];
    }

    \Civi::cache('long')->set($cache_path, $data);

    // Enable reCaptcha
    // buildForm() is called before validate(), so this tends to kick-in after
    // the expected cut-off (if max_count_recaptcha=1, it will start on the 2nd submit)
    // but at least it's more obvious later when the visitor returns, the captcha
    // will be there on the first form load.
    $max_count_recaptcha = \Civi::settings()->get('formprotection_floodcontrol_max_success_recaptcha');

    if ($max_count_recaptcha && $data['success'] >= $max_count_recaptcha) {
      \Civi::log()->warning("floodcontrol: enabling captcha [{$data['success']} success of $max_count_recaptcha]");
      \CRM_Utils_ReCAPTCHA::enableCaptchaOnForm($form);
    }
  }

  /**
   * Validate that the form has been loaded at least X seconds ago.
   * If not, block the user for Y seconds.
   *
   * Assuming it takes at least X seconds to fill in the simplest form, block
   * 10 seconds, reload and resubmit, a very caffeinated user should not be
   * stuck more than once (the X seconds starts from the initial form load and
   * is not reset when there are form errors).
   */
  public function validateForm(&$form, &$errors) {
    $context = $this->forms::getContextFromQuickform($form);
    if (!$this->forms->isFormProtectionActive('floodcontrol', $context)) {
      return;
    }

    $formName = $form->getName();

    $minimum_seconds_before_post = \Civi::settings()->get('formprotection_floodcontrol_minimum_seconds_before_post');

    if ($form->isSubmitted() && !empty($form->_submitValues['g-recaptcha-response'])) {
      \Civi::log()->info("floodcontrol: " . $this->clientIP . " PASS $formName with valid reCaptcha");
      return;
    }

    $cache_path = $this->getFormCachePath($form);
    $data = \Civi::cache('long')->get($cache_path);
    $time = time();
    $seconds_since_first_load = $time - $data['time'];

    if ($seconds_since_first_load < $minimum_seconds_before_post) {
      \Civi::log()->warning("floodcontrol: " . $this->clientIP . " FAIL $formName [{$data['fail']}], filled in $seconds_since_first_load seconds");

      // Simulate a timeout and slow down the attacker
      $slowdown = \Civi::settings()->get('formprotection_floodcontrol_delay_spammers_seconds');

      if ($slowdown) {
        sleep($slowdown);
      }

      $errors['qfKey'] = E::ts('There was a timeout while processing your request. Please wait a few seconds and try again.');

      // Increase before the reporterror email, because by default it's 0 anyway.
      $data['fail']++;
      \Civi::cache('long')->set($cache_path, $data);

      // If Firewall is enabled, log it there
      if (\Civi::settings()->get('formprotection_enable_firewall_integration') && class_exists('\Civi\Firewall\Event\FormProtectionEvent')) {
        \Civi\Firewall\Event\FormProtectionEvent::trigger($this->clientIP, 'floodcontrol: submission too fast on ' . $formName);
      }

      // If the 'reporterror' extension is enabled, send an email to admins.
      // This can be useful to detect false-positives.
      if (function_exists('reporterror_civicrm_handler')) {
        $variables = [
          'message' => 'floodcontrol-1 ' . $this->clientIP,
          'body' => $data['fail'] . ' attempts since @' . date('Y-m-d H:i:s', $data['time']) . ' (' . $seconds_since_first_load . ')',
        ];

        reporterror_civicrm_handler($variables);
      }

      $this->addCaptchaOnErrors($form);
    }
    else {
      // The user may have made an error in the form,
      // do not count it as a successfully processed transaction.
      if (empty($errors)) {
        $data['success']++;
      }

      \Civi::cache('long')->set($cache_path, $data);

      $max_count = \Civi::settings()->get('formprotection_floodcontrol_max_success_count');
      $max_period = \Civi::settings()->get('formprotection_floodcontrol_max_success_period');

      if (!$max_count || !$max_period) {
        \Civi::log()->info("floodcontrol: " . $this->clientIP . " PASS [{$data['success']}] $formName, filled in $seconds_since_first_load seconds");
        return;
      }

      // If the visitor has succeeded more than X time without Y seconds.
      if ($data['success'] > $max_count && $seconds_since_first_load < $max_period) {
        \Civi::log()->warning("floodcontrol: " . $this->clientIP . " FAIL $formName [{$data['fail']}], blocked because {$data['success']} success in $seconds_since_first_load seconds");
        $errors['qfKey'] = E::ts('There was a timeout while processing your request. Please wait a few seconds and try again.');

        // If Firewall is enabled, log it there
        if (\Civi::settings()->get('formprotection_enable_firewall_integration') && class_exists('\Civi\Firewall\Event\FormProtectionEvent')) {
          \Civi\Firewall\Event\FormProtectionEvent::trigger($this->clientIP, 'floodcontrol: too many successful submissions on ' . $formName);
        }

        // If the 'reporterror' extension is enabled, send an email to admins.
        // This can be useful to detect false-positives.
        if (function_exists('reporterror_civicrm_handler')) {
          $variables = [
            'message' => 'floodcontrol-2 ' . $this->clientIP,
            'body' => 'Blocked after ' . $data['success'] . ' attempts since @' . date('Y-m-d H:i:s', $data['time']) . ' (' . $seconds_since_first_load . ')',
          ];

          reporterror_civicrm_handler($variables);
        }

        // Simulate a timeout and slow down the attacker
        $slowdown = \Civi::settings()->get('formprotection_floodcontrol_delay_spammers_seconds');

        if ($slowdown) {
          sleep($slowdown);
        }

        $this->addCaptchaOnErrors($form);

        return;
      }

      if ($seconds_since_first_load > $max_period) {
        // The period expired, reset floodcontrol
        // Ex: say we set max 5 attempts in 120 seconds,
        // the spammer tries 3 times within 250 seconds, then gets whitelisted forever,
        // since the 'time' of first load doesn't get reset.
        // @todo CRM_Core_BAO_Cache::deleteGroup('floodcontrol_contribution', $cache_path);
      }

      \Civi::log()->info("floodcontrol: " . $this->clientIP . " PASS [{$data['success']}] $formName, filled in $seconds_since_first_load seconds");
    }
  }

}

# Form Protection

This provides various ways to help protect forms from spammers:

## Honeypot

Based on https://github.com/elisseck/com.elisseck.civihoneypot

## Flood control

Based on https://lab.civicrm.org/extensions/floodcontrol

## reCAPTCHA

Supports Google reCAPTCHA v2 and v3 (CiviCRM >= 5.53.0 is requried for reCAPTCHA v3)

## Setup

Configure via *Administer->System Settings->Form Protection Settings*

## Support and Maintenance

This extension is supported and maintained by:

[![MJW Consulting](docs/images/mjwconsulting.jpg)](https://www.mjwconsult.co.uk)

We offer paid [support and development](https://mjw.pt/support) as well as a [troubleshooting/investigation service](https://mjw.pt/investigation).

(function(angular, $, _) {
  angular.module('crmRecaptcha3').component('crmRecaptcha3', {
    templateUrl: '~/crmRecaptcha3/crmRecaptcha3.html',
    bindings: {
      recaptchakey: '@',
    },
    require: {
      afForm: '^^',
    },
    controller: function($scope, $element, gRecaptcha) {
      var ctrl = this;

      gRecaptcha.initialize({key: CRM.crmRecaptcha3.recaptchakey}).then(function (apiResult) {

        // @fixme: we would trigger gRecaptcha.execute() ONLY on validate/submit form.
        //   Then we wouldn't need to use a timeout and if a form is submitted again too quickly
        //   after validation it would not fail with duplicate/re-used token error.
        grecaptcha.ready(function () {
          gRecaptcha.execute()
            .then(function (token) {
              // returns token from Google Recaptcha
              var extra = ctrl.afForm.getData('extra');
              extra.recaptcha = token;
            });
          setInterval(function () {
            grecaptcha.ready(function () {
              gRecaptcha.execute()
                .then(function (token) {
                  // returns token from Google Recaptcha
                  var extra = ctrl.afForm.getData('extra');
                  extra.recaptcha = token;
                });
            });
          }, 60 * 1000);
        });
      });
    }
  });
})(angular, CRM.$, CRM._);

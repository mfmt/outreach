<?php

use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/quickform/
 */
class CRM_Mayfirst_Form_Anonymize extends CRM_Core_Form {

  /**
   * @throws \CRM_Core_Exception
   */
  public function buildQuickForm(): void {

    $contributionId = \CRM_Utils_Request::retrieve('contribution_id', 'Integer');
    $contribution = \Civi\Api4\Contribution::get()
      ->addSelect('receive_date', 'total_amount', 'contact_id.display_name')
      ->addWhere('id', '=', $contributionId)
      ->execute()->first();

    $this->assign('contribution_id', $contributionId);
    $this->assign('display_name', $contribution['contact_id.display_name']);
    $this->assign('receive_date', substr($contribution['receive_date'], 0, 10));
    $this->assign('total_amount', $contribution['total_amount']);

    $this->addButtons([
      [
        'type' => 'submit',
        'name' => E::ts('Yes, anonymize it'),
        'isDefault' => TRUE,
      ],
    ]);
    $this->add('hidden', 'contribution_id', $contributionId);
    // export form elements
    parent::buildQuickForm();
  }

  public function postProcess(): void {
    $contributionId = $this->exportValues()['contribution_id'] ?? NULL;
    if (!$contributionId) {
      CRM_Core_Session::setStatus(E::ts('No contribution id was found.'));
    }
    else {
      try {
        \Civi\Api4\MayfirstMember::anonymize()
          ->setContributionId($contributionId)
          ->execute();
        CRM_Core_Session::setStatus(E::ts('Contribution with Id "%1" is now assigned to the Anonymous user.', [
          1 => $contributionId,
        ]));
      }
      catch (\API_Exception $e) {
        CRM_Core_Session::setStatus(E::ts('The contribution was not anonymized: %1', [
          1 => $e->getMessage(),
        ]));
      }
    }
    parent::postProcess();
  }

}

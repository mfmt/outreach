<?php
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * @file Provides an entry point for the angularjs app allowing
 * members to calculate what they would owe depending on their
 * requirements. 
 */
class CRM_Mayfirst_Page_Calculate extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts("May First Membership Dues Calculator"));
    // This css file should be included automatically, but since we are invoking
    // the module in this way, it seems we have to explicitly add it.
    Civi::resources()->addStyleFile('mayfirst', 'ang/mayfirst.css');
    Civi::service('angularjs.loader')
      ->addModules(['mayfirst', 'api4'])
      ->setPageName('mayfirst/calculate')
      ->useApp([
        'defaultRoute' => '/mayfirst/calculate',
      ]);
    parent::run();
  
  }

}

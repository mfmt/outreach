<?php
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * This page is posted to by the renewal page when a user chooses to pay. 
 * It's job is to create the appropriate contribution records and then
 * redirect the user's browser to Pay Pal or Stripe Checkout so they can 
 * complete their transaction.
 *
 */

class CRM_Mayfirst_Page_CheckoutRedirect extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Checkout Redirect'));

    // We need the testMode and token.
    $name = 'testMode';
    $type = 'Int';
    $store = NULL;
    $abort = FALSE;
    $default = 0;
    $method = 'POST';
    $testMode = \CRM_Utils_Request::retrieve($name, $type, $store, $abort, $default, $method );

    $name = 'automaticRenewal';
    $automaticRenewal = \CRM_Utils_Request::retrieve($name, $type, $store, $abort, $default, $method );

    $name = 'token';
    $type = 'String';
    $default = NULL;
    $token = \CRM_Utils_Request::retrieve($name, $type, $store, $abort, $default, $method );

    // Is is stripe or paypal?
    $name = 'paymentMethod';
    $type = 'String';
    $store = NULL;
    $abort = FALSE;
    $default = NULL;
    $method = 'POST';
    $paymentMethod = \CRM_Utils_Request::retrieve($name, $type, $store, $abort, $default, $method );

    try {
      if ($paymentMethod == 'creditcard') {
        $key = 'stripe_payment_processor_id';
      }
      else if ($paymentMethod == 'paypal') {
        $key = 'paypal_payment_processor_id';
      }
      else {
        throw \Exception("Failed to locate payment processor based on paymentMethod: $paymentMethod");
      }

      // We need to fetch the payment processor id for PayPal
      $paymentProcessorId = \Civi\Api4\MayfirstMember::getPaymentProcessorInfo()
        ->setCheckPermissions(FALSE)
        ->setTestMode($testMode)
        ->execute()
        ->first()[$key];

      // This call will redirect the browser.
      \Civi\Api4\MayfirstMember::SubmitPayment()
        ->setCheckPermissions(FALSE)
        ->setToken($token)
        ->setTestMode($testMode)
        ->setPaymentProcessorId($paymentProcessorId)
        ->setAutomaticRenewal($automaticRenewal)
        ->execute();
    }
    catch (\Exception $e) {
      // Let's catch all exceptions so the user doesn't get an ugly error.
      \Civi::log()->debug("Something went wrong redirecting the user. The error is: " . $e->getMessage());
      if ($e->getCode() == 1) {
        $msg = E::ts("It appears that you already have a recurring contribution setup. Please contact members@mayfirst.org if you still need to make a payment.");
      }
      else {
        $msg = E::ts("Sorry, something went wrong when we tried to send you off to pay. Please email support@mayfirst.org to help get this situation resolved. Here is the error: %1.", [1 => $e->getMessage()]);
      }
      CRM_Core_Session::singleton()->setStatus($msg);
      parent::run();

    }
    // echo "Test mode: '$testMode' and token: '$token' and automaticRenewal: '$automaticRenewal' ppid: $paymentProcessorId";
  }


}

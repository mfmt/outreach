<?php
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * @file provide an entry point for the angularjs app allowing
 * members to pay their dues.
 */
class CRM_Mayfirst_Page_Pay extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts("May First Membership"));
    Civi::resources()->addScriptUrl('https://js.stripe.com/v3/');
    // This css file should be included automatically, but since we are invoking
    // the module in this way, it seems we have to explicitly add it.
    Civi::resources()->addStyleFile('mayfirst', 'ang/mayfirst.css');
    // $loader = new \Civi\Angular\AngularLoader();
    Civi::service('angularjs.loader')
      ->addModules(['mayfirst', 'api4', 'angularFileUpload'])
      ->setPageName('mayfirst/pay')
      ->useApp([
        'defaultRoute' => '/mayfirst/pay',
      ]);
    //$loader->load();
    parent::run();
  
  }

}

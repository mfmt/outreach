<?php
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * @file Provides an entry point for the angularjs app allowing
 * members to both sign up for a membership or change their
 * membership. 
 */
class CRM_Mayfirst_Page_Save extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts("May First Membership"));
    Civi::resources()->addScriptUrl('https://js.stripe.com/v3/');
    // This css file should be included automatically, but since we are invoking
    // the module in this way, it seems we have to explicitly add it.
    Civi::resources()->addStyleFile('mayfirst', 'ang/mayfirst.css');
    Civi::service('angularjs.loader')
      ->addModules(['mayfirst', 'api4'])
      ->setPageName('mayfirst/save')
      ->useApp([
        'defaultRoute' => '/mayfirst/save',
      ]);
    parent::run();
  
  }

}

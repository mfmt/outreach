<?php
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Users who pay their membership dues via Pay Pal are 
 * redirected to this page when they complete their payment. 
 *
 */

class CRM_Mayfirst_Page_Thanks extends CRM_Core_Page {

  public function run() {
    Civi::resources()->addStyleFile('mayfirst', 'css/thanks.css');
    CRM_Utils_System::setTitle(E::ts('Thank you for your payment!'));

    $type = 'success';
    $title = E::ts('Payment Complete');
    $msg = E::ts("Your membership dues payment has been successfully applied.");
    CRM_Core_Session::singleton()->setStatus($msg, $title, $type);
    parent::run();

  }


}

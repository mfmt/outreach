<?php
// phpcs:disable
use CRM_Mayfirst_ExtensionUtil as E;
// phpcs:enable

/**
 * Collection of upgrade steps.
 */
class CRM_Mayfirst_Upgrader extends CRM_Extension_Upgrader_Base {
  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).

  /**
   * Install new table for tracking membership contribution metadata.
   *
   * @return TRUE on success
   * @throws CRM_Core_Exception
   */
  public function upgrade_1000(): bool {
    $this->ctx->log->info('Applying update 1000');
    $this->executeSqlFile('sql/mayfirst_install.sql');
    return TRUE;
  }
  /**
   * Copy data to new table.
   *
   * @return TRUE on success
   * @throws CRM_Core_Exception
   */
  public function upgrade_1001(): bool {
    $this->ctx->log->info('Applying update 1001');
    $sql = "INSERT INTO civicrm_mayfirst_contribution SELECT NULL, entity_id, membership_term_start_date_125, deferred_amount_of_membership_pa_126 FROM civicrm_value_membership_du_23 WHERE membership_term_start_date_125 IS NOT NULL";
    \CRM_Core_DAO::executeQuery($sql);
    return TRUE;
  }

  /**
   * Back fill our exchange rate using the rates we
   * had in place for all dates in the past year.
   */
  public function upgrade_1002(): bool {
    $history = [];
    $today = new \DateTimeImmutable();
    $oneYear = new \DateInterval("P1Y");
    $dateIndex = $today->sub($oneYear);
    $oneDay = new \DateInterval("P1D");
    while ($dateIndex < $today) {
      $rate = '20.27';
      $dateIndexString = $dateIndex->format('Y-m-d');
      if ($dateIndexString > '2023-01-12') {
        $rate = '19.026';
      }
      $history[$dateIndexString] = $rate;
      $dateIndex = $dateIndex->add($oneDay);
    }
    \Civi::settings()->set('mayfirst_exchange_rate_history', $history);
    return TRUE;
  }

  /**
   * The translation is too unpredictable when we try to support
   * es_MX. Just make everyone es_ES. 
   */
  public function upgrade_1003(): bool {
    $sql = "UPDATE civicrm_contact SET preferred_language = 'es_ES' WHERE preferred_language = 'es_MX'";
    \CRM_Core_DAO::executeQuery($sql);
    return TRUE;
  }

  /**
   * Moving dues reduction requests to an activity so it's dated
   * and has a status that is tracked. And move internal memo to
   * the notes section so we have just one place to look.
   */
  public function upgrade_1004(): bool {
    $duesReductionRequests = \Civi\Api4\Membership::get()
      ->addWhere('Membership_Details.Dues_Reduction_Description', '!=', '')
      ->addSelect('Membership_Details.Dues_Reduction_Description', 'contact_id')
      ->execute();

    foreach ($duesReductionRequests as $request) {
      $id = $request['contact_id'];
      $requestText = $request['Membership_Details.Dues_Reduction_Description'];
      \Civi\Api4\Activity::create()
        ->addValue('activity_type_id:name', 'Membership_Dues_Reduction_Request')
        ->addValue('subject', 'Dues Reduction Request')
        ->addValue('activity_date_time', '2023-02-09 12:00:00')
        ->addValue('details', $requestText)
        ->addValue('status_id:name', 'Completed')
        ->addValue('target_contact_id', [ $id ])
        ->addValue('source_contact_id', 1)
        ->execute();
    }

    $internalNotes = \Civi\Api4\Membership::get()
      ->addWhere('Membership_Details.Internal_Memo', '!=', '')
      ->addSelect('Membership_Details.Internal_Memo', 'contact_id')
      ->execute();

    foreach ($internalNotes as $note) {
      $id = $note['contact_id'];
      $noteText = $note['Membership_Details.Internal_Memo'];
      \Civi\Api4\Note::create()
        ->addValue('entity_table', 'civicrm_contact')
        ->addValue('entity_id', $id)
        ->addValue('contact_id', 4)
        ->addValue('note', $noteText)
        ->addValue('subject', 'Migrated from Internal Memo')
        ->addValue('note_date', date('Y-m-d H:i:s'))
        ->addValue('created_date', date('Y-m-d H:i:s'))
        ->addValue('modified_date', date('Y-m-d H:i:s'))
        ->addValue('source_contact_id', 1)
        ->execute();
    }
    return TRUE;
  }
}

<?php

namespace Civi\Api4\Action\ControlPanel;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Create all appropriate items in the control panel for this submission.
 *
 * @method string getMemberName
 * @method $this setMemberName(string $memberName)
 * @method string getShortName
 * @method $this setShortName(string $shortName)
 * @method string getDomainName
 * @method $this setDomainName(string $domainName)
 * @method string getEmail
 * @method $this setEmail(string $email)
 * @method string getFirstName
 * @method $this setFirstName(string $firstName)
 * @method string getLastName
 * @method $this setLastName(string $lastName)
 * @method string getPreferredLanguage()
 * @method $this setPreferredLanguage(string $preferredLanguage)
 * @method bool getHostingBenefits
 * @method $this setHostingBenefits(bool $hostingBenefits)
 * @method int setMemberContactId(int $memberContactId)
 * @method $this getMemberContactId(int $memberContactId)
 * @method string setAllocationQuota(int $allocationQuota)
 * @method $this getAllocationQuota(string $allocationQuota)

 *
 */
class CreateMembership extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Member Name 
   *
   * @var string 
   * @required
   *
   */
  protected $memberName = NULL;

  /**
   * Short Name 
   *
   * Used to create the unix group and the initial username.
   *
   * @var string 
   * @required
   *
   */
  protected $shortName = NULL;

  /**
   * Email. 
   *
   * For sending login details.
   *
   * @var string 
   * @required
   *
   */
  protected $email;

  /**
   * First Name. 
   *
   * For setting up password reset contact.
   *
   * @var string
   * @required
   *
   */
  protected $firstName;

  /**
   * Last Name. 
   *
   * For setting up password reset contact.
   *
   * @var string
   * @required
   *
   */
  protected $lastName;

  /**
   * Preferred Language
   *
   * For sending login details.
   *
   * @var string
   * @required.
   */
  protected $preferredLanguage;

  /**
   * Domain Name 
   *
   * @var string 
   * @required
   *
   */
  protected $domainName = NULL;

  /**
   * hosting benefits
   *
   * 0 means no hosting benefits, 1 means yes to hosting benefits. 
   *
   * @var int 
   *
   */
  protected $hostingBenefits = NULL;

  /**
   * Membership Type - 1 or Organization, 2 for Individual. 
   *
   * @var string 
   * @required
   *
   */
  protected $membershipType = NULL;

  /**
   * Member Contact ID. 
   *
   * @var int
   * @required
   *
   */
  protected $memberContactId = NULL;

  /**
   * Allocation Quota
   *
   * The limit on amount the member can allocate.
   *
   * @var int
   * @required
   */
  protected $allocationQuota;

  /**
   * ControlPanel ID. 
   *
   * The value of the membership id from the control panel. When creating
   * a membership in the control panel, this value is populated from the
   * value created.
   *
   * @var int
   *
   */
  private $controlPanelMemberId = NULL;

  /**
   * Unique Unix Group Id
   *
   * This variable is populated upon creation of the group
   *
   * @var int
   */
  private $uniqueUnixGroupId = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    
    // We have not yet created these objects in the control panel.
    $this->validate();
    $this->createUniqueUnixGroup();  
    $this->createMember();
    $this->createHostingOrder();
    $this->createContact();
    $this->updateCiviContact();
    $this->syncUsage();

    $result[] = [
      'id' => $this->controlPanelMemberId,
    ];
  }

  function syncUsage() {
    $membershipId = \Civi\Api4\Membership::get()
      ->addWhere('contact_id', '=', $this->getMemberContactId())
      ->addSelect('id')
      ->execute()->first()['id'];

    \Civi\Api4\ControlPanel::SyncUsage()
      ->setMembershipId($membershipId)
      ->execute();
  }

  function updateCiviContact() {
    $external_identifier = 'member:' . $this->controlPanelMemberId;
    try {
      \Civi\Api4\Contact::update()
        ->setCheckPermissions(FALSE)
        ->addWhere("id", "=", $this->getMemberContactId())
        ->addValue('external_identifier', $external_identifier)
        ->execute();
    }
    catch(\Exception $e) {
      throw new \API_Exception(E::ts("The membership was created, but I failed to update the Civi contact with the external identifier: %1. Exception: %2", [ 
        1 => $external_identifier,
        2 => $e->getMessage(),
      ]));
    }
  }

  function validate() {
    // Make sure unique_unix_group_name is reasonable.
    if (!preg_match('/^[a-z]+$/', $this->getShortName())) {
      throw new \API_Exception(E::ts("Please ensure group short name is all lower case and only letters."));
    }

    // unique unix group
    $params = array(
      'action' => 'select',
      'object' => 'unique_unix_group',
      'where:unique_unix_group_name' => $this->getShortName(),
      'where:unique_unix_group_status' => 'active'
    );
    $results = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    if (isset($results['values'])) {
      throw new \API_Exception(E::ts("That short group name already exists, please try a different one."));
    }

    // unique member name 
    $params = array(
      'action' => 'select',
      'object' => 'member',
      'where:member_friendly_name' => $this->getMemberName(),
      'where:member_status' => 'active'
    );
    $results = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    if (isset($results['values'])) {
      throw new \API_Exception(E::ts("That member name already exists, maybe they are already members?"));
    }

    // unique login name.
    $params = array(
      'action' => 'select',
      'object' => 'item',
      'where:service_id' => 1,  // 1 is a user account
      'where:user_account_login' => $this->getShortName(),
      'where:item_status' => 'active'
    );
    $results = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    if (isset($results['values'])) {
      throw new \API_Exception(E::ts("That short name is in use as a login. Please choose another."));
    }

    // unique domain name.
    $params = array(
      'action' => 'select',
      'object' => 'item',
      'where:service_id' => 9,  // 1 is a domain name 
      'where:dns_fqdn' => $this->getDomainName(),
      'where:item_status' => 'active'
    );
    $results = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    if (isset($results['values'])) {
      throw new \API_Exception(E::ts("That domain name is in use. Please check to see if this membership has already been created, perhaps under a different name?"));
    }
  }

  function createMember() {
    // membership
    if ($this->getHostingBenefits()) {
      $benefits_level = 'standard';
    }
    else {
      $benefits_level = 'basic';
    }

    if ($this->getMembershipType() == 2) {
      $member_type = 'individual';
    }
    else {
      $member_type = 'organization';
    }
    $params = array(
      'action' => 'insert',
      'object' => 'member',
      'set:member_friendly_name' => $this->getMemberName(),
      'set:member_status' => 'active',
      'set:member_benefits_level' => $benefits_level,
      'set:member_type' => $member_type,
      'set:member_quota' => $this->getAllocationQuota() * 1024 * 1024 * 1024,
    );
    $params['set:unique_unix_group_id'] = $this->uniqueUnixGroupId;
    $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);

    if (!array_key_exists('values', $ret)) {
      throw new \API_Exception(E::ts("Something went wrong creating the membership. Membership creation failed"));
    }
    $this->controlPanelMemberId = $ret['values'][0]['member_id'];

    
  }

  function createContact() {
    $params = array(
      'action' => 'insert',
      'object' => 'contact',
      'set:member_id' => $this->controlPanelMemberId,
      'set:contact_first_name' => $this->getFirstName(),
      'set:contact_last_name' => $this->getLastName(),
      'set:contact_email' => $this->getEmail(),
      'set:contact_lang' => $this->getPreferredLanguage() == 'es_ES' ? 'es_MX' : 'en_US',
      'set:contact_billing' => 'n',
    );
    $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);

    if (!array_key_exists('values', $ret)) {
      throw new \API_Exception(E::ts("Something went wrong creating the contact."));
    }
  }
  function createHostingOrder() {
    $params = array(
      'action' => 'insert',
      'object' => 'hosting_order',
      'set:member_id' => $this->controlPanelMemberId,
      'set:hosting_order_identifier' => $this->getDomainName(),
      'set:hosting_order_name' => "Initial hosting order",
      'set:notification_email' => $this->getEmail(),
      'set:notification_lang' => $this->getPreferredLanguage() == 'es_ES' ? 'es_MX' : 'en_US',
      'set:unique_unix_group_name' => $this->getShortName(),
    );
    $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);

    if (!array_key_exists('values', $ret)) {
      throw new \API_Exception(E::ts("Something went wrong creating the hosting order."));
    }
  }

  function createUniqueUnixGroup() {
    $params = array(
      'action' => 'insert',
      'object' => 'unique_unix_group',
      'set:unique_unix_group_name' => $this->getShortName(),
    );
  
    $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    if (!array_key_exists('values', $ret)) {
      throw new \API_Exception(E::ts("Something went wrong creating the membership. Unique unix group creation failed."));
    }
    $this->uniqueUnixGroupId = $ret['values'][0]['unique_unix_group_id'];
  }
}

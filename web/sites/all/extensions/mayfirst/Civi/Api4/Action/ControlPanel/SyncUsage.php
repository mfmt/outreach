<?php

namespace Civi\Api4\Action\ControlPanel;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Sync Usage 
 *
 * Query the control panel to retrieve the usage stats (member quota, disk
 * space allocated, disk space in use, VPS details, hosts in use) and update the
 * corresponding membership custom fields.
 *
 * By default, query for all current members. If a membershipId is passed, only
 * query for that membership id.
 *
 * @method string getMembershipId
 *
 */
class SyncUsage extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Membership Id
   *
   * @var int
   *
   */
  protected $membershipId;

  private $processed = 0;
  private $updated = 0;
  private $servers = [];
  // List of membership ids causing errors.
  private $errors = [];
  public function _run(\Civi\Api4\Generic\Result $result) {
    $optionValues = \Civi\Api4\OptionValue::get()
      ->addSelect('value')
      ->addWhere('option_group_id.name', '=', 'ControlPanelServers')
      ->execute();
    foreach ($optionValues as $optionValue) {
      $this->servers[] = $optionValue['value'];
    }
    $query = \Civi\Api4\Membership::get()
      ->addSelect(
        'id', 
        'contact_id',
        'contact_id.external_identifier', 
        'contact_id.Member_Hosting_Details.Control_Panel_Quota',
        'contact_id.Member_Hosting_Details.Control_Panel_Allocated',
        'contact_id.Member_Hosting_Details.Control_Panel_Virtual_Private_Servers',
        'contact_id.Member_Hosting_Details.Control_Panel_Cpu',
        'contact_id.Member_Hosting_Details.Control_Panel_Ram',
        'contact_id.Member_Hosting_Details.Control_Panel_Ssd',
        'contact_id.Member_Hosting_Details.Control_Panel_Usage',
        'contact_id.Member_Hosting_Details.Servers',
      )
      ->addWhere('status_id:name', 'IN', ['Current', 'Grace', 'Expired', 'Disabled'])
      ->addWhere('contact_id.is_deleted', '=', 0);

    if ($this->getMembershipId()) {
      // Restrict to just the requested membership id.
      $query = $query->addWhere('id', '=', $this->getMembershipId());
    }
    $memberships = $query->execute();
    if ($memberships->count() == 0) {
      throw new \API_Exception("No matching memberships were found.");
    }

    foreach($memberships as $membership) {
      $this->setMembershipControlPanelUse($membership);
    }
    if (count($this->errors) > 0) {
      throw new \API_Exception("Could not determine the control panel membership id for the following membership ids:" . implode(',', $this->errors));
    }
    $result[] = [ 
      'Processed memberships:' => $this->processed,
      'Updated memberships:' => $this->updated,
    ];
  }

  protected function extractControlPanelMemberId($externalIdentifier) {
    // The external id should be in the form: member:1234. We want to extra the
    // 1234 to get the member_id value in the control panel.
    return intval(str_replace('member:', '', $externalIdentifier));
  }

  protected function setMembershipControlPanelUse($membership) {
    $membershipId = $membership['id'];
    $contactId = $membership['contact_id'];
    $controlPanelMemberId = $this->extractControlPanelMemberId($membership['contact_id.external_identifier']); 
    if (!$controlPanelMemberId) {
      \Civi::log()->debug("Could not extract the control panel member id from the external 
        identifier for contact assigned to membership with membership id: " . $membershipId);
      $this->errors[] = $membershipId;
      return;
    }

    // Retrieve info from the control panel.
    $params = [
      'action' => 'select',
      'object' => 'member',
      'where:member_id' => $controlPanelMemberId,
    ];
    try {
      $results = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    }
    catch (\Exception $e) {
      // Don't let a control panel error stop us.
      \Civi::log()->debug("Got a control panel error while sync storage stats. Membership ID: 
        $membershipId, Control Panel member id: $controlPanelMemberId and the error is: " . 
        $e->getMessage());
      return;
    }
    $this->processed++;
    $values = array_pop($results['values']);
    $memberQuota = $values['member_quota'] ?? 0;
    $memberAllocatedQuota = $values['member_allocated_quota'] ?? 0;
    $memberDiskUsage = $values['member_disk_usage'] ?? 0;
    $memberSsd = $values['member_ssd'] ?? 0;
    $memberServers = array_filter(explode(',', $values['member_servers'] ?? []));

    // Ensure we have a value for every memberServer.
    foreach ($memberServers as $server) {
      if (!in_array($server, $this->servers)) {
        $this->servers[] = $server;
        $results = \Civi\Api4\OptionValue::create()
          ->addValue('option_group_id.name', 'ControlPanelServers')
          ->addValue('label', $server)
          ->addValue('name', $server)
          ->addValue('value', $server)
          ->execute();
      }
    }
    $controlPanelValues = [
      'contact_id.Member_Hosting_Details.Control_Panel_Quota' => round(intval($memberQuota) / 1024 / 1024 / 1024),
      'contact_id.Member_Hosting_Details.Control_Panel_Allocated' => round(intval($memberAllocatedQuota) / 1024 / 1024 / 1024),
      'contact_id.Member_Hosting_Details.Control_Panel_Usage' => round(intval($memberDiskUsage) / 1024 / 1024 / 1024),
      'contact_id.Member_Hosting_Details.Control_Panel_Virtual_Private_Servers' => $values['member_virtual_private_servers'] ?? 0,
      'contact_id.Member_Hosting_Details.Control_Panel_Cpu' => $values['member_cpu'] ?? 0,
      'contact_id.Member_Hosting_Details.Control_Panel_Ram' => $values['member_ram'] ?? 0,
      'contact_id.Member_Hosting_Details.Control_Panel_Ssd' => round(intval($memberSsd) / 1024 / 1024 / 1024),
      'contact_id.Member_Hosting_Details.Servers' => $memberServers,
    ];

    // Only update if different.
    $update = FALSE;
    foreach ($controlPanelValues as $key => $value) {
      if ($membership[$key] != $value) {
        $update = TRUE;
      }
    }
    if ($update) {
      $update = \Civi\Api4\Contact::update();
      foreach ($controlPanelValues as $key => $value) {
        $key = str_replace('contact_id.', '', $key);
        $update = $update->addValue($key, $value);
      }
      $update = $update->addWhere('id', '=', $contactId);
      $update->execute();
      $this->updated++;
    }
  }


}

<?php

namespace Civi\Api4\Action\ControlPanel;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Update the Quota in the control panel for the
 * given contactId.
 *
 * @method int setMembershipId(int $membershipId)
 * @method $this getMembershipId(int $membershipId)
 */
class UpdateQuota extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Membership ID. 
   *
   * @var int
   * @required
   */
  protected $membershipId = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    
    $membershipId = $this->getMembershipId();

    // First sync usage to make sure we can set this quota (the member have allocated more
    // than the quota already).
    \Civi\Api4\ControlPanel::SyncUsage()
      ->setMembershipId($membershipId)
      ->execute();

    $contactId = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $membershipId)
      ->addSelect('contact_id')
      ->execute()->first()['contact_id'];

    $quota = \Civi\Api4\Dues::get()
      ->setContactId($contactId)
      ->execute()->first()['storage'];
    
    $contact = \Civi\Api4\Contact::get()
      ->addWhere('id', '=', $contactId)
      ->addSelect('external_identifier', 'Member_Hosting_Details.Control_Panel_Allocated')
      ->execute()->first();
    $externalId = $contact['external_identifier'];
    $allocated = $contact['Member_Hosting_Details.Control_Panel_Allocated'];

    if ($quota < $allocated) {
      throw new \API_Exception("The member has already allocated $allocated GB, so you cannot set the quota to the lower value of $quota.");
    }
    $externalIdParts = explode(':', $externalId);
    $controlPanelId = $externalIdParts[1];

    if (!$controlPanelId) {
      throw new \API_Exception("Failed to extract control panel id from member with contact id: $contactId and external identifier: $externalId");
    }

    
    $params = array(
      'action' => 'update',
      'object' => 'member',
      'set:member_quota' => $quota * 1024 * 1024 * 1024,
      'where:member_id' => $controlPanelId,
    );
    $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);

    $error = $ret['is_error'];
    if ($error != 0) {
      throw new \API_Exception(E::ts("Something went wrong updating the membership: %1.", [ 1 =>  $ret['error_message']]));
    }

    // Sync Usage again so we see the new number reflected.
    \Civi\Api4\ControlPanel::SyncUsage()
      ->setMembershipId($membershipId)
      ->execute();

    $result[] = [ "Quota updated to $quota" ];
  }

}

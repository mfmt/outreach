<?php

namespace Civi\Api4\Action\ControlPanel;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Update the Quota in the control panel for all records.
 * If the member has allocated more then allowed, then
 * update the quota to be 10% more then what they have
 * allocated.
 *
 */
class UpdateQuotaAll extends \Civi\Api4\Generic\AbstractAction {

  public function _run(\Civi\Api4\Generic\Result $result) {
    
    $memberships = \Civi\Api4\Membership::get()
      ->addSelect('id', 'Membership_Details.Plan', 'contact_id.display_name', 'contact_id')
      ->addWhere('status_id:name',  'IN', ['Disabled','Expired'])
      ->addWhere('membership_type_id:name', 'IN', ['Monthly', 'Annual'])
      ->execute();

    $count = 0;
    $adjusted1 = 0;
    $adjusted2 = 0;
    $adjusted3 = 0;
    $adjusted4 = 0;
    $rows = [];
    foreach($memberships as $membership) {
      $membershipId = $membership['id'];
      $displayName = $membership['contact_id.display_name'];
      $plan = $membership['Membership_Details.Plan'];
      $contactId = $membership['contact_id']; 

      $quota = \Civi\Api4\Dues::get()
        ->setContactId($contactId)
        ->execute()->first()['storage'];
      
      $contact = \Civi\Api4\Contact::get()
        ->addWhere('id', '=', $contactId)
        ->addSelect(
          'external_identifier',
          'Member_Hosting_Details.Control_Panel_Allocated',
          'Member_Hosting_Details.Control_Panel_Usage',
          'Member_Hosting_Details.Control_Panel_Quota'
        )
        ->execute()->first();
      $externalId = $contact['external_identifier'];
      $allocated = $contact['Member_Hosting_Details.Control_Panel_Allocated'];
      $usage = $contact['Member_Hosting_Details.Control_Panel_Usage'];
      $currentQuota = $contact['Member_Hosting_Details.Control_Panel_Quota'];

      if ($currentQuota == $quota) {
        // Already up to date.
        continue;
      }

      if ($quota < $allocated) {
        // Reset $quota to be 10% more then allocated.
        $quota = .10 * $allocated + $allocated;
        $adjustVar = "adjusted{$plan}";
        $$adjustVar++;
      }
      $externalIdParts = explode(':', $externalId);
      $controlPanelId = $externalIdParts[1];

      #echo "$plan,$controlPanelId,\"$displayName\",$quota,$allocated,$usage\n";
      if (!$controlPanelId) {
        echo "Failed to extract control panel id from member with contact id: $contactId and external identifier: $externalId\n";
        continue;
      }

      $params = array(
        'action' => 'update',
        'object' => 'member',
        'set:member_quota' => ceil($quota * 1024 * 1024 * 1024),
        'where:member_id' => $controlPanelId,
      );
      try {
        $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);
      }
      catch (\Exception $e) {
        echo "Member contact $contactId has error updating control panel: " . $e->getMessage() . "\n";
        continue;
      }

      $error = $ret['is_error'];
      if ($error != 0) {
        echo "Member contact $contactId has error updating control panel: " . $e->getMessage() . "\n";
        continue;
        // throw new \API_Exception(E::ts("Something went wrong updating the membership: " . $ret['error_message']));
      }
      $count++;
    }

    $result[] = [ "Total:" => $count, "Adjusted1:" => $adjusted1, 'Adjusted2' => $adjusted2, 'Adjusted3' => $adjusted3, 'Adjusted4' => $adjusted4 ];
  }

}

<?php

namespace Civi\Api4\Action\Dues;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Calculate dues based on plan, and requested services. 
 *
 * This call doesn't touch the database and disclose any private information.
 *
 * @method string getFrequency
 * @method $this setFrequeny(string $frequency)
 * @method int getVirtualPrivateServers
 * @method $this setVirtualPrivateServers(int $virtualPrivateServers)
 * @method int getExtraStorage
 * @method $this setExtraStorage(int $extraStorage)
 * @method int getPlan
 * @method $this setPlan(int $plan)
 * @method int getIncome
 * @method $this setIncome(int $income)
 * @method string getCurrency
 * @method $this setCurrency(string $currency)
 * @method int getHostingBenefits
 * @method $this setHostingBenefits(int $hostingBenefits)
 * @method int cpu getCpu
 * @method $this setCpu(int $cpu)
 * @method int ram getRam
 * @method $this setRam(int $ram)
 * @method int getSsd
 * @method $this setSsd(int $ssd)
 *
 */

class Calculate extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Dues payment frequency: annual or monthly 
   *
   * @var string 
   *
   */
  protected $frequency;

  /*
   * Currency frequency.
   */
  protected $_FREQUENCY_ANNUAL = 'Annual';
  protected $_FREQUENCY_MONTHLY = 'Monthly';

  /**
   * Number of virtual private servers. 
   *
   * @var int
   */
  protected $virtualPrivateServers = 0;

  /**
   * Extra storage in GBs. 
   *   
   * If you want to purchase extra storage, enter the
   * amount in GBs.
   *
   * @var int 
   */
  protected $extraStorage = 0;

  /**
   * Plan
   *
   * Plan should be left out if it's a "basic" plan.
   * @var int
   */
  protected $plan;

  /**
   * Income 
   *
   * Only required with VPS.
   *
   * @var int
   */
  protected $income;

  /** Cpu 
   *
   * Only required with VPS.
   *
   * @var int
   */
  protected $cpu = 0;

  /** Ram 
   *
   * Only required with VPS.
   *
   * @var int
   */
  protected $ram = 0;

  /** Ssd 
   *
   * Only required with VPS.
   *
   * @var int
   */
  protected $ssd = 0;

  /**
   * Currency - either USD or MXN
   *
   * @var string 
   */
  protected $currency = 'USD';

  /**
   *
   * storageLimit - throw an error if total storage size exceeds this number of
   * GB's and we are being called by a user that is not logged in. Set to 0 for
   * not limit.
   *
   * @var int
   */
  protected $storageLimit = 800;

  /**
   *
   * Static option for currency.
   *
   */
  protected $_CURRENCY_USD = 'USD';
  protected $_CURRENCY_MXN = 'MXN';

  /**
   * Hosting benefits or no hosting benefits? 
   *
   * Enter 0 for no hosting benefits and 1 for hosting benefits.
   *
   * @var int 
   */
  protected $hostingBenefits = 0;

  /**
   *
   * Static option for hosting benefits.
   *
   */
  protected $_HOSTING_BENEFITS_NO = 0;
  protected $_HOSTING_BENEFITS_YES = 1;

  /**
   * The calculated dues based on the other parameters provided
   * in the requested currency..
   *
   * @var float
   */
  protected $_dues;

  /**
   * The calculated dues based on the other parameters provided in
   * USD.
   *
   * @var float
   */
  protected $_dues_usd;

 /**
   * The calculated total storage based on the other parameters.
   *
   * @var int
   */
  protected $_storage;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    $this->calculate_annual_dues();
    $this->adjust_dues_based_on_frequency();
    $this->adjust_dues_based_on_exchange_rate();
    $result[] = [
      'frequency' => $this->getFrequency(),
      'virtualPrivateServers' => $this->getVirtualPrivateServers(),
      'extraStorage' => $this->getExtraStorage(),
      'plan' => $this->getPlan(),
      'income' => $this->getIncome(),
      'currency' => $this->getCurrency(),
      'hostingBenefits' => $this->getHostingBenefits(),
      'dues' => round($this->_dues),
      'dues_usd' => round($this->_dues_usd),
      'storage' => $this->_storage,
      'cpu' => $this->getCpu(),
      'ram' => $this->getRam(),
      'ssd' => $this->getSsd(),
    ];

  }

  private function calculate_annual_dues() {
    $associative = TRUE;
    $schedule = json_decode($this->get_dues_schedule(), $associative);

    // Basic sanity checking.
    $plan = $this->getPlan();
    if ($plan) {
      if (!array_key_exists($plan, $schedule['plan'])) {
        throw new \API_Exception(E::ts("You have specified a plan that does not exist: '%1'.", [ 1 => $this->getPlan() ]));
      }
    }
    $income = $this->getIncome();
    if ($income) { 
      if (!array_key_exists($income, $schedule['income'])) {
        throw new \API_Exception(E::ts("You have specified an income that does not exist: '%1'.", [ 1 => $this->getIncome() ]));
      }
      if ($income > $this->getPlan()) {
        throw new \API_Exception(E::ts("Your income level cannot be higher then your plan."));
      }
    }

    $allowed_frequencies = [ $this->_FREQUENCY_ANNUAL, $this->_FREQUENCY_MONTHLY ];
    if (!in_array($this->getFrequency(), $allowed_frequencies)) {
      throw new \API_Exception(E::ts("You have specified an invalid frequency: %1.", [ 1 =>  $this->getFrequency()]));
    }
    $allowed_currencies = [ $this->_CURRENCY_MXN, $this->_CURRENCY_USD ];
    if (!in_array($this->getCurrency(), $allowed_currencies)) {
      throw new \API_Exception(E::ts("We do not support that currency."));
    }
    $allowed_hosting_benefits = [ $this->_HOSTING_BENEFITS_NO, $this->_HOSTING_BENEFITS_YES ];
    if (!in_array($this->getHostingBenefits(), $allowed_hosting_benefits)) {
      throw new \API_Exception(E::ts("Please enter either 0 or 1 for hosting benefits."));
    }

    // Several fields must be non-negative integers.
    $non_negative_integers = ['ExtraStorage', 'Cpu', 'Ram', 'Ssd', 'VirtualPrivateServers'];
    foreach ($non_negative_integers as $field) {
      $func = 'get' . $field;
      $value = $this->$func();
      if ($value < 0) {
        throw new \API_Exception(E::ts("Please enter a number greater then 0."));
      }
    }
    // No hosting benefits is easy.
    if ($this->getHostingBenefits() == $this->_HOSTING_BENEFITS_NO) {
      if (!empty($this->getVirtualPrivateServers())) {
        throw new \API_Exception(E::ts("Virtual Private Servers are not allowed without hosting benefits."));
      }
      if ($this->getExtraStorage() > 0) {
        throw new \API_Exception(E::ts("Extra storage is not allowed without hosting benefits."));
      }
      $this->_storage = $schedule['plan']['basic']['storage'];
      $this->_dues = $schedule['plan']['basic']['fee'];
      return;
    }

    // No extras, easy.
    if ($this->getVirtualPrivateServers() == '' && $this->getExtraStorage() === 0) {
      $this->_storage = $schedule['plan'][$this->getPlan()]['storage'];
      $this->_dues = $this->getStorageCosts();
      return;
    }

    // Hold our base storage.
    $this->_storage = $schedule['plan'][$this->getPlan()]['storage'];

    // Add extra storage.
    if ($this->getExtraStorage() > 0) {
      $this->_storage += $this->getExtraStorage();
      if ($this->getPlan() == 4) {
        if (!\CRM_Core_Session::getLoggedInContactID()) {
          $storageLimit = $this->getStorageLimit();
          if ($storageLimit && $this->_storage > $storageLimit) {
            throw new \API_Exception(E::ts("Please email us if you need this much storage so we are sure we can provide it for you."));
          }
        }
      }
      else {
        if ($this->_storage >= $schedule['plan'][$this->getPlan() + 1]['storage']) {
          throw new \API_Exception(E::ts("If you need this much storage, please move to the next higher membership plan or if you are at the top, contact support for assistance."));
        }
      }
      // Extra storage is billed at 10GB increments.
    }
    // Calculate dues before VPS additions.
    $this->_dues = $this->getStorageCosts();

    $vps = $this->getVirtualPrivateServers();
    if ($vps > 0) {
      $income = $this->getIncome();
      if (is_null($income)) {
        throw new \API_Exception(E::ts("You must include income if you specify virtual private servers."));
      }
      if ($this->getCpu() < $vps) {
        throw new \API_Exception(E::ts("You must have at least as many CPUs as virtual private servers."));
      }
      if ($this->getRam() < $vps) {
        throw new \API_Exception(E::ts("You must have at least one GB of RAM per VPS."));
      }
      $this->_dues += $schedule['vps'][$income] * $vps * 12;

      // Now calculate RAM, CPU and SSD
      $this->_dues += $schedule["cpu"] * $this->getCpu() * 12;
      $this->_dues += $schedule["ram"] * $this->getRam() * 12;

      // ssd is per year.
      $this->_dues += $schedule["ssd"] * $this->getSsd();
    }
  }

  private function getStorageCosts() {
    if ($this->_storage > 500 ) {
      $perGig = 1.0;
    }
    else {
      $associative = TRUE;
      $schedule = json_decode($this->get_dues_schedule(), $associative);
      $var1 = $schedule["storage_algorithm"]["var1"];
      $var2 = $schedule["storage_algorithm"]["var2"];
      $var3 = $schedule["storage_algorithm"]["var3"];
      $exp = $schedule["storage_algorithm"]["exp"];
      // Using a 4 polynomial non-linear equation produced courtesy of:
      // https://mycurvefit.com/ based on our data.
      $perGig = $var1 + ( ($var2 - $var1) / (1 + pow(($this->_storage/$var3) , $exp)));
    }
    return $perGig * $this->_storage;

  }
  private function adjust_dues_based_on_frequency() {
    if ($this->getFrequency() == $this->_FREQUENCY_ANNUAL) {
      // Nothing to do.
      return;
    }

    // We lose .30 cents on every transaction, so pass that cost onto
    // people who want to pay monthly and ensure we round up. 
    $this->_dues = ceil($this->_dues / 12 + .30);
  }

  private function adjust_dues_based_on_exchange_rate() {
    $associative = TRUE;
    $schedule = json_decode($this->get_dues_schedule(),$associative);

    // Save the USD value.
    $this->_dues_usd = $this->_dues;
    if ($this->getCurrency() == $this->_CURRENCY_USD) {
      // Nothing to do.
      return;
    }
    // Calculated MXN value.
    $rate = \Civi\Api4\Dues::Exchange()->execute()->first()['rate'];
    $this->_dues = $this->_dues * $rate;
  }

  private function get_dues_schedule() {
    return file_get_contents(\CRM_Core_Config::singleton()->extensionsDir . "mayfirst/dues.json");
  }
}






?>

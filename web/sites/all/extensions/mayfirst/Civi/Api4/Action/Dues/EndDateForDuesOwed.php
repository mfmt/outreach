<?php

namespace Civi\Api4\Action\Dues;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Calculate the new end date of a membership based on change in amount owed in
 * the middle of a membership term.
 *
 * Given a start date, previous membership dues and new membership dues,
 * calculate a new expiration date. 
 *
 * This call should not touch the database or have access to private data.
 *
 */

class EndDateForDuesOwed extends \Civi\Api4\Generic\AbstractAction {

  /**
   * startDate. 
   *
   * @var string 
   * @required
   *
   */
  protected $startDate;

  /**
   * endDate. 
   *
   * @var string 
   * @required
   *
   */
  protected $endDate;

  /**
   * previousDues 
   *
   * @var int 
   * @required
   *
   */
  protected $oldDues;

  /**
   * newDues 
   *
   * @var int 
   * @required
   *
   */
  protected $newDues;


  public function _run(\Civi\Api4\Generic\Result $result) {
    $start = \DateTimeImmutable::createFromFormat('Y-m-d', $this->getStartDate());
    $end = \DateTimeImmutable::createFromFormat('Y-m-d', $this->getEndDate());
    $today = new \DateTimeImmutable();

    if ($today > $end) {
      // They are not up to date on their dues. No adjustment necessary.
      $result[] = [ 'date'  => 'Behind on dues, no adjustment necessary.' ];
      return;
    }

    if (!$start) {
      throw new \API_Exception(E::ts("Failed to parse the startDate - is it a valid ISO formatted date?"));
    }
    if (!$end) {
      throw new \API_Exception(E::ts("Failed to parse the endDate - is it a valid ISO formatted date?"));
    }

    // What is the interval between start and end? How many days is it?
    $intervalTerm = $start->diff($end);
    $daysTerm = $intervalTerm->format("%a");

    // As of today, how many days have passed since the term began? 
    $intervalPassed = $start->diff($today);
    $daysPassed = $intervalPassed->format("%a");

    // What percentage of the total term has passed?
    $percentPassed = $daysPassed / $daysTerm;

    // How much of the old dues have been consumed?
    $duesConsumed = $percentPassed * $this->getOldDues();

    // How much in dues have not yet been consumed?
    $duesPrePaid = $this->getOldDues() - $duesConsumed;

    // What is the new daily rate?
    $newDailyRate = $this->getNewDues() / $daysTerm;

    // How many future days are covered with the pre-paid amount? 
    $futureDaysPaid = $duesPrePaid / $newDailyRate;

    // How many total days are paid for?
    $totalDaysPaid = ceil($daysPassed + $futureDaysPaid);

    // What date is that?
    $interval = new \DateInterval("P{$totalDaysPaid}D");
    $result[] = [ 'date'  => $start->add($interval)->format('Y-m-d') ];
  }
}
?>

<?php

namespace Civi\Api4\Action\Dues;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Calculate the end date of a membership based on amount paid.
 *
 * Given a startDate, AmountOwed, TermLength, TermCount, and AmountPaid,
 * calculate an end date that takes into account a partial payment. 
 *
 * This call should not touch the database or have access to private
 * data.
 *
 */

class EndDateForDuesPaid extends \Civi\Api4\Generic\AbstractAction {

  /**
   * startDate. 
   *
   * @var string 
   * @required
   *
   */
  protected $startDate;

  /**
   * renewalAmount 
   *
   * We always round renewalAmounts to ints.
   * @var int 
   * @required
   *
   */
  protected $renewalAmount;

  /**
   * membershipType 
   *
   * @var string
   * @required
   */
  protected $membershipType;

  /**
   * valid options for termLength
   */
  private $membershipTypeOptions = [
    'Monthly', 'Annual'
  ];

  /**
   * termCount
   *
   * @var int
   * @required
   */
  protected $numTerms = 1;

  /**
   * amountPaid
   *
   * Might be a string or a float, will be converted to float.
   * @var mixed 
   * @required
   */
  protected $amountPaid;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $start = \DateTimeImmutable::createFromFormat('Y-m-d', $this->getStartDate());

    if (!$start) {
      throw new \API_Exception(E::ts("Failed to parse the startDate - is it a valid ISO formatted date?"));
    }
    if (!in_array($this->getMembershipType(), $this->membershipTypeOptions)) {
      throw new \API_Exception(E::ts("Please provide a membership type that is either 'Annual' or 'Monthly'."));
    }
    if ($this->getNumTerms() < 1) {
      throw new \API_Exception(E::ts("Please ensure numTerms is 1 or higher."));
    }
    
    // Set the standard number of days that the membeship should be extended.
    // "One Month" and "One Year" varies depending on the actual start date
    // (each month has a different number of days and the leap year has one
    // extra day).
    if ($this->getMembershipType() == 'Monthly') {
      $period = new \DateInterval("P1M");
    }
    else {
      $period = new \DateInterval("P1Y");
    }
    // Convert this period to days so we can do math on it.
    $end = $start->add($period);
    $interval = $start->diff($end);
    $days = $interval->format("%a");
    $termDays = $days * $this->getNumTerms();
    $percentPaid = floatVal($this->getAmountPaid()) / ($this->getRenewalAmount() * $this->getNumTerms());

    // Do some basic checking here. If things are too weird, we throw an error.
    if ($percentPaid < .5 && $this->getNumTerms() > 1) {
      // The admin has indicated that this payment should pay for more than 1
      // term AND the amount being paid is way too low to justify that.
      $msg = E::ts("You are indicating that the membership should be advanced %1 numTerms, but your payment (%2) seems way to low compared to what should be owed (%3).", 
        [1 => $this->getNumTerms(), 2 => $this->getAmountPaid(), 3 => $this->getRenewalAmount()]);
      throw new \API_Exception($msg);
    }
    elseif ($percentPaid > 2) {
      // The admin has entered a payment that is more then twice what
      // it should be.
      $msg = E::ts("You are indicating that the membership should be advanced %1 numTerms, but your payment (%2) is more than twice what should be owed (%3).", 
        [1 => $this->getNumTerms(), 2 => $this->getAmountPaid(), 3 => $this->getRenewalAmount()]);
      throw new \API_Exception($msg);
    }

    // Adjust the number of days based on the percentage that they are paying.
    $termDays = round($percentPaid * $termDays, 0);
    
    // Calculate the end date.
    $interval = new \DateInterval("P{$termDays}D");
    $result[] = [ 'date'  => $start->add($interval)->format('Y-m-d') ];
  }
}
?>

<?php

namespace Civi\Api4\Action\Dues;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Lookup (and cache) the USD/MXN exchange rate from the IMF and provide prices
 * in MXN. 
 *
 * This API call does a lot. 
 *
 * The most basic job is to provide the USD/MXN exchange rate for today or for
 * a given membershipId (based on the membership's end date) or simply for any
 * date in the past (if you provide the $date parameter). The source of truth
 * is the IMF API. This call takes care to not query the IMF API more than once
 * in a two hour period by caching results. If you want to bust the cache, pass
 * $force=1.
 *
 * In addition, we maintain a history of exchange rates going back one year.
 * Since we are never quite sure when the IMF will update the exchange rate for
 * "today" you should only reliably count on getting the accurate exchange rate
 * for yesterday. Also, we must call this API at least once a day, preferable
 * after 9:00 pm or so to ensure we set the history for todays's exchange rate.
 *
 */

class Exchange extends \Civi\Api4\Generic\AbstractAction {

  /**
   *
   * Force
   *
   * Set force to 1 to bypass the cache and update the cache and history.
   *
   * @var int 
   *
   **/
  protected $force = 0;

  /**
   *
   * Date
   *
   * Set the date to request the exchange rate for the given date
   * instead of today. Date must be a date within the last year, in
   * ISO YYYY-MM-DD format.
   *
   * @var string
   *
   */
  protected $date = NULL;

  /**
   *
   * MembershipId
   *
   * Optionally provide a membershipId to return the exchange rate
   * for the given membership (based on the membership's end date).
   *
   * @var int
   */
  protected $membershipId;

  /**
   *
   * rate
   *
   * The exchange rate.
   */
  private $rate;

  /**
   *
   * Amount
   *
   * The amount if MXN if a membershipId is provided.
   *
   * @var Decimal
   */
  private $amount;

  /**
   *
   * Explanation
   *
   * More details about the provided exchange rate if a membershipId is
   * provided (e.g. whether it is locked in or not).
   *
   */
  private $explanation;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    // Default explanation.
    $this->explanation = E::ts("The exchange rate provided is not locked in and may change when you make your payment.");

    if ($this->getForce()) {
      $this->debugLog("Force is enabled.");
    }

    if ($this->getMembershipId()) {
      $this->calculateMembershipRate();
    }
    elseif ($this->getDate()) {
      $this->calculatePreviousRate();   
    }
    else {
      $this->calculateTodayRate();
    }
    $result[] = [ 
      'rate' =>  $this->rate,
      'amount' => $this->amount,
      'explanation' => $this->explanation,
    ];
  }

  private function debugLog($msg) {
    \Civi::log()->debug($msg);
  }

  private function calculatePreviousRate() {
    // Check if date is valid.
    if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $this->getDate())) {
      throw new \API_Exception("That does not look like a valid date: " .
        $this->getDate());
    }
    $dateTs = strtotime($this->getDate());
    if (!$dateTs) {
      throw new \API_Exception("That may look like a valid date, but no 
        such date actually exist: " . $this->getDate());
    }
    if ($dateTs > time()) {
      throw new \API_Exception("Cannot use a date in the future: " .
        $this->getDate());
    }
    if ($dateTs < strtotime("-1 year")) {
      throw new \API_Exception("Cannot fetch the exchange rate for a date 
        that is more than one year ago: " . $this->getDate());
    }
    // Try to get the exchange rate for this date.
    $this->rate = \Civi::settings()->get('mayfirst_exchange_rate_history')[$this->getDate()] ?? NULL;
    if (!$this->rate) {
      // For whatever reason, we don't have an exchange rate for this date, and
      // we should. So, we use what today's exchange rate is and then save it
      // in our history file to be sure we consitently use it, even if it's
      // wrong.
      $this->calculateTodayRate();
      $this->debugLog("Missing history for date, setting to today rate: ". 
        $this->getDate());
      $this->processHistory();
    }
  }

  private function calculateTodayRate() {
    $lastUpdated = \Civi::settings()->get('mayfirst_exchange_rate_last_updated');
    $lastUpdatedTs = strtotime($lastUpdated);
    $this->rate = \Civi::settings()->get('mayfirst_exchange_rate');
    // Should we use the cached rate?
    if (!$this->getForce() && $lastUpdatedTs && $this->isValidRate()) {
      // We cache the exchange rate for up to 2 hours before we retrieve it again.
      $maxAge = 2 * 60 * 60;

      // Check for a recently cached value.
      $age = time() - $lastUpdatedTs;
      if ($age < $maxAge) {
        $this->debugLog("Using cached exchange rate (" . 
          $this->rate . ") when querying at " . date('Y-m-d H:i:s'));
        // Return early - no need to processHistory or update caches.
        return;
      }
    }
    
    $this->debugLog("Not using cached exchange rate (" . 
      $this->rate . "), with expiration ($lastUpdated and
      $lastUpdatedTs) when querying at " . date('Y-m-d H:i:s'));

    // No viable cached version available, call via IMF API.
    try {
      $this->rate = $this->getImfRate();
    }
    catch (\Exception $e ){
      // Catch all errors - we will just return the cached exchange rate rather than barf because
      // we can't connect to the IMF web site...
      $this->debugLog("Failed to query IMF: " . $e->getMessage());

      // ...unless we still don't have a valid rate, then we should bail.
      if (!$this->isValidRate()) {
        throw new \API_Exception("Failed to lookup a valid exchange rate. Please try again later.");
      }
    }
    $this->debugLog("Setting new exchange rate: " . $this->rate . " on " . date('Y-m-d H:i:s'));
    // Update our cached rate and date we cached it.
    \Civi::settings()->set('mayfirst_exchange_rate', $this->rate);
    \Civi::settings()->set('mayfirst_exchange_rate_last_updated', date('Y-m-d H:i:s'));
    // Ensure our history file is update to date.
    $this->processHistory();
  }

  private function getImfRate() {
    // Retrieve exchange rate from IMF.
    $url = 'https://www.imf.org/external/np/fin/data/rms_mth.aspx';
    $nowish = time();
    if (date('d') == "01") {
      // If the first of the month, request last month's report so we are sure
      // we have a value.
      $nowish = strtotime("yesterday");
    }
    $query = [ 
      'SelectDate' => date('Y-m-d', $nowish),
      'reportType' => 'REP',
      'tsvflag' => 'Y',
    ];
    $builtQuery = http_build_query($query);
    $getUrl = $url."?".$builtQuery;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $getUrl);
    curl_setopt($ch, CURLOPT_TIMEOUT, 80);
    $report = curl_exec($ch);
    if(curl_error($ch)){
      throw new \API_Exception('Request Error:' . curl_error($ch));
    }
    curl_close($ch);
    $lines = explode("\n", $report);
    $rate = NULL;
    foreach($lines as $line) {
      if (preg_match("/^Mexican peso/", $line)) {
        $columns = explode("\t", $line);
        while (count($columns) > 0) {
          $last = trim(array_pop($columns));
          if ($last == "NA") {
            continue;
          }
          elseif (!is_numeric($last)) {
            continue;
          }
          elseif (!$this->isValidRate($last)) {
            continue;
          }
          return $last;
        }
      }
    }
    $this->debugLog("No suitable lines: $report");
    throw new \API_Exception('Failed to find a suitable rate.');
  }

  // Avoid returning a rate that is obviously wrong (hopefuly this range will last?)
  private function isValidRate($rate = NULL) {
    if (is_null($rate)) {
      $rate = $this->rate;
    }
    $rate = intval($rate);
    $valid = ($rate > 10 && $rate < 30);
    if (!$valid) {
      $this->debugLog("Invalid rate detected: $rate");
    }
    return $valid;
  }

  /**
   *
   * We keep a history of exchange rates keyed to the ISO date. Every time we
   * lookup a new rate we should ensure the history file is up to date. That
   * means:
   *
   *  * Add the new rate to today's date * Remove old rates so we don't bloat.
   **/
  private function processHistory() {
    $onDate = $this->getDate();
    $history = \Civi::settings()->get('mayfirst_exchange_rate_history');
    if (!$history) {
      $history = [];
    }
    if (!$onDate) {
      $onDate = date("Y-m-d");
    }
    $existingRate = $history[$onDate] ?? FALSE;
    if ($existingRate == $this->rate) {
      // It's the same, no need to update.
      $this->debugLog("Not updating exchange rate history to " . 
        $this->rate . ", already properly set for $onDate.");
      return;
    }
    // Build a new array, ommitting values that should be expired.
    $newHistory = [];
    $oldest = date('Y-m-d', strtotime("-1 year"));
    $this->debugLog("Rebuilding history to exclude entries older than $oldest.");
    foreach ($history as $dateKey => $rateValue ) {
      if ($dateKey > $oldest) {
        $newHistory[$dateKey] = $rateValue;
      }
    }
    // Now ensure the latest one is update to date.
    $newHistory[$onDate] = $this->rate;
    $this->debugLog("Setting history for $onDate to " . $this->rate);
    \Civi::settings()->set('mayfirst_exchange_rate_history', $newHistory);
  }

  private function calculateMembershipRate() {
    $membershipId = $this->getMembershipId();
    $endDate = NULL;
    $renewalAmount = NULL;
    $membership = \Civi\Api4\Membership::get()
      ->setCheckPermissions($this->checkPermissions)
      ->addSelect('Membership_Details.Renewal_Amount', 'end_date')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first();
    if (!$membership) {
      throw new \API_Exception("No matching membership found.");
    }
    $endDate = $membership['end_date'];
    $endDateTs = strtotime($endDate);
    // The locked in exchange rate is the rate 15 days prior to your membership
    // end date.
    $exchangeDateTs = $endDateTs - 15 * 86400;
    $renewalAmount = $membership['Membership_Details.Renewal_Amount'];

    if ($exchangeDateTs > time()) {
      $this->explanation = E::ts("The expiration date for this membership is more than 15 days in the future, so the exchange rate provided is not locked in. If payment is made today, the given exchange rate will be used. If payment is made in the future, a new rate may be calculated.");
      $this->calculateTodayRate();
    }
    else {
      $this->setDate(date('Y-m-d', $exchangeDateTs));
      $this->calculatePreviousRate();
      $this->explanation = E::ts("The exchange rate is locked in and will be respected even if payment is not made today.");
    }
    $this->amount = ceil($this->rate * $renewalAmount);
  }
}
?>

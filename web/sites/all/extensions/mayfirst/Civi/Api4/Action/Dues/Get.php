<?php

namespace Civi\Api4\Action\Dues;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Given a contactId for a member, return the calculated
 * dues (as if you had called the Dues::Calculate() API
 * with the data saved for this member).
 *
 * @method $this setContactId(int $contactId)
 */

class Get extends \Civi\Api4\Generic\AbstractAction {

  /**
   * contactId of the member. 
   *
   * @var int
   *
   */
  protected $contactId;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $membership = \Civi\Api4\Membership::get()
      ->addWhere('contact_id', '=', $this->getContactId())
      ->addSelect(
        'Membership_Details.Renewal_Amount', 
        'Membership_Details.Plan',
        'Membership_Details.Extra_Storage',
        'Membership_Details.Hosting_Benefits',
        'Membership_Details.Virtual_Private_Servers',
        'Membership_Details.Cpu',
        'Membership_Details.Ram',
        'Membership_Details.Ssd',
        'membership_type_id:name',
        'contact_id.Member_Info.Income'
      )
      ->execute()->first();
    $result[] = \Civi\Api4\Dues::Calculate()
      ->setFrequency($membership['membership_type_id:name'])
      ->setVirtualPrivateServers($membership['Membership_Details.Virtual_Private_Servers'])
      ->setCpu($membership['Membership_Details.Cpu'])
      ->setRam($membership['Membership_Details.Ram'])
      ->setSsd($membership['Membership_Details.Ssd'])
      ->setIncome($membership['contact_id.Member_Info.Income'])
      ->setPlan($membership['Membership_Details.Plan'])
      ->setExtraStorage($membership['Membership_Details.Extra_Storage'])
      ->setHostingBenefits(intval($membership['Membership_Details.Hosting_Benefits']))
      ->execute()->first();
  }
}
?>

<?php

namespace Civi\Api4\Action\Dues;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Return our membership dues pricing schedule. 
 *
 * This call doesn't touch the database and disclose any private information.
 *
 */
class Schedule extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Currency for the amounts.
   *
   * @var string 
   */

  protected $currency = 'USD';
  protected $_CURRENCY_USD = 'USD';
  protected $_CURRENCY_MXN = 'MXN';

  public function _run(\Civi\Api4\Generic\Result $result) {
    // Our dues are stored in a simple json file at the root of this extension: dues.json.
    $associative = TRUE;
    $schedule = json_decode(file_get_contents(\CRM_Core_Config::singleton()->extensionsDir . "mayfirst/dues.json"), $associative);

    $allowedCurrencies = [ $this->_CURRENCY_USD, $this->_CURRENCY_MXN ];
    if (!in_array($this->getCurrency(), $allowedCurrencies)) {
      throw new \API_Exception(E::ts("That is not a valid currency."));
    }
    // Our schedule is in USD by default. Only convert if we need MXN.
    $exchangeRate = \Civi\Api4\Dues::Exchange()->execute()->first()['rate'];
    if ($this->getCurrency() == 'MXN') {
      // Adjust all amounts for MXN
      $indices = [ 'basic', 1, 2, 3, 4 ];
      $vps_specs = ['cpu', 'ram', 'ssd'];
      foreach($indices as $i) {
        // Plans
        $amount = $schedule['plan'][$i]['fee']; 
        $schedule['plan'][$i]['fee'] = $this->number_format($amount * $exchangeRate); 

        if ($i == 'basic') {
          continue;
        }
        // VPS 
        $amount = $schedule['vps'][$i]; 
        $schedule['vps'][$i] = $this->number_format($amount * $exchangeRate); 
      }

      foreach ($vps_specs as $vps_spec) {
        $amount = $schedule[$vps_spec];
        $schedule[$vps_spec] = $this->number_format($amount * $exchangeRate);
      }
    }

    // Now add labels.
    $income_ranges = [ 1, 2, 3, 4 ];
    foreach($income_ranges as $i) {
      $min = $schedule['income'][$i]['min'];
      $max = $schedule['income'][$i]['max'];

      if ($this->getCurrency() == 'MXN') {
        $min = $this->number_format($min * $exchangeRate);
        $max = $this->number_format($max * $exchangeRate);
      }
      
      if ($min == 0) {
        $schedule['income'][$i]['label'] = E::ts("Less than %1 %2", [ 1 => number_format($max), 2 => $this->getCurrency() ]);
      }
      else if ($max == 0) {
        $schedule['income'][$i]['label'] = E::ts("Greater than %1 %2", [ 1 => number_format($min), 2 => $this->getCurrency() ]);
      }
      else {
        $schedule['income'][$i]['label'] = E::ts("Between %1 and %2 %3", [ 1 => number_format($min), 2 => number_format($max), 3 => $this->getCurrency() ]);
      }
    }

    $result[] = $schedule;
  }

  private function number_format($num) {
    $decimal_separator = "";
    $thousands_separator = "";
    $decimals = 0;
    return number_format($num, $decimals, $decimal_separator, $thousands_separator);
  }
}
?>

<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Change a membership from Requested to Grace.
 *
 * @method int getMembershipId()
 * @method setMembershipId(int $membershipId);
 *
 */
class ActivateMembership extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The membership id to activate.
   *
   * @int
   * @required
   */
  protected $membershipId;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $ret = \Civi\Api4\Membership::update()
      ->addWhere('id', '=', $this->getMembershipId())
      ->addWhere('status_id:name', '=', 'Requested')
      ->addValue('status_id:name', 'Grace')
      ->execute();

    if ($ret->count() != 1) {
      throw \API_Exception(E::ts('Failed to update the membership - has it already been activated?'));
    }
    $result[] = [ 'Membership has been activated.' ];
  }
}






?>

<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Add a new related contact or update an existing one. 
 *
 * @method string getToken
 * @method $this setToken(string $token)
 * @method array getContact
 * @method $this getcontact(array $contact)

 *
 */
class AddRelatedContact extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The token (hash:memberhip_id). 
   *
   * @var string
   * @required
   */
  protected $token = NULL;

  /**
   * The contact to add or update.
   *
   * @var array
   * @required
   */
  protected $contact = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    $token = $this->getToken();
    $contact = $this->getContact();

    // Check for required contact indices.
    $required = [ 'first_name', 'last_name', 'preferred_language', 'email' ];
    foreach ($required as $index) {
      if (empty($this->getContact()[$index])) {
        throw new \API_Exception(E::ts("Your contact must include a value for %1.", [ 1 => $index ]));
      }
    }
    if (!\Civi\Mayfirst\Utils::validToken($token)) {
      throw new \API_Exception(E::ts("Your token does not seem valid."));
    }
    $tokenPieces = explode(':', $token);
    $membershipId = $tokenPieces[1];
    $type = $tokenPieces[2];
    if ($type != 'edit') {
      throw new \API_Exception(E::ts("Your token is not authorized to make changes to your membership."));
    }

    $memberContactId = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('contact_id')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first()['contact_id'];

    // If the related contact has contact id, it means they are editing it.
    // This is tricky business. We can't actually let them edit a contact
    // record. What if it's a contact record related to multiple members and
    // they change the name and contact info to a completely different person? 
    //
    // Instead, we delete the relationship between this contact and their
    // membership contact record. Then, we'll take their new info and see if it
    // matches any existing contacts (maybe it will match the same one we just
    // severed the relationship to - that would be ok).
    //
    // Then we re-attach the relationship.
    if ($contact['id']) {
      // Before anything else, make sure nobody is sneaking in a contact id
      // that is not already related to the membership contact.
      $relatedContact = \Civi\Api4\Contact::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('id')
        ->addWhere('id', '=', $contact['id'])
        ->addJoin('Relationship AS relationship', 'INNER', ['relationship.contact_id_a', '=', 'id'])
        ->addWhere('relationship.contact_id_b', '=', $memberContactId)
        ->addWhere('is_deleted', '=', 0)
        ->addWhere('is_deceased', '=', 0)
        ->execute();
      if ($relatedContact->count() == 0) {
        throw new \API_Exception(E::ts("The contact you are entering is not currently related to your membership."));
      }

      // Now sever the relationships.
      \Civi\Api4\Relationship::delete()
        ->setCheckPermissions(FALSE)
        ->addWhere('contact_id_a', '=', $contact['id'])
        ->addWhere('contact_id_b', '=', $memberContactId)
        ->execute();
    }
    // We have to translate preferred_language.
    $contact['preferred_language'] = $contact['preferred_language']['id'];

    // Let's see if there's a match in the database for the new data being submitted.
    $phone_numeric = preg_replace('/[^0-9]', '', $contact['phone']);
    $results = \Civi\Api4\Contact::get()
      ->setCheckPermissions(FALSE)
      ->setJoin([ 
        [ 'Email AS email', FALSE ],
        [ 'Phone AS phone', FALSE ] 
      ])
      ->addSelect('id')
      ->setGroupBy([ 'id' ])
      ->addWhere('first_name', '=', $contact['first_name'])
      ->addWhere('last_name', '=', $contact['last_name'])
      ->addWhere('is_deleted', '=', 0)
      ->addClause('OR', ['email.email', '=', $contact['email']], [ 'phone.phone_numeric', '=', $phone_numeric ])
      ->execute();

    if ($results->count() > 0) {
      // We'll take the first one.
      $savedContactId = $results->first()['id'];

      \Civi\Api4\Contact::update()
        ->setCheckPermissions(FALSE)
        ->addWhere('id', '=', $savedContactId)
        ->addValue('first_name', $contact['first_name'])
        ->addValue('last_name', $contact['last_name'])
        ->addValue('preferred_language', $contact['preferred_language'])
        ->execute();
    }
    else {
      $savedContactId = \Civi\Api4\Contact::create()
        ->setCheckPermissions(FALSE)
        ->addValue('first_name', $contact['first_name'])
        ->addValue('last_name', $contact['last_name'])
        ->addValue('preferred_language', $contact['preferred_language'])
        ->execute()->first()['id'];
    }

    // Add email. Email is a required field.
    $existingEmailId = \Civi\Api4\Email::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('email', '=', $contact['email'])
      ->addWhere('contact_id', '=', $savedContactId)
      ->execute()->first()['id'];
    
    if ($existingEmailId) {
      \Civi\Api4\Email::update()
        ->setCheckPermissions(FALSE)
        ->addWhere('id', '=', $existingEmailId)
        ->addValue('is_primary', TRUE)
        ->execute();
    }
    else {
      \Civi\Api4\Email::create()
        ->setCheckPermissions(FALSE)
        ->addValue('email', $contact['email'])
        ->addValue('is_primary', TRUE)
        ->addValue('location_type_id:name', 'Work')
        ->addValue('contact_id', $savedContactId)
        ->execute();
    }

    // Add phone if provided.
    if ($contact['phone']) {
      $existingPhoneId = \Civi\Api4\Phone::get()
        ->setCheckPermissions(FALSE)
        ->addWhere('phone', '=', $contact['phone'])
        ->addWhere('contact_id', '=', $savedContactId )
        ->execute()->first()['id'];
      
      if ($existingPhoneId) {
        \Civi\Api4\Phone::update()
          ->setCheckPermissions(FALSE)
          ->addWhere('id', '=', $existingPhoneId)
          ->addValue('is_primary', TRUE)
          ->addValue('phone_type_id:name', 'Mobile')
          ->execute();
      }
      else {
        \Civi\Api4\Phone::create()
          ->setCheckPermissions(FALSE)
          ->addValue('is_primary', TRUE)
          ->addValue('phone', $contact['phone'])
          ->addValue('phone_type_id:name', 'Mobile')
          ->addValue('location_type_id:name', 'Work')
          ->addValue('contact_id', $savedContactId)
          ->execute();
      }
    }

    // Now add the relationships based on permissions being granted.
    $relationships = [ 
      'Membership_General_Contact_For',
      'Membership_Admin_Contact_For',
      'Membership_Technical_Contact_For'
    ];

    $addRelationships = [ 'Membership_General_Contact_For' ];
    if ($contact['permission_admin']) {
      $addRelationships[] = 'Membership_Admin_Contact_For';
    }
    if ($contact['permission_technical']) {
      $addRelationships[] = 'Membership_Technical_Contact_For';
    }
    \Civi::log()->debug(print_r($addRelationships, TRUE));
    foreach ($relationships as $relationshipName) {
      $existing = \Civi\Api4\Relationship::get()
        ->setCheckPermissions(FALSE)
        ->addWhere('contact_id_a', '=', $savedContactId)
        ->addWhere('contact_id_b', '=', $memberContactId)
        ->addWhere('relationship_type_id:name', '=', $relationshipName)
        ->execute();

      \Civi::log()->debug("Name: $relationshipName");
      if ($existing->count() == 0 && in_array($relationshipName, $addRelationships)) {
        // Add it.
        \Civi::log()->debug("Adding it");
        \Civi\Api4\Relationship::create()
          ->setCheckPermissions(FALSE)
          ->addValue('contact_id_a', $savedContactId)
          ->addValue('contact_id_b', $memberContactId)
          ->addValue('relationship_type_id:name', $relationshipName)
          ->execute();
      }
      if ($existing->count() == 1 && !in_array($relationshipName, $addRelationships)) {
        // Remove it.
        $id = $existing->first()['id'];
        \Civi::log()->debug("deleting it where id is: $id ");
        \Civi\Api4\Relationship::delete()
          ->setCheckPermissions(FALSE)
          ->addWhere('id', '=', $id)->execute();
      }
    }

    $contact['id'] = $savedContactId;

    $result[] = $contact;
    
  }
}






?>

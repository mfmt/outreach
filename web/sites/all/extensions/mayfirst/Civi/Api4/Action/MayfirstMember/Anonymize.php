<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Given a contribution id, move it to the anonymous user
 * and completely disassociate it from the member who made
 * the contribution. 
 *
 *
 * @method int getContributionId()
 * @method $this setContributionId(int $contributionId)
 * @method int getAnonContactId()
 * @method $this setAnonContactId(int $contactId)

 **/

class Anonymize extends \Civi\Api4\Generic\AbstractAction {

  /**
   * contributionId
   *
   * The contributionId to anonymize.
   *
   * @var int
   * @required
   **/
  protected $contributionId = NULL;

  /**
   * anonContactId
   *
   * The contactId of the anonymous user.
   * The contribution will be assigned to this user.
   *
   * @var int
   **/
  protected $anonContactId = 8926;

  public function _run(\Civi\Api4\Generic\Result $result) {
    // Ensure the contribution exists and is one we can anonymize.
    $contribution = \Civi\Api4\Contribution::get()
      ->addSelect(
        'contact_id',
        'Membership_Dues_Info.Transfer_Receipt'
      )
      ->addWhere('id', '=', $this->getContributionId())
      ->execute()->first();

    if (!$contribution) {
      throw new \API_Exception(E::ts("Failed to locate that contribution"));
    }
    $transferReceiptId = $contribution['Membership_Dues_Info.Transfer_Receipt'] ?? NULL;

    if ($transferReceiptId) {
      throw new \API_Exception(E::ts("That contribution has a transfer receipt. Please delete it first."));
    }
    $origContactId = $contribution['contact_id'];

    // Check for recurring contributions. If any are in progress, throw an error. Otherwise
    // we will straight up delete them.
    $deleteContributionRecurIds = [];
    $contributionRecurs = \Civi\Api4\ContributionRecur::get()
      ->addSelect('id', 'contribution_status_id:name')
      ->addWhere('contact_id', '=', $origContactId)
      ->execute();
    foreach ($contributionRecurs as $contributionRecur) {
      if ($contributionRecur['contribution_status_id:name'] == 'In Progress') {
        throw new \API_Exception(E::ts("This user has recurring contributions. Please cancel ALL recurring contribution first."));
      }
      $deleteContributionRecurIds[] = $contributionRecur['id'];
    }
    if (count($deleteContributionRecurIds) > 0) {
      foreach($deleteContributionRecurIds as $contributionRecurId) {
        \Civi\Api4\ContributionRecur::delete()
          ->addWhere('id', 'IN', $deleteContributionRecurIds)
          ->execute();
      }
    }


    // Delete any membership payment record that links it to a membership.
    $sql = "DELETE FROM civicrm_membership_payment WHERE contribution_id = %1";
    \CRM_Core_DAO::executeQuery($sql, [1 => [$this->getContributionId(), 'Integer']]);

    // Update the financial items' contact IDs as well. Thanks LCD Service's Move Contrib module.
    $sql = "
      UPDATE civicrm_financial_item cfi
      JOIN civicrm_entity_financial_trxn cefti
        ON cefti.entity_id = cfi.id
      JOIN civicrm_financial_trxn cft
        ON cefti.entity_table = 'civicrm_financial_item'
        AND cft.id = cefti.financial_trxn_id
      JOIN civicrm_entity_financial_trxn ceftc
        ON ceftc.financial_trxn_id = cft.id
      SET cfi.contact_id = %1
      WHERE ceftc.entity_table = 'civicrm_contribution'
        AND ceftc.entity_id = %2
    ";
    $params = [
      1 => [$this->getAnonContactId(), 'Positive'],
      2 => [$this->getContributionId(), 'Positive'],
    ];
    \CRM_Core_DAO::executeQuery($sql, $params);

    // Build dictionary of activity type ids.
    $activityTypes = array_flip(\CRM_Activity_BAO_Activity::buildOptions('activity_type_id'));

    // Move the contribution and payment activity record. These are tied
    // to the contribution record via the source_record_id.
    $moneyActivityTypes = [
      $activityTypes['Payment'],
      $activityTypes['Contribution'],
    ];
    \Civi\Api4\ActivityContact::update()
      ->addValue('contact_id', $this->getAnonContactId())
      ->addWhere('activity_id.source_record_id', '=', $this->getContributionId())
      ->addWhere('activity_id.activity_type_id', 'IN', $moneyActivityTypes)
      ->execute();

    // Delete a bunch of activities that could link the payment to the member.
    $deleteActivityIds = [];

    // There is a Membership Renewal Activity. It's not tied directly to the
    // contribution, but you could use the date of the activity to correlate with a
    // date of contribution, so we will have to delete *all* such activities tied to
    // the contact id.
    //
    // And delete any personal email - this will get rid of receipts
    // and also dues reminders and any communications we might have
    // about a payment.
    $deleteActivityTypeIds = [
      $activityTypes['Membership Renewal'],
      $activityTypes['Email'],
    ];
    $activityContacts = \Civi\Api4\ActivityContact::get()
      ->addSelect('activity_id')
      ->addWhere('contact_id', '=', $origContactId)
      ->addWhere('record_type_id:name', '=', 'Activity Targets')
      ->addWhere('activity_id.activity_type_id', 'IN', $deleteActivityTypeIds)
      ->execute();
    foreach($activityContacts as $activityContact) {
      $activityId = $activityContact['activity_id'];
      if (!in_array($activityId, $deleteActivityIds)) {
        $deleteActivityIds[] = $activityId;
      }
    }

    // And delete the "Change Membership Status" which could be linked via date to
    // a payment. This one is separate because if a member pays it via the payment
    // page, they will be the "source" and there is no target. But, it's dangerous
    // to delete activities based on the source - because if you anonymize a staff
    // person's contribution, you could end up deleting a lot more records than you
    // wanted.
    $membershipIds = \Civi\Api4\Membership::get()
      ->addWhere('contact_id', '=', $origContactId)
      ->execute()->column('id');
    if ($membershipIds) {
      $activityIds = \Civi\Api4\Activity::get()
        ->addWhere('source_record_id', 'IN', $membershipIds)
        ->addWhere('activity_type_id', '=', $activityTypes['Change Membership Status'])
        ->execute()->column('id');
      $deleteActivityIds = array_merge($deleteActivityIds, $activityIds);
    }

    if (count($deleteActivityIds) > 0) {
      \Civi\Api4\Activity::delete()
        ->addWhere('id', 'IN', $deleteActivityIds)
        ->execute();
    }

    // Move the contribution
    \Civi\Api4\Contribution::update()
      ->addWhere('id', '=', $this->getContributionId())
      ->addValue('contact_id', $this->getAnonContactId())
      ->execute();

    $result[] = [E::ts("The contribution has een anonymized.") ];
  }
}


?>

<?php

namespace Civi\Api4\Action\MayfirstMember;

/**
 *
 * Export a CSV file of eligible voters.
 *
 * Get a list of email addresses linked to membership ids for
 * all eligible voters in a format that can be imported into our
 * vote.mayfirst.org election site.
 *
 */
class ExportVotingList extends \Civi\Api4\Generic\AbstractAction {

  /**
   * exportPath
   *
   * The path of the the file to save the exported
   * records to.
   *
   * @required
   * @var string
   */
  protected $exportPath;

  public function _run(\Civi\Api4\Generic\Result $result) {

    $f = fopen($this->getExportPath(), 'w');
    if ($f === FALSE) {
      throw \API_Exception("Failed to touch the given export file path: " . $this->getExportPath());
    }
    // We start by getting the contact we want to notify.
    $contacts = \Civi\Api4\Contact::get()
      ->addSelect(
        'contact.id',
        'contact.display_name', 
        'contact.contact_type', 
        'email.email', 
        // We don't need these values, just pull them in so we can eyeball the
        // results more easily.
        'display_name', 
        'contact.external_identifier', 
      )
      // Limit the contact records.
      // You should get a token even if you have opted out.
      // ->addWhere('is_opt_out', '=', FALSE)
      ->addWhere('is_deceased', '=', FALSE)
      // Join this contact with the email table to get their email address
      // (and don't bother with held or opted out addresses).
      ->addJoin('Email AS email', 'INNER')
      ->addWhere('email.is_primary', '=', TRUE)
      ->addWhere('email.on_hold', '=', 0)
      ->addWhere('is_opt_out', '=', 0)
      // Now, get the relationship (so we can link to the actual members we want). Limit to
      // the two relationship membership types we care about (Membership Admin Contact and
      // Membership Contact).
      ->addJoin('Relationship AS relationship', 'INNER', ['relationship.contact_id_a', '=', 'id'])
      ->addWhere('relationship.relationship_type_id', 'IN', [12, 13])
      ->addWhere('relationship.is_active', '=', TRUE)
      ->addClause('OR', ['relationship.end_date', '>', date('Y-m-d')], ['relationship.end_date', 'IS NULL'])
      // Now link back to the contact table to get the membership contact.
      ->addJoin('Contact AS contact', 'INNER', ['relationship.contact_id_b', '=', 'contact.id'])
      // Link from the Membership Contact to the membership table to only get current members.
      ->addJoin('Membership AS membership', 'INNER', ['contact.id', '=', 'membership.contact_id'])
      ->addWhere('membership.status_id:name', 'IN', ['Current', 'Grace'])
      ->execute();

    $seen = [];
    foreach ($contacts as $contact) {
      $key = $contact['email.email'] . '-' . $contact['contact.external_identifier'];
      if (in_array($key, $seen)) {
        // Is there a way to put SELECT DISTINCT into an Apiv4 call??
        continue;
      }
      $seen[] = $key;
      $row = [
        $contact['contact.id'],
        $contact['contact.display_name'],
        $contact['contact.contact_type'],
        $contact['email.email'],
        $contact['contact.external_identifier']
      ];
      fputcsv($f, $row, "\t");
    }
    fclose($f);
    $result[] = "Results exported to: " . $this->getExportPath();
  }
}
?>

<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * We provide a way to flush the cache because our angular code won't serve
 * a different language when reloading the page without flushing the cache
 * first.
 */

 /**
 * @method string getToken
 * @method $this setToken(string $token)
 *
 */

class FlushCache extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The token tied to the membership.
   *
   * The token is only used for authentication purposes. We don't want
   * any rando flushing our cache.
   *
   * @var string
   * @required
   */
  protected $token = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {

    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    $token = $this->getToken();

    // Now check to see if the token is valid.
    if (!\Civi\Mayfirst\Utils::validToken($token)) {
      $token_invalid = E::ts("Your token didn't work when trying to flush the cache.");
      throw new \API_Exception($token_invalid);
    }
   
    \CRM_Utils_System::flushCache();
    $result[] = [];
  }
}

?>

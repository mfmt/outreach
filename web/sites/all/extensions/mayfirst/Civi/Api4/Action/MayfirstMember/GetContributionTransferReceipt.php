<?php

namespace Civi\Api4\Action\MayfirstMember;

use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Get Contribution Transfer Receipt. 
 *
 * We want to display the transfer receipt that is attached to
 * our membership payments when a check is sent to us or a MXN
 * member uploads their bank transfer receipt.
 *
 * It's useful to display this receipt on the manual membership
 * renewal page, but we need to parse some data to make it possible.
 * This API calls gets the transfer receipt details that the manual
 * payment page needs to properly display it. 
 *
 **/

class GetContributionTransferReceipt extends \Civi\Api4\Generic\AbstractAction {

  /**
   * contributionId
   *
   * @var int
   * @required
   */
  protected $contributionId;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $contributionId = $this->getContributionId();
    $fileId = \Civi\Api4\Contribution::get()
      ->addSelect('Membership_Dues_Info.Transfer_Receipt') 
      ->addWhere('id', '=', $contributionId)
      ->execute()->first()['Membership_Dues_Info.Transfer_Receipt'];

    $displayable = FALSE;
    $link = NULL;

    if ($fileId) {
      $fileDetails = \Civi\Api4\File::get()
        ->addWhere('id', '=', $fileId)
        ->addSelect('mime_type', 'uri')
        ->execute()->first();

      $displayableMimeTypes = [
        'image/apng',
        'image/avif',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/svg+xml',
        'image/webp',
      ];

      if (in_array(trim($fileDetails['mime_type']), $displayableMimeTypes)) {
        $displayable = TRUE;
      }
      $hash = \CRM_Core_BAO_File::generateFileHash($contributionId, $fileId);
      $link = \CRM_Utils_System::url('civicrm/file', "reset=1&id={$fileId}&eid={$contributionId}&fcs=$hash");
    }
    $result[] = [ 'displayable' => $displayable, 'link' => $link ];
  }
}
?>

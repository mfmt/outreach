<?php

namespace Civi\Api4\Action\MayfirstMember;

/**
 *
 * Provide unrestricted access to retrieving a list of countries
 * so we can display a drop down for people joining or changing
 * their contact info.
 *
 */
class GetCountries extends \Civi\Api4\Generic\AbstractAction {

  public function _run(\Civi\Api4\Generic\Result $result) {
    $result[] = \Civi\Api4\Country::get()
      ->setCheckPermissions((FALSE))
      ->execute(); 
  }
}






?>

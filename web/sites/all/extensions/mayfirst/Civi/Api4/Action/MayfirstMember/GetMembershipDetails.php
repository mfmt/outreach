<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

  /**
   * Get membership details.
   *
   * Retrieve all the details for a given membership. We expect to either
   * get a token, which permits access to the embedded membershipId (and
   * does not required being logged in) or a membershipId or membeContactId 
   * without a token which relies on your logged in permissions to grant access.
   *
   * @method string getToken
   * @method $this setToken(string $token)
   * @method string getMembershipId
   * @method $this setMembershipId(int $membershipId)
   * @method string getMemberContactId
   * @method $this setMemberContactId(int $memberContactId)

   *
  **/

class GetMembershipDetails extends \Civi\Api4\Generic\AbstractAction {

  /**
   *
   * token
   *
   * A token grants access to the membership id embedded in the token. If the
   * token is valid, you get access to the membership details for the given
   * membershipId without any additional permission checks. 
   *
   * @var string
  */
  protected $token = NULL;

  /**
   *
   * The membershipId
   *
   * If passing membershipId, then you can have access to the given
   * membershipId but only if your login grants that access. All permission
   * checks are applied.
   *
   * @var int
   */
  protected $membershipId;

  /**
   *
   * The memberContactId
   *
   * If passing memberContactId, then you can have access to the given
   * membership but only if your login grants that access. All permission
   * checks are applied.
   *
   * @var int
   */
  protected $memberContactId;

  public function _run(\Civi\Api4\Generic\Result $result) {

    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    $token = $this->getToken();
    if ($token) {
      // Check to see if the token is valid.
      $tokenPieces = explode(':', $token);
      $this->setMembershipId($tokenPieces[1]);
      $type = $tokenPieces[2];
      $expire = intval($tokenPieces[3]);
      $token_invalid = E::ts("Your token didn't work. Sometimes links are broken due to a line break in email messages. If you clicked on a link, try copying and pasting the token instead.");
      if (strlen($expire) < 10) {
        // This means the expire timestamp is missing or too small. That usually means the URL
        // is mal-formed.
        throw new \API_Exception($token_invalid);
      }
      if ($expire < time()) {
        // As a courtesy, we check the expiration date first so we can give a
        // friendly and informative message about why the token didn't work.
        throw new \API_Exception(E::ts("Your token has expired. Please request a new one by entering your email address in this form."));
      }
      if (!\Civi\Mayfirst\Utils::validToken($token)) {
        throw new \API_Exception($token_invalid);
      }

      // Now we have a valid token and membershipId - we need to turn off
      // permission checks to allow the user to get the access they need for
      // this membership. 
      $this->checkPermissions = FALSE;
    }
    else {
      if ($this->getMemberContactId()) {
        $this->membershipId = \Civi\Api4\Membership::get()
          ->addWhere('contact_id', '=', $this->getMemberContactId())
          ->addWhere('contact_id.is_deleted', '=', FALSE)
          ->addWhere('status_id:name', 'IN', ['Current', 'Grace', 'Expired', 'Disabled'])
          ->addSelect('id')
          ->execute()->first()['id'];
        if (!$this->membershipId) {
          throw new \API_Exception(E::ts("The contact you passed in does not have a membership."));
        }
      }
      if (!$this->getMembershipId()) {
        throw new \API_Exception(E::ts("Please pass either a token, a membershipId or a memberContactId associated with a membership when using the getMembershipDetails API."));
      }
      $type = 'edit';
    }

    $membership = $this->getMembershipDetails();
    if (!$membership) {
      // The membershipId appended to the end is not a valid membership id. We should not call
      // this method on an anonymous request without first checking for a valid token! Assuming
      // the token is valid, we can provide an honest answer here.
      throw new \API_Exception(E::ts("Invalid Token."));
    }
    if ($membership['is_deceased'] || $membership['is_deleted']) {
      $msg = E::ts("The member contact for the membership id %1 no longer exists. Please contact support for help resolving this issue.", [ 1 => $this->getMembershipId() ]);
      throw new \API_Exception($msg);
    }
    if ($membership['membership_status'] == 'Cancelled') {
      $msg = E::ts("Membership is cancelled for membership id %1.", [ 1 => $this->getMembershipId()]);
      throw new \API_Exception($msg);
    }

    if ($type == 'pay') {
      $row = $this->getMembershipSummary($membership);
      $row['edit_permission'] = FALSE;
    }
    else {
      $row = $membership;
      $row['edit_permission'] = TRUE;
    }
    
    $result[] = $row;
  }

  /**
   * getMembershipDetails
   *
   * Gather all the available details for this membership and
   * return an array.
   */
  private function getMembershipDetails() {
   $membership = \Civi\Api4\Membership::get()
      ->setCheckPermissions($this->checkPermissions)
      ->addSelect(
        'id',
        'contact_id',
        'membership_type_id:name',
        'status_id:name',
        'end_date',
        'start_date',
        'join_date',
        'membership_type_id:name',
        'Membership_Details.Currency:name',
        'Membership_Details.Renewal_Amont',
        'contact_id.is_deleted',
        'contact_id.is_deceased',
        'contact_id.contact_type', 
        'contact_id.first_name', 
        'contact_id.last_name', 
        'contact_id.display_name',
        'contact_id.organization_name', 
        'contact_id.external_identifier', 
        'contact_id.Member_Info.Income',
        'contact_id.Member_Info.Percent_Reduction',
        'contact_id.Member_Info.Why_Join',
        'contact_id.Member_Info.How_Hear',
        'contact_id.Member_Info.are_you_a_coop',
        'Membership_Details.Plan',
        'Membership_Details.Extra_Storage',
        'Membership_Details.Renewal_Amount',
        'Membership_Details.Hosting_Benefits',
        'Membership_Details.Virtual_Private_Servers',
        'Membership_Details.Cpu',
        'Membership_Details.Currency:name',
        'Membership_Details.Ram',
        'Membership_Details.Ssd',
        'contact_id.Member_Hosting_Details.Control_Panel_Quota',
        'contact_id.Member_Hosting_Details.Control_Panel_Allocated',
        'contact_id.Member_Hosting_Details.Control_Panel_Usage',
        'contact_id.Member_Hosting_Details.Control_Panel_Virtual_Private_Servers',
        'contact_id.Member_Hosting_Details.Control_Panel_Cpu',
        'contact_id.Member_Hosting_Details.Control_Panel_Ram',
        'contact_id.Member_Hosting_Details.Control_Panel_Ssd'
      )
      ->addWhere('id', '=', $this->getMembershipId())
      ->execute()->first();

    // Now translate to easier to manage indices.
    $results['id'] = $membership['id'];
    $results['membership_type'] = $membership['membership_type_id:name'];
    $results['membership_status'] = $membership['status_id:name'];
    $results['join_date'] = $membership['join_date'];
    $results['start_date'] = $membership['start_date'];
    $results['end_date'] = $membership['end_date'];
    $results['contact_id'] = $membership['contact_id'];
    // Org records do not have is_deceased field.
    $results['is_deceased'] = $membership['contact_id.is_deceased'] ?? NULL;
    $results['is_deleted'] = $membership['contact_id.is_deleted'];
    $results['contact_type'] = $membership['contact_id.contact_type']; 
    $results['display_name'] = $membership['contact_id.display_name']; 
    $results['first_name'] = $membership['contact_id.first_name'] ?? '';
    $results['last_name'] = $membership['contact_id.last_name'] ?? '';
    $results['organization_name'] = $membership['contact_id.organization_name'] ?? '';
    $results['external_identifier'] = $membership['contact_id.external_identifier'];
    $results['control_panel_id'] = preg_match('/[0-9]/', $results['external_identifier']);
    $results['why_join'] = $membership['contact_id.Member_Info.Why_Join'];
    $results['how_hear'] = $membership['contact_id.Member_Info.How_Hear'];
    $results['income'] = $membership['contact_id.Member_Info.Income'];
    $results['percent_reduction'] = $membership['contact_id.Member_Info.Percent_Reduction'];
    $results['renewal_amount'] = $membership['Membership_Details.Renewal_Amount'];
    $results['control_panel_quota'] = intval($membership['contact_id.Member_Hosting_Details.Control_Panel_Quota']);
    $results['control_panel_allocated'] = intval($membership['contact_id.Member_Hosting_Details.Control_Panel_Allocated']);
    $results['control_panel_usage'] = intval($membership['contact_id.Member_Hosting_Details.Control_Panel_Usage']);
    $results['control_panel_virtual_private_servers'] = $membership['contact_id.Member_Hosting_Details.Control_Panel_Virtual_Private_Servers'];
    $results['control_panel_cpu'] = $membership['contact_id.Member_Hosting_Details.Control_Panel_Cpu'];
    $results['control_panel_ram'] = $membership['contact_id.Member_Hosting_Details.Control_Panel_Ram'];
    $results['control_panel_ssd'] = $membership['contact_id.Member_Hosting_Details.Control_Panel_Ssd'];
    $results['currency'] = $membership['Membership_Details.Currency:name'];
    $results['plan'] = $membership['Membership_Details.Plan'];
    $results['extra_storage'] = $membership['Membership_Details.Extra_Storage'];
    $results['hosting_benefits'] = intval($membership['Membership_Details.Hosting_Benefits']);
    $results['virtual_private_servers'] = $membership['Membership_Details.Virtual_Private_Servers'];
    $results['cpu'] = $membership['Membership_Details.Cpu'];
    $results['ram'] = $membership['Membership_Details.Ram'];
    $results['ssd'] = $membership['Membership_Details.Ssd'];
    $results['is_coop'] = $membership['contact_id.Member_Info.are_you_a_coop'];
    $results['related_contacts'] = $this->getRelatedContacts($membership['contact_id']);

    $address = \Civi\Api4\Address::get()
      ->setCheckPermissions($this->checkPermissions)
      ->addWhere('contact_id', '=', $results['contact_id']) 
      ->addWhere('is_primary', '=', TRUE)
      ->addSelect('street_address', 'city', 'state_province_id', 'state_province_id:name', 'country_id', 'country_id:name')
      ->execute()->first();
    $results['address'] = $address ? $address : [];
    // Add the address fields directly for convenience.
    $results['street_address'] = $address['street_address'] ?? '';
    $results['city'] = $address['city'] ?? '';
    $results['state_province_id'] = $address['state_province_id'] ?? '';
    $results['state_province_id:name'] = $address['state_province_id:name'] ?? '';
    $results['country_id'] = $address['country_id'] ?? '';
    $results['country_id:name'] = $address['country_id:name'] ?? '';
    $results['country_id:name'] = $address['country_id:name'] ?? '';
    // Collect domain name.
    $urlResult = \Civi\Api4\Website::get()
      ->setCheckPermissions($this->checkPermissions)
      ->addWhere('contact_id', '=', $results['contact_id'])
      ->addWhere('website_type_id:name', '=', 'Main')
      ->addSelect('url')
      ->addSelect('website_type_id:name', '=', 'Work')
      ->execute()->first();
    if ($urlResult) {
      // domain name needs translating from url.
      $results['domain_name'] = preg_replace('#https?://#', '', $urlResult['url']);
    }
    else {
      $results['domain_name'] = '';
    }

    $calculatedDues = \Civi\Api4\Dues::Calculate()
      ->setIncome($results['income'])
      ->setHostingBenefits(intval($results['hosting_benefits']))
      ->setPlan($results['plan'])
      ->setFrequency($results['membership_type'])
      ->setCpu($results['cpu'])
      ->setRam($results['ram'])
      ->setSsd($results['ssd'])
      ->setVirtualPrivateServers($results['virtual_private_servers'])
      ->setExtraStorage($results['extra_storage'])
      ->setCurrency('USD')
      // Some groups have approved storage limits over our default limit,
      // don't throw an error.
      ->setStorageLimit(0)
      ->execute()->first();

    $results['calculated_dues'] = $calculatedDues['dues'];
    $results['calculated_storage'] = $calculatedDues['storage'];

    // The csrfToken allows users to submit credit card payments.
    $csrfToken = NULL;
    if (class_exists('\Civi\Firewall\Firewall')) {
      $csrfToken = \Civi\Firewall\Firewall::getCSRFToken();
    }
    $results['csrf_token'] = $csrfToken;

    // We always store renewal amount in USD.
    $results['exchange_rate'] = '';
    $results['exchange_rate_explanation'] = '';
    $results['renewal_amount_localized'] = $results['renewal_amount'];
    if ($results['currency'] == 'MXN') {
      // We need to display the exchange rate so grab that.
      $exch = \Civi\Api4\Dues::Exchange()
        ->setCheckPermissions($this->checkPermissions)
        ->setMembershipId($this->getMembershipId())
        ->execute()->first();
      $results['exchange_rate'] = $exch['rate'];
      $results['exchange_rate_explanation'] = $exch['explanation'];
      $results['renewal_amount_localized'] = $exch['amount'];
    }
    $results['virtual_private_servers_summary'] = '';
    $results['membership_and_hosting_summary'] = '';
    if ($results['hosting_benefits']) {
      $duesSchedule = \Civi\Api4\Dues::Schedule()->setCurrency($results['currency'])->execute()->first();
      $plan = $results['plan'];
      $income = $results['income'];
      $incomeTitle = E::ts("Income Range") . ": " . $income;
      $incomeDetails = $duesSchedule["income"][$income]["label"];
      $hostingTitle =  E::ts("Hosting Plan") . ": " . $plan;
      $hostingStorage = $duesSchedule['plan'][$plan]['storage'] . ' ' . E::ts('GB included');
      $extraStorageDesc = NULL;
      if ($results['extra_storage']) {
        $extraStorageDesc = ' / ' . $results['extra_storage'] . ' ' . E::ts("GB extra storage"); 
      }
      $results['membership_and_hosting_summary'] = "$incomeTitle ($incomeDetails) / $hostingTitle ($hostingStorage) $extraStorageDesc";

      if ($results['virtual_private_servers']) {
        $results['virtual_private_servers_summary'] = 
          $results['virtual_private_servers'] . ' ' .
          E::ts('Virtual Private Servers') . ' / ' . 
          $results['cpu'] . ' ' .
          E::ts('CPUs') . ' / ' . 
          $results['ram'] . ' ' .
          E::ts('GBs of RAM') . ' / ' . 
          $results['ssd'] . ' ' .
          E::ts('GBs of SSD');
      }
    }
    $results['membership_auto_renew_in_progress'] = intval(\Civi\Mayfirst\Utils::membershipAutoRenewInProgress($membership['id']));

    // You are only eligible for auto renew if you are less than 15 days late (otherwise things go out
    // of sync and people complain about being on auto renew but getting email reminders to pay).
    $pastDate = date('Y-m-d', strtotime('-15 days'));
    if ($membership['end_date'] > $pastDate) {
      $autoRenewEligible = TRUE;
    }
    else {
      $autoRenewEligible = FALSE;
    }
    $results['membership_auto_renew_eligible'] = $autoRenewEligible;
    return $results;
  }

  /**
   * getRelatedContacts
   *
   * Create an array of all the contacts related to this membership.
   */
  private function getRelatedContacts($memberContactId) {
    // Get all related contacts.
    $relatedContactResults = \Civi\Api4\Relationship::get()
      ->setCheckPermissions($this->checkPermissions)
      ->addSelect(
        'contact_id_a',
        'relationship_type_id:name',
        'contact_id_a.display_name',
        'contact_id_a.first_name',
        'contact_id_a.last_name',
        'contact_id_a.preferred_language',
        'email.email',
        'phone.phone'
      )
      ->addJoin(
        'Email AS email', 
        'LEFT', 
        ['contact_id_a', '=', 'email.contact_id'], 
        ['email.is_primary', '=', TRUE]
      )
      ->addJoin(
        'Phone AS phone', 
        'LEFT', 
        ['contact_id_a', '=', 'phone.contact_id'], 
        ['phone.is_primary', '=', TRUE]
      )
      ->addWhere('contact_id_b', '=', $memberContactId)
      ->addWhere('is_active', '=', TRUE)
      ->addWhere('contact_id_a.is_deleted', '=', FALSE)
      ->addClause('OR', ['end_date', '>', date('Y-m-d')], ['end_date', 'IS NULL'])
      ->addWhere(
        'relationship_type_id:name', 
        'IN', 
        [
          'Membership_General_Contact_For', 
          'Membership_Admin_Contact_For', 
          'Membership_Technical_Contact_For'
        ]
      )
      ->execute();

    $relatedContactDetails = [];
    // Contacts with these relationships get special mention on the renew page.
    $reportRelationshipTypes = [
      'Membership_Admin_Contact_For' => E::ts('Admin'), 
      'Membership_Technical_Contact_For' => E::ts("Technical")
    ];
    foreach($relatedContactResults as $contact) {
      // We may get more then one row per contact if they have multiple relationships.
      $contactId = $contact['contact_id_a'];
      if (!array_key_exists($contactId, $relatedContactDetails)) {
        $relatedContactDetails[$contactId] = [
          'permission_admin' => FALSE,
          'permission_technical' => FALSE,
        ];
      }
      $relatedContactDetails[$contactId]['display_name'] = $contact['contact_id_a.display_name'];
      $relatedContactDetails[$contactId]['first_name'] = $contact['contact_id_a.first_name'];
      $relatedContactDetails[$contactId]['last_name'] = $contact['contact_id_a.last_name'];
      $relatedContactDetails[$contactId]['email'] = $contact['email.email'];
      $relatedContactDetails[$contactId]['phone'] = $contact['phone.phone'];
      $relatedContactDetails[$contactId]['phone_display'] = preg_replace('/[0-9]{4}$/', 'xxxx', $contact['phone.phone'] ?? ''); 
      $relatedContactDetails[$contactId]['preferred_language'] = $contact['contact_id_a.preferred_language'];
      if ($contact['contact_id_a.preferred_language'] == 'en_US') {
        $relatedContactDetails[$contactId]['preferred_language_display'] = E::ts('English');
      }
      elseif ($contact['contact_id_a.preferred_language'] == 'es_ES') {
        $relatedContactDetails[$contactId]['preferred_language_display'] = E::ts('Spanish');
      }
      $relType = $contact['relationship_type_id:name'];
      if (!array_key_exists('relationships', $relatedContactDetails[$contactId])) {
        $relatedContactDetails[$contactId]['relationships'] = [];
      }
      if (array_key_exists($relType, $reportRelationshipTypes)) {
        $relatedContactDetails[$contactId]['relationships'][] = $reportRelationshipTypes[$relType];
        if ($relType == 'Membership_Admin_Contact_For') {
          $relatedContactDetails[$contactId]['permission_admin'] = TRUE;
        }
        elseif ($relType == 'Membership_Technical_Contact_For') {
          $relatedContactDetails[$contactId]['permission_technical'] = TRUE;
        } 
      }
    }

    // Now iterate one more time to create a nice summary
    foreach($relatedContactDetails as $contactId => $contact) {
      $contactInfo = [];
      if ($contact['email'] ?? FALSE) {
        $contactInfo[] = $contact['email'];
      }
      if ($contact['phone'] ?? FALSE) {
        // We mask the phone number for privacy reasons.
        $contactInfo[] = preg_replace('/[0-9]{4}$/', 'xxxx', $contact['phone']); 
      }
      $contactInfoSummary = implode(', ', $contactInfo);
      $groupSummary = implode(', ', $contact['relationships']);
      $relatedContactDetails[$contactId]['summary'] = $contact['display_name'] . ' : ' . $contactInfoSummary . ' : ' . $groupSummary;
      $relatedContactDetails[$contactId]['id'] = $contactId;
    }
    // Lastly rekey them sequentially so they are easier to access (e.g. via $related_contacts[0])
    return array_values($relatedContactDetails);
  }

  /**
   * getMembershipSummary
   *
   * Provide a brief summary appropriate for displaying on the
   * pay page.
   *
   */
  private function getMembershipSummary($membership) {
    // Return a small subset of membership details - only what is required to pay.
    $record = [];
    $record['id'] = $membership["id"];
    $record['display_name'] = $membership['display_name'];
    $record['membership_type'] = $membership['membership_type'];
    $record['renewal_amount'] = $membership['renewal_amount'];
    $record['renewal_amount_localized'] = $membership['renewal_amount_localized'];
    $record['end_date'] = $membership['end_date'];
    $record['membership_status'] = $membership['membership_status'];
    $record['currency'] = $membership['currency'];
    $record['csrf_token'] = $membership['csrf_token'];
    $record['exchange_rate'] = $membership['exchange_rate'];
    $record['membership_auto_renew_in_progress'] = $membership['membership_auto_renew_in_progress'];
    $record['membership_auto_renew_eligible'] = $membership['membership_auto_renew_eligible'];
    return $record;
  }
}

?>

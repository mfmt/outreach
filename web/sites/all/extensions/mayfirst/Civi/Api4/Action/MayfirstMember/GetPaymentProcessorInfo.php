<?php

namespace Civi\Api4\Action\MayfirstMember;

/**
 * Get details about the payment processors used for
 * membership renewal. 
 *
 * @method int getTestMode
 * @method $this setTestMode(int $testMode)
 *
 */
class GetPaymentProcessorInfo extends \Civi\Api4\Generic\AbstractAction {
  /**
   * testMode 
   *
   * @var int 
  */
  protected $testMode = 0;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    // We support Stripe and Paypal. And we need to return the
    // username and the id.
    $stripeResults = \Civi\Api4\PaymentProcessor::get()
      ->setCheckPermissions(FALSE)
      ->setSelect(['id', 'user_name'])
      ->setWhere([
        ['is_test', '=', $this->getTestMode()], 
        ['class_name', '=', 'Payment_StripeCheckout']
      ])
      ->execute()->first();

    $paypalResults = \Civi\Api4\PaymentProcessor::get()
      ->setCheckPermissions(FALSE)
      ->setSelect(['id', 'user_name'])
      ->setWhere([
        ['is_test', '=', $this->getTestMode()], 
        ['class_name', '=', 'Payment_PayPalImpl']
      ])
      ->execute()->first();

    $result[] = [
      'stripe_user_name' => $stripeResults['user_name'],
      'stripe_payment_processor_id' => $stripeResults['id'],
      'paypal_user_name' => $paypalResults['user_name'],
      'paypal_payment_processor_id' => $paypalResults['id'],
    ];
  }
}






?>

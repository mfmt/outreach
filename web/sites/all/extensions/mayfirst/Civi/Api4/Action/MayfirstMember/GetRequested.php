<?php

namespace Civi\Api4\Action\MayfirstMember;

/**
 *
 * Provide a recent list of memberships requested.
 *
 */
class GetRequested extends \Civi\Api4\Generic\AbstractAction {

  public function _run(\Civi\Api4\Generic\Result $result) {
    $twoWeeksAgo = date('Y-m-d', strtotime('4 weeks ago'));
    $membershipIds = \Civi\Api4\Membership::get()
      ->addSelect('id')
      ->addClause('OR', ['status_id:name', '=', 'Requested'], ['join_date', '>', $twoWeeksAgo])
      ->addWhere('status_id:name', '!=', 'Cancelled')
      ->addWhere('membership_type_id:name', 'IN', [ 'Annual', 'Monthly' ])
      ->addWhere('contact_id.is_deleted', '=', FALSE)
      ->execute()->column('id');
    foreach($membershipIds as $membershipId) {
      $membership = \Civi\Api4\MayfirstMember::getMembershipDetails()
        ->setMembershipId($membershipId)
        ->execute()
        ->first();

      // Pull in any welcome message that has already been sent.
      // This is tough. We simply search for any email activity sent in the last
      // 2 weeks. This should not include bulk email, only activity based email.
      $activityId = \Civi\Api4\ActivityContact::get()
        ->addSelect('activity_id')
        ->addWhere('record_type_id:name', '=', 'Activity Targets')
        ->addWhere('activity_id.activity_type_id:name', '=', 'Email')
        ->addWhere('activity_id.activity_date_time', '>', $twoWeeksAgo)
        ->addWhere('contact_id', '=', $membership['contact_id'])
        ->addClause('OR', ['activity_id.subject', 'LIKE', 'Welcome%'], ['activity_id.subject', 'LIKE', '¡Bienvenido%'])
        ->execute()->first()['activity_id'] ?? NULL;

      // We always store renewal amount in USD.
      $requestStatus = 'pending'; 
      if ($membership['membership_status'] != 'Requested') {
        $requestStatus = 'processed';
      }

      $shortName = '';
      if (preg_match('/^([0-9a-z-]*)\./', $membership['domain_name'], $matches)) {
        $shortName = $matches[1];
      }

      // Check for any Dues Reduction request activities.
      $duesReductionRequest = \Civi\Api4\Activity::get()
        ->addJoin('ActivityContact AS activity_contact', 'INNER')
        ->addSelect('details')
        ->addWhere('activity_contact.record_type_id', '=', 3)
        ->addWhere('activity_type_id:name', '=', 'Membership_Dues_Reduction_Request')
        ->addWhere('activity_contact.contact_id', '=', $membership['contact_id'])
        ->addWhere('is_deleted', '=', FALSE)
        ->addOrderBy('activity_date_time', 'DESC')
        ->execute()->first()['details'] ?? '';

      // Now, re-write the indices so they are easier to work with.
      $return = [
        'member_contact_id' => $membership['contact_id'],
        'membership_status' => $membership['membership_status'],
        'membership_id' => $membership['id'],
        'membership_type' => $membership['membership_type'],
        'start_date' => $membership['start_date'],
        'plan' => $membership['plan'],
        'renewal_amount' => $membership['renewal_amount'],
        'renewal_amount_localized' => $membership['renewal_amount_localized'],
        'currency' => $membership['currency'],
        'hosting_benefits' => $membership['hosting_benefits'],
        'extra_storage' => $membership['extra_storage'],
        'virtual_private_servers' => $membership['virtual_private_servers'],
        'cpu' => $membership['cpu'],
        'ram' => $membership['ram'],
        'ssd' => $membership['ssd'],
        'dues_reduction_description' => $duesReductionRequest,
        'calculated_storage' => $membership['calculated_storage'],
        'display_name' => $membership['display_name'],
        'organization_name' => $membership['organization_name'],
        'first_name' => $membership['first_name'],
        'last_name' => $membership['last_name'],
        'contact_type' => $membership['contact_type'],
        'control_panel_id' => $membership['external_identifier'],
        'income' => $membership['income'],
        'why_join' => $membership['why_join'],
        'how_hear' => $membership['how_hear'],
        'city' => $membership['city'],
        'state_province' => $membership['state_province_id:name'],
        'country' => $membership['country_id:name'],
        'domain_name' => $membership['domain_name'],
        'related_first_name' => $membership['related_contacts'][0]['first_name'] ?? '',
        'related_last_name' => $membership['related_contacts'][0]['last_name'] ?? '',
        'related_email' => $membership['related_contacts'][0]['email'] ?? '',
        'related_phone' => $membership['related_contacts'][0]['phone'] ?? '',
        'related_preferred_language_display' => $membership['related_contacts'][0]['preferred_language_display'] ?? '',
        'related_preferred_language' => $membership['related_contacts'][0]['preferred_language'] ?? '',
        'welcome_email_activity_id' => $activityId,
        'request_status' => $requestStatus,
        'short_name' => $shortName,
      ];
      $result[] = $return;
    }
  }
}






?>

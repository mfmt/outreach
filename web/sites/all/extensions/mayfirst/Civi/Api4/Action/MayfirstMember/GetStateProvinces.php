<?php

namespace Civi\Api4\Action\MayfirstMember;

/**
 *
 * Provide unrestricted access to retrieving a list of states 
 * so we can display a drop down for people joining or changing
 * their contact info.
 *
 * @method int getCountryId()
 * @method setCountryId(int countryId)
 */
class GetStateProvinces extends \Civi\Api4\Generic\AbstractAction {

  /**
   * countryId
   *
   * @int
   * @required
   */
  protected $countryId;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $result[] = \Civi\Api4\StateProvince::get()
      ->setCheckPermissions((FALSE))
      ->addWhere('country_id','=', $this->getCountryId())
      ->execute(); 
  }
}






?>

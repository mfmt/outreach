<?php

namespace Civi\Api4\Action\MayfirstMember;

use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Import membership from control panel
 *
 * One time script for taking membership data from the red control panel
 * and importing it into our CiviCRM database. 
 *
 * @method path getPath() 
 **/

class Import extends \Civi\Api4\Generic\AbstractAction {

  /**
   * path to csv file. 
   *
   * @required
   * @str
   */
  protected $path;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $path = $this->getPath();
    if (!file_exists($path)) {
      throw new \API_Exception("Can't find {$path}. Please use a path the exists.");
    }

    $f = fopen($path, 'r');

    if ($f === false) {
      throw new \API_Exception("Can't open {$path}.");
    }

    $first = TRUE;
    // Track columns using the header in case we change the order.
    $columns = [];
    $count = 0;
    while (($row = fgetcsv($f)) !== false) {
      if ($first) {
        $columns = $row;
        $first = FALSE;
        continue;
      }
      if (count($row) < 2) {
        continue;
      }
      $data = [];
      foreach($columns as $i => $column) {
        $data[$column] = $row[$i];
      }
      // Now we can access each field by the name of the column.
      $memberId = $data['member_id'];
      $dues = $data['new_dues'];
      $dues_usd = round($data['dues_usd']);
      $name = $data['name'];
      $term = $data['term'];
      $vps = $data['vps_count'];
      $cpu = $data['cpu_count'];
      $ram = $data['ram_count'];
      $ssd = $data['ssd_count'];
      $expirationDate = $data['expiration_date'];
      $currency = $data['currency'];
      $hostingBenefits = $data['hosting_benefits'];
      $plan = $data['plan'];
      $income = $data['income'];
      $extraStorage = $data['extra_storage'];
      $duesReductionExplanation = $data['dues_reduction_explanation'];

      $membershipType = $term == 'year' ? 'Annual' : 'Monthly';
      // Try to find this member's existing membership.
      $membership = \Civi\Api4\Membership::get()
        ->addSelect('id', 'contact_id')
        ->addWhere('contact_id.external_identifier', '=', "member:{$memberId}")
        ->execute()->first();

      if (empty($membership)) {
        echo "No membership for: {$name}\n";
        continue;
      }
      \Civi\Api4\Membership::update()
        ->addValue('Membership_Details.Plan', $plan)
        ->addValue('Membership_Details.Currency:name', $currency)
        ->addValue('Membership_Details.Renewal_Amount', $dues_usd)
        ->addValue('Membership_Details.Extra_Storage', $extraStorage)
        ->addValue('Membership_Details.Hosting_Benefits', $hostingBenefits)
        ->addValue('Membership_Details.Dues_Reduction_Description', $duesReductionExplanation)
        ->addValue('Membership_Details.Virtual_Private_Servers', $vps)
        ->addValue('Membership_Details.Cpu', $cpu)
        ->addValue('Membership_Details.Ram', $ram)
        ->addValue('Membership_Details.Ssd', $ssd)
        ->addValue('membership_type_id:name', $membershipType)
        ->addValue('end_date', $expirationDate)
        ->addWhere('id', '=', $membership['id'])
        ->execute();

      \Civi\Api4\Contact::update()
        ->addWhere('id', '=', $membership['contact_id'])
        ->addValue('Member_Info.Currency:name', $currency)
        ->addValue('Member_Info.Income', $income)
        ->execute();
      $count++;
    }
    $result[] = [ 'Records update' => $count ];

  }
}
?>

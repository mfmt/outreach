<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Provide membership statistics 
 */

class MembershipStatistics extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The date cutoff.
   *
   * Get all memberships on or after the given date. Defaults to 2005.
   * Use YYYY-MM-DD format or any format understood by strtotime. 
   *
   * @var string
   */
  protected $date = '2014-01-01';

  /**
   * The path to the file to save as CSV. The file must not
   * exit.
   */
  protected $file;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $file = $this->getFile();
    $fp = NULL;
    if ($file) {
      if (file_exists($file)) {
        throw new \API_Exception("The path {$file} exists. Please delete it to avoid overwriting.");
      }
      $fp = fopen($file, 'w');
    }
    $activeBeginDate = new \DateTimeImmutable($this->getDate());
    $nowDate = new \DateTimeImmutable();
    $stats = [];
    if ($fp) {
      fputcsv($fp, ['period', 'total', 'new', 'lost', 'registered' ]);
    }
    while ($activeBeginDate < $nowDate) {
      // Add one month to the date
      $activeEndDate = $activeBeginDate->modify('+3 month');

      $countTotal = \Civi\Api4\Membership::get()
        ->addWhere('join_date', '<', $activeEndDate->format('Y-m-d'))
        // If a membership is in grace or current, it should be considered part of
        // the total, even if the membership end date falls before the end date
        // cut off - this is to ensure people who are simply late on their dues
        // or are not even behind yet will be counted in the total membersihp count.
        ->addClause('OR', 
          ['end_date', '>=', $activeEndDate->format('Y-m-d')], 
          ['status_id:name', 'IN', ['Grace', 'Current']]
        )
        ->execute()->count();
      $countNew = \Civi\Api4\Membership::get()
        ->addWhere('join_date', '>=', $activeBeginDate->format('Y-m-d'))
        ->addWhere('join_date', '<', $activeEndDate->format('Y-m-d'))
        ->execute()->count();
      $countLeft = \Civi\Api4\Membership::get()
        // Only count someone as having left if they membership is disabled or cancelled.
        ->addWhere('status_id:name', 'IN', ['Disabled', 'Cancelled'])
        ->addWhere('end_date', '>=', $activeBeginDate->format('Y-m-d'))
        ->addWhere('end_date', '<', $activeEndDate->format('Y-m-d'))
        ->execute()->count();
      $countRegistered = \Civi\Api4\Participant::get()
        // How many people registered for an event during this period? 
        ->addWhere('register_date', '>=', $activeBeginDate->format('Y-m-d'))
        ->addWhere('register_date', '<', $activeEndDate->format('Y-m-d'))
        ->execute()->count();

      $beginReport = $activeBeginDate->format('Y-m-d');
      $endReport = $activeEndDate->format('Y-m-d');
      $report = [ 'total' => $countTotal, 'new' => $countNew, 'left' => $countLeft, 'registered' => $countRegistered ];
      if (preg_match('/([0-9]{4})-04-01/', $endReport, $matches)) {
        $period = $matches[1] . " Q1";
      }
      elseif (preg_match('/([0-9]{4})-07-01/', $endReport, $matches)) {
        $period = $matches[1] . " Q2";
      }
      elseif (preg_match('/([0-9]{4})-10-01/', $endReport, $matches)) {
        $period = $matches[1] . " Q3";
      }
      elseif (preg_match('/([0-9]{4})-01-01/', $endReport, $matches)) {
        $period = $matches[1] - 1 . " Q4";
      }
      else {
        $period = "Unknown quarter";
      }
      $stats[$period] = $report;
      if ($fp) {
        fputcsv($fp, [ $period ] + $report);
      }
      $activeBeginDate = $activeEndDate;
    }
    if ($fp) {
      fclose($fp);
    }
    $result[] = $stats;
  }
  
}
?>

<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Print a list of members who should be reviewed or need special
 * attention. 
 *
 */
class ProblemMembers extends \Civi\Api4\Generic\AbstractAction {

  public function _run(\Civi\Api4\Generic\Result $result) {
    $memberships = \Civi\Api4\Membership::get()
      ->addSelect('id', 'Membership_Details.Plan', 'contact_id.display_name', 'contact_id', 'Membership_Details.Renewal_Amount', 'end_date')
      ->addWhere('status_id.is_current_member', '=', TRUE)
      ->addWhere('membership_type_id:name', 'IN', ['Monthly', 'Annual'])
      ->addOrderBy('end_date', 'ASC')
      ->execute();
    foreach($memberships as $membership) {
      $membershipId = $membership['id'];
      $displayName = $membership['contact_id.display_name'];
      $plan = $membership['Membership_Details.Plan'];
      $actualDues = intval($membership['Membership_Details.Renewal_Amount']);
      $contactId = $membership['contact_id']; 
      $endDate = $membership['end_date']; 
      
      $contact = \Civi\Api4\Contact::get()
        ->addWhere('id', '=', $contactId)
        ->addSelect(
          'Member_Hosting_Details.Control_Panel_Allocated',
          'Member_Hosting_Details.Control_Panel_Usage',
          'Member_Info.Percent_Reduction',
        )
        ->execute()->first();
      $allocated = intval($contact['Member_Hosting_Details.Control_Panel_Allocated']);
      $usage = $contact['Member_Hosting_Details.Control_Panel_Usage'];
      $reduction = $contact['Member_Info.Percent_Reduction'];

      $error = '';
      try {
        $dues = \Civi\Api4\Dues::get()
        ->setContactId($contactId)
        ->execute()->first();
        $quota = intval($dues['storage']);
        $calculatedDues = intval($dues['dues']);
      }
      catch (\API_Exception $e) {
        $quota = $calculatedDues = 0;
        $error = $e->getMessage();
      }
      if ($reduction) {
        $calculatedDues = $calculatedDues - round($reduction / 100 * $calculatedDues);
      }
      if ($quota < $allocated || $calculatedDues != $actualDues) {
        $overQuota = $allocated - $quota;
        if ($overQuota < 0) $overQuota = 0;
        $result[] = [  'id' => $contactId, 'name' => $displayName, 'allowed' => $quota, 'allocated' => $allocated,  'over_quota' => $overQuota, 'usage' => $usage, 'calculated_dues' => $calculatedDues, 'actual_dues' => $actualDues, 'dues_difference' => $calculatedDues - $actualDues, 'error' => $error, 'end_date' => $endDate ];
      }
    }
  }

}

<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Remove the relationship between a contact and a member. 
 *
 * @method string getToken
 * @method $this setToken(string $token)
 * @method array getId
 * @method $this setId(int $id)

 *
 */
class RemoveRelatedContact extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The token (hash:memberhip_id). 
   *
   * @var string
   * @required
   */
  protected $token = NULL;

  /**
   * The contact id to remove.
   *
   * @var int 
   * @required
   */
  protected $id = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    if (!\Civi\Mayfirst\Utils::validToken($this->getToken())) {
      throw new \API_Exception(E::ts("Invalid Token."));
    }
    $tokenPieces = explode(':', $this->getToken());
    $membershipId = $tokenPieces[1];

    $memberContactId =   \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('contact_id')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first()['contact_id'];

    \Civi\Api4\Relationship::delete()
        ->setCheckPermissions(FALSE)
        ->addWhere('contact_id_a', '=', $this->getId())
        ->addWhere('contact_id_b', '=', $memberContactId)
        ->execute();

    $result[] = [];
  }
}






?>

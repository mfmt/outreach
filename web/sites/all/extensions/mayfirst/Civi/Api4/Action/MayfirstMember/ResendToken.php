<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Send token and renew links for the given email address. 
 *
 * @method string getEmail
 * @method $this setEmail(string $email)
 *
 */
class ResendToken extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The email requesting the token. 
   *
   * @var string
   * @required
   */
  protected $email = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    $email = filter_var($this->getEmail(), FILTER_VALIDATE_EMAIL);
    if (!$email) {
      throw new \API_Exception(E::ts("'%1' did not look like a valid email.", [ 1 => $this->getEmail() ]));
    }
    $result[] = \Civi\Mayfirst\Email::sendRenewalLinkForEmail($email); 
  }
}






?>

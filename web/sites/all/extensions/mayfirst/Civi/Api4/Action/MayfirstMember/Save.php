<?php

namespace Civi\Api4\Action\MayfirstMember;

use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Save Membership contact and details.
 *
 * This API is used to save membership details, either when submitting
 * a new membership or modifying an existing one. 
 *
 * @method array getFields() 
 * @method setFields(array fields) 
 * @method str getToken() 
 * @method setToken(string token) 
 **/

class Save extends \Civi\Api4\Generic\AbstractAction {

  /**
   * fields 
   *
   * @required
   * @array
   */
  protected $fields;

  /**
   *
   * token
   *
   * @string
   */
  protected $token;

  /**
   * AutoAdjustEndDate
   *
   * If the membership includes more storage or a different plan
   * should we try to autoadjust the expiration date? Default
   * False.
   *
   * @bool
   */
  protected $autoAdjustEndDate = FALSE;
  
  /**
   * memberContactId
   *
   * The member's contactId.
   */
  private $memberContactId;

  /**
   * relatedContactId
   *
   * The related contactId.
   */
  private $relatedContactId;

  /**
   * MembershipId
   *
   */
  private $membershipId;

  /**
   * duplicateIndividualContactId
   *
   * If there is a matching individual in the database
   * keep track of their contactId.
   */
  private $duplicateIndividualContactId;

  /**
   * duplicateOrganizationContactId
   *
   * If there is a matching organization in the database
   * keep track of their contactId.
   */
  private $duplicateOrganizationContactId;

  /**
   * duesReductionRequest
   *
   * Keep track if the user is submitting a new dues reduction
   * reqest and if so be sure to notify staff so it can be
   * processed.
   */
  private $duesNotificationRequest = FALSE;

  /**
   * savedMembership
   *
   * The membership details before we applied any changes.
   */
  private $savedMembership = [];

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    // Be sure to run MayfirstMember::Validate() before running
    // this API.  
    
    // Convert some of the weirdness from angular's format to
    // something we expect in CiviCRM.
    $this->translateFields();

    if ($this->getToken()) {
      // We are editing an existing membership.
      if (!\Civi\Mayfirst\Utils::validToken($this->getToken())) {
        throw new \API_Exception(E::ts("Your token does not seem valid."));
      }
      $tokenPieces = explode(':', $this->getToken());
      $this->membershipId = $tokenPieces[1];
      $type = $tokenPieces[2];
      if ($type != 'edit') {
        throw new \API_Exception(E::ts("Your token is not authorized to make changes to your membership."));
      }
      // We are saving an existing membership. Determine the memberContactId
      // so we can properly save the member details.
      $this->memberContactId = \Civi\Api4\Membership::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('contact_id')
        ->addWhere('id', '=', $this->membershipId)
        ->execute()->first()['contact_id'];

      // Populate $this->savedMembership;
      $this->setSavedMembership();
      $this->updateMemberContact();
      $this->saveMemberContactAddress();
      $this->saveMemberWebsite();
      $this->updateMembership();
    }
    else {
      // It's a new membership submission.
      // Check for duplicates. If we find more then one duplicate we will take
      // the first one we find rather then throw an error.
      if ($this->getFields()['contact_type'] == 'Organization') {
        $this->checkForOrganizationDuplicate();
      }

      // When creating a new membership We will always have an individual contact
      // - either the related contact to the organization or both the member and
      // the related contact to that member.
      $this->checkForIndividualDuplicate();

      // The member.
      $this->saveMemberContact();
      $this->saveMemberContactAddress();
      $this->saveMemberWebsite();

      // The related contact. On update related contacts are handled through
      // the AddRelatedContact and RemoveRelatedContact APIs.
      $this->saveRelatedContact();
      $this->saveRelatedContactEmail();
      $this->saveRelatedContactPhone();
      $this->saveRelatedContactRelationship();

      // And finally, the membership.
      $this->createMembership();
      $this->notify();
    }
    // With a new membership or an existing one, there might be a request for a
    // dues reduction. If so, create an activity.
    $this->createDuesReductionRequestActivity();

    $result[] = [ 'id' => $this->membershipId ];
  }

  /**
   * Translate fields from angularjs syntax to CiviCRM syntax
   *
   */
  private function translateFields() {
    $fields = $this->getFields();

    // Remove any extra white space.
    foreach ($fields as $k => $v) {
      if (is_string($v)) {
        $fields[$k] = trim($v);
      }
    }

    $fields['country_id'] = intval($fields['country']['id']);
    $fields['state_province_id'] = intval($fields['state_province']['id']);
    unset($fields['country']);
    unset($fields['state_province']);
    $integer_fields = [ 'hosting_benefits', 'cpu', 'ram', 'virtual_private_servers', 'extra_storage' ];
    foreach ($integer_fields as $integer_field) {
      if (!array_key_exists($integer_field, $fields)) {
        $fields[$integer_field] = 0;
      }
      else {
        $fields[$integer_field] = intval($fields[$integer_field]);
      }
    }
    // Normalize the domain name - in case people enter an URL.
    $fields['domain_name'] = preg_replace('#^https?://#', '', $fields['domain_name'] ?? '');
    // Get rid of a trailing slash.
    $fields['domain_name'] = preg_replace('#/$#', '', $fields['domain_name'] ?? '');

    $fields['mobile_phone_numeric'] = preg_replace('/[^0-9]/', '', $fields['mobile_phone'] ?? '');
    $this->setFields($fields);
  }

  private function checkForOrganizationDuplicate() {
    $org = \Civi\Api4\Contact::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('organization_name', '=', $this->getFields()['organization_name'])
      ->addWhere('contact_type', '=', 'Organization')
      ->execute()->first();
    if ($org) {
      // We have a duplicate. That's cool. We will update this record.
      $this->duplicateOrganizationContactId = $org['id'];
    }
  }

  private function checkForIndividualDuplicate() {
    $ind = \Civi\Api4\Contact::get()
      ->setCheckPermissions(FALSE)
      ->setJoin([ 
        [ 'Email AS email', FALSE ],
        [ 'Phone AS phone', FALSE ] 
      ])
      ->addSelect('id', 'is_deceased', 'external_identifier')
      ->setGroupBy([ 'id' ])
      ->addWhere('contact_type', '=', 'Individual')
      ->addWhere('first_name', '=', $this->getFields()['first_name'])
      ->addWhere('last_name', '=', $this->getFields()['last_name'])
      ->addWhere('is_deleted', '=', 0)
      ->addClause('OR', ['email.email', '=', $this->getFields()['email_address'] ?? ''], [ 'phone.phone_numeric', '=', $this->getFields()['mobile_phone'] ?? ''])
      ->execute()->first();
    if ($ind) {
      $this->duplicateIndividualContactId = $ind['id'];
    }
  }

  private function addressAlreadyAdded() {
    if (!$this->memberContactId) {
      return FALSE;
    }
    $result = \Civi\Api4\Address::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('contact_id', '=', $this->memberContactId)
      ->addWhere('city', '=', $this->getFields()['city'])
      ->addWhere('state_province_id', '=', $this->getFields()['state_province_id'])
      ->addWhere('country_id', '=', $this->getFields()['country_id'])
      ->execute();
    if ($result->count() == 0) {
      return FALSE;
    }
    return TRUE;
  }

  private function websiteAlreadyAdded() {
    if (!$this->memberContactId) {
      return FALSE;
    }
    $domainName = $this->getFields()['domain_name'];

    // If they didn't provide a domain name, then we consider
    // it "already added".
    if (empty($domainName)) {
      return TRUE;
    }

    $result = \Civi\Api4\Website::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('contact_id', '=', $this->memberContactId)
      ->addWhere('url', '=', 'https://' . $domainName)
      ->addWhere('website_type_id:name', '=', 'Work')
      ->execute();
    if ($result->count() == 0) {
      return FALSE;
    }
    return TRUE;
  }

  private function phoneAlreadyAdded() {
    if (!$this->memberContactId) {
      return FALSE;
    }
    $result = \Civi\Api4\Phone::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('contact_id', '=', $this->relatedContactId)
      ->addWhere('phone_numeric', '=', $this->getFields()['mobile_phone_numeric'])
      ->execute();
    if ($result->count() == 0) {
      return FALSE;
    }
    return TRUE;
  }

  private function emailAlreadyAdded() {
    if (!$this->relatedContactId) {
      return FALSE;
    }
    $result = \Civi\Api4\Email::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('contact_id', '=', $this->relatedContactId)
      ->addWhere('email', '=', $this->getFields()['email_address'])
      ->execute();
    if ($result->count() == 0) {
      return FALSE;
    }
    return TRUE;
  }

  private function relationshipAlreadyAdded($relationship_name) {
    if (!$this->memberContactId) {
      return FALSE;
    }

    $result = \Civi\Api4\Relationship::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('contact_id_a', '=', $this->relatedContactId)
      ->addWhere('contact_id_b', '=', $this->memberContactId)
      ->addWhere('relationship_type_id:name', '=', $relationship_name)
      ->execute();
    if ($result->count() == 0) {
      return FALSE;
    }
    return TRUE;
  }

  private function workLocationTypeId() {
    return \CRM_Core_Pseudoconstant::getKey(
      'CRM_Core_BAO_Address', 
      'location_type_id', 
      'Work'
    );
  }

  private function saveRelatedContact() {
    if ($this->duplicateIndividualContactId) {
      // We will use the existing, matching contact rather than create a new one.
      $this->relatedContactId = $this->duplicateIndividualContactId;
    }
    else {
      // If the contact type is Individual, then we already added the contact
      // when creating the member above because the memberContact and the 
      // relatedContact are the same.
      if ($this->getFields()['contact_type'] == 'Individual') {
        $this->relatedContactId = $this->memberContactId;
      }
      else {
        // It's an organization, so we have to create a new related contact.
        $this->relatedContactId = \Civi\Api4\Contact::create()
          ->setCheckPermissions(FALSE)
          ->addValue('first_name', $this->getFields()['first_name'])
          ->addValue('last_name', $this->getFields()['last_name'])
          ->addValue('contact_type', 'Individual')
          ->execute()->first()['id'];
      }
    }

    // Update preferred language.
    \Civi\Api4\Contact::update()
      ->setCheckPermissions(FALSE)
      ->addValue('preferred_language', $this->getFields()['preferred_language'])
      ->addWhere('id', '=', $this->relatedContactId)
      ->execute();
  }

  private function saveRelatedContactPhone() {
    $phone = $this->getFields()['mobile_phone'] ?? '';
    if ($phone && !$this->phoneAlreadyAdded()) {
      $mobilePhoneTypeId = \CRM_Core_Pseudoconstant::getKey(
        'CRM_Core_BAO_Phone', 
        'phone_type_id', 
        'Mobile'
      );
      \Civi\Api4\Phone::create()
        ->setCheckPermissions(FALSE)
        ->addValue('contact_id', $this->relatedContactId)
        ->addValue('location_type_id', $this->workLocationTypeId())
        ->addValue('phone', $phone)
        ->addValue('is_primary', TRUE)
        ->addValue('phone_type_id', $mobilePhoneTypeId)
        ->execute();
    }
  }

  private function saveRelatedContactEmail() { 
    $email = $this->getFields()['email_address'];
    if ($email && !$this->emailAlreadyAdded()) {
      \Civi\Api4\Email::create()
        ->setCheckPermissions(FALSE)
        ->addValue('contact_id', $this->relatedContactId)
        ->addValue('location_type_id', $this->workLocationTypeId())
        ->addValue('is_primary', TRUE)
        ->addValue('email', $email)
        ->execute();
    }
  }

  private function saveMemberWebsite() { 
    $domainName = $this->getFields()['domain_name'];
    if ($domainName && !$this->websiteAlreadyAdded()) {
      \Civi\Api4\Website::create()
        ->setCheckPermissions(FALSE)
        ->addValue('contact_id', $this->memberContactId)
        ->addValue('website_type_id:name', 'Work')
        ->addValue('url', 'https://' . $domainName)
        ->execute();
    }
  }

  // Create relationships: General, Admin, and Technical.
  private function saveRelatedContactRelationship() { 
    $relationships = [ 
      'Membership_General_Contact_For',
      'Membership_Admin_Contact_For',
      'Membership_Technical_Contact_For',
    ];

    foreach ($relationships as $relationshipName) {
      if (!$this->relationshipAlreadyAdded($relationshipName)) {
        \Civi\Api4\Relationship::create()
          ->setCheckPermissions(FALSE)
          ->addValue('contact_id_a', $this->relatedContactId)
          ->addValue('contact_id_b', $this->memberContactId)
          ->addValue('relationship_type_id:name', $relationshipName)
          ->execute();
      }
    }
  }

  private function saveMemberContact() {
    $records = [
      'contact_type' => $this->getFields()['contact_type'],
      'Member_Info.Income' => $this->getFields()['income'] ?? 0,
      'Member_Info.Why_Join' => $this->getFields()['why_join'] ?? '',
      'Member_Info.How_Hear' => $this->getFields()['how_hear'] ?? '',
      'preferred_language' => $this->getFields()['preferred_language'],
    ]; 

    if ($this->getFields()['contact_type'] == 'Organization') {
      $records['organization_name'] = $this->getFields()['organization_name'];
      if ($this->duplicateOrganizationContactId) {
        $records['id'] = $this->duplicateOrganizationContactId;
      }
    }
    else {
      $records['first_name'] = $this->getFields()['first_name'];
      $records['last_name'] = $this->getFields()['last_name'];
      if ($this->duplicateIndividualContactId) {
        $records['id'] = $this->duplicateIndividualContactId;
      }
    }
        
    $this->memberContactId = \Civi\Api4\Contact::save()
      ->setCheckPermissions(FALSE)
      ->setRecords([$records])
      ->execute()->first()['id'];
  }

  private function updateMemberContact() {
    $update = \Civi\Api4\Contact::update()
      ->setCheckPermissions(FALSE)
      ->addValue('Member_Info.Income', $this->getFields()['income'] ?? 0)
      ->addvalue('Member_Info.Why_Join', $this->getFields()['why_join'] ?? '')
      ->addvalue('Member_Info.How_Hear', $this->getFields()['how_hear'] ?? '')
      ->addWhere('id', '=', $this->memberContactId);

    if ($this->getFields()['contact_type'] == 'Organization') {
      $update->addValue('organization_name', $this->getFields()['organization_name']);
    }
    else {
      $update->addValue('first_name', $this->getFields()['first_name']);
      $update->addValue('last_name', $this->getFields()['last_name']);
    }

    $update->execute(); 

  }
  private function saveMemberContactAddress() {
    if (!$this->addressAlreadyAdded()) {
      // If there already is a primary address, replace it with this
      // address.
      $addressId = \Civi\Api4\Address::get()
        ->addWhere('is_primary', '=', TRUE) 
        ->addWhere('contact_id', '=', $this->memberContactId)
        ->addSelect('id')
        ->execute()->first()['id'] ?? NULL;

      if ($addressId) {
        \Civi\Api4\Address::update()
          ->setCheckPermissions(FALSE)
          ->addValue('contact_id', $this->memberContactId)
          ->addValue('city', $this->getFields()['city'])
          ->addValue('state_province_id', $this->getFields()['state_province_id'])
          ->addValue('country_id', $this->getFields()['country_id'])
          ->addWhere('id', '=', $addressId)
          ->execute();

      }
      else {
        \Civi\Api4\Address::create()
          ->setCheckPermissions(FALSE)
          ->addValue('contact_id', $this->memberContactId)
          ->addValue('location_type_id', $this->workLocationTypeId())
          ->addValue('city', $this->getFields()['city'])
          ->addValue('state_province_id', $this->getFields()['state_province_id'])
          ->addValue('country_id', $this->getFields()['country_id'])
          ->addValue('is_primary', TRUE)
          ->execute();
      }
    }
  }

  private function getDues() {
    return \Civi\Api4\Dues::Calculate()
      ->setFrequency($this->getFields()['membership_type'])
      ->setCpu($this->getFields()['cpu'] ?? 0)
      ->setRam($this->getFields()['ram'] ?? 0)
      ->setSsd($this->getFields()['ssd'] ?? 0)
      ->setVirtualPrivateServers($this->getFields()['virtual_private_servers'] ?? '')
      ->setExtraStorage($this->getFields()['extra_storage'] ?? '')
      ->setPlan($this->getFields()['plan'] ?? 0)
      ->setIncome($this->getFields()['income'] ?? 0)
    // Note: We always save dues in USD and convert to MXN just prior to paying so
    // we can adjust the exchange rate as needed while still maintaining the canonical 
    // amount of their membership in USD.
      ->setCurrency('USD')
      ->setHostingBenefits($this->getFields()['hosting_benefits'])
      ->setCpu($this->getFields()['cpu'] ?? '')
      ->execute()->first()['dues'];
  }

  private function createMembership() {
    // Start by calculating the dues.
    $dues = $this->getDues();
      
    $this->membershipId = \Civi\Api4\Membership::create()
      ->setCheckPermissions(FALSE)
      ->addValue('contact_id', $this->memberContactId)
      ->addValue('Membership_Details.Hosting_Benefits', $this->getFields()['hosting_benefits'])
      ->addValue('Membership_Details.Plan', $this->getFields()['plan'] ?? 0)
      ->addValue('Membership_Details.Currency:name', $this->getFields()['currency'])
      ->addValue('Membership_Details.Renewal_Amount', $dues)
      ->addValue('Membership_Details.Extra_Storage', $this->getFields()['extra_storage'] ?? 0)
      ->addValue('Membership_Details.Virtual_Private_Servers', $this->getFields()['virtual_private_servers'] ?? 0)
      ->addValue('Membership_Details.Cpu', $this->getFields()['cpu'] ?? 0)
      ->addValue('Membership_Details.Ram', $this->getFields()['ram'] ?? 0)
      ->addValue('Membership_Details.Ssd', $this->getFields()['ssd'] ?? 0)
      ->addValue("status_id:name", 'Requested')
      ->addValue("membership_type_id:name", $this->getFields()['membership_type'])
      ->addValue('start_date', date('YmdHis'))
      ->addValue('end_date', date('YmdHis'))
      ->addValue('join_date', date('YmdHis'))
      ->execute()->first()['id'];
  }

  private function setSavedMembership() {
    $savedMembership = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect(
        'start_date',
        'end_date',
        'Membership_Details.Renewal_Amount', 
        'Membership_Details.Plan',
        'Membership_Details.Extra_Storage',
        'Membership_Details.Hosting_Benefits',
        'Membership_Details.Virtual_Private_Servers',
        'Membership_Details.Cpu',
        'Membership_Details.Ram',
        'Membership_Details.Ssd',
        'membership_type_id:name',
        'contact_id.Member_Info.Income'
      )
      ->addWhere('id', '=', $this->membershipId)
      ->execute()->first();
    $savedMembership['storage'] = \Civi\Api4\Dues::Calculate()
      ->setCheckPermissions(FALSE)
      ->setFrequency($savedMembership['membership_type_id:name'])
      ->setVirtualPrivateServers($savedMembership['Membership_Details.Virtual_Private_Servers'])
      ->setCpu($savedMembership['Membership_Details.Cpu'])
      ->setRam($savedMembership['Membership_Details.Ram'])
      ->setSsd($savedMembership['Membership_Details.Ssd'])
      ->setIncome($savedMembership['contact_id.Member_Info.Income'])
      ->setPlan($savedMembership['Membership_Details.Plan'])
      ->setExtraStorage($savedMembership['Membership_Details.Extra_Storage'])
      ->setHostingBenefits(intval($savedMembership['Membership_Details.Hosting_Benefits']))
      ->execute()->first()['storage'];
    $this->savedMembership = $savedMembership;
  }

  private function updateControlPanel($quota) {
    $contact = \Civi\Api4\Contact::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('id', '=', $this->memberContactId)
      ->addSelect('external_identifier', 'Member_Hosting_Details.Control_Panel_Allocated')
      ->execute()->first();
    $externalId = $contact['external_identifier'];
    $allocated = $contact['Member_Hosting_Details.Control_Panel_Allocated'];

    if ($quota < $allocated) {
      throw new \API_Exception("You are already allocated $allocated GB, so you cannot set the quota to the lower value of $quota. Please contact the office for support.");
    }
    $externalIdParts = explode(':', $externalId);
    $controlPanelId = $externalIdParts[1];

    if (!$controlPanelId) {
      throw new \API_Exception("Failed to extract control panel id from member with contact id: $contactId and external identifier: $externalId. Please contact the office for support.");
    }

    $params = array(
      'action' => 'update',
      'object' => 'member',
      'set:member_quota' => $quota * 1024 * 1024 * 1024,
      'where:member_id' => $controlPanelId,
    );
    $ret = \Civi\Mayfirst\Utils::callControlPanelApi($params);
    $error = $ret['is_error'];
    if ($error != 0) {
      throw new \API_Exception(E::ts("Something went wrong updating the membership quota in the control panel. Please contact the office: %1.", [ 1 =>  $ret['error_message']]));
    }
  }

  private function updateMembership() {
    // Start by calculating the dues.
    $dues = $this->getDues();
      
    // Check the saved values.
    $savedMembership = $this->savedMembership;
    $savedDues = $savedMembership['Membership_Details.Renewal_Amount'];
    $savedStartDate = $savedMembership['start_date'];
    $savedEndDate = $savedMembership['end_date'];
    $savedQuota = $savedMembership['storage'];
    $newEndDate = NULL;
    if ($this->getAutoAdjustEndDate()) {
      // See if dues have changed.
      \Civi::log()->debug("Saving membership, comparing old dues ({$savedDues}) to new dues ({$dues})");
      if ($savedDues != $dues) {
        // Calculate a new end date.
        $newEndDate = \Civi\Api4\Dues::EndDateForDuesOwed()
          ->setCheckPermissions(FALSE)
          ->setStartDate($savedStartDate)
          ->setEndDate($savedEndDate)
          ->setOldDues($savedDues)
          ->setNewDues($dues)
          ->execute()->first()['date'];
        \Civi::log()->debug("Dues have changed, change end date {$savedEndDate} to {$newEndDate}");
      }
    }
    $update = \Civi\Api4\Membership::update()
      ->setCheckPermissions(FALSE)
      ->addValue('Membership_Details.Hosting_Benefits', $this->getFields()['hosting_benefits'])
      ->addValue('Membership_Details.Plan', $this->getFields()['plan'] ?? 0)
      ->addValue('Membership_Details.Currency:name', $this->getFields()['currency'])
      ->addValue('Membership_Details.Renewal_Amount', $dues)
      ->addValue('Membership_Details.Extra_Storage', $this->getFields()['extra_storage'])
      ->addValue('Membership_Details.Virtual_Private_Servers', $this->getFields()['virtual_private_servers'])
      ->addValue('Membership_Details.Cpu', $this->getFields()['cpu'] ?? 0)
      ->addValue('Membership_Details.Ram', $this->getFields()['ram'] ?? 0)
      ->addValue('Membership_Details.Ssd', $this->getFields()['ssd'] ?? 0)
      ->addValue("membership_type_id:name", $this->getFields()['membership_type'])
      ->addWhere('id', '=', $this->membershipId);

    if ($newEndDate) {
      $update = $update->addValue('end_date', $newEndDate);
    }
    $update->execute();

    $newQuota = $this->savedMembership['storage'];
    
    if ($newQuota != $savedQuota) {
      // Update control panel disk quota as well.
      $this->updateControlPanel($newQuota); 
    }
  }

  private function createDuesReductionRequestActivity() {
    $duesReductionRequest = $this->getFields()['dues_reduction_description'] ?? '';
    if (!$duesReductionRequest) {
      // Nothing to do.
      return;
    }

    // Assign to the contact with the members@mayfirst.org address so
    // the notification is set to an address that is checked.
    $email = 'members@mayfirst.org';
    $assigneeContact = \Civi\Api4\Email::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('contact_id', 'contact_id.display_name')
      ->addWhere('contact_id.is_deleted', '=', FALSE)
      ->addWhere('email', '=', $email)
      ->execute()->first();
    $assigneeContact['display_name'] = $assigneeContact['contact_id.display_name'];

    $activityId = \Civi\Api4\Activity::create()
      ->setCheckPermissions(FALSE)
      ->addValue('activity_type_id:name', 'Membership_Dues_Reduction_Request')
      ->addValue('subject', 'Dues Reduction Request')
      ->addValue('activity_date_time', date('Y-m-d H:i:s'))
      ->addValue('details', $duesReductionRequest)
      ->addValue('status_id:name', 'Scheduled')
      ->addValue('target_contact_id', [$this->memberContactId])
      ->addValue('source_contact_id', $this->memberContactId)
      ->addValue('assignee_contact_id', [$assigneeContact['contact_id']])
      ->execute()->first()['id'];

    // Ugg! Api4 doesn't send the notification. We have to do that.
    $mailToContacts = [ $email => $assigneeContact ];
    $activity = new \CRM_Activity_DAO_Activity();
    $activity->id = $activityId;
    $activity->find();
    $activity->fetch();
    $sent = \CRM_Activity_BAO_Activity::sendToAssignee($activity, $mailToContacts);

  }
  private function notify() {
    // If it's a new membership request: 
    // 1. Send email to the Receive New Membership Notification group.
    // 2. Send email to the requester letting them know it's been received.
    if (!$this->getToken()) {
      // Get contactIds of the welcome group.
      $contacts = \Civi\Api4\GroupContact::get()
        ->setCheckPermissions(FALSE)
        ->addWhere('group_id:name', '=', 'Receive_New_Member_Notifications')
        ->addSelect('contact_id')
        ->execute();

      $recipientContactIds = [];
      foreach($contacts as $contact) {
        $id = $contact['contact_id'];
        $recipientContactIds[$id] = NULL;
      }
      if (count($recipientContactIds) > 0) {
        $emailer = new \Civi\Mayfirst\Email($this->membershipId);
        $details = \Civi\Api4\Contact::get()
          ->setCheckPermissions(FALSE)
          ->addSelect('display_name', 'Member_Info.Why_Join', 'Member_Info.How_Hear')
          ->addWhere('id', '=', $this->memberContactId)
          ->execute()->first();

        $emailer->prependContent = 
          "<p>New Membership: " . $details['display_name'] . '</p>' .
          "<p>Why Join: " . $details['Member_Info.Why_Join'] . '</p>';
          "<p>How Heard About May First: " . $details['Member_Info.How_Hear'] . '</p>';

        $emailer->workflowName = 'mayfirst_notify_new_member_signup';
        $emailer->recipientContactIds = $recipientContactIds;
        $emailer->send();
      }

      // Send an email to the person submitting the membership request
      // letting them know it's been received.
      $emailer = new \Civi\Mayfirst\Email($this->membershipId);
      $emailer->recipientContactIds = [ $this->relatedContactId => NULL ];
      $emailer->workflowName = 'mayfirst_request_received';
      $emailer->send();
    }
  }
}


?>

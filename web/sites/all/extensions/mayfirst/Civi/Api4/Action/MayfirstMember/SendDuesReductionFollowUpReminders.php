<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Send reminder to May First admins to confirm dues reduction.
 *
 * Check to see if any member has the Reduction Follow Up Date set in
 * the past. Send an email to May First admins letting them know that
 * they should follow up with them to ensure they still need that
 * reduction.
 *
 */
class SendDuesReductionFollowUpReminders extends \Civi\Api4\Generic\AbstractAction {

  /**
   * time
   *
   * A timestamp representing "now" if you want to test for future runs.
   */
  protected ?int $time = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    if (is_null($this->getTime())) {
      $now = time();
    }
    else {
      $now = $this->getTime();
    }
    // Check for members that we should be reminded to notify.
    $memberships = \Civi\Api4\Membership::get()
      ->addWhere('contact_id.Member_Info.Percent_Reduction', 'IS NOT EMPTY')
      ->addWhere('contact_id.Member_Info.Percent_Reduction', '>', 0)
      ->addWhere('contact_id.Member_Info.Reduction_Follow_Up_Date', 'IS NOT EMPTY')
      ->addWhere('contact_id.Member_Info.Reduction_Follow_Up_Date', '<', date('Y-m-d', $now))
      ->addSelect('id', 'contact_id')
      ->execute();

    $newFollowUpDate = date('Y-m-d', $now + 365 *  86400);
    if ($memberships->count() == 0) {
      $result[] = 'No members need a dues reduction follow up';
      return;
    }

    // Get contactIds of the welcome group.
    $contacts = \Civi\Api4\GroupContact::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('group_id:name', '=', 'Receive_New_Member_Notifications')
      ->addSelect('contact_id')
      ->execute();

    $recipientContactIds = [];
    foreach($contacts as $contact) {
      $id = $contact['contact_id'];
      $recipientContactIds[$id] = NULL;
    }
    if (count($recipientContactIds) == 0) {
      throw new \APIException("No contacts to send dues reduction follow up reminder.");
    }
    $send = 0;
    foreach ($memberships as $membership) {
      $membershipId = $membership['id'];
      $contactId = $membership['contact_id'];
      $emailer = new \Civi\Mayfirst\Email($membershipId);
      $emailer->workflowName = 'mayfirst_dues_reduction_follow_up_reminders';
      $emailer->recipientContactIds = $recipientContactIds;
      $emailer->send();
      $send++;
      \Civi\Api4\Contact::update()
        ->addValue('Member_Info.Reduction_Follow_Up_Date', $newFollowUpDate)
        ->addWhere('id', '=', $contactId)
        ->execute();
    }

    $result[] = "Send {$send} dues reduction follow up reminders.";
  }
}



?>

<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Send payment receipt.
 *
 * Send a payment receipt for the last contribution made to this member. 
 *
 */
class SendPaymentReceipt extends \Civi\Api4\Generic\AbstractAction {

  /**
   * membershipId 
   *
   * The membership id to send to.
   * @required
   */
  protected $membershipId;

  /**
   * contributionId 
   *
   * The contributionId for the contribution receipt.
   * @required
   */
  protected $contributionId;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $reminder = new \Civi\Mayfirst\Email($this->getMembershipId());
    $reminder->contributionId = $this->getContributionId();
    $reminder->workflowName = 'mayfirst_payment_receipt';
    $reminder->send();
    // Now update the receipt_date so we don't send two when automating
    // this process.
    \Civi\Api4\Contribution::update()
      ->addWhere('id', '=', $this->getContributionId())
      ->addValue('receipt_date', date("Y-m-d H:i:s"))
      ->execute();
    $result[] = 'Sent';
  }
}



?>

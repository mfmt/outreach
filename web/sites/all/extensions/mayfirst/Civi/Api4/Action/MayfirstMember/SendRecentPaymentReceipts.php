<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Send recent payment receipts.
 *
 * Send a payment receipt for for any membership payment made since the date
 * specified by since (or by default the last four hours) that does not already
 * have a receipt_date value. 
 *
 */
class SendRecentPaymentReceipts extends \Civi\Api4\Generic\AbstractAction {

  /**
   * since 
   *
   * A date string parseable by strtotime.
   */
  protected $since;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $since = $this->getSince();
    if (!$since) {
      $since = "4 hours ago";
    }

    $sinceTs = strtotime($since);
    if (!$sinceTs) {
      throw new \API_Exception("Failed to parse since string $since.");
    }
    $sinceDate = date('YmdHis', $sinceTs);

    // Find all membership payments made within the since date.
    $sql = "SELECT c.id AS contribution_id, mp.membership_id AS membership_id
      FROM 
        civicrm_contribution c JOIN
        civicrm_membership_payment mp ON c.id = mp.contribution_id
      WHERE
        c.contribution_status_id = 1 AND
        c.receipt_date IS NULL AND
        c.receive_date > %0";
    $params = [ 0 => [ $sinceDate, 'Timestamp' ]];
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    $count = 0;
    while ($dao->fetch()) {
      \Civi\Api4\MayfirstMember::SendPaymentReceipt()
        ->setContributionId($dao->contribution_id)
        ->setMembershipId($dao->membership_id)
        ->execute();
      $count++;
    } 
    $result[] = [ 'sent_count' => $count ];
  }
}



?>

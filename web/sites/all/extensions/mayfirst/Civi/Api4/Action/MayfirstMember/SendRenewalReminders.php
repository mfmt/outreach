<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Send renewal reminders.
 *
 * Send an email to everyone with an end date less then 2 weeks from
 * today (or from the time value) who hasn't been notified in the last
 * 2 weeks and has a membership new Current, Grace or Expired status. 
 *
 */
class SendRenewalReminders extends \Civi\Api4\Generic\AbstractAction {

  /**
   * time
   *
   * A timestamp representing "now" if you want to test for future runs.
   */
  protected $time = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $report = \Civi\Mayfirst\Email::sendRenewalReminders($this->getTime());
    $result[] = $report;
  }
}



?>

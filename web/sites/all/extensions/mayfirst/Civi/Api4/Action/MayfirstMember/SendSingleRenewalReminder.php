<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Send renewal reminders.
 *
 * Send a single renewal reminder for the given membership id. 
 *
 */
class SendSingleRenewalReminder extends \Civi\Api4\Generic\AbstractAction {

  /**
   * membershipId 
   *
   * The membership id to send to.
   * @required
   */
  protected $membershipId = 0;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $activityId = \Civi\Mayfirst\Email::sendSingleRenewalReminder($this->getMembershipId());
    $result[] = [ 'activity_id' => $activityId ];
  }
}



?>

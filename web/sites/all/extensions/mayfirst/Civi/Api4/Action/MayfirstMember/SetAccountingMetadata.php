<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Automatically update accounting metadata where it's missing.
 *
 * When a membershp payment is made, it's not clear when it should
 * be recorded as income. If someone pays for a membership today, but
 * they are paying ahead, then perhaps it should be considered income
 * for next year. Or, if they are behind, it should be considered income
 * for last year.
 *
 * This metadata is automatically recorded when a payment is first made
 * or is made manually, but it is not recorded for recurring payments.
 *
 * This job should run daily and look for recurring payments that have no
 * accounting meta data recorded and record that data.
 *
 */
class SetAccountingMetadata extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Contribution ID
   *
   * Only run for the given contributionId.
   */
  protected $contributionId = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {
    $report = [];
    $financial_type_member_dues = \CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'financial_type_id', 'Member Dues'); 
    $status_id_completed = \CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Completed'); 

    // First, attempt to create civicrm_membership_payment records for any
    // payments that are not properly recorded. This can happen with a one off
    // payment using our contribution page for people who some how can't get
    // our regular payment page to work.
    $params = [
      0 => [ $status_id_completed, 'Integer'],
      1 => [ $financial_type_member_dues, 'Integer'],
    ];

    $sql = "SELECT c.contact_id, id FROM civicrm_contribution c
        WHERE 
          financial_type_id = %1 AND 
          is_test != 1 AND 
          contribution_status_id = %0 AND
          id NOT IN 
            (SELECT contribution_id from civicrm_membership_payment)";

    $dao = \CRM_Core_DAO::executeQuery($sql, $params); 
    while ($dao->fetch()) {
      $contactId = $dao->contact_id;
      $contributionId = $dao->id;
      // Check if this contact id has an active membership.
      $membershipId = \Civi\Api4\Membership::get()
        ->addWhere('contact_id', '=', $contactId)
        ->addWhere('status_id:name', 'IN', ['Current', 'Grace'])
        ->addSelect('id')
        ->execute()->first()['id'] ?? NULL;
      if ($membershipId) {
        $paymentParams = [
          0 => [$membershipId, 'Integer'],
          1 => [$contributionId, 'Integer'],
        ];
        $sql = "INSERT INTO civicrm_membership_payment VALUES(NULL, %0, %1)";
        \CRM_Core_DAO::executeQuery($sql, $paymentParams); 
        \Civi::log()->debug("Entered membership payment for contact id {$contactId}, contribution id {$contributionId}, membership id {$membershipId}.");
      }
      else {
        \Civi::log()->debug("Can't find membersihp for contribution with contact id {$contactId}.");
      }
    }

    $sql = "SELECT DISTINCT c.id AS contribution_id, mt.name as membership_type,
      c.total_amount, c.receive_date
      FROM 
        civicrm_contribution c JOIN civicrm_membership_payment mp ON c.id = mp.contribution_id
        JOIN civicrm_membership m ON m.id = mp.membership_id
        JOIN civicrm_membership_type mt ON m.membership_type_id = mt.id
      WHERE 
        c.currency = 'USD' AND 
        c.contribution_status_id = %0 AND 
        c.financial_type_id = %1 AND 
        c.id NOT IN (SELECT contribution_id FROM civicrm_mayfirst_contribution)";
    if ($this->getContributionId()) {
      $params[2] = [ $this->getContributionId(), 'Integer' ]; 
      $sql .= " AND c.id = %2";
      $report['contribution_id'] = $this->getContributionId();
    }
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    $count = 0;
    while ($dao->fetch()) {
      $contributionId = $dao->contribution_id;
      $membershipType = $dao->membership_type;

      // This is a hopeful guess - that the date we received the contribution is more or less the
      // date it should be applied to.
      $paymentStartDate = $dao->receive_date;
      $totalAmount = $dao->total_amount;
      $numTerms = 1;
      $endDate = \Civi\Mayfirst\Utils::calculateEndDate($paymentStartDate, $membershipType, $numTerms);
      $deferredAmount = \Civi\Mayfirst\Utils::getDeferredAmount($paymentStartDate, $endDate, $totalAmount);
      // echo "Start: $paymentStartDate, End: $endDate, deferred: $deferredAmount, id: $contributionId\n";
      \Civi\Mayfirst\Utils::setAccountingMetadata($contributionId, $paymentStartDate, $deferredAmount);
      $count++;
    }
    $report['count'] = $count;
    $result[] = $report;
  }
}

?>

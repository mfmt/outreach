<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Submit a check, cash or EFT payment towards a membership.
 *
 */
class SubmitManualPayment extends \Civi\Api4\Generic\AbstractAction {

  /**
   * memberhipId 
   *
   * @var int 
   * @required
   */
  protected $membershipId;

  /**
   * contactId 
   *
   * @var int 
   * @required
   */
  protected $contactId;

  /**
   * totalAmount
   *
   * @var mixed 
   * @required
   */
  protected $totalAmount;

  /**
   * currency
   *
   * @var string
   * @required
   */
  protected $currency;

  /**
   * numTerms 
   *
   * @var int
   * @required
   */
  protected $numTerms = 1;

  /**
   *
   * receiveDate 
   *
   * The date the payment was received.
   *
   * @var string
   * @required
   */
  protected $receiveDate;

  /**
   *
   * endDate 
   *
   * The end date to set the membership to after applying
   * the payment.
   *
   * @var string
   * @required
   */
  protected $endDate;

  /**
   *
   * paymentInstrumentId
   * @var int
   * @required
   *
   */
  protected $paymentInstrumentId;

  /**
   *
   * trxnId 
   * @var string
   */
  protected $trxnId;

  /**
   *
   * checkNumber 
   *
   * @var int 
   */
  protected $checkNumber;

  /**
   *
   * contributionId 
   *
   * If present, update the contribution rather than saving a new one.
   *
   * @var int 
   */
  protected $contributionId;

  public function _run(\Civi\Api4\Generic\Result $result) {

    $totalAmount = floatval($this->getTotalAmount());
    if ($totalAmount < 1) {
      throw new \API_Exception(E::ts("Please submit a contribution that is greater than 1."));
    }
    // We need the membership type to get the priceFieldValue and the original end_date.
    $membership = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $this->getMembershipId())
      ->addSelect('membership_type_id', 'end_date')
      ->execute()->first();
    $membershipTypeId = $membership['membership_type_id'];
    $paymentStartDate = $membership['end_date'];
    $invoiceId = sha1(rand(100000000, getrandmax()));

    if ($this->getContributionId()) {
      $contributionId = $this->getContributionId();
      // If they are passing a contributionId, we need to check the amount. We
      // may be making a payment that is of a different price than the original
      // contribution. If that's the case we have to update the contribution
      // and line items to match what is actually being paid.
      $savedContribution = \Civi\Api4\Contribution::get()
        ->addWhere('id', '=', $contributionId)
        ->addSelect('total_amount', 'currency')
        ->execute()->first();
      if ($this->getCurrency() != $savedContribution['currency']) {
        throw new \API_Exception(E::ts("The currency of the saved contribution does not match the currency of the updated contribution. If this is intentional, please delete the saved contribution and create a new one."));
      }
      if ($totalAmount != $savedContribution['total_amount']) {
        // Let's change the amount.
        $new = \Civi\Api4\Contribution::update()
          ->addValue('total_amount', $totalAmount)
          ->addWhere('id', '=', $contributionId)
          ->execute();

        // Don't forget the line item table.
        \Civi\Api4\LineItem::update()
          ->setCheckPermissions(FALSE)
          ->addValue('unit_price', $totalAmount)
          ->addValue('line_total', $totalAmount)
          ->addWhere('contribution_id', '=', $contributionId)
          ->execute();
      }
    }
    else {
      // Use the Orders API
      // (https://docs.civicrm.org/dev/en/latest/financial/orderAPI/) to create
      // the Order, which will generate update the Membership and generate
      // pending contribution, line items and everything.
      $priceFieldValueId = \Civi\Api4\PriceFieldValue::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('id')
        ->addWhere('membership_type_id', '=', $membershipTypeId)
        ->execute()->first()['id'];

      $orderParams = [
        "contact_id" => $this->getContactId(),
        "total_amount" => $totalAmount,
        "financial_type_id" => "Member Dues",
        "receive_date" => $this->getReceiveDate(),
        "contribution_recur_id" => NULL,
        "is_test" => FALSE,
        "source" => "Member office payment",
        "payment_instrument_id" => $this->getPaymentInstrumentId(),
        "invoice_id" => $invoiceId,
        "currency" => $this->getCurrency(),
        "check_number" => $this->getCheckNumber(),
        "line_items" => [
          [
            "params" => [
              "id" => $this->getMembershipId(),
              "membership_type_id" => $membershipTypeId,
              "contact_id" => $this->getContactId(),
            ],
            "line_item" => [
              [
                "entity_table" =>"civicrm_membership",
                // Special generic price field for memberships.
                "price_field_id" => "2", 
                "price_field_value_id" => $priceFieldValueId, 
                "qty" => "1",
                "unit_price" => $totalAmount,
                "line_total" => $totalAmount,
              ]
            ]
          ]
        ]
      ];

      $orderResult = civicrm_api3('Order', 'create', $orderParams);
      $contributionId = $orderResult['id'];
    }

    // Figure out if any amount of this payment should be deferred to next
    // year. This happens if you pay for a year of membership in, say,
    // December, then most of the payment should be deferred for the next
    // calendar year.
    $deferredAmount = \Civi\Mayfirst\Utils::getDeferredAmount($paymentStartDate, $this->getEndDate(), $totalAmount);
    // Save the payment start date and deferred amount data for this contribution.
    \Civi\Mayfirst\Utils::setAccountingMetadata($contributionId, $paymentStartDate, $deferredAmount);

    $component = 'membership';

    // Create the payment which should handle all the status changes. 
    $paymentResult = civicrm_api3('Payment', 'create', [
      'fee_amount' => 0,
      'total_amount' => $totalAmount,
      'payment_instrument_id' => $this->getPaymentInstrumentId(),
      'trxn_id' => $this->getTrxnId(),
      'trxn_date' => $this->getReceiveDate(),
      'contribution_id' => $contributionId,
      'is_send_contribution_notification' => FALSE,
    ]);

    // Left untouched, the Orders API should advance the membershp by
    // exactly one term. But we may want it advanced differently, so
    // we end by manually updating the end date.
    \Civi\Api4\Membership::update()
      ->addValue('end_date', $this->getEndDate())
      ->addWhere('id', '=', $this->getMembershipId())
      ->execute();

    $result[] = [
      'contribution_id' => $contributionId,
    ];
  }
}

?>

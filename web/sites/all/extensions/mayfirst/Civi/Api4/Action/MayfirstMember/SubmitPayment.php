<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Submit a stripe payment intent/method and token OR handle a 
 * PayPal IPN notification. Based on verification, create a payment
 * and renew a membership.
 *
 * @method string getToken
 * @method $this setToken(string $token)
 * @method string getPaymentMethodOrIntent
 * @method $this setPaymentMethodOrIntent(string $paymentMethodOrIntent)
 * @method int getPaymentProcessorId
 * @method $this setPaymentProcessorId(int $paymentProcessorId)
 * @method int getTestMode
 * @method $this setTestMode(int $testMode)
 * @method string getAutomaticRenewal
 * @method $this setAutomaticRenewal(int $automaticRenewal)
 *
 */
class SubmitPayment extends \Civi\Api4\Generic\AbstractAction {

  /**
   * The token (hash:memberhip_id). 
   *
   * @var string
   * @required
   */
  protected $token = NULL;

  /**
   * The paymentMethodOrIntent 
   *
   * This is only required on Stripe submissions.
   * @var string
   */
  protected $paymentMethodOrIntent = NULL;

  /**
   * paymentProcessorId 
   *
   * @var int 
   * @required
   */
  protected $paymentProcessorId;

  /**
   * testMode 
   *
   * @var int 
   */
  protected $testMode = 0;

  /**
   * automaticRenewal
   *
   * @var int 
   */
  protected $automaticRenewal = 0;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    // These critical variables will be populated along the way.
    $contributionId = NULL;
    $contributionRecurId = NULL;
    $trxnId = NULL;
    $contactId = NULL;
    $amount = NULL;
    $currency = NULL;
    $frequencyUnit = NULL;
    $membershipTypeId = NULL;
    // Credit Card is 1.
    $paymentInstrumentId = 1;

    // Make sure the token is legit.
    $tokenPieces = explode(':', $this->getToken());
    $membershipId = $tokenPieces[1];
    if (!\Civi\Mayfirst\Utils::validToken($this->getToken())) {
      throw new \API_Exception(E::ts("Invalid token given when submitting a membership renewal."));
    }

    // Make sure we don't already have a recurring contribution.
    if ($this->getAutomaticRenewal() == 1 && \Civi\Mayfirst\Utils::membershipAutoRenewInProgress($membershipId)) {
      // Track the error code so we can handle it differently - this is a message that should be sent back to the user.
      $errCode = 1;
      throw new \CRM_Core_Exception(E::ts("There is already a recurring contribution set for this membership that is in progress."), $errCode);
    }

    // Gather details about this membership.
    $details = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect(
        'membership_type_id',
        'membership_type_id:name',
        'Membership_Details.Currency:name', 
        'Membership_Details.Renewal_Amount',
        'contact_id',
        'end_date',
      )
      ->addWhere('id', '=', $membershipId)
      ->execute()->first();

    $contactId = $details['contact_id'];
    $membershipTypeId = $details['membership_type_id'];
    $membershipType = $details['membership_type_id:name'];
    $amount = $details['Membership_Details.Renewal_Amount'];
    $currency = $details['Membership_Details.Currency:name'];
    $paymentStartDate = $details['end_date'];
 
    if ($currency != 'USD') {
      throw new \CRM_Core_Exception(E::ts("We can only process payments in USD."));
    }
    if ($membershipType == 'Annual') {
      $frequencyUnit = 'year';
    }
    else {
      $frequencyUnit = 'month';
    }

    // Create the payment processor - either Stripe or Paypal.
    $paymentProcessor = \Civi\Api4\PaymentProcessor::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('id', '=', $this->getPaymentProcessorId())
      ->execute()->first();
    $processorClass = 'CRM_Core_' . $paymentProcessor['class_name'];
    $pp = new $processorClass(NULL, $paymentProcessor);

    // By default we direct the user to the contribution page after they
    // are sent to paypal/stripe. But we don't have a contribution page
    // so override that setting here:
    $pp->setSuccessUrl(CIVICRM_UF_BASEURL . 'civicrm/public/thanks');
    // Since we redirect the user away to complete their transaction,
    // we have to set the invoiceId and send that along with the user to
    $invoiceId = sha1(rand(100000000, getrandmax()));

    if ($this->getAutomaticRenewal() == 1) {
      // If we have automatic renewal turned on, we have to create the
      // recurring contribution before we process the payment so we can send
      // the contribution and recurring contribution ids along with the request
      // to do the payment.
      
      // Create the recurring contribution.
      $contributionRecur = \Civi\Api4\ContributionRecur::create()
        ->setCheckPermissions(FALSE)
        ->addValue('contact_id', $contactId)
        ->addValue('amount', $amount)
        ->addValue('currency', $currency)
        ->addValue('frequency_unit:name', $frequencyUnit)
        ->addValue('start_date', date('Y-m-d'))
        ->addValue('frequency_interval', 1)
        ->addValue('create_date', date('Y-m-d'))
        ->addValue('payment_processor_id', $this->getPaymentProcessorId())
        ->addValue('contribution_status_id:name', 'Pending')
        ->addValue('financial_type_id:name', 'Member Dues')
        ->addValue('payment_instrument_id:name', 'Credit Card')
        ->addValue('invoice_id', $invoiceId)
        ->addValue('is_test', $this->getTestMode())
        ->addValue('is_email_receipt', FALSE)
        ->execute()->first();
      
      $contributionRecurId = $contributionRecur["id"];
    }

    // Next, use the Orders API
    // (https://docs.civicrm.org/dev/en/latest/financial/orderAPI/) to create
    // the Order, which will generate or update the Membership and generate
    // pending contributioon, line items and everything.
    $priceFieldValueId = \Civi\Api4\PriceFieldValue::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('id')
      ->addWhere('membership_type_id', '=', $membershipTypeId)
      ->execute()->first()['id'];

    $orderParams = [
      "contact_id" => $contactId,
      "total_amount" => $amount,
      "financial_type_id" => "Member Dues",
      "receive_date" => date('YmdHis'),
      "contribution_recur_id" => $contributionRecurId,
      "is_test" => $this->getTestMode(),
      "source" => "Member self payment",
      "payment_instrument_id" => $paymentInstrumentId,
      "invoice_id" => $invoiceId,
      "currency" => $currency,
      "line_items" => [
        [
          "params" => [
            "id" => $membershipId,
            "membership_type_id" => $membershipTypeId,
            "contact_id" => $contactId,
          ],
          "line_item" => [
            [
              "entity_table" =>"civicrm_membership",
              // Special generic price field for memberships.
              "price_field_id" => "2", 
              "price_field_value_id" => $priceFieldValueId, 
              "qty" => "1",
              "unit_price" => $amount,
              "line_total" => $amount,
            ]
          ]
        ]
      ]
    ];

    $orderResult = civicrm_api3('Order', 'create', $orderParams);
    $contributionId = $orderResult['id'];

    // Prepare the parameters for processing the credit card payment.
    $paymentParams = [
      'payment_processor_id' => $this->getPaymentProcessorId(),
      'amount' => $amount,
      'email' => NULL,
      'contactID' => $contactId,
      'description' => 'May First Membership Renewal',
      'currencyID' => $currency,
      // Avoid missing key php errors by adding these un-needed parameters.
      'qfKey' => NULL,
      'entryURL' => 'http://civicrm.localhost/civicrm/test?foo',
      'query' => NULL,
      'invoiceID' => $invoiceId,
      'contributionID' => $contributionId,
      'additional_participants' => [],
    ];

    if ($this->getAutomaticRenewal() == 1) {
      // For both Pay Pal and Stripe, we have to add extra paramenters
      // for recurring contributions.
      $paymentParams['is_recur'] = 1;
      $paymentParams['contributionRecurID'] = $contributionRecurId; 
      $paymentParams['frequency_unit'] = $frequencyUnit; 
      $paymentParams['frequency_interval'] = 1;
      $paymentParams['installments'] = NULL;
    }

    // Figure out if any amount of this payment should be deferred to next
    // year. This happens if you pay for a year of membership in, say,
    // December. Most of the payment should be deferred for the next calendar
    // year.
    $numTerms = 1;
    $endDate = \Civi\Mayfirst\Utils::calculateEndDate($paymentStartDate, $membershipType, $numTerms);
    $deferredAmount = \Civi\Mayfirst\Utils::getDeferredAmount($paymentStartDate, $endDate, $amount);
    // Save the payment start date and deferred amount data for this contribution.
    \Civi\Mayfirst\Utils::setAccountingMetadata($contributionId, $paymentStartDate, $deferredAmount);

    $component = 'membership';
    // In Pay pal mode, this code results in the user's browser being redirected to
    // PayPal and civiExit being called. That's good. But, when we hit this spot in
    // our unit test, our unit test craps out because it doesn't expect to die. So
    // we wrap that in an exception to prevent it from dying.
    try {
      $ppResults = $pp->doPayment($paymentParams, $component);
      // Unless testing, the user is redirected here. Buh bye.
    }
    catch (\CRM_Core_Exception_PrematureExitException $e) {
      $e;
      // These return values are only used by our unit tests.
      $result[] = [
        'invoice_id' => $invoiceId,
        'contribution_id' => $contributionId,
        'contribution_recur_id' => $contributionRecurId,
      ];
      return;
    }

    // We are only here in test mode.
    $result[] = [
      'trxn_id' => $trxnId,
      'contribution_id' => $contributionId,
      'contribution_recur_id' => $contributionRecurId,
    ];
  }
}

?>

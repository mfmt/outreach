<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 *
 * Create a pending contribution via the Orders API and then attach a
 * receipt to it. 
 *
 * We expect to only process ajax requests in which the file is passed in via
 * $_FILES and the token is passed in via $_GET.
 *
 * We require both a file and a valid token.
 *
 */
class UploadReceipt extends \Civi\Api4\Generic\AbstractAction {

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    if (empty($_FILES['file'])) {
      throw new \API_Exception(E::ts('No file was uploaded. Please select a file to upload first.'));
    }
    $token = $_GET['token'] ?? NULL;
    if (empty($token)) {
      throw new \API_Exception(E::ts('Your token was not included with your file upload.'));
    }

    if (!\Civi\Mayfirst\Utils::validToken($token)) {
      throw new \API_Exception(E::ts('Your token is invalid.'));
    }

    // We know the name of the custom field, but we need the id because we depend on 
    // apiv3 to add the attachment.
    $customFieldName = 'Transfer_Receipt';
    // We depend on apiv3 for the attachment, so we need the api v3 name.
    $customFieldId = \Civi\Api4\CustomField::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('id')
      ->addWhere('name', '=', $customFieldName)
      ->execute()->first()['id'];

    if (empty($customFieldId)) {
      throw new \API_Exception(E::ts('Failed to lookup custom field id.'));
    }

    $tokenPieces = explode(':', $token);
    $membershipId = $tokenPieces[1];

    $facturaRequested = $_GET['factura'] ?? NULL;
    $testMode = 0;
    if (!empty($_GET['testMode'])) {
      $testMode = $_GET['testMode'];
    }
    $membershipDetails = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('id', '=', $membershipId)
      ->addSelect('contact_id', 'Membership_Details.Renewal_Amount', 'Membership_Details.Currency:name', 'membership_type_id')
      ->execute()->first();

    $contactId = $membershipDetails['contact_id'];
    $currency = $membershipDetails['Membership_Details.Currency:name'];
    $membershipTypeId = $membershipDetails['membership_type_id'];
    $renewalAmountLocalized = $membershipDetails['Membership_Details.Renewal_Amount'];
    if ($currency == 'MXN') {
      // Convert to pesos.
      $renewalAmountLocalized = \Civi\Api4\Dues::Exchange()
        ->setCheckPermissions(FALSE)
        ->setMembershipId($membershipId)
        ->execute()->first()['amount'];
    }
    $invoiceId = sha1(rand(100000000, getrandmax()));

    // Use the Orders API
    // (https://docs.civicrm.org/dev/en/latest/financial/orderAPI/) to create
    // the Order, which will generate update the Membership and generate
    // pending contribution, line items and everything.
    $priceFieldValueId = \Civi\Api4\PriceFieldValue::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('id')
      ->addWhere('membership_type_id', '=', $membershipTypeId)
      ->execute()->first()['id'];

    $paymentInstrumentId = \Civi\Api4\OptionValue::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('option_group_id:name', '=', 'payment_instrument')
      ->addWhere('name', '=', 'EFT')
      ->addSelect('value')
      ->execute()->first()['value'];

    $orderParams = [
      "contact_id" => $contactId,
      "total_amount" => floatval($renewalAmountLocalized),
      "financial_type_id" => "Member Dues",
      "receive_date" => date('Y-m-d H:i:s'),
      "contribution_recur_id" => NULL,
      "is_test" => FALSE,
      "source" => "Member office payment",
      "payment_instrument_id" => $paymentInstrumentId,
      "invoice_id" => $invoiceId,
      "currency" => $currency,
      "line_items" => [
        [
          "params" => [
            "id" => $membershipId,
            "membership_type_id" => $membershipTypeId,
            "contact_id" => $contactId,
          ],
          "line_item" => [
            [
              "entity_table" =>"civicrm_membership",
              // Special generic price field for memberships.
              "price_field_id" => "2", 
              "price_field_value_id" => $priceFieldValueId, 
              "qty" => "1",
              "unit_price" => floatval($renewalAmountLocalized),
              "line_total" => floatval($renewalAmountLocalized),
            ]
          ]
        ]
      ]
    ];

    $orderResult = civicrm_api3('Order', 'create', $orderParams);
    $contributionId = $orderResult['id'];

    // Update the factura field.
    \Civi\Api4\Contribution::update()
      ->setCheckPermissions(FALSE)
      ->addValue('Membership_Dues_Info.Factura_Requested', $facturaRequested)
      ->addWhere('id', '=', $contributionId)
      ->execute();

    // Upload the attachment.
    $attachmentParams = [
      'field_name' => 'custom_' . $customFieldId,
      'entity_id' => $contributionId,
      'mime_type' => $_FILES['file']['type'],
      'name' => $_FILES['file']['name'],
      'options' => [
        'move-file' => $_FILES['file']['tmp_name'],
      ],
    ];
    $file = civicrm_api3('Attachment', 'create', $attachmentParams);

    // Now update the custom field with the uploaded file.
    \Civi\Api4\Contribution::update()
      ->setCheckPermissions(FALSE)
      ->addvalue('id', $contributionId)
      ->addValue($customFieldName, $file['id'])
      ->execute();

    // And lastly notify someone.
    // Get contactIds of the welcome group.
    $contacts = \Civi\Api4\GroupContact::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('group_id:name', '=', 'Receive_Uploaded_Receipt_Notifications')
      ->addSelect('contact_id')
      ->execute();

    $recipientContactIds = [];
    foreach($contacts as $contact) {
      $id = $contact['contact_id'];
      $recipientContactIds[$id] = NULL;
    }
    if (count($recipientContactIds) > 0) {
      $emailer = new \Civi\Mayfirst\Email($membershipId);
      $emailer->workflowName = 'mayfirst_notify_uploaded_receipt';
      $emailer->recipientContactIds = $recipientContactIds;
      $emailer->send();
    }

    $result[] = [ 'id' => $contributionId ];
  }
}

?>

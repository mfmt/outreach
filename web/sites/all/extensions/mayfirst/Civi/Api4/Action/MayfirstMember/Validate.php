<?php

namespace Civi\Api4\Action\MayfirstMember;
use CRM_Mayfirst_ExtensionUtil as E;

use API_Exception;

/**
 *
 * Validate membership data
 *
 * Provided a set of proposed fields for saving a membership and the current
 * page in the form, throw an error if there are any validation errors or
 * return the next page that should be displayed. 
 *
 * @method array getFields() 
 * @method setFields(array fields) 
 * @method string getPage() 
 * @method setPage(string page)
 * @method string getToken() 
 * @method setToken(string token) 
 *
 */
class Validate extends \Civi\Api4\Generic\AbstractAction {

  /**
   * fields 
   *
   * @array
   */
  protected $fields;

  /**
   * page
   *
   * @str
   */
  protected $page;
  public const PAGE_WELCOME = 'welcome';
  public const PAGE_ABOUT = 'about';
  public const PAGE_BENEFITS = 'benefits';
  public const PAGE_CURRENCY = 'currency';
  public const PAGE_INCOME = 'income';
  public const PAGE_SERVICES = 'services';
  public const PAGE_EXTRA = 'extra';
  public const PAGE_CONFIRM = 'confirm';
  public const ALLOWED_PAGES = [
    self::PAGE_WELCOME,
    self::PAGE_ABOUT,
    self::PAGE_BENEFITS,
    self::PAGE_CURRENCY,
    self::PAGE_INCOME,
    self::PAGE_SERVICES,
    self::PAGE_EXTRA,
    self::PAGE_CONFIRM,
  ];

  /**
   *
   * token
   *
   * @string
   */
  protected $token;

  /**
   * nextPage
   *
   * If the fields validate, $nextPage holds the value
   * for the next page in the form.
   *
   * @str
   */
  private $nextPage;

  public function _run(\Civi\Api4\Generic\Result $result) {
    \CRM_Core_Config::singleton()->userPermissionTemp = new \CRM_Core_Permission_Temp();
    \CRM_Core_Config::singleton()->userPermissionTemp->grant('view debug output');

    if ($this->getToken()) {
      if (!\Civi\Mayfirst\Utils::validToken($this->getToken())) {
        throw new \API_Exception(E::ts("Your token does not seem valid."));
      }
    }
    if (!in_array($this->getPage(), self::ALLOWED_PAGES)) {
      throw new API_Exception(E::ts("Please pass a valid page."));
    }

    $this->translateFields();
    switch ($this->getPage()) {
      case 'welcome':
        $this->validateWelcome();
        break;
      case 'about':
        $this->validateAbout();
        break;
      case 'benefits':
        $this->validateBenefits();
        break;
      case 'currency':
        $this->validateCurrency();
        break;
      case 'income':
        $this->validateIncome();
        break;
      case 'services':
        $this->validateServices();
        break;
      case 'extra':
        $this->validateExtra();
        break;
      case 'confirm':
        $this->validateAbout();
        $this->validateBenefits();
        $this->validateCurrency();
        $this->validateIncome();
        $this->validateServices();
        $this->validateExtra();
        $this->nextPage = 'done';
        break;

    }
    $result[] = ['nextPage' => $this->nextPage];
  }

  /**
   * Translate fields from angularjs syntax to CiviCRM syntax
   *
   */
  private function translateFields() {
    $fields = $this->getFields();
    // Remove any extra white space.
    foreach ($fields as $k => $v) {
      if (is_string($v)) {
        $fields[$k] = trim($v);
      }
    }
    $fields['country_id'] = $fields['country']['id'];
    $fields['state_province_id'] = $fields['state_province']['id'];
    unset($fields['country']);
    unset($fields['state_province']);
    // Hosting Benefits should be a 0 or a 1 and we want an integer.
    if (!array_key_exists('hosting_benefits', $fields)) {
      $fields['hosting_benefits'] = 0;
    }
    else {
      $fields['hosting_benefits'] = intval($fields['hosting_benefits']);
    }
    // Normalize the domain name - in case people enter an URL.
    if (array_key_exists('domain_name', $fields)) {
      $fields['domain_name'] = preg_replace('#^https?://#', '', $fields['domain_name']);
      // Get rid of a trailing slash.
      $fields['domain_name'] = preg_replace('#/$#', '', $fields['domain_name']);
    }

    $this->setFields($fields);
  }

  private function validateWelcome() {
    // Easy. Nothing to validate.
    $this->nextPage = 'about';
  }

  private function validateAbout() {
    $this->nextPage = 'benefits';
    $required = [
      'city' => E::ts("City"),
      'contact_type' => E::ts("Membership Type"),
      'country_id' => E::ts("Country"),
      'state_province_id' => E::ts("State or Province"),
      'why_join' => E::ts("About yourself"),
    ];

    // When no token is present, it's a new membership and name and
    // email are required. Otherwise, related contacts are handled
    // separately.
    if (!$this->getToken()) {
      $required['first_name'] = E::ts("First name");
      $required['email_address'] = E::ts("Email address");
      $required['preferred_language'] = E::ts("Preferred Language");
    }

    $this->checkRequired($required);
    if (!in_array($this->getFields()['contact_type'], [ 'Individual', 'Organization' ])) {
      throw new API_Exception(E::ts("Membership type must be Individual or Organization."));
    }

    if ($this->getToken()) {
      // The remaining validation checks are only relevant for new submissions. 
      return;
    }
    $languages = [ 'en_US', 'es_ES' ];
    if (!in_array($this->getFields()['preferred_language'], $languages)) {
      throw new API_Exception(E::ts("Invalid language."));
    }

    // Ensure nobody with the same name has an existing membership.
    $dupe = NULL;
    if ($this->getFields()['contact_type'] == 'Organization') {
      $required[] = 'organization_name';
      $dupe = \Civi\Api4\Membership::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('contact_id', 'status_id:name')
        ->addWhere('contact_id.organization_name', '=', $this->getFields()['organization_name'])
        ->addWhere('contact_id.is_deleted', '=', 0)
        ->execute();
      $dupeStatusKey = 'status_id:name';
    }
    else {
      $dupe = \Civi\Api4\Contact::get()
        ->setCheckPermissions(FALSE)
        ->setJoin([ 
          [ 'Email AS email', FALSE ],
          [ 'Phone AS phone', FALSE ], 
          [ 'Membership AS membership', TRUE ] 
        ])
        ->addSelect('id', 'membership.status_id:name')
        ->setGroupBy([ 'id' ])
        ->addWhere('first_name', '=', $this->getFields()['first_name'])
        ->addWhere('last_name', '=', $this->getFields()['last_name'])
        ->addWhere('is_deleted', '=', 0)
        ->addClause('OR', ['email.email', '=', $this->getFields()['email_address']], [ 'phone.phone_numeric', '=', $this->getFields()['mobile_phone']])
        ->execute();
        $dupeStatusKey = 'membership.status_id:name';
    }
    if ($dupe->count() > 0) {
      $existingMembershipStatus = $dupe->first()[$dupeStatusKey];
      if ($existingMembershipStatus == 'Requested') {
          $msg = E::ts("We have already received your membership request. Please be patient while we process the request!");
      }
      elseif ($existingMembershipStatus == 'Current') {
        $msg = E::ts("Sorry, your name and/or contact details match an existing member. Or are you already a member?!?");
      }
      elseif ($existingMembershipStatus == 'Expired' || $existingMembershipStatus == 'Cancelled') {
        $msg = E::ts("Sorry, your name and/or contact details match an existing contact. If you are re-joining after your membership expired, please email info@mayfirst.org to get started again.");
      }
      else {
        $msg = E::ts("Sorry, we hit a problem! We found a matching membership in an unexpected status. Please email info@mayfirst.org to get it sorted out.");
      }
      throw new \API_Exception($msg);
    }
  }

  private function validateBenefits() {
    $benefits = $this->getFields()['hosting_benefits'];

    if ($benefits !== 0 && $benefits !== 1) {
      throw new API_Exception(E::ts("Invalid selection for hosting benefits."));
    }
    $this->nextPage = 'currency';
  }

  private function validateCurrency() {
    $currencies = [ 'USD', 'MXN' ];
   
    $membershipTypes = [ 'Monthly', 'Annual' ];

    if (!in_array($this->getFields()['currency'], $currencies)) {
      throw new API_Exception(E::ts("Invalid currency."));
    }
    
    if (!in_array($this->getFields()['membership_type'], $membershipTypes)) {
      throw new API_Exception(E::ts("Invalid membership payment frequency."));
    }
    if ($this->getFields()['hosting_benefits']) {
     $this->nextPage = 'income';
    }
    else {
     $this->nextPage = 'confirm';
    }
  }

  private function validateIncome() {
    $income = $this->getFields()['income'] ?? 0;

    if ($income < 1 && $income > 4) {
      throw new API_Exception(E::ts("Invalid selection for income, should be a number between 1 and 4."));
    }
    $this->nextPage = 'services';
  }
  private function validateServices() {
    $plan = $this->getFields()['plan'] ?? 0;
    $income = $this->getFields()['income'] ?? 0;

    if ($plan < 1 && $plan > 4) {
      throw new API_Exception(E::ts("Invalid selection for plan, should be a number between 1 and 4."));
    }
    if ($plan < $income) {
      throw new API_Exception(E::ts("Your plan cannot be lower then your income range."));
    }

    $domainName = trim($this->getFields()['domain_name'] ?? '');
    if ($domainName) {
      // Thanks to https://www.geeksforgeeks.org/how-to-validate-a-domain-name-using-regular-expression/
      // We allow up to six parts: x.x.x.x.x.x
      if (!preg_match('/^[A-Za-z0-9-]{1,63}\.([A-Za-z0-9-]{1,63}\.){0,4}[A-Za-z]{2,6}$/', $domainName )) {
        throw new API_Exception(E::ts("That does not seem like a valid domain name (only 6 domain parts are allowed): %1", [1 => $domainName]));
      }
    }
    $extra_storage = $this->getFields()['extra_storage'] ?? 0;

    if ($extra_storage) {
      if ($extra_storage < 0) {
        throw new API_Exception(E::ts("Extra storage cannot be a negative number."));
      }
      if (floor($extra_storage) != $extra_storage)  {
        throw new API_Exception(E::ts("Please choose a whole number for extra storage."));
      }
    }
    if (floor($extra_storage) != $extra_storage) {
      throw new \API_Exception(E::ts("Please enter a whole number for extra storage. Decimals are not allowed."));
    }
    // When editing your services, you can go up, but you can't go down (this prevents people from lowering
    // their disk quota to an amount that is lower then their current usage)
    $token = $this->getToken();
    if ($token) {
      $tokenPieces = explode(':', $token);
      $membershipId = $tokenPieces[1];
      $membership = \Civi\Api4\Membership::get()
        ->setCheckPermissions(FALSE)
        ->addWhere('id', '=', $membershipId)
        ->addSelect('Membership_Details.Plan', 'Membership_Details.Extra_Storage')
        ->execute()->first();
      if (intval($membership['Membership_Details.Plan']) > intval($plan)) {
        throw new API_Exception(E::ts("You are welcome to lower your plan! But, we cannot handle that without human intervention. Please contact support to complete that process:."));
      }
      if (intval($membership['Membership_Details.Extra_Storage']) > intval($extra_storage)) {
        throw new API_Exception(E::ts("You are welcome to lower your extra storage! But, we cannot handle that without human intervention. Please contact support to complete that process:."));
      }
    }
    if ($this->getFields()['additional_resources'] ?? '') {
      $this->nextPage = 'extra';
    }
    else {
      $this->nextPage = 'confirm';
    }
  }

  private function validateExtra() {
    $virtualPrivateServers = $this->getFields()['virtual_private_servers'] ?? 0;
    $cpu = $this->getFields()['cpu'] ?? 0;
    $ram = $this->getFields()['ram'] ?? 0;
    $ssd = $this->getFields()['ssd'] ?? 0;

    if (!$this->getFields()['hosting_benefits'] ?? 0) {
      if ($virtualPrivateServers || $cpu || $ram || $ssd) {
        throw new API_Exception(E::ts("Virtual private servers and related settings are only available if you select hosting benefits."));
      }
    }

    $nonNegativeInts = [
      'virtualPrivateServers' => E::ts("Virtual Private Servers"), 
      'cpu' => E::ts("CPU"),
      'ram' => E::ts("RAM"), 
      'ssd' => E::ts("Solid state drive")
    ];
    foreach ($nonNegativeInts as $field) {
      if (isset($$field)) {
        if ($$field < 0) {
          throw new API_Exception(E::ts("The value for %1 should be a number.", [ 1 => $nonNegativeInts[$field] ]));
        }
        if (floor($$field) != $$field) {
          throw new API_Exception(E::ts("The value for %1 should be a whole number, not a fraction.", [ 1 => $nonNegativeInts[$field] ]));
        }
      }
    }
    if ($virtualPrivateServers && !is_numeric($virtualPrivateServers)) {
      throw new API_Exception(E::ts("Virtual private servers should be a number."));
    }
    if ($cpu && !is_numeric($cpu)) {
      throw new API_Exception(E::ts("CPU should be a number."));
    }
    if ($ram && !is_numeric($ram)) {
      throw new API_Exception(E::ts("RAM should be a number."));
    }
    if ($ssd && !is_numeric($ssd)) {
      throw new API_Exception(E::ts("SSD should be a number."));
    }

    if (intval($virtualPrivateServers) > intval($cpu)) {
      throw new API_Exception(E::ts("You must have at least as many CPUs as Virtual Private Servers."));
    }
    if (intval($virtualPrivateServers) > intval($ram)) {
      throw new API_Exception(E::ts("You must have at least 1 GB of RAM per virtual private server."));
    }
    if ($virtualPrivateServers == 0) {
      if (intval($ssd) > 0 || intval($ram) > 0 || intval($cpu) > 0) {
        throw new API_Exception(E::ts("Please add at least one Virtual Private Server before adding CPU, RAM or SSD."));
      }
    }
    
    if (intval($ssd) % 5) {
      throw new API_Exception(E::ts("Please add SSD in increments of 5 GB."));
    }

    if (intval($virtualPrivateServers) > 2) {
      throw new API_Exception(E::ts("Please limit your initial request to two virtual private servers. We have plenty to go around but need have a conversation with you on timing before we can commit to more."));
    }
    if (intval($virtualPrivateServers) && intval($cpu) / intval($virtualPrivateServers) > 4) {
      throw new API_Exception(E::ts("Please limit your initial request to no more then 4 CPUs per virtual private server. We have plenty to go around but need to discuss to ensure we can provision in a timely manner."));
    }
    if (intval($virtualPrivateServers) && intval($ram) / intval($virtualPrivateServers) > 8) {
      throw new API_Exception(E::ts("Please limit your initial request to no more then 8 GB RAM per virtual private server. We have plenty to go around but need to discuss to ensure we can provision in a timely manner."));
    }
    if (intval($virtualPrivateServers) && intval($ssd) / intval($virtualPrivateServers) > 20) {
      throw new API_Exception(E::ts("Please limit your initial request to no more then 20 GB SSD per virtual private server. We have plenty to go around but need to discuss to ensure we can provision in a timely manner."));
    }
    $this->nextPage = 'confirm';
  }

  private function checkRequired($required) {
    foreach($required as $field => $label) {
      if (empty($this->getFields()[$field] ?? FALSE)) {
        throw new API_Exception(E::ts("%1 field is required.", [ 1 => $label ]));
      }
    }

  }
}






?>

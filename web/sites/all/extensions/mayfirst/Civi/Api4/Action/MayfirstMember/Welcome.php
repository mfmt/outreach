<?php

namespace Civi\Api4\Action\MayfirstMember;


/**
 * Send a welcome message to this new member. 
 *
 *
 * @method int getMembershipId()
 * @method $this setMembershipId(int $membershipId)
 * @method int getCcContactId()
 * @method $this setCcContactId(int $ccContactId)
 * @method string getMessage()
 * @method $this setMessage(string $message)
 **/

class Welcome extends \Civi\Api4\Generic\AbstractAction {

  /**
   * membershipId 
   *
   * The membershipId of the contact that we are welcoming.
   *
   * @var int
   * @required
   **/
  protected $membershipId = NULL;

  /**
   * ccContactId 
   *
   * The email will go to everyone who is related to the member. In addition, you can
   * add another contact id (e.g. the id of a board member who will get a copy of the
   * message as an indication that they should provide a follow up welcome message).
   *
   * @var int
   *
   **/
  protected $ccContactId;

  /**
   * message 
   *
   * The message to preappend to our standard welcome.
   *
   * @var string 
   * @required
   **/
  protected $message = NULL;

  public function _run(\Civi\Api4\Generic\Result $result) {

    $membershipId = $this->getMembershipId();
    $ccContactId = $this->getCcContactId();
    
    // Get the existing related contacts for this membership.
    $sql = "
      SELECT r.contact_id_a AS contact_id
      FROM civicrm_membership m
        JOIN civicrm_relationship r ON m.contact_id = r.contact_id_b
        JOIN civicrm_contact c ON c.id = r.contact_id_a
        JOIN civicrm_relationship_type rt ON r.relationship_type_id = rt.id
      WHERE
        m.id = %0 AND
        r.is_active = 1 AND rt.name_a_b = 'Membership_General_Contact_For' AND
        c.is_deleted != 1 AND c.is_deceased != 1
    ";
    $params = [
      0 => [$membershipId, 'Integer' ],
    ];
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    $recipients = [];
    while ($dao->fetch()) {
      $recipients[$dao->contact_id] = NULL;
    }

    

    $emailer = new \Civi\Mayfirst\Email($membershipId);
    $emailer->recipientContactIds = $recipients;
    if ($ccContactId) {
      \Civi::log()->debug("adding cc contactid $ccContactId");
      $emailer->ccContactIds[] = $ccContactId;
    }
    $emailer->workflowName = 'mayfirst_welcome';
    $emailer->prependContent = nl2br($this->getMessage());

    $activityId = $emailer->send();
    // Set the renewal notification date - otherwise they will get a reminder
    // to pay their dues right after getting the first notice.
    $emailer->updateRenewalNotificationDate();

    $result[] = [
      'id' => $activityId,
    ];
  }
}


?>

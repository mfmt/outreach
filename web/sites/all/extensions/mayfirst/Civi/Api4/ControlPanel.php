<?php
namespace Civi\Api4;


/**
 * ControlPanel entity.
 *
 * Provided by the mayfirst extension.
 *
 * @package Civi\Api4
 */
class ControlPanel extends Generic\AbstractEntity {

  public static function getFields() {
    return new Generic\BasicGetFieldsAction(__CLASS__, __FUNCTION__, function() {
      return [
        [
          'name' => 'memberName',
          'data_type' => 'String',
        ],
        [
          'name' => 'shortName',
          'data_type' => 'String',
        ],
        [
          'name' => 'DomainName',
          'data_type' => 'String',
        ],
        [ 
          'name' => 'firstName',
          'data_type' => 'String',
        ],
        [
          'name' => 'firstName',
          'data_type' => 'String',
        ],
        [
          'name' => 'lastName',
          'data_type' => 'String',
        ],
        [
          'name' => 'email',
          'data_type' => 'String',
        ],
        [
          'name' => 'hostingBenefits',
          'data_type' => 'Integer',
        ],
        [
          'name' => 'contactId',
          'data_type' => 'Integer',
        ],
      ];
    });
  }
}


<?php
namespace Civi\Api4;


/**
 * Dues entity.
 *
 * Provided by the mayfirst extension.
 *
 * @package Civi\Api4
 */
class Dues extends Generic\AbstractEntity {

  public static function permissions() {
    return [
      'meta' => ['access AJAX API'],
      'default' => ['access AJAX API'],
      'Calculate' => ['access AJAX API'],
    ];
  }

  public static function getFields() {
    return new Generic\BasicGetFieldsAction(__CLASS__, __FUNCTION__, function() {
      return [
        [
          'name' => 'frequency',
          'data_type' => 'String',
        ],
        [
          'name' => 'virtualPrivateServers',
          'data_type' => 'String',
        ],
        [
          'name' => 'extraStorage',
          'data_type' => 'Integer',
        ],
        [
          'name' => 'tier',
          'data_type' => 'Integer',

        ],
        [
          'name' => 'currency',
          'data_type' => 'String',
        ],
        [
          'name' => 'hostingBenefits',
          'data_type' => 'Integer',
      ],
    ];
    });
  }
}


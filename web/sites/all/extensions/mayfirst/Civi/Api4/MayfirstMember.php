<?php
namespace Civi\Api4;


/**
 * MayfirstMember entity.
 *
 * Provided by the mayfirst extension.
 *
 * @package Civi\Api4
 */
class MayfirstMember extends Generic\AbstractEntity {

  public static function permissions() {
    return [
      'meta' => ['access CiviCRM'],
      'default' => ['administer CiviCRM'],
      'accounting' => ['access CiviContribute'],
      'getMembershipDetails' => ['access AJAX API'],
      'submitPayment' => ['access AJAX API'],
      'save' => ['access AJAX API'],
      'resendToken' => ['access AJAX API'],
      'getPaymentProcessorInfo' => ['access AJAX API'],
      'uploadReceipt' => ['access AJAX API'],
      'addRelatedContact' => ['access AJAX API'],
      'removeRelatedContact' => ['access AJAX API'],
      'flushCache' => ['access AJAX API'],
      'getCountries' => ['access AJAX API'],
      'getStateProvinces' => ['access AJAX API'],
      'validate' => ['access AJAX API'],
    ];
  }

  public static function getFields() {
    return new Generic\BasicGetFieldsAction(__CLASS__, __FUNCTION__, function() {
      return [ ];
    });
  }
}


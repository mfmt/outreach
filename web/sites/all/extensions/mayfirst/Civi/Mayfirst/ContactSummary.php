<?php

namespace Civi\Mayfirst;

/**
 * Contact Summary 
 *
 * Collect details about a given membership so they can be displayed on the
 * contact Summary page (see mayfirst_civicrm_summary()). 
 */
class ContactSummary  {

  static public function getData($contactId) {
    // Check to see if this record has a membership.
    $membership = \Civi\Api4\Membership::get()
      ->addSelect('id')
      ->addWhere('contact_id', '=', $contactId )
      ->addWhere('membership_type_id:name', 'IN', [ 'Annual', 'Monthly' ])
      ->addClause('OR', ['status_id.is_current_member', '=', TRUE], ['status_id:name', '=', 'Expired'], ['status_id:name', '=', 'Disabled'])
      ->execute()->first();

    $membershipId = $membership['id'] ?? NULL;
    if (empty($membershipId)) {
      // This contactId is not a member.
      return [];
    }

    $data = \Civi\Api4\MayfirstMember::GetMembershipDetails()
      ->setMembershipId($membershipId)
      ->execute()->first();

    $membershipStatus = $data['membership_status'];
    $membershipEndDate = $data['end_date'];
    $expire = strtotime("+4 weeks");
    $editToken = \Civi\Mayfirst\Utils::getToken($membershipId, 'edit', $expire);
    $payToken = \Civi\Mayfirst\Utils::getToken($membershipId, 'pay', $expire);
    $editUrl = \CRM_Utils_System::url('civicrm/public/pay#mayfirst/pay', NULL, TRUE) . '?t=' . $editToken;   
    $payUrl = \CRM_Utils_System::url('civicrm/public/pay#mayfirst/pay', NULL, TRUE) . '?t=' . $payToken;   

    $jsVars = [
      'membership_id' => $membershipId,
      'pay_token' => $payToken,
      'edit_token' => $editToken,
    ];

    $class = "crm-summary-block crm-inline-block-content";
    if ($membershipStatus == 'Grace') {
      $class .= ' mayfirst-grace';
    }
    else if ($membershipStatus == 'Expired') {
      $class .= ' mayfirst-expired';
    }
    else if ($membershipStatus == 'Disabled') {
      $class .= ' mayfirst-disabled';
    }
      
    $duesReductionRequestPendingCount = \Civi\Api4\Activity::get()
      ->addJoin('ActivityContact AS activity_contact', 'INNER')
      ->addWhere('activity_contact.record_type_id', '=', 3)
      ->addWhere('status_id:name', '=', 'Scheduled')
      ->addWhere('activity_type_id:name', '=', 'Membership_Dues_Reduction_Request')
      ->addWhere('activity_contact.contact_id', '=', $contactId)
      ->addWhere('is_deleted', '=', FALSE)
      ->execute()->count();

    $duesReductionRequestCompletedCount = \Civi\Api4\Activity::get()
      ->addJoin('ActivityContact AS activity_contact', 'INNER')
      ->addWhere('activity_contact.record_type_id', '=', 3)
      ->addWhere('status_id:name', '=', 'Completed')
      ->addWhere('activity_type_id:name', '=', 'Membership_Dues_Reduction_Request')
      ->addWhere('activity_contact.contact_id', '=', $contactId)
      ->addWhere('is_deleted', '=', FALSE)
      ->execute()->count();

    $notesCount = \Civi\Api4\Note::get()
      ->addWhere('entity_table', '=', 'civicrm_contact')
      ->addWhere('entity_id', '=', $contactId)
      ->execute()->count();

    $renewalNotes = '';
    $calculatedDues = $data['calculated_dues'];
    $renewalAmount = $data['renewal_amount'];
    if ($calculatedDues == $renewalAmount) {
      $renewalNotes = "No adjustments made to renewal amount.";
    }
    else {
      $actualPercentReduction = 100 - round($renewalAmount/$calculatedDues * 100, 2);
      $renewalNotes = "Calculated dues are \${$calculatedDues} USD, getting a {$actualPercentReduction}% discount.";
      if (strval($data['percent_reduction']) != strval($actualPercentReduction)) {
        $renewalNotes .= " Mismatch reduction, Member Info reduction is {$data['percent_reduction']}%. Please update that.";
      }
    }

    $frequency = $data['membership_type'];
    $renewalAmountFriendly = "\${$renewalAmount} USD {$frequency}";
    if ($data['currency'] == 'MXN') {
      $exch = \Civi\Api4\Dues::Exchange()
        ->setMembershipId($membershipId)
        ->execute()->first();
      $exchangeRate = $exch['rate'];
      $pesos = $exch['amount'];
      $renewalAmountFriendly = "\${$renewalAmount} USD = \${$pesos} MXN {$frequency} at 1 USD = {$exchangeRate} MXN";
    }
    
    // Get last contribution (if any)
    $contribution = \Civi\Api4\Contribution::get()
      ->addSelect('id', 'total_amount', 'currency', 'receive_date')
      ->addWhere('contact_id', '=', $contactId)
      ->addWhere('contribution_status_id:name', '=', 'Completed')
      ->addOrderBy('receive_date', 'DESC')
      ->setLimit(1)
      ->execute()->first();

    if ($contribution) {
      $jsVars['contribution_id'] = $contribution['id'];
      $data['contribution_amount'] = $contribution['total_amount'];
      $data['contribution_currency'] = $contribution['currency'];
      $data['contribution_receive_date'] = $contribution['receive_date'];
    }

    $pay = '<a href="' . $payUrl . '">pay</a> or <button id="mayfirst-copy-pay-token" class="mayfirst-copy-token-button">📋</button>';
    $payAndEdit = '<a href="' . $editUrl . '">pay and edit</a> or <button id="mayfirst-copy-edit-token" class="mayfirst-copy-token-button">📋</button>';

    $data['class'] = $class;
    $data['js_vars'] = $jsVars;
    $data['pay'] = $pay;
    $data['pay_and_edit'] = $payAndEdit;
    $data['renewal_amount_friendly'] = $renewalAmountFriendly;
    $data['renewal_notes'] = $renewalNotes;
    $data['membership_end_date'] = $membershipEndDate;
    $data['dues_reduction_request_pending_count'] = $duesReductionRequestPendingCount;
    $data['dues_reduction_request_completed_count'] = $duesReductionRequestCompletedCount;
    $data['notes_count'] = $notesCount;

    return $data;
  }
}


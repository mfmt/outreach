<?php

namespace Civi\Mayfirst;
use CRM_Mayfirst_ExtensionUtil as E;

class NoRenewalRecipientsException extends \Exception { }

/** 
 *
 * Class for managing May First email messages.
 *
 * The built-in CiviCRM scheduled reminders don't provide as much flexibility
 * and control as we need for renewal email messages. Specifically, we want our
 * tokens to be replaced based on the contact representing the membership, but
 * we want the email messages to go to the contacts with an admin relationship
 * to the member. 
 *
 * Also, if someone requests their renewal token, they may have multiple email
 * addresses and might want it sent to an address that is not coded as their
 * primary address. 
 *
 * Common uses:
 *
 * Public function for sending out all pending renewal reminders:
 *
 * \Civi\Mayfirst\Email::sendRenewalReminders()
 *
 * Public function for sending a token renewal links email for
 * someone who doesn't know their renewal token:
 *
 * \Civi\Mayfirst\Email::sendRenewalLinkForEmail($email) {
 *
 * Common custom workflow:
 *
 * $reminder = new \Civi\Mayfirst\Email($membership_id);
 * $reminder->workflowName = 'foo';
 * $reminder->recipientContactIds[$contact_id] = NULL;
 * $reminder->send();
 *
 */

class Email {

  /**
   * Membership ID
   *
   * The id of the membership for which the renewal message
   * is being sent.
   *
   * @var int
   * @required
   *
   */
  public $membershipId;

  /**
   *
   * recipient contact ids
   *
   * An array of contact ids to send the message to in which the contact_id is
   * the key and the value is either NULL or the value of the email address to
   * use when sending the message. If there is no email value, then it will be
   * sent to the primary email address.
   *
   * If this attribute is left empty, it will be auto set to the admin 
   * contacts for the membership.
   *
   * @var array
   *
   */
  public $recipientContactIds = [];

  /**
   * CC contact ids
   *
   * An array of contact ids that will be cc on each email sent. Unlike
   * recipient contact ids (they each get their own copy in their preferred
   * language), CC contact ids are cc'ed on each email sent.
   */
  public $ccContactIds = [];

  /**
   *
   * Workflow name
   *
   * The name of the template workflow to use when sending the message.
   *
   * $var string
   * @required
   *
   **/
  public $workflowName;

  /**
   *
   * Prepend content
   *
   * Untranslated content that will be prepended to the message
   * being sent.
   *
   * @var string
   *
   */
  public $prependContent;

  /**
   * Contribution Id
   *
   * If sending a receipt for a contribution, include
   * the optional contributionId
   */
  public $contributionId;

  public function __construct($membership_id) {
    $this->membershipId = $membership_id;
  }

  /**
   * Send a single renewal reminder for the given membership_id
   *
   */
  static public function sendSingleRenewalReminder($membershipId) {
    $status = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('status_id:name')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first()['status_id:name'];
    if (empty($status)) {
      throw new \Exception(E::ts("Failed to find a membership with that id."));
    }
    if ($status == 'Current') {
        $workflow_name = 'mayfirst_dues_reminder_active';
    }
    else if ($status == 'Grace') {
      $workflow_name = 'mayfirst_dues_reminder_grace';
    }
    else if ($status == 'Expired') {
      $workflow_name = 'mayfirst_dues_reminder_expired';
    }
    else if ($status == 'Disabled') {
      // Re-use expired.
      $workflow_name = 'mayfirst_dues_reminder_expired';
    }
    else {
      // We don't notify on any other statuses.
      throw new \Exception(E::ts("The membership is in the %1 state - we don't have a template for that.", [ 1 => $status ] ));
    }

    $reminder = new self($membershipId);
    $reminder->workflowName = $workflow_name;
    // send() returns the activity id created.
    return $reminder->send();
  }
 
  /**
   * Check for memberships that are scheduled for a renewal email.
   *
   * Optionally pass in time - a time stamp representing "now" if
   * you want to test for a future date.
   *
   * @return int - number of emails sent.
   */
  static public function sendRenewalReminders($time = NULL) {
    $memberships = self::getMembershipsNeedingReminder($time);
    $sent = 0;
    $failed = [];
    foreach ($memberships as $membershipId => $status) {
      if ($status == 'Current') {
        $workflow_name = 'mayfirst_dues_reminder_active';
      }
      else if ($status == 'Grace') {
        $workflow_name = 'mayfirst_dues_reminder_grace';
      }
      else if ($status == 'Expired') {
        $workflow_name = 'mayfirst_dues_reminder_expired';
      }
      else {
        // We don't notify on any other statuses.
        continue;
      }

      $reminder = new self($membershipId);
      $reminder->workflowName = $workflow_name;
      try {
        $reminder->send();
        $sent++;
        $reminder->updateRenewalNotificationDate();
      }
      catch (NoRenewalRecipientsException $e) {
        $failed[] = $e->getCode();
      }
    }
    return [ 'sent_count' => $sent, 'failed_count' => count($failed), 'failed_member_ids' => $failed ];
  }

  /**
   *
   *  Set the admin contact ids for the given membership to be the
   *  recipients of the email message.
   *
   */
  private function setAdminContactsToBeRecipients() {
    // Find the admin contacts for this membership. Exclude deleted contacts,
    // contacts without an email, contacts on hold, dead contacts, etc.
    $sql = "
      SELECT DISTINCT(r.contact_id_a) AS contact_id
      FROM civicrm_membership m
        JOIN civicrm_relationship r ON m.contact_id = r.contact_id_b
        JOIN civicrm_contact c ON c.id = r.contact_id_a
        JOIN civicrm_email e ON 
          e.contact_id = r.contact_id_a AND 
          e.is_primary = 1 AND
          e.on_hold != 1
        JOIN civicrm_relationship_type rt ON r.relationship_type_id = rt.id
      WHERE
        m.id = %0 AND
        r.is_active = 1 AND rt.name_a_b = 'Membership_Admin_Contact_For' AND
        (r.end_date IS NULL OR r.end_date > CURDATE()) AND
        c.is_deleted != 1 AND c.is_deceased != 1
    ";
    $params = [
      0 => [$this->membershipId, 'Integer' ],
    ];
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    while ($dao->fetch()) {
      $this->recipientContactIds[$dao->contact_id] = NULL;
    }
  }

  /**
   *
   * Return a list of member ids and statuses for all memberships due for a
   * reminder.
   *
   * Our goal is send a dues reminder to every membership every two weeks
   * starting when their membership end date is in two weeks.
   *
   * We should continue sending a reminder every two weeks - as the membership
   * progresses from Current, to Grace, to Expired.
   *
   * We stop sending reminders when the memberhip goes to Cancelled.
   *
   * Optionally pass in $time - a timestamp represenging "now" if you want
   * to test for a future date.
   *
   */
  static public function getMembershipsNeedingReminder($time = NULL) {
    if (is_null($time)) {
      $time = time();
    }
    $two_weeks_from_now = date('YmdHis', $time + (15 * 86400));
    $two_weeks_ago = date('YmdHis', $time - (14 * 86400));

    $memberships = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('id', 'status_id:name')
      ->addWhere('contact_id.is_deleted', '=', 0)
      ->addWhere('end_date', '<', $two_weeks_from_now)
      ->addClause('OR', 
        ['Membership_Details.Renewal_Notification_Date', '<', $two_weeks_ago],
        ['Membership_Details.Renewal_Notification_Date', 'IS NULL']
      )
      ->addWhere('status_id.name', 'IN', ["Current", "Grace", "Expired"])
      ->execute();
    $return = [];
    foreach ($memberships as $membership) {
      $id = $membership['id'];
      $return[$id] = $membership['status_id:name'];
    }
    return $return;
  }

  /*
   * Set the renewal notification date to now.
   */
  public function updateRenewalNotificationDate() {
    \Civi\Api4\Membership::update()
      ->setCheckPermissions(FALSE)
      ->addWhere('id', '=', $this->membershipId)
      ->addValue('Membership_Details.Renewal_Notification_Date', date('Ymdhis'))
      ->execute();
  }

  /**
   * Process "forgot my renewal token" requests.
   *
   * For a given email address, check if it's associated with a contact that has
   * a relationship to a member. If so, send a renewal link and token for each
   * member they are associated with.
   *
   * Return the number of email messages sent.
   */
  static public function sendRenewalLinkForEmail($email) {
    $memberships = self::getMembershipAndContactIdForEmail($email);
    $ret = [];
    foreach ($memberships as $membership_id => $contact_id) {
      $sender = new self($membership_id);
      $sender->recipientContactIds[$contact_id] = $email;
      $sender->workflowName = 'mayfirst_dues_payment_info';
      $sender->send();
      $ret[] = $membership_id;
    }
    return $ret;
  }

  static public function getMembershipAndContactIdForEmail($email) {
    // Get a list of distinct and active contacts for the given email address.
    // Hopefully there is just one, but we might have duplicates.
    $sql = "
      SELECT distinct m.id AS membership_id, c.id AS contact_id
      FROM
        civicrm_contact c JOIN civicrm_email e ON c.id = e.contact_id
        JOIN civicrm_relationship r ON e.contact_id = r.contact_id_a
        JOIN civicrm_relationship_type rt ON r.relationship_type_id = rt.id
        JOIN civicrm_membership m ON r.contact_id_b = m.contact_id
        JOIN civicrm_membership_status ms ON ms.id = m.status_id
      WHERE c.is_deleted = 0 AND (ms.is_current_member = 1 OR ms.name = 'Cancelled')
        AND rt.name_a_b = 'Membership_General_Contact_For' AND e.email = %0
    ";

    $params = [
      0 => [ $email, 'String' ],
    ];
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    $ret = [];
    while($dao->fetch()) {
      $ret[$dao->membership_id] = $dao->contact_id;
    }
    return $ret;
  }

  /**
   *
   * Send a mayfirst membership related email and record an activity.
   *
   **/
  public function send() {
    // Ensure we have a workflow template name.
    if (!$this->workflowName) {
      throw new \Exception(E::ts("Please set the workflow name before sending the email."));
    }

    // If no recipients are set, automatically set them to the admin contacts.
    if (count($this->recipientContactIds) == 0) {
      $this->setAdminContactsToBeRecipients();
    }

    // If we still don't have any recipients, we are in trouble.
    if (count($this->recipientContactIds) == 0) {
      $msg = E::ts("No recipients! Does this membership have any admin contacts? Membership Id: %1", array(1 => $this->membershipId));
      //require_once('NoRenewalRecipientsException.php');
      throw new NoRenewalRecipientsException($msg, $this->membershipId);
    }

    // First, get the membership contact id that will be used for the context when
    // evaluating the message tokens.
    $memberships = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('contact_id')
      ->addWhere('id', '=', $this->membershipId)
      ->execute();

    $membership = $memberships->first(); 
    $membershipContactId = $membership['contact_id'];

    if (empty($membershipContactId)) {
      throw new \Exception(E::ts("Could not find the contact id for this membership: %1.", [ 1 => $this->membershipId]));
    }

    // Build our list of recipients.
    $recipients = [];
    foreach (array_keys($this->recipientContactIds) as $contactId) {
      $cResult = \Civi\Api4\Contact::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('display_name', 'email.email', 'preferred_language')
        ->addJoin('Email AS email', 'LEFT')
        ->addWhere('id', '=', $contactId)
        ->addClause('OR', ['email.location_type_id:name', '=', 'Billing'], ['email.is_primary', '=', TRUE])
        ->addOrderBy('email.is_primary', 'ASC')
        ->setLimit(1)
        ->execute();

      $contact = $cResult->first();

      // If an email is specified we use it. Otherwise we use the billing 
      // email we just got or primary if no billing email.
      if ($this->recipientContactIds[$contactId]) {
        $toEmail = $this->recipientContactIds[$contactId];
      }
      else {
        $toEmail = $contact['email.email'];
      }

      if (empty($toEmail)) {
        \Civi::log()->debug("Could not get an email for '$contactId'");
        continue;
      }
      $recipients[$contactId] = [
        'display_name' => $contact['display_name'],
        'email' => $toEmail,
        'locale' => $contact['preferred_language'],
      ];
    }

    // Build our list of cc recipients
    $ccRecipients = [];
    foreach ($this->ccContactIds as $contactId) {
      $ccResult = \Civi\Api4\Contact::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('display_name', 'email.email')
        ->addJoin('Email AS email', 'LEFT')
        ->addWhere('id', '=', $contactId)
        ->addWhere('email.is_primary', '=', TRUE)
        ->execute();
      $contact = $ccResult->first();

      // If there is no email, we bail
      $toEmail = $contact['email.email'] ?? NULL; 
      if (empty($toEmail)) {
        \Civi::log()->debug("Could not get an email for '$contactId'");
        continue;
      }
      $ccRecipients[$contactId] = '"' . $contact['display_name'] . '" <' . $toEmail . '>';
    }

    // Get the message template.
    $tResult = \Civi\Api4\MessageTemplate::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('msg_html')
      ->addSelect('msg_text')
      ->addSelect('msg_subject')
      ->addWhere('workflow_name', '=', $this->workflowName)
      ->addWhere('is_default', '=', 1)
      ->execute();
    $template = $tResult->first();

    if (empty($template)) {
      throw new \Exception(E::ts("Failed to find a message template matching the work flow name: %1.", [ 1 => $this->workflowName ]));
    }

    if (empty($template['msg_html'])) {
      throw new \Exception(E::ts("There is no HTML content in the message template matching the work flow name: %1.", [ 1 => $this->workflowName ]));
    }
    if ($this->prependContent) {
      // If we are prepending, ensure we add it early to the HTML
      // portion (so it can be properly converted to text). Also
      // ensure it is properly segmented from the main message.
      $this->prependContent .= "\n<hr>\n";
    }

    $html = $this->prependContent . $template['msg_html'];
    $subject = $template['msg_subject'];
    
    $p = new \Civi\Token\TokenProcessor(\Civi::dispatcher(), array(
      'controller' => __CLASS__,
      // We need smarty for translation.
      'smarty' => TRUE,
      'schema' => [ 'contactId', 'membershipId' ],
    ));

    // Fill the processor with a batch of data.
    $p->addMessage('body_html', $html, 'text/html');
    $p->addMessage('subject', $subject, 'text/plain');

    // Now we have to create content for each of the contactIds that we
    // need to send to. Each message might be different (different first name
    // and also it might be in english or spanish). 
    foreach($recipients as $recipientContactId => $recipient) {
      $p->addRow()
        ->context('contactId', $recipientContactId)
        ->context('membershipId', $this->membershipId)
        ->context('contributionId', $this->contributionId)
        ->context('locale', $recipient['locale']);
    }
    $p->evaluate();

    // Prepare to create an activity and send an email to each recipient.
    $activityTypeId = \CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_Activity', 'activity_type_id', 'Email');
    $activityStatusId = \CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_Activity', 'activity_status_id', 'Completed');
    // This is the May First organizational contact, all emails will be "sent" from this contact.
    $mayfirstContactId = 1;
    $from = '"May First Movement Technology" <info@mayfirst.coop>';

    $ccRecipientContactIds = array_keys($ccRecipients);
    $ccRecipientsString = implode(',', $ccRecipients);
    foreach ($p->getRows() as $row) {
      $recipientContactId = $row->context['contactId'];
      $bodyHtml = $row->render('body_html');
      $subject = $row->render('subject');

      // Create an email activity.
      $targetContactIds = array_merge($ccRecipientContactIds, [ $recipientContactId, $membershipContactId ]);
      $activity_result = \Civi\Api4\Activity::create()
        ->setCheckPermissions((FALSE))
        ->addValue('target_contact_id', $targetContactIds)
        ->addValue('source_contact_id', $mayfirstContactId)
        ->addValue('activity_status_id', $activityStatusId)
        ->addValue('subject', $subject)
        // Remove line breaks so the activity displays without double line breaks.
        ->addValue('details', str_replace(["\n","\r"], "", $bodyHtml))
        ->addValue('activity_type_id', $activityTypeId)
        ->execute();
  
      // Send the email.
      $emailParams = [
        'from' => $from,
        'subject' => $subject,
        'html' => $bodyHtml,
        'text' => \CRM_Utils_String::htmlToText($bodyHtml),
        'toName' => $recipients[$recipientContactId]['display_name'],
        'toEmail' => $recipients[$recipientContactId]['email'],
        'cc' => $ccRecipientsString,
      ];
      $success = \CRM_Utils_Mail::send($emailParams);
      if ($success != 1) {
        \Civi::log()->debug("There was an error sending an membership renewal email.");
        return FALSE;
      }
      else {
        \Civi::log()->debug("Sent email to $recipientContactId.");
      }
    }
  }
}

?>

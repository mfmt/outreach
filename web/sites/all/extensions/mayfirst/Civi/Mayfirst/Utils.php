<?php

namespace Civi\Mayfirst;
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Utilities
 *
 * A collection of functions to assist with May First membership
 * calculations.
 */
class Utils  {

  /**
   * Return the membership hash for a given contact hash and membership id.
   */
  static public function hashItUp($contactHash, $membershipId, $type, $expire) {
    return hash("sha256", $contactHash . $membershipId . $type . $expire);
  }

  static public function validToken($givenToken) {
    $tokenPieces = explode(':', $givenToken);
    if (count($tokenPieces) != 4) {
      \Civi::log()->debug("Submitted token cannot be split by the colon: $givenToken.");
      return FALSE;
    }
    foreach($tokenPieces as $tokenPiece) {
      if (empty($tokenPiece)) {
        \Civi::log()->debug("The token was split but had an empty part: $givenToken.");
        return FALSE;
      }
    }

    $membershipId = $tokenPieces[1];
    $type = $tokenPieces[2];
    $expireTs = $tokenPieces[3];

    if (!is_numeric($membershipId)) {
      \Civi::log()->debug("The token's memberrship id part does not seem to be numeric: $givenToken.");
      return FALSE;
    }
    if (!is_numeric($expireTs)) {
      \Civi::log()->debug("The token's time stamp part does not seem to be numeric: $givenToken.");
      return FALSE;
    }
    if ($type != 'edit' && $type != 'pay') {
      \Civi::log()->debug("The token type is wrong: $type in $givenToken.");
      return FALSE;
    }
    $time = time();
    if ($expireTs < $time) {
      \Civi::log()->debug("The token is expired: $givenToken $expireTs vs $time");
    }

    $verifiedToken = self::getToken($membershipId, $type, $expireTs);

    if ($verifiedToken != $givenToken) {
      \Civi::log()->debug("Token hash is wrong: $givenToken and wanted $verifiedToken");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get token for membership id
   *
   * The membership token grants access to either pay dues for a membership
   * or pay dues and edit the membership itself.
   *
   * @param integer $membership_id - the membership id
   * @param string $type - either edit or pay depending on the permission
   * @param string $expire - timestamp.
   *
   */
  public static function getToken($membershipId, $type = 'pay', $expire = NULL) {
    if (is_null($expire)) {
      $expire = strtotime('+2 weeks');
    }
    $allowedTypes = [ 'edit', 'pay' ];
    if (!in_array($type, $allowedTypes)) {
      throw new \Exception(E::ts("Token type should be edit or pay."));
    }
    // Get the contact hash associated with the contact record associated with this membership.
    $hash = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('contact_id.hash')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first()['contact_id.hash'];
    if (empty($hash)) {
      throw new \Exception(E::ts("Failed to generate the token. No matching contact hash for that membership id."));
    }

    $membershipHash = \Civi\Mayfirst\Utils::hashItUp($hash, $membershipId, $type, $expire);
    return "{$membershipHash}:{$membershipId}:{$type}:{$expire}";
  }

  /*
   * Calculate Dues for Membership
   *
   * Given an existing membership calculate the dues and benefits based
   * on the standard dues schedule.
   */
  public static function calculateDues($membership_id) {
    $result = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('membership_type_id:name')
      ->addSelect('Membership_Details.Income')
      ->addSelect('Membership_Details.Plan')
      ->addSelect('Membership_Details.Extra_Storage')
      ->addSelect('Membership_Details.Cpu')
      ->addSelect('Membership_Details.Ram')
      ->addSelect('Membership_Details.Ssd')
      ->addSelect('Membership_Details.Currency:name')
      ->addSelect('Membership_Details.Hosting_Benefits')
      ->addSelect('Membership_Details.Virtual_Private_Servers')
      ->addSelect('Membership_Details.Renewal_Amount')
      ->addSelect('contact_id.Member_Info.Income')
      ->addWhere('id', '=', $membership_id)
      ->execute()->first();

    if (empty($result)) {
      throw new \API_Exception(E::ts("Could not find membership id when re-calculating dues."));
    }

    $dues = \Civi\Api4\Dues::calculate()
      ->setCheckPermissions(FALSE)
      ->setFrequency($result['membership_type_id:name'])
      ->setCurrency($result['Membership_Details.Currency:name'])
      ->setVirtualPrivateServers($result['Membership_Details.Virtual_Private_Servers'])
      ->setPlan($result['Membership_Details.Plan'])
      ->setIncome($result['contact_id.Member_Info.Income'])
      ->setExtraStorage($result['Membership_Details.Extra_Storage'])
      ->setRam($result['Membership_Details.Ram'])
      ->setCpu($result['Membership_Details.Cpu'])
      ->setSsd($result['Membership_Details.Ssd'])
      ->setHostingBenefits(intval($result['Membership_Details.Hosting_Benefits']))
      ->execute()->first();

    $dues['saved_renewal_amount'] = $result['Membership_Details.Renewal_Amount'];
    return $dues;

  }

  static public function callControlPanelApi($data) {
    if (!defined('CONTROL_PANEL_USER')) {
      throw new \Exception(E::ts("The CONTROL_PANEL_USER constant is not defined."));
    }
    $api['user'] = CONTROL_PANEL_USER;
    $api['password'] = CONTROL_PANEL_PASSWORD;
    $api['url'] = CONTROL_PANEL_URL;

    $data['user_name'] = $api['user'];
    $data['user_pass'] = $api['password'];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api['url']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $out = curl_exec($ch);
    if(!$out) {
      throw new \Exception(E::ts("There was a curl error connecting to the API: %1.", [ 1 =>  curl_error($ch) ]));
    }
    curl_close($ch);
    $assoc = TRUE;
    $decoded = json_decode($out, $assoc);
    if (!array_key_exists('is_error', $decoded)) {
      throw new \Exception(E::ts("The control panel did not respond properly: %1.", [ 1 => $out ]));
    }
    if ($decoded['is_error'] != 0) {
      $error_message = '';
      foreach($decoded['error_message'] as $error) {
        $error_message .= " " . $error;
      }
      throw new \Exception(E::ts("The control panel reported an error: %1", [ 1 =>  trim($error_message) ]));
    }
    return $decoded;
  }

  /**
   * Check if a membership has auto renew setup
   *
   * Also, ensure that the recurring contribution is in fact
   * "In Progress".
   */
  static public function membershipAutoRenewInProgress($membershipId) {
    $autoRenew = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('id')
      ->addWhere('id', '=', $membershipId)
      ->addWhere('contribution_recur_id.contribution_status_id:name', '=', 'In Progress')
      ->execute();
    if ($autoRenew->count() == 0) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   *
   * Calculate how much of a membership dues payment should be deferred
   * til next year. If a membership term spans more then one fiscal year
   * we have to record the percent of the payment the pays for next
   * year as deferred income.
   *
   **/
  static public function getDeferredAmount($start, $end, $amountPaid) {
    $tsStart = strtotime($start);
    $tsEnd = strtotime($end);
    $year = date('Y', $tsStart);
    $tsEndOfYear = strtotime("{$year}-12-31 11:59:59");

    // If the membership ends this year, nothing gets deferred
    // (e.g. a monthly payment).
    if ($tsEnd < $tsEndOfYear) {
      return 0;
    }

    // If the membership begins after this year, all of it gets
    // deferred. (Note: this is inexact. If we are pre-paying years
    // into the future, we just count it as income next year.)
    if ($tsStart > $tsEndOfYear) {
      return $amountPaid;
    }

    // Otherwise, we calculate.
    $deferredTime = $tsEnd - $tsEndOfYear;
    $deferredPercent = $deferredTime / ($tsEnd - $tsStart);
    return round($amountPaid * $deferredPercent, 2);
  }

  /** 
   * Helper to recalculate end date.
   *
   * Based on a given start date and a membership type, and number of terms,
   * figure out the end date.
   */
  public static function calculateEndDate($start, $membershipType, $numTerms = 1) {
    if ($membershipType == 'Monthly') {
      $period = "P{$numTerms}M";
    }
    else {
      $period = "P{$numTerms}Y";
    }
    $interval = new \DateInterval($period);
    $reCalculatedEnd = new \DateTime();
    $reCalculatedEnd = $reCalculatedEnd->createFromFormat('Y-m-d', substr($start, 0, 10));
    return $reCalculatedEnd->add($interval)->format('Y-m-d');
  }

  /**
   * Save contribution metadata
   *
   * We have to record in our accounting system when date period this
   * membership contribution is paying for so we record the start date
   * and the amount that should be deferred to the following year.
   *
   * This data is collecting in the Accounting Api.
   */
  public static function setAccountingMetadata($contributionId, $start, $deferredAmount) {
    $start = str_replace([ '-', ' ', ':' ], '', $start);
    $params = [ 
      0 => [$contributionId, 'Integer'],
      1 => [$start, 'Date'],
      2 => [$deferredAmount, 'Float']
    ];
    $sql = "INSERT INTO civicrm_mayfirst_contribution SET contribution_id = %0, 
      invoice_start_date = %1, deferred_amount = %2";
    \CRM_Core_DAO::executeQuery($sql, $params);
  }

  /**
   * Get the invoice start date for a given contribution
   *
   * Used to determine the invoice date for a contribution for
   * accounting purposes.
   */
  public static function getInvoiceStartDateForContribution($contributionId) {
    $params = [ [ $contributionId, 'Integer' ] ];
    $sql = "SELECT invoice_start_date FROM civicrm_mayfirst_contribution
      WHERE contribution_id = %0";
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    $dao->fetch();
    return $dao->invoice_start_date;
    
  }
  /**
   * Get deferred amount for a given contribution
   *
   * Used to determine the amount of a contribution that should
   * be considered deferred income for the following year.
   */
  public static function getDeferredAmountForContribution($contributionId) {
    $params = [ [ $contributionId, 'Integer' ] ];
    $sql = "SELECT deferred_amount FROM civicrm_mayfirst_contribution
      WHERE contribution_id = %0";
    $dao = \CRM_Core_DAO::executeQuery($sql, $params);
    $dao->fetch();
    return floatval($dao->deferred_amount);
    
  }
}


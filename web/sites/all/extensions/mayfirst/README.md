# Mayfirst

This extension provides enhanced membership handling customized for [May First Movement Technology](https://mayfirst.coop).

Specifically, it provides:

1. A CiviCRM angular app for administrators to review new membership requests submitted through the join web forms and create CiviCRM contacts and membership.
2. A CiviCRM angular app for handling members who are paying to renew their membership.

The extension is designed to accept both payments in USD and provide a method for members paying in MXN to upload their receipt.

It also automatically calculates dues based on our sliding scale system after
members enter their annual income.

## Translations.

All strings meant for users to see are wrapped in a special function (`t()`).

A special command is run to collect all of these strings and place them into a
translation template file (`l10n/mayfirst.pot`).

This template file (with all the strings *to be* translated but no strings
actually translated) is then copied to the `l10n/es_ES/LC_MESSAGES/mayfirst.po`
file and translation are added (initially by the `polyglot` program which does
machine translations and then by a human for a second pass).

Every time the code changes and strings have been changed, deleted or added, we
have to update the translation files and fix the translations.

### Generate a new translation file.

To generate a new translation file follow [the instructions to install
`civistrings`](https://github.com/civicrm/civistrings).

(Note it is included in
[civibuildkit(https://github.com/civicrm/civicrm-buildkit)].)

Then change into this extension's root folder and run: `civistrings -o l10n/mayfirst.pot .`

This commands generates `l10n/mayfirst.pot`.

### Merge it

Next, you will want to merge these new file with our existing one. Be sure you
have the `gettext` package installed and you are in the root mayfirst extension
directory. Then...

    msgmerge l10n/es_ES/LC_MESSAGES/mayfirst.po l10n/mayfirst.pot -o l10n/es_ES/LC_MESSAGES/mayfirst.po

That command should preserve all existing translations while adding new or changed ones.

Now, install [polyglot](https://github.com/riccardoFasan/polyglot) to automate our
first pass at the new translations.

With `polyglot` installed, change into `l10n/es_ES/LC_MESSAGES/` and run:

    polyglot translate -p mayfirst.po -t ES -s EN

Then rename the resulting files:

    mv es.mo mayfirst.mo && mv es.po mayfirst.po

Or, if you edit the .po file by hand, you can re-generate the .mo file with:

    msgfmt --use-fuzzy l10n/es_ES/LC_MESSAGES/mayfirst.po -o l10n/es_ES/LC_MESSAGES/mayfirst.mo



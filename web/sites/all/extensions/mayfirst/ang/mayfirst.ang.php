<?php
// This file declares an Angular module which can be autoloaded
// in CiviCRM. See also:
// \https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules/n
return [
  'js' => [
    'ang/mayfirst.js',
    'ang/mayfirst/*.js',
    'ang/mayfirst/*/*.js',
  ],
  'css' => [
    'ang/mayfirst.css',
  ],
  'partials' => [
    'ang/mayfirst',
  ],
  'requires' => [
    'crmUi',
    'crmUtil',
    'ngRoute',
  ],
  'settings' => [],
];

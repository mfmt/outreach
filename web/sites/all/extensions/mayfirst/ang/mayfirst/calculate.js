// Provides a public membership dues calculator for members to fill out
// and learn how much they would have to pay for their membership.
(function(angular, $, _) {

  angular.module('mayfirst').config(function($routeProvider) {
      $routeProvider.when('/mayfirst/calculate', {
        controller: 'Calculate',
        controllerAs: '$ctrl',
        templateUrl: '~/mayfirst/calculate.html',
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular.module('mayfirst').controller('Calculate', function($scope, crmApi4, crmStatus, crmUiHelp, $location, $timeout) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('mayfirst');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/Mayfirst/Calculate'}); // See: templates/CRM/Mayfirst/Calculate.hlp
    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;

    // All fields we submit to the backend are kept in the .fields object.
    ctrl.fields = {};

    // Track language
    ctrl.url = new URL(window.location.href); 
    if (ctrl.url.pathname.startsWith('/es/') ) {
      ctrl.currentLanguage = 'es';
      ctrl.languageUrlPrefix = '/es';
      ctrl.displayToggleLanguage = 'English';
    }
    else {
      ctrl.currentLanguage = 'en';
      ctrl.languageUrlPrefix = '';
      ctrl.displayToggleLanguage = 'Español';
    }
    // Keep track of which bread crumbs are active.
    ctrl.journey = {
      'benefits_active': null,
      'currency_active': null,
      'income_active': null,
      'services_active': null,
      'extra_active': null,
      'confirm_active': null,
    };

    $scope.$watch('$ctrl.fields', function(newValue, oldValue) {
      // If income is higher then plan, bump the plan to avoid validation
      // errors (the user might be going back and changing the income after
      // having already set the plan).
      var income = newValue['income'];
      if (income > ctrl.fields["plan"]) {
        ctrl.fields["plan"] = income;
        console.log("Setting plan to ", income);
      }

      var reCalculateDuesFields = [ 
        'income', 'currency', 'membership_type', 'plan', 
        'extra_storage', 'virtual_private_servers', 'ssd', 
        'ram', 'cpu', 'hosting_benefits' ];
      var key;
      for (i = 0; i < reCalculateDuesFields.length; i++) {
        key = reCalculateDuesFields[i];
        if (oldValue[key] != newValue[key]) {
          ctrl.calculateTotalDues();
        }
      }
      var vps = newValue['virtual_private_servers'];
      // You must have at least as many CPU and RAM
      // as VPS.
      if (vps > 0) {
        if (vps > ctrl.fields["ram"]) {
          ctrl.fields["ram"] = vps;
        }
        if (vps > ctrl.fields["cpu"]) {
          ctrl.fields["cpu"] = vps;
        }
      }
      else {
        // If you decide you don't want any VPS, then
        // eliminate RAM and CPU too.
        ctrl.fields["ram"] = vps;
        ctrl.fields["cpu"] = vps;
      }
    }, true);

    ctrl.init = function() {
      // Initialize all variables.
      // Pages are: benefits -> currency -> income -> services -> extra -> confirmation
      var default_page = 'benefits';
      ctrl.page = $location.search().p || default_page;
      ctrl.options = {};
      ctrl.options["languages"] = [
        {
        'id': 'en_US',
        'name': ts('English')
        },
        {
        'id': 'es_ES',
        'name': ts('Spanish')
        }
      ];
      ctrl.fetchDuesSchedule();

      // Initialize some default values so the calculated part at the bottom
      // fills out nicely.
      ctrl.fields["currency"] = 'USD';
      ctrl.fields["membership_type"] = 'Annual';
      ctrl.fields["additional_resources"] = "0";
      ctrl.fields['virtual_private_servers'] = 0;
      ctrl.fields['cpu'] = 0;
      ctrl.fields['extra_storage'] = 0;
      ctrl.fields['ram'] = 0;
      ctrl.fields['ssd'] = 0;
    };

    ctrl.updateDisplayPreferredLanguage = function(newPreferredLanguage) {
      // When a preferred language is selected, ensure the displayed language
      // on the confirmation page shows something friendlier then "en_US".
      if (newPreferredLanguage == 'en_US') {
        ctrl.display_preferred_language = ts('English');
      }
      else {
        ctrl.display_preferred_language = ts('Spanish');
      }
    }
    ctrl.fetchDuesSchedule = function() {
      crmApi4(
        'Dues',
        'Schedule',
        { 'currency': ctrl.fields["currency"] }
      ).then(function(result) {
        ctrl.duesSchedule = result[0];
        // Get the current exchange rate.
        return crmApi4(
          'Dues',
          'Exchange'
        );
      }).then(function(result) {
        ctrl.exchange_rate = result[0]['rate'];
      });
    };

    ctrl.journeyClass = function(page) {
      if (ctrl.page == page) {
        return 'mayfirst-calculate-journey-active';
      }
      else {
        return '';
      }
    };

    
    ctrl.validate = function(page) {
      ctrl.clearMessage();
      var params = { fields: ctrl.fields, page: page, token: ctrl.token};
      crmApi4(
        'MayfirstMember', 
        'Validate', 
        params 
      ).then(function(result) {
        ctrl.page = result[0]['nextPage'];
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });

    }

    ctrl.previousPage = function(currentPage) {
      ctrl.clearMessage();
      if (currentPage == 'currency') {
        ctrl.page = 'benefits';
      }
      if (currentPage == 'income') {
        ctrl.page = 'currency';
      }
      if (currentPage == 'services') {
        ctrl.page = 'income';
      }
      if (currentPage == 'extra') {
        ctrl.page = 'services';
      }
      if (currentPage == 'confirm') {
        if (ctrl.fields.hosting_benefits == 0) {
          ctrl.page = 'currency';
        }
        else if (ctrl.fields.additional_resources == "0") {
          ctrl.page = 'services';
        }
        else {
          ctrl.page = 'extra';
        }
      }
    }

    /*
     * Clear hosting values
     *
     * If someone goes through the whole application and then decides
     * they don't want hosting benefits, we have to clear out any
     * hosting values they chose so their total returns to a basic
     * membership total.
     */
    ctrl.clearHostingBenefits = function() {
      ctrl.fields["virtual_private_servers"] = 0;
      ctrl.fields["cpu"] = 0;
      ctrl.fields["ram"] = 0;
      ctrl.fields["ssd"] = 0;
      ctrl.fields["additional_information"] = "";
      ctrl.fields["additional_resources"] = "0";
      ctrl.fields["extra_storage"] = 0;
    };

    /*
     * Clear VPS values
     *
     * If someone chooses a VPS and then changes
     * their mind and clicks NO to extra services,
     * clear out the VPS choices.
     */
    ctrl.clearVpsValues = function() {
      ctrl.fields["virtual_private_servers"] = 0;
      ctrl.fields["cpu"] = 0;
      ctrl.fields["ram"] = 0;
      ctrl.fields["ssd"] = 0;
    };

    ctrl.calculateTotalDues = function() {
      var params = {
        // Our Calculate API expects certain values to be integers and if they are
        // not, it throws cryptic user errors. So force decimals to integers to
        // avoid the errors, and we give better feedback in our call to Validate.
        frequency: ctrl.fields.membership_type || 'Annual',
        virtualPrivateServers: Math.round(ctrl.fields.virtual_private_servers) || 0,
        extraStorage: Math.round(ctrl.fields.extra_storage) || 0,
        income: Math.round(ctrl.fields.income) || null,
        plan: Math.round(ctrl.fields.plan) || ctrl.fields.income || 1,
        cpu: Math.round(ctrl.fields.cpu) || 0,
        ram: Math.round(ctrl.fields.ram) || 0,
        ssd: Math.round(ctrl.fields.ssd) || 0,
        currency: ctrl.fields.currency || 'USD',
        hostingBenefits: ctrl.fields.hosting_benefits || 0
      };

      // A bit of a hack is necessary. If someone chooses 1 virtual private
      // server before they have had a chance to choose one CPU, they will
      // immediately get a validation error. We smooth over that immediate
      // error by calculating what the minimum dues would be if the params
      // were correct. When they click Next on the form, they will get the
      // validation error and will be able to change their values.
      if (params["virtualPrivateServers"] > params["cpu"]) {
        params["cpu"] = params["virtualPrivateServers"];
      }
      if (params["virtualPrivateServers"] > params["ram"]) {
        params["ram"] = params["virtualPrivateServers"];
      }
      // Clear any existing error message.
      ctrl.clearMessage('error');
      crmApi4(
        'Dues', 
        'Calculate', 
        params 
      ).then(function(result) {
        ctrl.dues = result[0]['dues'];
        ctrl.dues_usd = result[0]['dues_usd'];
        ctrl.storage = result[0]['storage'];
        if (ctrl.fields.membership_type == 'Annual') {
          ctrl.display_frequency = ts('Year');
        }
        else {
          ctrl.display_frequency = ts('Month');
        }
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });
    };

    ctrl.clearMessage = function(type = 'error') {
      if (type == 'error') {
        var el = document.getElementById('mayfirst-calculate-error-message');
      }
      else {
        var el = document.getElementById('mayfirst-calculate-success-message');
      }
      el.style.display = 'hide';
    };

    ctrl.showMessage = function(msg, type = 'error') {
      if (type == 'error') {
        var el = document.getElementById('mayfirst-calculate-error-message');
      }
      else {
        var el = document.getElementById('mayfirst-calculate-success-message');
      }
      el.innerHTML = msg;
      el.style.display = 'block';
    };

    ctrl.clearMessage = function () {
      var errorEl = document.getElementById('mayfirst-calculate-error-message');
      var successEl = document.getElementById('mayfirst-calculate-success-message');
      errorEl.innerHTML = '';
      errorEl.style.display = 'none';
      successEl.innerHTML = '';
      successEl.style.display = 'none';

    };

    ctrl.toggleLanguage = function() {
      if (ctrl.currentLanguage == 'es') {
        // We in spanish, switch to english.
        var pathname = ctrl.url.pathname.substring(3);
        location.href = pathname + ctrl.url.hash + ctrl.url.search;
      }
      else {
        // We are in english, switch to spanish.
        location.href = '/es' + ctrl.url.pathname + ctrl.url.hash + ctrl.url.search;
      }
    }
    ctrl.init();

  });
})(angular, CRM.$, CRM._);

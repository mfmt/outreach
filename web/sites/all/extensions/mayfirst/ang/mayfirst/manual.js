// This code allows administrators to manually record membership
// payments.
(function(angular, $, _) {
  
  angular.module('mayfirst').config(function($routeProvider) {
      
      $routeProvider.when('/mayfirst/manual', {
        controller: 'Manual',
        controllerAs: '$ctrl',
        templateUrl: '~/mayfirst/manual.html',
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular.module('mayfirst').controller('Manual', function($scope, crmApi4, crmStatus, crmUiHelp, $location) {
  
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('mayfirst');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/Mayfirst/Manual'});

    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;
    ctrl.options = {};
    ctrl.options['contributions'] = [];
    ctrl.options['currencies'] = [
      {
        'id': 'MXN',
        'name': 'MXN' 
      },
      {
        'id': 'USD',
        'name': 'USD' 
      }
    ];

    ctrl.membership = {};

    // A list of contributions for the given member that are in the pending state.
    ctrl.pending_contributions = {};
    // The contribution selected from the drop down list of options.
    ctrl.selected_contribution = {
      id: null
    };
    // The contribution that will be saved.
    ctrl.contribution = {}
    // The contribution id (populated after the contribution is saved).
    ctrl.saved_contribution_id = null;

    this.init = function() {
      ctrl.initDynamicOptions();
    };

    this.initDynamicOptions = function() {
      CRM.api4('OptionValue', 'get', {
        select: ["value", "label", "option_group_id:name"],
        where: [["option_group_id:name", "IN", ["payment_instrument", "contribution_status"]]]
      }).then(function(optionValues) {
        ctrl.options['payment_instrument'] = [];
        ctrl.options['contribution_status'] = [];
        var name;
        optionValues.forEach(function(result) {
          name = result['option_group_id:name'];
          ctrl.options[name].push({
            'id': result['value'],
            'name': result['label']
          });
        });
      });
    }
    this.updateSelectedContact = function() {
      ctrl.saved_contribution_id = null;
      ctrl.updateContributionOptions();
    }
    this.updateContributionOptions = function() {
      ctrl.error_contact = null;
      ctrl.options["contributions"] = [];
      crmApi4(
        'MayfirstMember',
        'GetMembershipDetails',
        { 
          'memberContactId': ctrl.member_contact_id
        }
      ).then(function(result) {
        ctrl.membership = result[0];
        // Reformat end date in user's locale
        var endDate = new Date(result[0].end_date.replace(/-/g, '\/'));
        ctrl.membership.local_end_date = endDate.toLocaleDateString();
        return  crmApi4(
          'Contribution',
          'Get',
          { 
            where: [["contact_id", "=", ctrl.member_contact_id], ['contribution_status_id:name', '=', 'Pending']],
            select: [ "id", "receive_date", "total_amount", "currency", "contribution_status_id:name" ],
            orderBy: {"receive_date":"DESC"}
          }
        );
      }).then(function(result) {
        ctrl.pending_contributions = result;
        if (ctrl.pending_contributions.length == 0) {
          // No pending contributions, go straight to adding a new contribution.
          ctrl.selected_contribution.id = 0;
          ctrl.loadContributionForm();
        }
        else {
          // Provide option of loading an existing pending contribution.
          ctrl.options["contributions"].push({
            'id': 0,
            'name': ts('create new payment')
          });
          result.forEach(function (val) {
            ctrl.options["contributions"].push({
              'id': val['id'],
              'name': val['id'] + ": " + val['receive_date'] + " $" + val['total_amount'] + " " + val['currency'] + " " + val['contribution_status_id:name']
            }); 
          });
        }
      }).catch(function(err) {
        ctrl.saved_contribution_id = null;
        ctrl.member_contact_id = null;
        ctrl.error_contact = err.error_message;
      });
 
    };

    this.loadContributionForm = function () {
      ctrl.contribution = {
        'num_terms': 1
      };
      if (ctrl.selected_contribution.id == 0) {
        ctrl.contribution['total_amount'] = 0;
        if (ctrl.membership.currency == 'USD') {
          ctrl.contribution['currency'] = { "id": 'USD', "name": "USD" };
        }
        else {
          ctrl.contribution['currency'] = { "id": 'MXN', "name": "MXN" };
        }
        var currentDate = new Date();
        ctrl.contribution['receive_date'] = currentDate;
        ctrl.setMatchingOptionValue('payment_instrument', 'EFT');
        ctrl.setMatchingOptionValue('contribution_status', 'Completed');
        ctrl.updateEndDate();
      } 
      else {
        crmApi4(
          'Contribution',
          'Get',
          { 
            where: [["id", "=", ctrl.selected_contribution.id]],
            select: [ "id", "receive_date", "total_amount", "currency", "contribution_status_id:name", "payment_instrument:name", "Membership_Dues_Info.Factura_Requested", "Membership_Dues_Info.Transfer_Receipt"  ]
          }
        ).then(function(result) {
          ctrl.contribution['total_amount'] = result[0]['total_amount'];
          ctrl.contribution['currency'] = { "id":  result[0]['currency'], "name": result[0]['currency'] };
          ctrl.contribution['receive_date'] = new Date(result[0]['receive_date']);
          ctrl.setMatchingOptionValue('payment_instrument', 'EFT');
          ctrl.setMatchingOptionValue('contribution_status', result[0]['contribution_status_id:name']);
          ctrl.contribution["factura_requested"] = result[0]['Membership_Dues_Info.Factura_Requested'];
          ctrl.contribution["transfer_receipt_displayable"] = false;
          ctrl.contribution["transfer_receipt_link"] = '';
          var transfer_receipt_id = result[0]['Membership_Dues_Info.Transfer_Receipt'];
          ctrl.updateEndDate();

          if (transfer_receipt_id) {
            crmApi4(
              'MayfirstMember',
              'GetContributionTransferReceipt',
              { 
                contributionId: ctrl.selected_contribution.id
              }
            ).then(function(result) {
              ctrl.contribution.transfer_receipt_link = result[0]['link'];
              ctrl.contribution.transfer_receipt_displayable = result[0]['displayable'];
            });
          }
        });
      }
    };

    // We have the name of the option we want (e.g. "Completed"), but we need
    // to get the id of the option, which is only present in the
    // ctrl.options['contribution_status'] array. This helper finds the right
    // value in that array and sets it it.
    this.setMatchingOptionValue = function(optionIndex, desiredValue) {
      ctrl.options[optionIndex].forEach(function(option) {
        if (desiredValue == option['name']) {
          ctrl.contribution[optionIndex] = option;
        }
      });
    };

    this.updateEndDate = function() {
      ctrl.error_end_date = null;
      var renewal_amount = ctrl.membership.renewal_amount;
      if (ctrl.contribution.currency.id == 'MXN') {
        renewal_amount = ctrl.membership.renewal_amount_localized;
      }
      crmApi4(
          'Dues',
          'EndDateForDuesPaid',
          { 
            'startDate': ctrl.membership.end_date,
            'renewalAmount': renewal_amount,
            'membershipType': ctrl.membership.membership_type,
            'amountPaid': ctrl.contribution.total_amount || '0',
            'numTerms': ctrl.contribution.num_terms || 1
          }
        ).then(function(result) {
          // Our API returns YYYY-MM-DD, which is a standard ISO date. If we
          // pass that directly to javascript, it will assume it's a UTC date
          // which could be 4 or 5 hours ahead of time, which could produce a
          // date that is tomorrow instead of today. So, we replace the dashes
          // to get YYYY/MM/DD instead, which triggers javascript to assume
          // it's a local date and it will be converted to local time. What a
          // mess.
          ctrl.new_end_date = new Date(result[0]['date'].replace(/-/g, '\/'));
        }).catch(function(err) {
          ctrl.error_end_date = err.error_message;
        });

    };
    this.saveContribution = function() {
      ctrl.error_save_contribution = '';
      if (ctrl.contribution.contribution_status.name != 'Completed' ) {
        ctrl.error_save_contribution = ts('Be sure to set the contribution status to Completed before submitting.'); 
        return;
      }
      if (ctrl.contribution.total_amount < 1 ) {
        ctrl.error_save_contribution = ts('Please set the total amount to a value greater than 1.'); 
        return;
      }
      if (ctrl.contribution.payment_instrument.name == 'Credit Card' && !ctrl.contribution.trxn_id) {
        ctrl.error_save_contribution = ts('When paying by credit card, the transaction id is required.'); 
        return;
      }
      if (ctrl.contribution.payment_instrument.name == 'Check' && !ctrl.contribution.check_number) {
        ctrl.error_save_contribution = ts('When paying by check, the check number is required.'); 
        return;
      }
      crmApi4(
        'MayfirstMember',
        'SubmitManualPayment',
        { 
          'membershipId': ctrl.membership.id,
          'contactId': ctrl.member_contact_id,
          'totalAmount': ctrl.contribution.total_amount,
          'currency': ctrl.contribution.currency.id,
          'receiveDate': ctrl.contribution.receive_date,
          'numTerms': ctrl.contribution.num_terms,
          "endDate": ctrl.new_end_date,
          "trxnId": ctrl.contribution.trxn_id,
          "checkNumber": ctrl.contribution.check_number,
          'paymentInstrumentId': ctrl.contribution.payment_instrument.id,
          'contributionId': ctrl.selected_contribution.id,
        }
      ).then(function(result) {
        console.log("results", result);
        ctrl.saved_contribution_id = result[0]['contribution_id'];
        console.log("Saved contribution", ctrl.saved_contribution_id);
        ctrl.selected_contribution = {
          id: null
        };
        // Reset the form to get new values.
        ctrl.updateContributionOptions();
      }).catch(function(err) {
        ctrl.error_save_contribution = err.error_message;
      });
      
    };

    ctrl.init();
  });

})(angular, CRM.$, CRM._);

// Provides the main interface for members to pay their dues.
(function(angular, $, _) {

  angular.module('mayfirst').config(function($routeProvider) {
      $routeProvider.when('/mayfirst/pay', {
        controller: 'Pay',
        controllerAs: '$ctrl',
        templateUrl: '~/mayfirst/pay.html',
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular.module('mayfirst').controller('Pay', function($scope, crmApi4, crmStatus, crmUiHelp, $location, $timeout, FileUploader) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('mayfirst');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/Mayfirst/Pay'}); // See: templates/CRM/mayfirst/pay.hlp
    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;

    // Record the hash and membership id (if passed in via URL)
    ctrl.token = $location.search().t;
    // add testMode=1 to use the test processors.
    ctrl.testMode = $location.search().testMode;

    // Track language. The langugaePrefix is used to generate links
    // to other pages - it ensures that if you are reading this page
    // in spanish, the link you go to will also be in spanish.
    ctrl.url = new URL(window.location.href); 
    if (ctrl.url.pathname.startsWith('/es/') ) {
      ctrl.currentLanguage = 'es';
      ctrl.languageUrlPrefix = '/es';
      ctrl.displayToggleLanguage = 'English';
    }
    else {
      ctrl.currentLanguage = 'en';
      ctrl.languagePrefix = '';
      ctrl.displayToggleLanguage = 'Español';
    }

    ctrl.submitting_payment = false;
    ctrl.valid_token = false;
    ctrl.payment_processed = false;
    ctrl.membership_id = null;
    ctrl.display_name = null;
    ctrl.renewal_amount = null;
    ctrl.renewal_amount_localized = null;
    ctrl.end_date = null;
    ctrl.csrf_token = null;
    ctrl.automatic_renewal = 1;
    ctrl.payment_method = null;
    ctrl.show_pay_by_check_details = false;
    ctrl.show_pay_by_check_link_text = ts("Click here for details");
    ctrl.hide_pay_by_check_link_text = ts("Hide details");

    // The init() function intializes all the membership values based on the given
    // token.
    this.init = function() {
      // A token is a sha256 hash, which is 64 characters. Plus a colon and at least one number.
      if (this.token.length > 65 && this.token.includes(':')) {
        ctrl.initForm();
      }
      else {
        displayError(ts("That did not look like a valid token."));
      }
    }

    this.displayError = function(err) {
      var msg = "";
      // CiviCRM Style
      if (err.error_message) {
        msg = err.error_message;
      }
      else {
        msg = err;
      }
      alert(ts("Sorry, we hit a problem. ") + msg);
    };

    this.initForm = function() {
      crmApi4(
        'MayfirstMember', 
        'GetPaymentProcessorInfo', 
        { 'testMode': ctrl.testMode }
      ).then(function(result) {
        crmApi4(
          'MayfirstMember', 
          'GetMembershipDetails', 
          { 
            'token': ctrl.token
          }
        ).then(function(result) {
          ctrl.valid_token = true;
          ctrl.membership_id = result[0]['membership_id'];
          ctrl.display_name = result[0]['display_name'];
          ctrl.renewal_amount = result[0]['renewal_amount'];
          ctrl.renewal_amount_localized = result[0]['renewal_amount_localized'];
          ctrl.end_date = result[0]['end_date'];
          ctrl.membership_status = result[0]['membership_status'];
          ctrl.membership_type = result[0]['membership_type'];
          ctrl.currency = result[0]['currency'];
          ctrl.csrf_token = result[0]['csrf_token'];
          ctrl.exchange_rate = result[0]['exchange_rate'];
          ctrl.exchange_rate_explanation = result[0]['exchange_rate_explanation'];
          ctrl.membership_auto_renew_in_progress = result[0]['membership_auto_renew_in_progress'];
          ctrl.membership_auto_renew_eligible = result[0]['membership_auto_renew_eligible'];
          if (ctrl.membership_auto_renew_in_progress || !ctrl.membership_auto_renew_eligible) {
            // If auto renew is already in progress for their membership don't let them
            // set it again.
            ctrl.automatic_renewal = 0;
          }

          ctrl.edit_permission = result[0]['edit_permission'] || false;
          ctrl.hosting_benefits = result[0]['hosting_benefits'] || 0;
          ctrl.related_contacts = result[0]['related_contacts'] || {};
          ctrl.membership_and_hosting_summary = result[0]['membership_and_hosting_summary'] || '';
          ctrl.virtual_private_servers_summary = result[0]['virtual_private_servers_summary'] || '';
          ctrl.submit_payment_text = ts("Make the payment");
          ctrl.country = result[0]['country_id:name'];
          ctrl.state = result[0]['state_province_id:name'];
          ctrl.city = result[0]['city'];
          ctrl.is_coop = result[0]['is_coop'];
          console.log(ctrl);
        }).catch(function (err) {
          ctrl.displayError(err); 
          // Unset the token, it doesn't work.
          ctrl.token = "";
        });
      
      });
    };

    this.processTokenSubmission = function() {
      // The user has submitted the Token form - either a token
      // or an email address to request a token.
      if (ctrl.token && ctrl.email) {
        alert(ts("Please enter either a token OR an email address. If your token is invalid, remove the token."));
      }
      else if (ctrl.token) {
        ctrl.init();
      }
      else {
        if (!ctrl.email) {
          alert(ts("Please enter a token or email address"));
        }
        else {
          return ctrl.resendToken();
        }
      }
    };
    this.resendToken = function() {
      crmApi4(
        'MayfirstMember', 
        'ResendToken', 
        { 'email': ctrl.email }
      ).then(function(result) {
        var count_emails_sent = result[0].length;
        if (count_emails_sent == 0) {
          throw new Error(ts("Your email is not linked to any memberships."));
        }
        else { 
          alert(ts("An email was sent with your token."));
        }
      }).catch(function(err) {
        ctrl.displayError(err); 
      });
    };

    this.uploaderFacturaNotRequested = new FileUploader({
      url: CRM.url('civicrm/ajax/api4/MayfirstMember/UploadReceipt', { 'token': ctrl.token, 'factura': 0 }),
      headers: {'X-Requested-With': 'XMLHttpRequest'}
    });
    this.uploaderFacturaRequested = new FileUploader({
      url: CRM.url('civicrm/ajax/api4/MayfirstMember/UploadReceipt', { 'token': ctrl.token, 'factura': 1 }),
      headers: {'X-Requested-With': 'XMLHttpRequest'}
    });
    this.uploaderFacturaRequested.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
      alert(response.error_message || ts("There was a problem uploading your file. Please ask support for help."));
    };
    this.uploaderFacturaNotRequested.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
      alert(response.error_message || ts("There was a problem uploading your file. Please ask support for help."));
    };
    this.uploaderFacturaRequested.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      ctrl.payment_processed = true;
    };
    this.uploaderFacturaNotRequested.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      ctrl.payment_processed = true;
    };

    this.uploadReceipt = function() {
      if (ctrl.factura == 1) {
        if (!document.getElementById('mayfirst-pay-receipt-factura').value) {
          alert(ts("Please click browse and select a file to upload"));
          return;
        };
        ctrl.uploaderFacturaRequested.uploadAll();
      }
      else {
        if (!document.getElementById('mayfirst-pay-receipt-no-factura').value) {
          alert(ts("Please click browse and select a file to upload"));
          return;
        };
        ctrl.uploaderFacturaNotRequested.uploadAll();
      }
    };

    ctrl.toggleLanguage = function() {
      if (ctrl.currentLanguage == 'es') {
        // We in spanish, switch to english.
        var pathname = ctrl.url.pathname.substring(3);
        location.href = pathname + ctrl.url.hash + ctrl.url.search;
      }
      else {
        // We are in english, switch to spanish.
        location.href = '/es' + ctrl.url.pathname + ctrl.url.hash + ctrl.url.search;
      }
    }
    if (ctrl.token) {
      this.init();
    }

  });
  
})(angular, CRM.$, CRM._);

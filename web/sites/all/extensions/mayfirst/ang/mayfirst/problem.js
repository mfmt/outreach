(function(angular, $, _) {
  
  angular.module('mayfirst').config(function($routeProvider) {
      
      $routeProvider.when('/mayfirst/problem', {
        controller: 'Problem',
        controllerAs: '$ctrl',
        templateUrl: '~/mayfirst/problem.html',
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular.module('mayfirst').controller('Problem', function($scope, crmApi4, crmStatus, crmUiHelp, $location) {
  
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('mayfirst');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/Mayfirst/Problem'}); // See: templates/CRM/mayfirst/createMembership.hlp

    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;

    // The list of members with some kind of problem that should be reviewed.
    ctrl.members = [];

    ctrl.loading = true;
    // Initialize with a list of problematic members.
    this.init = function() {
      crmApi4(
        'MayfirstMember',
        'ProblemMembers',
        {}
      ).then(function(result) {
        ctrl.members = result;
        ctrl.total = ctrl.members.length;
        ctrl.loading = false;
      }); 
    };
    ctrl.init();
  });

})(angular, CRM.$, CRM._);

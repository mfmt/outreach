// Allows admins to review pending memberships and approve them.
(function(angular, $, _) {
  
  angular.module('mayfirst').config(function($routeProvider) {
      
      $routeProvider.when('/mayfirst/review', {
        controller: 'Review',
        controllerAs: '$ctrl',
        templateUrl: '~/mayfirst/review.html',
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular.module('mayfirst').controller('Review', function($scope, crmApi4, crmStatus, crmUiHelp, $location) {
  
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('mayfirst');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/Mayfirst/Review'}); // See: templates/CRM/mayfirst/createMembership.hlp

    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;

    // The list of membership requests waiting to be processed.
    ctrl.requests = [];

    ctrl.url = new URL(window.location.href); 
    if (ctrl.url.pathname.startsWith('/es/') ) {
      ctrl.currentLanguage = 'es';
      ctrl.displayToggleLanguage = 'English';
    }
    else {
      ctrl.currentLanguage = 'en';
      ctrl.displayToggleLanguage = 'Español';
    }

    // The currently chosen request.
    ctrl.activeRequest = null;

    ctrl.options = {};
    ctrl.options["preferredLanguages"] = [
      {
        id: 'en_US',
        name: ts('English')
      },
      {
        id: 'es_ES',
        name: ts("Spanish")
      }
    ];
    ctrl.options["welcomeContacts"] = [];
    

    // Initialize with a list of pending memberships. A membership is pending if it's
    // status is set to "Requested";
    this.init = function() {
      crmApi4(
        'MayfirstMember',
        'GetRequested',
        {}
      ).then(function(result) {
        ctrl.requests = result;
        // Reset the active requests if necessary.
        if (ctrl.activeRequest) {
          ctrl.setActiveRequest(ctrl.activeRequest.membership_id);
        }
        // Populate the list of contacts in the Welcome Committee.
        return crmApi4('GroupContact', 'get', {
          select: ["contact_id", "contact_id.display_name"],
          where: [["group_id:name", "=", "New_Member_Welcoming_Committee"]]
        });
      }).then(function(groupContacts) {
        console.log("groupcontacts", groupContacts);
        for (var i=0; i < groupContacts.length; i++) {
          ctrl.options.welcomeContacts.push({ 
            id: groupContacts[i]["contact_id"],
            name: groupContacts[i]["contact_id.display_name"]
          });
        }
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        alert(msg)
      }); 
    };

    ctrl.toggleLanguage = function() {
      console.log('url is ', ctrl.url);
      if (ctrl.currentLanguage == 'es') {
        // We in spanish, switch to english.
        var pathname = ctrl.url.pathname.substring(3);
        console.log('new pathname', pathname);
        location.href = pathname + ctrl.url.hash + ctrl.url.search;
      }
      else {
        // We are in english, switch to spanish.
        location.href = '/es' + ctrl.url.pathname + ctrl.url.hash + ctrl.url.search;
        console.log('going to: /es' + url.pathname + ctrl.url.hash + ctrl.url.search)
      }
    }

    this.createControlPanelEntries = function() {
      var params = {};
      params['memberName'] = ctrl.activeRequest.display_name;
      params['domainName'] = ctrl.activeRequest.domain_name;
      params['shortName'] = ctrl.activeRequest.short_name;
      params['firstName'] = ctrl.activeRequest.related_first_name;
      params['lastName'] = ctrl.activeRequest.related_last_name;
      params["hostingBenefits"] = parseInt(ctrl.activeRequest.hosting_benefits);
      params["membershipType"] = ctrl.activeRequest.contact_type;
      params["memberContactId"] = ctrl.activeRequest.member_contact_id;
      params['email'] = ctrl.activeRequest.related_email;
      params['preferredLanguage'] = ctrl.activeRequest.related_preferred_language;
      params['allocationQuota'] = ctrl.activeRequest.calculated_storage;

      crmApi4(
        'ControlPanel', 
        'CreateMembership', 
        params
      ).then(function(result) {
        ctrl.init();
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        alert(msg);
      });
    };

    this.sendWelcomeMessage = function() {
      var params = {};
      params['membershipId'] = ctrl.activeRequest.membership_id;
      params['message'] = ctrl.welcome_message;
      if ("welcome_contact_id" in ctrl) {
        params['ccContactId'] = ctrl.welcome_contact_id.id;
      }
      crmApi4(
        'MayfirstMember', 
        'Welcome', 
        params
      ).then(function(result) {
        var msg = ts("The welcome email was sent.");
        alert(msg);
        ctrl.init();
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        alert(msg);
      }); ;
    };

    this.setActiveRequest = function(membershipId) {
      for (var i=0; i < ctrl.requests.length; i++) {
        if (ctrl.requests[i]['membership_id'] == membershipId) {
          ctrl.activeRequest = ctrl.requests[i];
          break;
        }
      }
    };

    this.activateMembership = function() {
      crmApi4(
        'MayfirstMember', 
        'ActivateMembership',
        { membershipId: ctrl.activeRequest.membership_id }
      ).then(function(result) {
        ctrl.init();
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        alert(msg);
      }); 
    };

    ctrl.init();
  });

})(angular, CRM.$, CRM._);

// The public interface for new people to join May First.
(function(angular, $, _) {

  angular.module('mayfirst').config(function($routeProvider) {
      $routeProvider.when('/mayfirst/save', {
        controller: 'Save',
        controllerAs: '$ctrl',
        templateUrl: '~/mayfirst/save.html',
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular.module('mayfirst').controller('Save', function($scope, crmApi4, crmStatus, crmUiHelp, $location, $timeout) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('mayfirst');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/Mayfirst/Save'}); // See: templates/CRM/Mayfirst/Save.hlp
    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;

    // Record the hash and membership id (if passed in via URL)
    ctrl.token = $location.search().t;

    // All fields we submit to the backend are kept in the .fields object.
    ctrl.fields = {};

    // When editing a membership contact we show the form.
    ctrl.show_related_contact_form = false;

    // When the token is present, we show this warning (and then hide it
    // after the data has been submitted).
    ctrl.show_unsaved_changes_warning = false;

    // Track language
    ctrl.url = new URL(window.location.href); 
    if (ctrl.url.pathname.startsWith('/es/') ) {
      ctrl.currentLanguage = 'es';
      ctrl.languageUrlPrefix = '/es';
      ctrl.displayToggleLanguage = 'English';
    }
    else {
      ctrl.currentLanguage = 'en';
      ctrl.languageUrlPrefix = '';
      ctrl.displayToggleLanguage = 'Español';
    }

    // We change the submit button after the click it.
    ctrl.submitButtonText = ts("Submit Membership Details");

    // Keep track of which bread crumbs are active.
    ctrl.journey = {
      'welcome_active': null,
      'about_active': null,
      'benefits_active': null,
      'currency_active': null,
      'income_active': null,
      'services_active': null,
      'extra_active': null,
      'confirm_active': null,
    };

    $scope.$watch('$ctrl.fields', function(newValue, oldValue) {
      // If income is higher then plan, bump the plan to avoid validation
      // errors (the user might be going back and changing the income after
      // having already set the plan).
      var income = newValue['income'];
      if (income > ctrl.fields["plan"]) {
        ctrl.fields["plan"] = income;
        console.log("Setting plan to ", income);
      }

      // If any of these fields change, re-calculate the dues.
      var reCalculateDuesFields = [ 
        'income', 'currency', 'membership_type', 'plan', 
        'extra_storage', 'virtual_private_servers', 'ssd', 
        'ram', 'cpu', 'hosting_benefits' ];
      var key;
      for (i = 0; i < reCalculateDuesFields.length; i++) {
        key = reCalculateDuesFields[i];
        if (oldValue[key] != newValue[key]) {
          ctrl.calculateTotalDues();
        }
      }
      var vps = newValue['virtual_private_servers'];
      // You must have at least as many CPU and RAM
      // as VPS.
      if (vps > 0) {
        if (vps > ctrl.fields["ram"]) {
          ctrl.fields["ram"] = vps;
        }
        if (vps > ctrl.fields["cpu"]) {
          ctrl.fields["cpu"] = vps;
        }
      }
      else {
        // If you decide you don't want any VPS, then
        // eliminate RAM and CPU too.
        ctrl.fields["ram"] = vps;
        ctrl.fields["cpu"] = vps;
      }
    }, true);

    ctrl.init = function() {
      // Initialize all variables.
      // Pages are: welcome -> about -> benefits -> currency -> income -> services -> extra -> confirmation
      // (although if token is present, welcome and confirmation are not displayed).
      var default_page = null;
      if (ctrl.token) {
        default_page = 'about';
        ctrl.show_unsaved_changes_warning = true;
      }
      else {
        default_page = 'welcome';
      }
      ctrl.page = $location.search().p || default_page;
      ctrl.options = {};
      // confirm_remove keeps track of related contacts that the user has clicked to delete but
      // has not yet confirmed deletion of.
      ctrl.confirm_remove = {};
      ctrl.options["languages"] = [
        {
        'id': 'en_US',
        'name': ts('English')
        },
        {
        'id': 'es_ES',
        'name': ts('Spanish')
        }
      ];
      ctrl.fetchCountryOptions();

      if (ctrl.token) {
        ctrl.getMembershipDetails();
      }
      else {
        // Initialize some default values so the calculated part at the bottom
        // fills out nicely.
        ctrl.fields["currency"] = 'USD';
        ctrl.fields["membership_type"] = 'Annual';
        ctrl.fields["additional_resources"] = "0";
        ctrl.fields['virtual_private_servers'] = 0;
        ctrl.fields['cpu'] = 0;
        ctrl.fields['extra_storage'] = 0;
        ctrl.fields['ram'] = 0;
        ctrl.fields['ssd'] = 0;
        ctrl.fetchDuesSchedule();
      }

    };

    ctrl.updateDisplayPreferredLanguage = function(newPreferredLanguage) {
      // When a preferred language is selected, ensure the displayed language
      // on the confirmation page shows something friendlier then "en_US".
      if (newPreferredLanguage == 'en_US') {
        ctrl.display_preferred_language = ts('English');
      }
      else {
        ctrl.display_preferred_language = ts('Spanish');
      }
    }
    ctrl.fetchDuesSchedule = function() {
      crmApi4(
        'Dues',
        'Schedule',
        { 'currency': ctrl.fields["currency"] }
      ).then(function(result) {
        ctrl.duesSchedule = result[0];
        // Get the current exchange rate.
        return crmApi4(
          'Dues',
          'Exchange'
        );
      }).then(function(result) {
        ctrl.exchange_rate = result[0]['rate'];
      });
    };

    ctrl.journeyClass = function(page) {
      if (ctrl.page == page) {
        return 'mayfirst-save-journey-active';
      }
      else {
        return '';
      }
    };

    ctrl.fetchCountryOptions = function() {
      crmApi4(
        'MayfirstMember', 
        'GetCountries', 
      ).then(function(result) {
        ctrl.options['countries'] = result[0];
      });
    };
    ctrl.fetchStateProvinceOptions = function() {
      if (!ctrl.fields.country || !ctrl.fields.country["id"]) {
        return;
      }
      crmApi4(
        'MayfirstMember', 
        'GetStateProvinces', 
        { countryId: ctrl.fields.country["id"] }
      ).then(function(result) {
        ctrl.options['stateProvinces'] = result[0];
      });
    };

    ctrl.getMembershipDetails = function () {
      crmApi4(
        'MayfirstMember', 
        'GetMembershipDetails', 
        { 
          'token': ctrl.token
        }
      ).then(function(result) {
        ctrl.fields.street_address = result[0]['street_address'];
        ctrl.fields.city = result[0]['city'];
        ctrl.fields.country = { 'id': result[0]['country_id'] };
        // Update state province options so our state is displayed.
        ctrl.fetchStateProvinceOptions();
        ctrl.fields.state_province = { 'id': result[0]['state_province_id'], 'name': result[0]['state_province_id:name'] };
        ctrl.fields.first_name = result[0]['first_name'];
        ctrl.fields.last_name = result[0]['last_name'];
        ctrl.fields.organization_name = result[0]['organization_name'];
        ctrl.fields.why_join = result[0]['why_join'];
        ctrl.fields.how_hear = result[0]['how_hear'];
        ctrl.fields.membership_type = result[0]['membership_type'];
        ctrl.fields.contact_type = result[0]['contact_type'];
        ctrl.fields.hosting_benefits = result[0]['hosting_benefits'].toString();
        ctrl.fields.currency = result[0]['currency'];
        ctrl.fields.plan = result[0]['plan'].toString() || "0";
        ctrl.fields.extra_storage = result[0]['extra_storage'] || 0;
        
        ctrl.fields.income = result[0]['income'].toString() || "0";
        ctrl.fields.related_contacts = result[0]['related_contacts'] || {};
        ctrl.fields.domain_name = result[0]['domain_name'] || '';
        ctrl.fields.dues_reduction_description = '';

        // You cannot provision virtual private servers when editing your membership
        // details. You must request via support.
        ctrl.fields.additional_resources = "0";

        // These are still stored so when we update the membership the correct values
        // are kept.
        ctrl.fields.virtual_private_servers = result[0]['virtual_private_servers'] || 0;
        ctrl.fields.cpu = result[0]['cpu'] || 0;
        ctrl.fields.ram = result[0]['ram'] || 0;
        ctrl.fields.ssd = result[0]['ssd'] || 0;

        // Now that we have currency set, we should fetch the dues schedule.
        ctrl.fetchDuesSchedule();
      }).catch(function (err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });

    }
    ctrl.validate = function(page) {
      ctrl.clearMessage();
      var params = { fields: ctrl.fields, page: page, token: ctrl.token};
      crmApi4(
        'MayfirstMember', 
        'Validate', 
        params 
      ).then(function(result) {
        ctrl.page = result[0]['nextPage'];
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });

    }

    ctrl.previousPage = function(currentPage) {
      ctrl.clearMessage();
      if (currentPage == 'about') {
        ctrl.page = 'welcome';
      }
      if (currentPage == 'benefits') {
        ctrl.page = 'about';
      }
      if (currentPage == 'currency') {
        ctrl.page = 'benefits';
      }
      if (currentPage == 'income') {
        ctrl.page = 'currency';
      }
      if (currentPage == 'services') {
        ctrl.page = 'income';
      }
      if (currentPage == 'extra') {
        ctrl.page = 'services';
      }
      if (currentPage == 'confirm') {
        if (ctrl.fields.hosting_benefits == 0) {
          ctrl.page = 'currency';
        }
        else if (ctrl.fields.additional_resources == "0") {
          ctrl.page = 'services';
        }
        else {
          ctrl.page = 'extra';
        }
      }
    }

    /*
     * Clear hosting values
     *
     * If someone goes through the whole application and then decides
     * they don't want hosting benefits, we have to clear out any
     * hosting values they chose so their total returns to a basic
     * membership total.
     */
    ctrl.clearHostingBenefits = function() {
      ctrl.fields["virtual_private_servers"] = 0;
      ctrl.fields["cpu"] = 0;
      ctrl.fields["ram"] = 0;
      ctrl.fields["ssd"] = 0;
      ctrl.fields["additional_information"] = "";
      ctrl.fields["additional_resources"] = "0";
      ctrl.fields["extra_storage"] = 0;
    };

    /*
     * Clear VPS values
     *
     * If someone chooses a VPS and then changes
     * their mind and clicks NO to extra services,
     * clear out the VPS choices.
     */
    ctrl.clearVpsValues = function() {
      ctrl.fields["virtual_private_servers"] = 0;
      ctrl.fields["cpu"] = 0;
      ctrl.fields["ram"] = 0;
      ctrl.fields["ssd"] = 0;
    };

    ctrl.calculateTotalDues = function() {
      var params = {
        // Our Calculate API expects certain values to be integers and if they are
        // not, it throws cryptic user errors. So force decimals to integers to
        // avoid the errors, and we give better feedback in our call to Validate.
        frequency: ctrl.fields.membership_type || 'Annual',
        virtualPrivateServers: Math.round(ctrl.fields.virtual_private_servers) || 0,
        extraStorage: Math.round(ctrl.fields.extra_storage) || 0,
        income: Math.round(ctrl.fields.income) || null,
        plan: Math.round(ctrl.fields.plan) || ctrl.fields.income || 1,
        cpu: Math.round(ctrl.fields.cpu) || 0,
        ram: Math.round(ctrl.fields.ram) || 0,
        ssd: Math.round(ctrl.fields.ssd) || 0,
        currency: ctrl.fields.currency || 'USD',
        hostingBenefits: ctrl.fields.hosting_benefits || 0
      };

      // A bit of a hack is necessary. If someone chooses 1 virtual private
      // server before they have had a chance to choose one CPU, they will
      // immediately get a validation error. We smooth over that immediate
      // error by calculating what the minimum dues would be if the params
      // were correct. When they click Next on the form, they will get the
      // validation error and will be able to change their values.
      if (params["virtualPrivateServers"] > params["cpu"]) {
        params["cpu"] = params["virtualPrivateServers"];
      }
      if (params["virtualPrivateServers"] > params["ram"]) {
        params["ram"] = params["virtualPrivateServers"];
      }
      // Clear any existing errors
      ctrl.clearMessage('error');
      crmApi4(
        'Dues', 
        'Calculate', 
        params 
      ).then(function(result) {
        ctrl.dues = result[0]['dues'];
        ctrl.dues_usd = result[0]['dues_usd'];
        ctrl.storage = result[0]['storage'];
        if (ctrl.fields.membership_type == 'Annual') {
          ctrl.display_frequency = ts('Year');
        }
        else {
          ctrl.display_frequency = ts('Month');
        }
      }).catch(function(err) {
        console.log(err);
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });
    };

    ctrl.saveMembership = function() {
      ctrl.submitButtonText = ts("Saving...");
      crmApi4(
        'MayfirstMember', 
        'Validate', 
        { fields: ctrl.fields, page: 'confirm', token: ctrl.token } 
      ).then(function(result) { 
        // Sometimes we get API errors that don't trigger
        // an exception. Boo. Check for those manually to avoid
        // having someone submit a request and think it went 
        // through properly.
        if (result['error_message']) {
          ctrl.submitButtonText = ts("Submit Membership Details");
          throw ts("Oops. There was an error! Please contact support. The returned error is: ") + result['error_message'];
        }
        var autoAdjustEndDate = false;
        if (ctrl.token) {
          // If we are updating a membership, then auto update expiration date if
          // they are increasing their plan or disk alotment. 
          autoAdjustEndDate = true;
        }

        return crmApi4(
          'MayfirstMember', 
          'Save', 
          { 
            fields: ctrl.fields, 
            token: ctrl.token,
            autoAdjustEndDate: autoAdjustEndDate
          } 
        );
      }).then(function(result) {
        if (result['error_message']) {
          ctrl.submitButtonText = ts("Submit Membership Details");
          throw ts("Oops. There was an error! Please contact support. The returned error is: ") + result['error_message'];
        }
        // Hide all other elements to ensure they see this message
        document.getElementById('confirmForm').style.display = 'none';
        var msg;
        if (ctrl.token) {
          ctrl.submitButtonText = ts("Submit Membership Details");
          ctrl.show_unsaved_changes_warning = false;
          msg = ts("Your changes have been saved!");
          msg += ' <a href="' + ctrl.languageUrlPrefix + '/civicrm/public/pay#/mayfirst/pay?t=' +  ctrl.token +  '">'  + ts('Pay dues.') + '</a>';
        }
        else {
          msg = ts("Thank you for submitting your membership! We review all requests manually, so it may take up to 24 hours to process. When we do you will receive an email with the full details of how to get involved in May First and how to use your hosting services.");
          // Also hide the submit and previous buttons to avoid double submissions
          document.getElementById('mayfirst-save-buttons').style.display = 'none';
        }
        ctrl.showMessage(msg, 'success');
      }).catch(function(err) {
        var msg = ts("Sorry, there was an error.");
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });

    };

    ctrl.clearMessage = function(type = 'error') {
      if (type == 'error') {
        var el = document.getElementById('mayfirst-save-error-message');
      }
      else {
        var el = document.getElementById('mayfirst-save-success-message');
      }
      el.style.display = 'none';
    };

    ctrl.showMessage = function(msg, type = 'error') {
      if (type == 'error') {
        var el = document.getElementById('mayfirst-save-error-message');
      }
      else {
        var el = document.getElementById('mayfirst-save-success-message');
      }
      el.innerHTML = msg;
      el.style.display = 'block';
    };

    ctrl.clearMessage = function () {
      var errorEl = document.getElementById('mayfirst-save-error-message');
      var successEl = document.getElementById('mayfirst-save-success-message');
      errorEl.innerHTML = '';
      errorEl.style.display = 'none';
      successEl.innerHTML = '';
      successEl.style.display = 'none';

    };

    ctrl.showRelatedContactForm = function(contact = {}) {
      ctrl.show_related_contact_form = true;
      contact.preferred_language = { id: contact.preferred_language || 'en_US'  };
      ctrl.related_contact = contact;
    };
    ctrl.addRelatedContact = function () {
      return crmApi4(
        'MayfirstMember', 
        'AddRelatedContact', 
        { 
          'token': ctrl.token,
          'contact': ctrl.related_contact
        }
      ).then(function() {
        ctrl.show_related_contact_form = false;
        ctrl.init();
      }).catch(function(err) {
        var msg = err;
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });
    };

    ctrl.removeRelatedContact = function(id) {
      ctrl.confirm_remove[id] = true;
    };
    ctrl.confirmRemoveRelatedContact = function (id) {
      return crmApi4(
        'MayfirstMember', 
        'RemoveRelatedContact', 
        { 
          'token': ctrl.token,
          'id': id 
        }
      ).then(function() {
        ctrl.show_related_contact_form = false;
        ctrl.init();
      }).catch(function(err) {
        var msg = err;
        if (err.error_message) {
          msg = err.error_message;
        }
        ctrl.showMessage(msg);
      });
    };

    ctrl.toggleLanguage = function() {
      if (ctrl.currentLanguage == 'es') {
        // We in spanish, switch to english.
        var pathname = ctrl.url.pathname.substring(3);
        location.href = pathname + ctrl.url.hash + ctrl.url.search;
      }
      else {
        // We are in english, switch to spanish.
        location.href = '/es' + ctrl.url.pathname + ctrl.url.hash + ctrl.url.search;
      }
    }
    ctrl.init();

  });
})(angular, CRM.$, CRM._);

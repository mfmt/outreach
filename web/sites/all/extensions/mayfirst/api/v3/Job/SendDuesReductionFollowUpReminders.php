<?php


/**
 * This job sends out dues reduction follow up reminders. 
 *
 * @param array $params
 *
 * @return array
 *   API result array.
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_send_dues_reduction_follow_up_reminders($params) {
  $results = \Civi\Api4\MayfirstMember::sendDuesReductionFollowUpReminders()
    ->execute()->first();
  
  return civicrm_api3_create_success($results, $params);
}



<?php


/**
 * This job sends payment receipts for any membership contribution
 * that has not had a receipt sent. 
 *
 * @param array $params
 *
 * @return array
 *   API result array.
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_send_recent_payment_receipts($params) {
  $results = \Civi\Api4\MayfirstMember::SendRecentPaymentReceipts()
    ->execute()->first();
  
  $sent_count = $results['sent_count']; 
  $msg = "Successfully sent {$sent_count} payment receipts.";
  return civicrm_api3_create_success($msg, $params);
}



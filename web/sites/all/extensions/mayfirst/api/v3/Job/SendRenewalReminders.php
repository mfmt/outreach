<?php


/**
 * This job sends out all membership dues notifications. 
 *
 * @param array $params
 *
 * @return array
 *   API result array.
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_send_renewal_reminders($params) {
  $results = \Civi\Api4\MayfirstMember::sendRenewalReminders()
    ->execute()->first();
  
  $sent_count = $results['sent_count']; 
  $msg = "Successfully sent {$sent_count} renewal reminders.";
  if ($results['failed_count'] == 0) {
    $msg .= " No failures.";
    return civicrm_api3_create_success($msg, $params);
  }
  $msg .= " No recipients found to send a renewal reminder for the following membership_ids: ";
  $msg .= implode(',', $results['failed_member_ids']);
  $msg .= " Please update those memberships and re-run this scheduled job.";
  return civicrm_api3_create_error($msg);
}



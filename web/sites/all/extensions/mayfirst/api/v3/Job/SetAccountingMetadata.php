<?php


/**
 * This job updates membership contribution meta data
 * so we can record when a membership contribution
 * should count as income and if any of it should be
 * deferred to next year. 
 *
 * @param array $params
 *
 * @return array
 *   API result array.
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_set_accounting_metadata($params) {
  $results = \Civi\Api4\MayfirstMember::SetAccountingMetadata()
    ->execute()->first();
  $count = $results['count']; 
  $msg = "Successfully processed {$count} contributions.";
  return civicrm_api3_create_success($msg, $params);
}



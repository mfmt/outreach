<?php


/**
 * This job copies storage data from our control panel to Civi. 
 *
 * @param array $params
 *
 * @return array
 *   API result array.
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_sync_control_panel_usage($params) {
  $results = \Civi\Api4\ControlPanel::syncUsage()
    ->execute()->first();
  
  return civicrm_api3_create_success($results, $params);
}

/**
 * @param array $params
 *
 */
function _civicrm_api3_job_sync_control_panel_usage_spec(&$params) {
  $params['sync_control_panel_usage']['title'] = 'May First Sync Storage Statistics';
  $params['sync_control_panel_usage']['description'] = 'Synchronize usage data from our Control Panel to Civi.';
}

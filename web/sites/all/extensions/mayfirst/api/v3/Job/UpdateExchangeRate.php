<?php


/**
 *
 * This job gets the latest exchange rate from the
 * IMF and updates our cache with that rate.
 *
 * We don't know when the IMF will update the exchange rate
 * for today, so we run every few hours to ensure we get it
 * stored historically.
 *
 * @param array $params
 *
 * @return array
 *   API result array.
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_update_exchange_rate($params) {
  $results = \Civi\Api4\Dues::Exchange()
    ->execute()->first();
  return civicrm_api3_create_success($results, $params);
}



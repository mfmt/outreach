(function($, _, ts) {
  $(document).on('crmLoad', function(e) {
    if ($('#mayfirst-recalculated-dues-label').length == 0) {
      $('<span id="mayfirst-recalculated-dues-label">Recalculated: </span>$<span id="mayfirst-recalculated-dues"></span> USD / <span id="mayfirst-recalculated-storage"></span> GB / <span id="mayfirst-recalculated-end-date"></span>').insertAfter('[data-crm-custom="Membership_Details:Renewal_Amount"]');
      $('[data-crm-custom="Membership_Details:Plan"]').change(updateCalculatedDues);
      $('[data-crm-custom="Membership_Details:Hosting_Benefits"]').change(updateCalculatedDues);
      $('[data-crm-custom="Membership_Details:Virtual_Private_Servers"]').change(updateCalculatedDues);
      $('[data-crm-custom="Membership_Details:cpu"]').change(updateCalculatedDues);
      $('[data-crm-custom="Membership_Details:ram"]').change(updateCalculatedDues);
      $('[data-crm-custom="Membership_Details:ssd"]').change(updateCalculatedDues);
      $('[data-crm-custom="Membership_Details:Extra_Storage"]').change(updateCalculatedDues);
      $('#membership_type_id_1').change(updateCalculatedDues);
      updateCalculatedDues();
    }
  });

  function updateCalculatedDues() {
    var plan;
    var hostingBenefits;
    var virtualPrivateServers = parseInt($('[data-crm-custom="Membership_Details:Virtual_Private_Servers"]').val());
    var cpu = parseInt($('[data-crm-custom="Membership_Details:Cpu"]').val());
    var ram = parseInt($('[data-crm-custom="Membership_Details:Ram"]').val());

    if (virtualPrivateServers > cpu) {
      $('[data-crm-custom="Membership_Details:Cpu"]').val(virtualPrivateServers);
    }
    if (virtualPrivateServers > ram) {
      $('[data-crm-custom="Membership_Details:Ram"]').val(virtualPrivateServers);
    }
    $('[data-crm-custom="Membership_Details:Plan"]').each(function() {
      if (this.checked) {
        plan = parseInt(this.value);
      }
    });
    $('[data-crm-custom="Membership_Details:Hosting_Benefits"]').each(function() {
      if (this.checked) {
        hostingBenefits = parseInt(this.value);
      }
    });
    if (!plan) {
      // There's a timing issue and sometimese this triggers before the plan is filled out and we get an
      // error. Just return to avoid it.
      return;
    }
    CRM.api4('Dues', 'calculate', {
      plan: plan,
      hostingBenefits: hostingBenefits,
      cpu: parseInt($('[data-crm-custom="Membership_Details:Cpu"]').val()),
      virtualPrivateServers: parseInt($('[data-crm-custom="Membership_Details:Virtual_Private_Servers"]').val()),
      ram: parseInt($('[data-crm-custom="Membership_Details:Ram"]').val()),
      ssd: parseInt($('[data-crm-custom="Membership_Details:Ssd"]').val()),
      extraStorage: parseInt($('[data-crm-custom="Membership_Details:Extra_Storage"]').val()),
      income: CRM.vars.mayfirst_edit_membership.income,
      frequency: $("#membership_type_id_1 option:selected").text(),
    }).then(function(results) {
      $('#mayfirst-recalculated-dues').html(results[0]['dues_usd']);
      $('#mayfirst-recalculated-storage').html(results[0]['storage']);
      // Get the current dues for this membership.
      return CRM.api4('Membership', 'get', {
        select: ["Membership_Details.Renewal_Amount"],
        where: [["id", "=", CRM.vars.mayfirst_edit_membership.membership_id]],
      });
    }).then(function(results) {
      params = {
        startDate: $('#start_date').val(),
        endDate: $('#end_date').val(),
        // Pull in old dues from database in case user changes it on form.
        //oldDues: parseInt(results[0]["Membership_Details:Renewal_Amount"]),
        oldDues: parseInt(results[0]["Membership_Details.Renewal_Amount"]),
        newDues: parseInt($('#mayfirst-recalculated-dues').text()),
      };
      return CRM.api4('Dues', 'EndDateForDuesOwed', params);
    }).then(function(results) {
      $('#mayfirst-recalculated-end-date').html(results[0]['date']);
    }).catch(function(error) {
      if (error.error_message) {
        alert(error.error_message);
      }
      else {
        alert(error);
      }
    });
  }
})(CRM.$, CRM._, CRM.ts('mayfirst'));


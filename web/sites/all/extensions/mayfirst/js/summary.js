(function($, _, ts) {
  var reminderSent = false;
  var receiptSent = false;
  var quotaSet = false;
  $(document).on('crmLoad', function(e) {
    $('#mayfirst-send-payment-receipt').click(function() {
      if (receiptSent) {
        console.log("Receipt button clicked twice.");
        return;
      }
      receiptSent = true;
      // disable to prevent accidental multiple clicks.
      $('#mayfirst-send-payment-receipt').prop('disabled', true);
      $('#mayfirst-send-payment-receipt').addClass('btn-secondary');
      CRM.api4('MayfirstMember', 'sendPaymentReceipt', {
        membershipId: CRM.vars.mayfirst_summary.membership_id,
        contributionId: CRM.vars.mayfirst_summary.contribution_id
      }).then(function(results) {
        console.log(results);
        alert("The receipt was sent!");
      }, function(failure) {
        console.log(failure);
        alert("Failed to send! Check console log for details.");
      });
    });
    $('#mayfirst-send-renewal-reminder').click(function() {
      if (reminderSent) {
        console.log("Reminder button clicked twice.");
        return;
      }
      reminderSent = true;
      // disable to prevent accidental multiple clicks.
      $('#mayfirst-send-renewal-reminder').prop('disabled', true);
      $('#mayfirst-send-renewal-reminder').addClass('btn-secondary');
      CRM.api4('MayfirstMember', 'sendSingleRenewalReminder', {
        membershipId: CRM.vars.mayfirst_summary.membership_id 
      }).then(function(results) {
        console.log(results);
        alert("The email was sent!");
      }, function(failure) {
        console.log(failure);
        alert("Failed to send! Check console log for details.");
      });
    });
    $('#mayfirst-update-control-panel-quota').click(function() {
      if (quotaSet) {
        console.log("Set Quota button clicked twice.");
        return;
      }
      quotaSet = true;
      // disable to prevent accidental multiple clicks.
      $('#mayfirst-update-control-panel-quota').prop('disabled', true);
      $('#mayfirst-update-control-panel-quota').addClass('btn-secondary');
      CRM.api4('ControlPanel', 'UpdateQuota', {
        membershipId: CRM.vars.mayfirst_summary.membership_id 
      }).then(function(results) {
        console.log(results);
        alert("The member quota was updated.");
      }, function(failure) {
        console.log(failure);
        alert("Failed to update the quota! Check console log for details.");
      });
    });

    $('#mayfirst-copy-pay-token').click(function() {
      navigator.clipboard.writeText(CRM.vars.mayfirst_summary.pay_token);
    });
    $('#mayfirst-copy-edit-token').click(function() {
      navigator.clipboard.writeText(CRM.vars.mayfirst_summary.edit_token);
    });
  });
})(CRM.$, CRM._, CRM.ts('mayfirst'));

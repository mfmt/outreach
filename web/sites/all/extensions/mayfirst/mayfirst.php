<?php

require_once 'mayfirst.civix.php';
use CRM_Mayfirst_ExtensionUtil as E;

/**
 * Add token services to the container.
 *
 * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
 */
function mayfirst_civicrm_container($container) {
  $container->addResource(new \Symfony\Component\Config\Resource\FileResource(__FILE__));
  $container->findDefinition('dispatcher')->addMethodCall('addListener',
    ['civi.token.list', 'mayfirst_register_tokens']
  )->setPublic(TRUE);
  $container->findDefinition('dispatcher')->addMethodCall('addListener',
    ['civi.token.eval', 'mayfirst_evaluate_tokens']
  )->setPublic(TRUE);
}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function mayfirst_civicrm_config(&$config) {
  _mayfirst_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function mayfirst_civicrm_install() {
  _mayfirst_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function mayfirst_civicrm_enable() {
  _mayfirst_civix_civicrm_enable();

  
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function mayfirst_civicrm_managed(&$entities) {
  // After generating our managed entities, we have to update some records.

  // We have to update our custom group to restrict it to our
  // membership types.
  $monthlyId = NULL;
  $annualId = NULL;
  $monthly = \Civi\Api4\MembershipType::get()
    ->setWhere([ ['name', '=', 'Monthly'] ])
    ->setSelect(['id'])
    ->execute()->first();

  if ($monthly) {
    $monthlyId = $monthly['id'];
   }
  $annual = \Civi\Api4\MembershipType::get()
    ->setWhere([ ['name', '=', 'Annual'] ])
    ->setSelect(['id'])
    ->execute()->first();

  if ($annual) {
    $annualId = $annual['id'];
  }

  if ($annualId && $monthlyId) {
    // Update the custom group
    \Civi\Api4\CustomGroup::update()
      ->addValue('extends_entity_column_value', [ $monthlyId, $annualId ])
      ->addValue('serialize', '')
      ->setWhere([ ['name', '=', 'Membershp_Details'] ])
      ->execute();
  }
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function mayfirst_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function mayfirst_civicrm_navigationMenu(&$menu) {
  _mayfirst_civix_insert_navigation_menu($menu, 'Memberships', array(
    'label' => E::ts('Review pending membership requests'),
    'name' => 'mayfirst_pending_memberships',
    'url' => 'civicrm/a/#/mayfirst/review',
    'permission' => 'edit memberships',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _mayfirst_civix_insert_navigation_menu($menu, 'Memberships', array(
    'label' => E::ts('Review flagged memberships'),
    'name' => 'mayfirst_problematic_memberships',
    'url' => 'civicrm/a/#/mayfirst/problem',
    'permission' => 'edit memberships',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _mayfirst_civix_insert_navigation_menu($menu, 'Memberships', array(
    'label' => E::ts('Manually renew memberships'),
    'name' => 'mayfirst_manual_renewal',
    'url' => 'civicrm/a/#/mayfirst/manual',
    'permission' => 'edit memberships',
    'operator' => 'OR',
    'separator' => 0,
  ));

  _mayfirst_civix_navigationMenu($menu);
}

/**
 * Implementation of hook_civicrm_tokens
 */
function mayfirst_register_tokens(\Civi\Token\Event\TokenRegisterEvent $e) {
  $e->entity('mayfirst')
    ->register('member_contact_id', ts('May First Membership Contact Id'))
    ->register('member_contact_link', ts('May First Link to Membership Contact record'))
    ->register('membership_pay_token', ts('May First Membership Payment Token'))
    ->register('membership_edit_token', ts('May First Membership Payment Token (with edit privileges)'))
    ->register('membership_renewal_link', ts('May First payment link'))
    ->register('membership_renewal_link_edit_privs', ts('May First payment link with edit privileges'))
    ->register('membership_edit_link', ts('May First edit membership link'))
    ->register('membership_admin_edit_link', ts('May First admin edit contact link'))
    ->register('membership_renewal_due_date', ts('May First membership due date'))
    ->register('membership_next_term_end_date', ts('Date the May First membership will end if renewed'))
    ->register('membership_renewal_amount', ts('May First membership amount due in USD'))
    ->register('membership_renewal_amount_localized', ts('May First membership amount due in the requested currency'))
    ->register('membership_renewal_reference_id', ts('A unique id for members who need a reference number when paying, composed of the membership expiration date and membership id'))
    ->register('membership_currency', ts('May First membership currency'))
    ->register('membership_frequency', ts('May First membership frequency/term (annual or monthly)'))
    ->register('why_join', ts('May First membership why join'))
    ->register('first_name_or_compa', ts('The contacts first name or "compa" if first name is empty'))
    ->register('member_name', ts('The name of the contact holding the membership'))
    ->register('exchange_rate', ts('The current USD-MXN exchange rate'))
    ->register('exchange_rate_explanation', ts('Caveat about changing exchange rates, only filled in if MXN'))
    ->register('spanish_pronoun_explanation', ts('Explanation of our user of tu/su/usted, only filled in if in spanish.'))
    ->register('membership_auto_renew_notice', ts('A notice if automatic renewal is enabled for this membership'));
}

function mayfirst_evaluate_tokens(\Civi\Token\Event\TokenValueEvent $e) {
  // These are the only tokens we care about:
  $ourTokens = [
    'member_name',
    'member_contact_id',
    'member_contact_link',
    'membership_edit_token',
    'membership_pay_token',
    'membership_renewal_link',
    'membership_renewal_link_edit_privs',
    'membership_edit_link',
    'membership_admin_edit_link',
    'membership_renewal_due_date',
    'membership_next_term_end_date',
    'membership_renewal_amount',
    'membership_renewal_amount_localized',
    'membership_renewal_reference_id',
    'membership_currency',
    'membership_frequency',
    'first_name_or_compa',
    'why_join',
    'exchange_rate',
    'exchange_rate_explanation',
    'spanish_pronoun_explanation',
    'membership_auto_renew_notice',
  ];

  $messageTokens = $e->getTokenProcessor()->getMessageTokens();
  $intersect = array_intersect($ourTokens, $messageTokens['mayfirst'] ?? []);
  if (count($intersect) == 0) {
    // None of the tokens in this message match our tokens so bail early.
    return;
  }
  foreach ($e->getRows() as $row) {
    $contactId = $row->context['contactId'] ?? NULL;
    $membershipId = $row->context['membershipId'] ?? NULL;
    $contributionId = $row->context['contributionId'] ?? NULL;
    $values = mayfirst_get_token_values($contactId, $membershipId, $contributionId);
    $row->format('text/plain');
    foreach($values as $key => $val) {
      // For some reason setting an empty token seems to cause
      // problems.
      if ($val) {
        $row->tokens('mayfirst', $key, $val);
      }
    }
  }
}

/**
 * Return token values
 *
 * Given a contactId and MembershipId, return the values
 * of the tokens we would need to fill.
 */
function mayfirst_get_token_values($contactId, $membershipId, $contributionId) {
  // We minimally need a contactId and a membershipId
  if (empty($contactId) || empty($membershipId)) {
    return [];
  }
  // Get the name of the membership
  $member = \Civi\Api4\Membership::get()
    ->setCheckPermissions((FALSE))
    ->addSelect('contact_id', 'contact_id.display_name')
    ->addWhere('id', '=', $membershipId)
    ->execute()->first();

  $memberName = $member['contact_id.display_name'];
  $memberContactId = $member['contact_id'];
  $memberContactLink = CIVICRM_UF_BASEURL . '/civicrm/contact/view?reset=1&cid=' . $member['contact_id'];

  // Check for first name and language pref.
  $contactDetails = \Civi\Api4\Contact::get()
    ->setCheckPermissions((FALSE))
    ->addSelect('first_name', 'preferred_language')
    ->addWhere('id', '=', $contactId)
    ->execute()->first();
  $firstNameOrCompa = $contactDetails['first_name'];
  $lang = $contactDetails['preferred_language'];
  if (empty($firstNameOrCompa)) {
    $firstNameOrCompa = 'Compa';
  }

  // Get the contribution if contributionId is set
  $contribution = NULL;
  $paymentAmount = NULL;
  $paymentDate = NULL;
  $paymentCurrency = NULL;
  $paymentTrxnId = NULL;
  $paymentCheckNumber = NULL;
  $renewalReferenceId = NULL;
  if ($contributionId) {
    $contribution = \Civi\Api4\Contribution::get()
      ->addSelect(
        'total_amount',
        'receive_date',
        'currency',
        'trxn_id',
        'check_number',
      )
      ->setCheckPermissions(FALSE)
      ->addWhere('id', '=', $contributionId)
      ->execute()->first();
    $invoiceStartDate = substr(\Civi\Mayfirst\Utils::getInvoiceStartDateForContribution($contributionId), 0, 10);
    $paymentAmount = $contribution['total_amount'];
    $paymentDate = $contribution['receive_date'];
    $paymentCurrency = $contribution['currency'];
    $paymentTrxnId = $contribution['trxn_id'];
    $paymentCheckNumber = $contribution['check_number'];
    $renewalReferenceId = str_replace('-', '', $invoiceStartDate) . $membershipId;
  }

  // Now get the token data for this membership id.
  $expire = strtotime("+2 weeks");
  $editToken = \Civi\Mayfirst\Utils::getToken($membershipId, 'edit', $expire);
  $payToken = \Civi\Mayfirst\Utils::getToken($membershipId, 'pay', $expire);
  $renewLink = CIVICRM_UF_BASEURL .
    '/civicrm/public/pay#/mayfirst/pay?t=' .
    $payToken;

  $renewLinkEditPrivs = CIVICRM_UF_BASEURL .
    '/civicrm/public/pay#/mayfirst/pay?t=' .
    $editToken;

  $editLink = CIVICRM_UF_BASEURL .
    '/civicrm/public/save#/mayfirst/save?t=' .
    $editToken;
  $adminEditLink = CIVICRM_UF_BASEURL .
    'civicrm/contact/view?reset=1&cid=' .
    $contactId;

  $membershipDetails = \Civi\Api4\Membership::get()
    ->setCheckPermissions((FALSE))
    ->addWhere('id', '=', $membershipId)
    ->addSelect(
      'id',
      'Membership_Details.Renewal_Amount',
      'Membership_Details.Currency:name',
      'end_date',
      'membership_type_id:name')
    ->execute()->first();

  $currency = $membershipDetails['Membership_Details.Currency:name'];
  $frequency = $membershipDetails['membership_type_id:name'];
  $renewalAmount = $membershipDetails['Membership_Details.Renewal_Amount'];
  $renewalAmountLocalized = $renewalAmount;
  $exchangeRateExplanation = "";
  $exchangeRate = "";
  $spanishPronounExplanation = "";
  if ($currency == 'MXN') {
    $exch = \Civi\Api4\Dues::Exchange()
      ->setCheckPermissions(FALSE)
      ->setMembershipId($membershipId)
      ->execute()->first();
    $exchangeRate = $exch['rate'];
    $exchangeRateExplanation = E::ts("Your dues are calculated based on a USD exchange rate of %1.", [ 1 => $exchangeRate ]) . " " . $exch['explanation'];
  }
  if ($lang == 'es_ES') {
    $spanishPronounExplanation = E::ts("We used tu/su/usted in this way for these reasons.");
  }

  // Get the why_join value. This value is useful when sending an email to our welcome
  // committee to ask them to welcome this member.
  $result = \Civi\Api4\Contact::get()
    ->setCheckPermissions(FALSE)
    ->addSelect('Member_Info.Why_Join')
    ->addWhere('id', '=', $memberContactId)
    ->execute();
  $whyJoin = $result->first()['Member_Info.Why_Join'];

  if ($currency == 'MXN') {
    // Calculate the localized renewal amount.
    $renewalAmountLocalized = round($exchangeRate * $renewalAmount);
  }

  $membershipAutoRenewNotice = '';
  if (\Civi\Mayfirst\Utils::membershipAutoRenewInProgress($membershipId)) {
    $status = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('status_id:name')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first()['status_id:name'];
    if ($status == 'Current') {
      $membershipAutoRenewNotice = E::ts('Note: Your membership is setup with automatic renewal so there is no need to manually submit your payment. You will be charged automatically.');
    }
    else {
      $membershipAutoRenewNotice = E::ts('Note: Your membership is setup with automatic renewal. However, your membership is not current so something may be wrong. Please email info@mayfirst.org to learn more.');
    }
  }

  $nextTermEndDate = DateTime::createFromFormat('Y-m-d', $membershipDetails['end_date']);
  if ($frequency == 'Monthly') {
    $interval = new \DateInterval('P1M');
  }
  else {
    $interval = new \DateInterval('P1Y');
  }
  $nextTermEndDateDisplay = $nextTermEndDate->add($interval)->format("Y-m-d");

  // Note: if we have a contribution id, then we pull the referenceId from the
  // contribution, not the membership.
  if (empty($renewalReferenceId)) {
    $renewalReferenceId = str_replace('-', '', $membershipDetails['end_date']) . $membershipDetails['id'];
  }
  $values = [
    'member_name' => $memberName,
    'member_contact_id' => $memberContactId,
    'member_contact_link' => $memberContactLink,
    'first_name_or_compa' => $firstNameOrCompa,
    'membership_edit_token' => $editToken,
    'membership_pay_token' => $payToken,
    'membership_renewal_link' => $renewLink,
    'membership_renewal_link_edit_privs' => $renewLinkEditPrivs,
    'membership_edit_link' => $editLink,
    'membership_admin_edit_link' => $adminEditLink,
    'membership_renewal_amount' => $renewalAmount,
    'membership_renewal_amount_localized' => $renewalAmountLocalized,
    'membership_renewal_reference_id' => $renewalReferenceId,
    'membership_currency' => $currency,
    'membership_frequency' => $frequency,
    'membership_renewal_due_date' => $membershipDetails['end_date'],
    'membership_next_term_end_date' => $nextTermEndDateDisplay,
    'exchange_rate' => $exchangeRate,
    'exchange_rate_explanation' => $exchangeRateExplanation,
    'spanish_pronoun_explanation' => $spanishPronounExplanation,
    'membership_auto_renew_notice' => $membershipAutoRenewNotice,
    'why_join' => $whyJoin,
    'payment_date' => $paymentDate,
    'payment_amount' => $paymentAmount,
    'payment_currency' => $paymentCurrency,
    'payment_trxn_id' => $paymentTrxnId,
    'payment_check_number' => $paymentCheckNumber,
  ];
  return $values;
}

/**
 * Implementation of hook_civicrm_summary
 *
 * For memberships, we display the token values and put all the relevant
 * membership information in one convenient spot.
 *
 */
function mayfirst_civicrm_summary($contactId, &$content, &$contentPlacement) {
  $contentPlacement = \CRM_Utils_Hook::SUMMARY_ABOVE;
  try {
    $data = \Civi\Mayfirst\ContactSummary::getData($contactId);
    if (empty($data)) {
      // Not a member, nothing to display;
      return;
    }
  }
  catch(\API_Exception $e) {
    Civi::resources()->addStyleFile('mayfirst', 'css/summary.css');
    // Note: sometimes the membership might be configuured with an unsupported option, like a
    // hosting plan that is lower then the income level.
    $start = '<div id="mayfirst-membership-summary" class="mayfirst-api-error crm-summary-block crm-inline-block-content">';
    $title = '<h3>Membership Summary</h3>';
    $msg = "<p>Failed to query membership info for this membership. The error returned is: " . $e->getMessage() . '</p>';
    $end = '</div>';
    $content = "{$start} {$title} {$msg} {$end}";
    return;
  }

  Civi::resources()->addStyleFile('mayfirst', 'css/summary.css');
  // The javascript code handles clicking the buttons and copying the tokens.
  Civi::resources()->addScriptFile('mayfirst', 'js/summary.js')
      ->addVars('mayfirst_summary', $data['js_vars']);

  // The class determines the color of the border and varies depending on the membership status.
  $start = '<div id="mayfirst-membership-summary" class="crm-summary-block crm-inline-block-content ' . $data['class'] . '">';
  $title = '<h3>Membership Summary</h3>';
  $end = '</div>';

  // If you have allocated more then 95% or more of the disk space you are allowed, then the usage shows up red.
  $controlPanelAllocated = $data['control_panel_allocated'] . ' GB';
  if ($data['control_panel_allocated'] / $data['calculated_storage'] > .95) {
    $controlPanelAllocated = '<span class="mayfirst-overage-alert">' . $controlPanelAllocated . '</span>';
  }

  // If the calculated storage is not in sync with the control panel quota, then allowed allocation
  // is in red and a button appears allowing you to copy the calculated storage to the control panel
  // to update it.
  $showUpdateQuotaButton = FALSE;
  $allowedAllocation = $data['calculated_storage'] . ' GB';
  if ($data['control_panel_quota'] != $data['calculated_storage']) {
    $allowedAllocation = '<span class="mayfirst-overage-alert">' . $allowedAllocation . '</span>';
    $showUpdateQuotaButton = TRUE;
  }

  $controlPanelUsage = $data['control_panel_usage'] . ' GB';

  $renewalNotes = $data['renewal_notes'];
  if ($data['dues_reduction_request_pending_count'] > 0) {
    $renewalNotes .= '<br /><span class="mayfirst-pending-dues-reduction-request">' . E::ts('See un-resolved dues reduction request in Activities tab.') . '</span>';
  }
  if ($data['dues_reduction_request_completed_count'] > 0) {
    $renewalNotes .= '<br /><span>' . E::ts('See resolved dues reduction request in Activities tab.') . '</span>';
  }
  if ($data['notes_count'] > 0) {
    $renewalNotes .= '<br />' . E::ts('See additional info in Notes tab.') . '</span>';
  }
  $rows = [];
  $rows[] = [
    'Pay links and Tokens',
    $data['pay'] . " | " . $data['pay_and_edit'],
    'VPS / CPU / RAM / SSD',
    $data['virtual_private_servers'] . ' / ' . $data['cpu'] . ' / '. $data['ram'] . ' / ' . $data['ssd'],
  ];

  $rows[] = [
    'Income / Plan / Hosting Benefits',
    $data['income'] . ' / ' . $data['plan'] . ' / ' . $data['hosting_benefits'],
    'Extra Storage',
    $data['extra_storage'] . ' GB',

  ];

  $rows[] = [
    'Renewal Amount / Status / Expiration Date',
    $data['renewal_amount_friendly'] . ' / ' . $data['membership_status'] . ' / ' . $data['membership_end_date'],
    'Disk Space Used',
    $controlPanelUsage,
  ];

  $rows[] = [
    'Renewal Notes',
    $renewalNotes,
    'Allocation: Used / Allowed',
    $controlPanelAllocated . ' / ' . $allowedAllocation,
  ];

  $table = '<table class="mayfirst-summary-table">';
  foreach ($rows as $row) {
    $table .=
      "<tr>
      <th>$row[0]</th>
      <td>$row[1]</td>
      <th>$row[2]</th>
      <td>$row[3]</td>
      </tr>";
  }

  $table .= "</table>";

  $renewalButton = '<button id="mayfirst-send-renewal-reminder">Send Renewal Reminder</button>';
  $contributionAmount = $data['contribution_amount'] ?? NULL;
  $contributionDate = $data['contribution_receive_date'] ?? NULL;
  $contributionCurrency = $data['contribution_currency'] ?? NULL;
  if ($contributionAmount) {
    $contributionDate = substr($contributionDate, 0, 10);
    $receiptButton = '<button id="mayfirst-send-payment-receipt">Send Receipt for last payment ($' . "{$contributionAmount} {$contributionCurrency} on {$contributionDate})" . '</button>';
  }
  else {
    $receiptButton = NULL;
  }
  if ($showUpdateQuotaButton) {
    $updateQuotaButton = '<button id="mayfirst-update-control-panel-quota">Update Control Panel Quota to ' . $data['calculated_storage'] . ' GB </button>';
  }
  else {
    $updateQuotaButton = NULL;
  }
  $content = "{$start} {$title} {$table} {$renewalButton} {$receiptButton} {$updateQuotaButton} {$end} ";
}

/**
 * Add auto calculator to membership dues form
 *
 * When editing a membership and changing the resources included
 * with that membership, it's handy to see the dues amount and
 * total disk space updated in real time via javascript.
 **/
function mayfirst_civicrm_buildForm($formName, $form) {
  if ($formName == 'CRM_Member_Form_Membership') {
    if (!$form->_id) {
      return;
    }
    $membershipId = $form->_id;
    $income = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $membershipId)
      ->addSelect('contact_id.Member_Info.Income')
      ->execute()->first()['contact_id.Member_Info.Income'];
    $data = [
      'income' => $income,
      'membership_id' => $membershipId,
    ];
    \Civi::log()->debug("Adding vars");
    Civi::resources()->addScriptFile('mayfirst', 'js/edit-membership.js')
      ->addVars('mayfirst_edit_membership', $data);
    Civi::resources()->addStyleFile('mayfirst', 'css/edit-membership.css');

  }
}

/**
 *
 * Adjust membership status.
 *
 * We have two special membership types:
 *
 * 1. Requested is our initial membershp type, with the start and end date set
 * to the same date - the date they joined.
 *
 * Requested essentially means un-approved and it should remain in this state
 * until a human reviews it and approves it by changing it to Grace. 
 *
 * 2. Disabled means that the membership was expired, and a human has
 * intervened to disable the hosting records. 
 *
 * If we let the regular membership processing system take effect Requested
 * will get transitioned to Grace and Disabled to Expired when it runs. This
 * hook stops that from happening.
 *
 * Also, when you join for the first time, your start and end date are the same
 * and technically you are "Current" but we want you to show up as "Grace" so
 * you pay your dues and aren't confused.
 *
 */
function mayfirst_civicrm_alterCalculatedMembershipStatus(&$membershipStatus, $arguments, $membership) {
  // Only call if we are transitioning to Grace, Current, or Expired...
  $checkStatuses = ['Grace', 'Current', 'Expired'];
  $nextStatus = $membershipStatus['name'];
  if (in_array($nextStatus, $checkStatuses)) {
    $membershipId = $membership['id'] ?? NULL;
    $currentStatus = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('status_id:name')
      ->addWhere('id', '=', $membershipId)
      ->execute()->first()['status_id:name'];

    // Technically would transition to "Current" but we want it to remain Grace for new member.
    if ($arguments['start_date'] == $arguments['end_date'] && $currentStatus == 'Grace' && $nextStatus == 'Current') {
      $membershipStatus['name'] = 'Grace';
      $membershipStatus['id'] = \Civi\Api4\MembershipStatus::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('id')
        ->addWhere('name', '=', 'Grace')
        ->execute()->first()['id'];
    }
    else {
      // Never auto transition away from Requested.
      if ($currentStatus == 'Requested') {
        // Override, ensuring we stay Requested.
        $membershipStatus['name'] = 'Requested';
        $membershipStatus['id'] = \Civi\Api4\MembershipStatus::get()
          ->setCheckPermissions(FALSE)
          ->addSelect('id')
          ->addWhere('name', '=', 'Requested')
          ->execute()->first()['id'];
      }
      // Never auto transition from Disabled to Expired (but allow Disabled
      // to Current).
      if ($currentStatus == 'Disabled' && $nextStatus == 'Expired') {
        $membershipStatus['name'] = 'Disabled';
        $membershipStatus['id'] = \Civi\Api4\MembershipStatus::get()
          ->setCheckPermissions(FALSE)
          ->addSelect('id')
          ->addWhere('name', '=', 'Disabled')
          ->execute()->first()['id'];
      }
      elseif ($currentStatus == 'Disabled' && $nextStatus == 'Current') {
        // We allow this to happen, but want to notify an admin so we can re-enable their
        // hosting order.
        //
        // Get contactIds of the welcome group.
        $contacts = \Civi\Api4\GroupContact::get()
          ->setCheckPermissions(FALSE)
          ->addWhere('group_id:name', '=', 'Receive_New_Member_Notifications')
          ->addSelect('contact_id')
          ->execute();

        $recipientContactIds = [];
        foreach($contacts as $contact) {
          $id = $contact['contact_id'];
          $recipientContactIds[$id] = NULL;
        }
        if (count($recipientContactIds) > 0) {
          $controlPanelId = NULL;
          $details = \Civi\Api4\Membership::get()
            ->setCheckPermissions(FALSE)
            ->addSelect('contact_id.external_identifier', 'contact_id.display_name')
            ->addWhere('id', '=', $membershipId)
            ->execute()->first();

          if (!$details) {
            $message = "A membership has converted from disabled to current in CiviCRM
              but there was an error extracting the control panel id. You will have to
              check CiviCRM manually to figure it out.";
          } 
          else {
            $displayName = $details['contact_id.display_name'] ?? NULL;
            $externalIdentifier = $details['contact_id.external_identifier'] ?? NULL;
            $link = NULL;

            if ($externalIdentifier) {
              // change member:1234 to just 1234 to get the control panel id
              // so we can send a link.
              $controlPanelId = substr($externalIdentifier,7); 

              $link = 'https://members.mayfirst.org/cp/index.php?' .
                http_build_query(['area' => 'member', 'member_id' => $controlPanelId]);
            }
            $message = "<p>A member that was disabled has paid their dues. Please re-enable hosting orders for the member {$displayName}.</p>";
            if ($link) {
              $message .= "<p><a href=\"{$link}\">Click here to access their hosting orders in the Control Panel.</a></p>";
            }
          } 

          $emailer = new \Civi\Mayfirst\Email($membershipId);
          $emailer->prependContent = $message;
          $emailer->workflowName = 'mayfirst_notify_disabled_to_current';
          $emailer->recipientContactIds = $recipientContactIds;
          $emailer->send();
        }
      }
    }
  }
}

/**
 *
 * Warn when cancelling a membership with a recurring donation that
 * is still active.
 *
 **/
function mayfirst_civicrm_post($op, $objectName, $id, &$obj) {
  if ($objectName == 'Membership' && $op == 'edit') {
    $membership = \Civi\Api4\Membership::get()
      ->setCheckPermissions(FALSE)
      ->addWhere('id', '=', $id)
      ->addSelect('status_id', 'contribution_recur_id')
      ->execute()->first();
    $statusId = $membership['status_id'];
    $contributionRecurId = $membership['contribution_recur_id'];
   
    if ($contributionRecurId) {
      $cancelledStatusId = \Civi\Api4\MembershipStatus::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('id')
        ->addWhere('name', '=', 'Cancelled')
        ->execute()->first()['id'];
      if ($cancelledStatusId == $statusId) {
        $contributionRecurStatus = \Civi\Api4\ContributionRecur::get()
          ->setCheckPermissions(FALSE)
          ->addWhere('id', '=', $contributionRecurId)
          ->addSelect('contribution_status_id:name')
          ->execute()->first()['contribution_status_id:name'];

        if ($contributionRecurStatus == 'In Progress') {
          \CRM_Core_Session::singleton()->setStatus(E::ts("This membership has an in progress recurring contribution. Be sure to cancel the recurring contribution."), E::ts("Recurring Contribution is still active"), 'error');
        }
      }
    }
  }
}

/**
 *
 * Make adjustments to membership start date.
 *
 **/
function mayfirst_civicrm_pre($op, $objectName, $id, &$params) {
  if ($objectName == 'Membership') {
    // Make sure CiviCRM isn't adjusting the start and end
    // dates of an expired membership. Expired members have to
    // pay their back dues - they don't get automatically bumped.
    $currentStatusId = \Civi\Api4\MembershipStatus::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('id')
      ->addWhere('name', '=', 'Current')
      ->execute()->first()['id'];

    // Only trigger on updates of existing memberships in which the destination
    // status id is current.
    if ($id && $params['status_id'] == $currentStatusId) {
      $membershipId = $id;

      // If we are moving to current, we never want override to be in place. We
      // may have set to override when we set to disabled.
      $params['is_override'] = 0;
      $params['status_override_end_date'] = NULL;

      // Get the membership status of the membership record in the database.
      $existing = \Civi\Api4\Membership::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('status_id:name', 'start_date', 'end_date')
        ->addWhere('id', '=', $membershipId)
        ->execute()->first();
      $existingStatus = $existing['status_id:name'];

      // Define the statuses we are looking for.
      $adjustStatuses = [ 'Expired', 'Grace', 'Disabled' ];

      // Let's return early if we don't have to do anything.
      if (!in_array($existingStatus, $adjustStatuses)) {
        // We only need to mess with expired or grace memberships.
        \Civi::log()->debug("Returning early for member id $id because status $existingStatus does not get auto adjusted.");
        return;
      }

      // Calculate dates.
      $existingStartStr = str_replace('-', '', $existing['start_date']);
      $existingEndStr = str_replace('-', '', $existing['end_date']);

      $newStartStr = str_replace('-', '', $params['start_date']);
      $newEndStr = str_replace('-', '', $params['end_date']);

      // Another chance to return without making changes.
      if ($existingStartStr == $newStartStr) {
        // For expired membership in which there is no attempt to change the
        // start date, no need to interfere.
        \Civi::log()->debug("Returning early since member id $id start date of $newStartStr does not need adjustment.");
        return;
      }

      // Calculate the intended interval for this membership.
      $newStart = \DateTimeImmutable::createFromFormat('Ymd', $newStartStr);
      $newEnd = \DateTimeImmutable::createFromFormat('Ymd', $newEndStr);
      if (!$newStart) {
        // Bail early - something went wrong.
        \Civi::log()->debug("Failed to correct dates for membership {$membershipId}. Failed to generate newStart date from: {$newStartStr}.");
        return;
      }
      if (!$newEnd) {
        // Bail early - something went wrong.
        \Civi::log()->debug("Failed to correct dates for membership {$membershipId}. Failed to generate newStart date from: {$newEndStr}.");
        return;
      }

      $interval = $newStart->diff($newEnd);

      // Fix the start date, setting it to the existing end date like it should be.
      $fixedStart = \DateTimeImmutable::createFromFormat('Ymd', $existingEndStr);
      if (!$fixedStart) {
        // Bail early - something went wrong.
        \Civi::log()->debug("Failed to correct dates for membership {$membershipId}. Failed to generate date from: {$existingEndStr}.");
        return;
      }

      // Now re-calculate the new end date based on the intended interval.
      $fixedEnd = $fixedStart->add($interval);

      // Stringify it.
      $fixedStartStr = $fixedStart->format('Ymd');
      $fixedEndStr = $fixedEnd->format('Ymd');
      \Civi::log()->debug("Reset start and end for {$membershipId}. Original start and end: {$newStartStr} - {$newEndStr}. Fixed start and end: {$fixedStartStr} - {$fixedEndStr}");

      // Reset the start date and end date.
      $params['start_date'] = $fixedStartStr;
      $params['end_date'] = $fixedEndStr;
    }
  }
}

function mayfirst_civicrm_postIPNProcess(&$IPNData) {
  \Civi::log()->debug("IPNData: " . print_r($IPNData, TRUE));
}

/**
 * Anonymize contribution
 *
 * Add an option to the "..." menu of a contribution that allows
 * us to anonymize it.
 *
 * Thanks to LCDServices "movecontrib" extension.
 */
function mayfirst_civicrm_searchColumns($objectName, &$headers, &$rows, &$selector) {
  if ($objectName == 'contribution' && CRM_Core_Permission::check('allow Move Contribution')) {
    foreach ($rows as &$row) {
      //action column is either a series of links, or a series of links plus a subset
      //unordered list (more button) -- all of which is enclosed in a span
      //we want to inject our option at the end, regardless, so we look for the existence
      //of a <ul> tag and adjust our injection accordingly
      $url = CRM_Utils_System::url(
        'civicrm/mayfirst/anonymize',
        "contribution_id={$row['contribution_id']}",
        FALSE,
      );
      $class = 'action-item crm-hover-button mayfirst-anonymize';
      $urlLink = "<a href='{$url}' class='{$class}'>Anonymize Contribution</a>";
      if (strpos($row['action'], '</ul>') !== FALSE) {
        $row['action'] = str_replace(
          '</ul></span>',
          "<li>{$urlLink}</li></ul></span>",
          $row['action']
        );
      }
      else {
        $row['action'] = str_replace(
          '</span>',
          "{$urlLink}</span>",
          $row['action']
        );
      }
    }
  }
}

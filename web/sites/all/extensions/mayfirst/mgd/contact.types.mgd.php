<?php

return [
  [
    "name" => "RelationshipType_Admin_Contact",
    "entity" => "RelationshipType",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name_a_b" => "Membership_Admin_Contact_For",
        "label_a_b" => "Membership Admin Contact for",
        "name_b_a" => "Membership_Admin_Contact_Is",
        "label_b_a" => "Membership Admin Contact is",
        "description" => null,
        "contact_type_a" => "Individual",
        "contact_type_b" => null,
        "contact_sub_type_a" => null,
        "contact_sub_type_b" => null,
        "is_reserved" => null,
        "is_active" => true
      ],
    ],
  ],
  [
    "name" => "RelationshipType_Technical_Contact",
    "entity" => "RelationshipType",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name_a_b" => "Membership_Technical_Contact_For",
        "label_a_b" => "Membership Technical Contact for",
        "name_b_a" => "Membership_Technical_Contact_Is",
        "label_b_a" => "Membership Technical Contact is",
        "description" => null,
        "contact_type_a" => "Individual",
        "contact_type_b" => null,
        "contact_sub_type_a" => null,
        "contact_sub_type_b" => null,
        "is_reserved" => null,
        "is_active" => true
      ],
    ],
  ],
  [
    "name" => "RelationshipType_General_Contact",
    "entity" => "RelationshipType",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name_a_b" => "Membership_General_Contact_For",
        "label_a_b" => "Membership Contact for",
        "name_b_a" => "Membership_General_Contact_Is",
        "label_b_a" => "Membership Contact is",
        "description" => null,
        "contact_type_a" => "Individual",
        "contact_type_b" => null,
        "contact_sub_type_a" => null,
        "contact_sub_type_b" => null,
        "is_reserved" => null,
        "is_active" => true,
      ],
    ],
  ],
];

?>

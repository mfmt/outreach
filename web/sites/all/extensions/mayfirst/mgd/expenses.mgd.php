<?php

return [
  [
    'name' => 'OptionValue_Expense',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'match' => ['name'],
      'values' => [
        'option_group_id.name' => 'activity_type',
        'label' => 'Expense',
        'value' => '60',
        'name' => 'Expense',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'weight' => 60,
        'description' => '<p>Use to record all expenses in a uniform way so we can export them to our accounting system.</p>',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => 'fa-money',
        'color' => NULL,
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Expense',
    'entity' => 'CustomGroup',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Expense',
        'title' => 'Expense',
        'extends' => 'Activity',
        'extends_entity_column_id' => NULL,
        'extends_entity_column_value' => [
          '60',
        ],
        'style' => 'Inline',
        'collapse_display' => FALSE,
        'help_pre' => '',
        'help_post' => '',
        'weight' => 16,
        'is_active' => TRUE,
        'is_multiple' => FALSE,
        'min_multiple' => NULL,
        'max_multiple' => NULL,
        'collapse_adv_display' => TRUE,
        'created_date' => '2023-01-01 09:33:48',
        'is_reserved' => FALSE,
        'is_public' => TRUE,
        'icon' => '',
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Expense_CustomField_Reference_Code',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Expense',
        'name' => 'Reference_Code',
        'label' => 'Reference Code',
        'data_type' => 'String',
        'html_type' => 'Text',
        'default_value' => NULL,
        'is_required' => FALSE,
        'is_searchable' => TRUE,
        'is_search_range' => FALSE,
        'help_pre' => 'Enter the invoice number, transactioon id or some method of identifying this expense with the printed receipt or invoice.',
        'help_post' => NULL,
        'mask' => NULL,
        'attributes' => NULL,
        'javascript' => NULL,
        'is_active' => TRUE,
        'is_view' => FALSE,
        'options_per_line' => NULL,
        'text_length' => 255,
        'start_date_years' => NULL,
        'end_date_years' => NULL,
        'date_format' => NULL,
        'time_format' => NULL,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'reference_code_130',
        'option_group_id' => NULL,
        'serialize' => 0,
        'filter' => NULL,
        'in_selector' => FALSE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Expense_CustomField_Date_Spent',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Expense',
        'name' => 'Date_Spent',
        'label' => 'Date Spent',
        'data_type' => 'Date',
        'html_type' => 'Select Date',
        'default_value' => NULL,
        'is_required' => FALSE,
        'is_searchable' => TRUE,
        'is_search_range' => TRUE,
        'help_pre' => 'Enter the date that money should be counted as an expense in our accounting system.',
        'help_post' => NULL,
        'mask' => NULL,
        'attributes' => NULL,
        'javascript' => NULL,
        'is_active' => TRUE,
        'is_view' => FALSE,
        'options_per_line' => NULL,
        'text_length' => 255,
        'start_date_years' => 2,
        'end_date_years' => 5,
        'date_format' => 'yy-mm-dd',
        'time_format' => NULL,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'date_spent_127',
        'option_group_id' => NULL,
        'serialize' => 0,
        'filter' => NULL,
        'in_selector' => FALSE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Expense_CustomField_Date_Paid',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Expense',
        'name' => 'Date_Paid',
        'label' => 'Date Paid',
        'data_type' => 'Date',
        'html_type' => 'Select Date',
        'default_value' => NULL,
        'is_required' => FALSE,
        'is_searchable' => TRUE,
        'is_search_range' => TRUE,
        'help_pre' => 'Enter the date the money left our bank account.',
        'help_post' => NULL,
        'mask' => NULL,
        'attributes' => NULL,
        'javascript' => NULL,
        'is_active' => TRUE,
        'is_view' => FALSE,
        'options_per_line' => NULL,
        'text_length' => 255,
        'start_date_years' => 2,
        'end_date_years' => 5,
        'date_format' => 'yy-mm-dd',
        'time_format' => NULL,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'date_paid_128',
        'option_group_id' => NULL,
        'serialize' => 0,
        'filter' => NULL,
        'in_selector' => FALSE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Expense_CustomField_Amount',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Expense',
        'name' => 'Amount',
        'label' => 'Amount',
        'data_type' => 'Money',
        'html_type' => 'Text',
        'default_value' => NULL,
        'is_required' => FALSE,
        'is_searchable' => TRUE,
        'is_search_range' => TRUE,
        'help_pre' => NULL,
        'help_post' => NULL,
        'mask' => NULL,
        'attributes' => NULL,
        'javascript' => NULL,
        'is_active' => TRUE,
        'is_view' => FALSE,
        'options_per_line' => NULL,
        'text_length' => 255,
        'start_date_years' => NULL,
        'end_date_years' => NULL,
        'date_format' => NULL,
        'time_format' => NULL,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'amount_131',
        'option_group_id' => NULL,
        'serialize' => 0,
        'filter' => NULL,
        'in_selector' => FALSE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category',
    'entity' => 'OptionGroup',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Expense_Expense_Category',
        'title' => 'Expense :: Expense Category',
        'description' => NULL,
        'data_type' => 'String',
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'is_locked' => FALSE,
        'option_value_fields' => [
          'name',
          'label',
          'description',
        ],
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Colocation_Fees',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Colocation Fees',
        'value' => '1',
        'name' => 'Colocation_Fees',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Computer_Related_Supplies',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Computer Related Supplies',
        'value' => '2',
        'name' => 'Computer_Related_Supplies',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Conference_Registration',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Conference Registration',
        'value' => '3',
        'name' => 'Conference_Registration',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Consulting_Fees',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Consulting Fees',
        'value' => '4',
        'name' => 'Consulting_Fees',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Disability',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Disability',
        'value' => '5',
        'name' => 'Disability',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Domain_Name_Registration',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Domain Name Registration',
        'value' => '6',
        'name' => 'Domain_Name_Registration',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Dues_and_Subscription',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Dues and Subscriptions',
        'value' => '7',
        'name' => 'Dues_and_Subscriptions',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Food_and_Entertainment',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Food and Entertainment',
        'value' => '8',
        'name' => 'Food_and_Entertainment',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Hard_Drives',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Hard Drives',
        'value' => '9',
        'name' => 'Hard_Drives',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Insurance',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Insurance',
        'value' => '10',
        'name' => 'Insurance',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Interpretation',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Interpretation',
        'value' => '11',
        'name' => 'Interpretation',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Lodging',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Lodging',
        'value' => '12',
        'name' => 'Lodging',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Miscellaneous',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Miscellaneous',
        'value' => '13',
        'name' => 'Miscellaneous',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Office_Supplies',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Office Supplies',
        'value' => '14',
        'name' => 'Office_Supplies',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Postage_and_Delivery',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Postage and Delivery',
        'value' => '15',
        'name' => 'Postage_and_Delivery',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Printing_and_Reproduction',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Printing and Reproduction',
        'value' => '16',
        'name' => 'Printing_and_Reproduction',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Professional_Fees',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Professional Fees',
        'value' => '17',
        'name' => 'Professional_Fees',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Salaries',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Salaries',
        'value' => '19',
        'name' => 'Salaries',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Servers',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Servers',
        'value' => '20',
        'name' => 'Servers',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Space_Rental',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Space Rental',
        'value' => '21',
        'name' => 'Space_Rental',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Telephone_and_Fax',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Telephone and Fax',
        'value' => '22',
        'name' => 'Telephone_and_Fax',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_Travel',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'Travel',
        'value' => '24',
        'name' => 'Travel',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Expense_Category_OptionValue_ARIN_Fees',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Expense_Expense_Category',
        'label' => 'ARIN Fees',
        'value' => '26',
        'name' => 'ARIN_Fees',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => '',
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Expense_CustomField_Expense_Category',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Expense',
        'name' => 'Expense_Category',
        'label' => 'Expense Category',
        'data_type' => 'String',
        'html_type' => 'Select',
        'default_value' => NULL,
        'is_required' => FALSE,
        'is_searchable' => TRUE,
        'is_search_range' => FALSE,
        'help_pre' => NULL,
        'help_post' => NULL,
        'mask' => NULL,
        'attributes' => NULL,
        'javascript' => NULL,
        'is_active' => TRUE,
        'is_view' => FALSE,
        'options_per_line' => NULL,
        'text_length' => 255,
        'start_date_years' => NULL,
        'end_date_years' => NULL,
        'date_format' => NULL,
        'time_format' => NULL,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'expense_category_129',
        'option_group_id.name' => 'Expense_Expense_Category',
        'serialize' => 0,
        'filter' => NULL,
        'in_selector' => FALSE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
    [
    'name' => 'OptionGroup_Expense_Bank_Account',
    'entity' => 'OptionGroup',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'match' => [ 'name' ],
      'values' => [
        'name' => 'Expense_Bank_Account',
        'title' => 'Expense :: Bank Account',
        'description' => NULL,
        'data_type' => 'String',
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'is_locked' => FALSE,
        'option_value_fields' => [
          'name',
          'label',
          'description',
        ],
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Bank_Account_OptionValue_Citibank',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'match' => [ 'name' ],
      'values' => [
        'option_group_id.name' => 'Expense_Bank_Account',
        'label' => 'Citibank',
        'value' => '1',
        'name' => 'Citibank',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Expense_Bank_Account_OptionValue_Paypal',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'match' => [ 'name' ],
      'values' => [
        'option_group_id.name' => 'Expense_Bank_Account',
        'label' => 'Paypal',
        'value' => '2',
        'name' => 'Paypal',
        'grouping' => NULL,
        'filter' => 0,
        'is_default' => FALSE,
        'description' => NULL,
        'is_optgroup' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
        'component_id' => NULL,
        'domain_id' => NULL,
        'visibility_id' => NULL,
        'icon' => NULL,
        'color' => NULL,
      ],
    ],
  ],
  [
    'name' => 'CustomField_Bank_Account',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'match' => [ 'name' ],
      'values' => [
        'custom_group_id.name' => 'Expense',
        'name' => 'Bank_Account',
        'label' => 'Bank Account',
        'data_type' => 'String',
        'html_type' => 'Select',
        'default_value' => NULL,
        'is_required' => FALSE,
        'is_searchable' => FALSE,
        'is_search_range' => FALSE,
        'weight' => 6,
        'help_pre' => NULL,
        'help_post' => NULL,
        'mask' => NULL,
        'attributes' => NULL,
        'javascript' => NULL,
        'is_active' => TRUE,
        'is_view' => FALSE,
        'options_per_line' => NULL,
        'text_length' => 255,
        'start_date_years' => NULL,
        'end_date_years' => NULL,
        'date_format' => NULL,
        'time_format' => NULL,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'bank_account_132',
        'option_group_id.name' => 'Expense_Bank_Account',
        'serialize' => 0,
        'filter' => NULL,
        'in_selector' => FALSE,
      ],
    ],
  ],
    [
    'name' => 'OptionValue_Membership_Dues_Reduction_Request', 
    'entity' => 'OptionValue', 
    'cleanup' => 'unused', 
    'update' => 'unmodified', 
    'params' => [
      'version' => 4, 
      'match' => [ 'name' ],
      'values' => [
        'option_group_id.name' => 'activity_type', 
        'label' => 'Membership Dues Reduction Request', 
        'value' => '61', 
        'name' => 'Membership_Dues_Reduction_Request', 
        'grouping' => NULL, 
        'filter' => 0, 
        'is_default' => FALSE, 
        'weight' => 61, 
        'description' => NULL, 
        'is_optgroup' => FALSE, 
        'is_reserved' => FALSE, 
        'is_active' => TRUE, 
        'component_id' => NULL, 
        'domain_id' => NULL, 
        'visibility_id' => NULL, 
        'icon' => NULL, 
        'color' => NULL,
      ],
    ],
  ],
];

?>

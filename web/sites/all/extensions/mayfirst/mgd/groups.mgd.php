<?php

return [
  [
    "name" => "Group_New_Member_Welcoming_Committee",
    "entity" => "Group",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "New_Member_Welcoming_Committee",
        "title" => "New Member Welcoming Committee",
        "description" => "Contacts in this group are available to send personal welcome messages to new members.",
        "source" => null,
        "saved_search_id" => null,
        "is_active" => true,
        "visibility" => "User and User Admin Only",
        "where_clause" => null,
        "select_tables" => null,
        "where_tables" => null,
        "group_type" => [],
        "cache_date" => null,
        "refresh_date" => null,
        "parents" => [],
        "children" => '',
        "is_hidden" => false,
        "is_reserved" => false,
        "frontend_title" => null,
        "frontend_description" => null
      ]
    ]
  ],
  [
    "name" => "Group_Receive_Uploaded_Receipt_Notifications",
    "entity" => "Group",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "Receive_Uploaded_Receipt_Notifications",
        "title" => "Receive Uploaded Receipt Notifications",
        "description" => "Contacts in this group receive an email everytime a membership renewal is submitted with a receipt that must be verified.",
        "source" => null,
        "saved_search_id" => null,
        "is_active" => true,
        "visibility" => "User and User Admin Only",
        "where_clause" => null,
        "select_tables" => null,
        "where_tables" => null,
        "group_type" => [],
        "cache_date" => null,
        "refresh_date" => null,
        "parents" => [],
        "children" => '',
        "is_hidden" => false,
        "is_reserved" => false,
        "frontend_title" => null,
        "frontend_description" => null
      ]
    ]
  ],
  [
    "name" => "Group_Receive_New_Member_Notifications",
    "entity" => "Group",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "Receive_New_Member_Notifications",
        "title" => "Receive New Member Notifications",
        "description" => "Contacts in this group receive an email everytime a new membership is submitted that needs to be processed.",
        "source" => null,
        "saved_search_id" => null,
        "is_active" => true,
        "visibility" => "User and User Admin Only",
        "where_clause" => null,
        "select_tables" => null,
        "where_tables" => null,
        "group_type" => [],
        "cache_date" => null,
        "refresh_date" => null,
        "parents" => [],
        "children" => '',
        "is_hidden" => false,
        "is_reserved" => false,
        "frontend_title" => null,
        "frontend_description" => null
      ],
    ],
  ],
];

?>

<?php

use CRM_Mayfirst_ExtensionUtil as E;

return [
  [
    'name' => 'CustomGroup_Member_Info',
    'entity' => 'CustomGroup',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Member_Info',
        'title' => E::ts('Member Info'),
        'style' => 'Inline',
        'help_pre' => '',
        'help_post' => '',
        'weight' => 3,
        'created_date' => '2021-01-12 09:50:01',
        'icon' => '',
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_member_income_ranges',
    'entity' => 'OptionGroup',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'member_income_ranges',
        'title' => E::ts('Member Income Ranges'),
        'data_type' => 'Integer',
        'is_reserved' => FALSE,
        'option_value_fields' => [
          'name',
          'label',
          'description',
        ],
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_member_income_ranges_OptionValue_Income_Range_1',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'member_income_ranges',
        'label' => E::ts('Income Range 1'),
        'value' => '1',
        'name' => 'Income Range_1',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_member_income_ranges_OptionValue_Income_Range_2',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'member_income_ranges',
        'label' => E::ts('Income Range 2'),
        'value' => '2',
        'name' => 'Income Range_2',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_member_income_ranges_OptionValue_Income_Range_3',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'member_income_ranges',
        'label' => E::ts('Income Range 3'),
        'value' => '3',
        'name' => 'Income Range_3',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_member_income_ranges_OptionValue_Income_Range_4',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'member_income_ranges',
        'label' => E::ts('Income Range 4'),
        'value' => '4',
        'name' => 'Income Range_4',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Income',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Income',
        'label' => E::ts('Income Range'),
        'data_type' => 'Int',
        'html_type' => 'Radio',
        'is_searchable' => TRUE,
        'text_length' => 255,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'income_range',
        'option_group_id.name' => 'member_income_ranges',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Why_Join',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Why_Join',
        'label' => E::ts('Why Join'),
        'data_type' => 'Memo',
        'html_type' => 'TextArea',
        'attributes' => 'rows=4, cols=60',
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'why_join',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Fixed_Minimum',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Fixed_Minimum',
        'label' => E::ts('Fixed Min Dues'),
        'data_type' => 'Money',
        'html_type' => 'Text',
        'default_value' => '0',
        'is_searchable' => TRUE,
        'is_search_range' => TRUE,
        'help_post' => E::ts("Fixed minimum is for members who agree to pay more then what they should. If their calculated dues is less then this amount, then they will pay the fixed minimum. If it's higher, they will pay the higher amount."),
        'text_length' => 16,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'fixed_minimum',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Percent_Reduction',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Percent_Reduction',
        'label' => E::ts('Percent Reduction'),
        'data_type' => 'Float',
        'html_type' => 'Text',
        'default_value' => '0',
        'is_searchable' => TRUE,
        'is_search_range' => TRUE,
        'help_post' => E::ts('The percent reduction this member will receive whenever their dues are calculated, e.g. 20 or 10.5 (do not include the percent sign).'),
        'text_length' => 5,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'percent_reduction',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Reduction_Follow_Up_Date',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Reduction_Follow_Up_Date',
        'label' => E::ts('Reduction Follow Up Date'),
        'data_type' => 'Date',
        'html_type' => 'Select Date',
        'is_searchable' => TRUE,
        'is_search_range' => TRUE,
        'help_pre' => E::ts('If a dues reduction is granted, what date should we follow up with the member to find out if the reduction should be continued?'),
        'text_length' => 255,
        'start_date_years' => 0,
        'end_date_years' => 2,
        'date_format' => 'mm/dd/yy',
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'reduction_follow_up_date',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_How_Hear',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'How_Hear',
        'label' => E::ts('How did you hear about May First?'),
        'data_type' => 'Memo',
        'html_type' => 'TextArea',
        'attributes' => 'rows=4, cols=60',
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'how_hear',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Why_Left',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Why_Left',
        'label' => E::ts('Why Left'),
        'data_type' => 'Memo',
        'html_type' => 'TextArea',
        'is_searchable' => TRUE,
        'attributes' => 'rows=4, cols=60',
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'why_left',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_are_you_a_coop',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'are_you_a_coop',
        'label' => E::ts('Are you a Coop?'),
        'data_type' => 'Boolean',
        'html_type' => 'Radio',
        'default_value' => '0',
        'is_searchable' => TRUE,
        'text_length' => 255,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'are_you_a_coop',
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_Info_Key_Issue_Areas',
    'entity' => 'OptionGroup',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Member_Info_Key_Issue_Areas',
        'title' => E::ts('Member Info :: Key Issue Areas'),
        'data_type' => 'String',
        'is_reserved' => FALSE,
        'option_value_fields' => [
          'name',
          'label',
          'description',
        ],
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_Info_Key_Issue_Areas_OptionValue_Reproductive_Justice',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Member_Info_Key_Issue_Areas',
        'label' => E::ts('Reproductive Justice'),
        'value' => '1',
        'name' => 'Reproductive_Justice',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_Info_Key_Issue_Areas_OptionValue_Immigrant_Rights',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Member_Info_Key_Issue_Areas',
        'label' => E::ts('Immigrant Rights'),
        'value' => '2',
        'name' => 'Immigrant_Rights',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_Info_Key_Issue_Areas_OptionValue_Palestinian_Rights',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Member_Info_Key_Issue_Areas',
        'label' => E::ts('Palestinian Rights'),
        'value' => '3',
        'name' => 'Palestinian_Rights',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_Info_Key_Issue_Areas_OptionValue_Legal_Movement_Defense',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Member_Info_Key_Issue_Areas',
        'label' => E::ts('Legal Movement Defense'),
        'value' => '4',
        'name' => 'Legal_Movement_Defense',
        'description' => '',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_Member_Info_CustomField_Key_Issue_Areas',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'Member_Info',
        'name' => 'Key_Issue_Areas',
        'label' => E::ts('Key Issue Areas'),
        'html_type' => 'Autocomplete-Select',
        'is_searchable' => TRUE,
        'help_pre' => E::ts('Please indicate if this member engages in organizing in any of our key political issues.'),
        'text_length' => 255,
        'note_columns' => 60,
        'note_rows' => 4,
        'column_name' => 'key_issue_areas',
        'option_group_id.name' => 'Member_Info_Key_Issue_Areas',
        'serialize' => 1,
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
];

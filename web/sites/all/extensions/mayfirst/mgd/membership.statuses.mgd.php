<?php

return [
  // Not a new status, instead we ensure New is disabled.
  [
    'name' => 'MembershipStatus_New',
    'entity' => 'MembershipStatus',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'match' => [ 'name' ],
      'values' => [
        'name' => 'New',
        'label' => 'New',
        'start_event' => 'join_date',
        'start_event_adjust_unit' => NULL,
        'start_event_adjust_interval' => NULL,
        'end_event' => 'join_date',
        'end_event_adjust_unit' => 'month',
        'end_event_adjust_interval' => 3,
        'is_current_member' => TRUE,
        'is_admin' => FALSE,
        'weight' => 2,
        'is_default' => FALSE,
        'is_active' => FALSE,
        'is_reserved' => FALSE,
      ],
    ],
  ],
  [
    "name" => "MembershipStatus_Requested",
    "entity" => "MembershipStatus",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "Requested",
        "label" => "Requested",
        "start_event" => "join_date",
        "start_event_adjust_unit" => NULL,
        "start_event_adjust_interval" => NULL,
        "end_event" => "join_date",
        "end_event_adjust_unit" => "month",
        "end_event_adjust_interval" => NULL,
        "is_current_member" => FALSE,
        "is_admin" => TRUE,
        "weight" => 0,
        "is_default" => FALSE,
        "is_active" => TRUE,
        "is_reserved" => TRUE,
      ],
    ],
  ],
  [
    "name" => "MembershipStatus_Disabled",
    "entity" => "MembershipStatus",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "Disabled",
        "label" => "Disabled",
        "start_event" => "join_date",
        "start_event_adjust_unit" => NULL,
        "start_event_adjust_interval" => NULL,
        "end_event" => "join_date",
        "end_event_adjust_unit" => "month",
        "end_event_adjust_interval" => NULL,
        "is_current_member" => FALSE,
        "is_admin" => TRUE,
        "weight" => 6,
        "is_default" => FALSE,
        "is_active" => TRUE,
        "is_reserved" => TRUE,
      ],
    ],
  ],
];

?>

<?php

return [
  [
    "name" => "MembershipType_Annual",
    "entity" => "MembershipType",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "Annual",
        "description" => null,
        "financial_type_id.name" => "Member Dues",
        "minimum_fee" => 0,
        "duration_unit" => "year",
        "duration_interval" => 1,
        "period_type" => "rolling",
        "fixed_period_start_day" => null,
        "fixed_period_rollover_day" => null,
        "relationship_type_id" => null,
        "relationship_direction" => null,
        "max_related" => null,
        "visibility" => "Public",
        "weight" => 3,
        "receipt_text_signup" => null,
        "receipt_text_renewal" => null,
        "auto_renew" => 1,
        "is_active" => true,
        "domain_id" => "current_domain",
        "member_of_contact_id" => 1, // Hard coded to contact id 1, which is May First.
      ]
    ]
  ],
  [
    "name" => "MembershipType_Monthly",
    "entity" => "MembershipType",
    "cleanup" => "unused",
    "update" => "always",
    "params" => [
      "version" => 4,
      "values" => [
        "name" => "Monthly",
        "description" => null,
        "financial_type_id.name" => "Member Dues",
        "minimum_fee" => 0,
        "duration_unit" => "month",
        "duration_interval" => 1,
        "period_type" => "rolling",
        "fixed_period_start_day" => null,
        "fixed_period_rollover_day" => null,
        "relationship_type_id" => null,
        "relationship_direction" => null,
        "max_related" => null,
        "visibility" => "Public",
        "weight" => 4,
        "receipt_text_signup" => null,
        "receipt_text_renewal" => null,
        "auto_renew" => 2,
        "is_active" => true,
        "domain_id" => "current_domain",
        "member_of_contact_id" => 1, // Hard coded to contact id 1, which is May First.
      ]
    ]
  ],
];

?>

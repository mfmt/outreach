<?php

return [
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_dues_payment_info',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Dues Payment Info',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesPaymentInfo/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesPaymentInfo/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_dues_payment_info',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_dues_reminder_active',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Dues Invoice (active)',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReminderActive/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReminderActive/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_dues_reminder_active',
        'is_default' => TRUE,
      ],
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_dues_reminder_grace',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Dues Reminder (grace)',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReminderGrace/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReminderGrace/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_dues_reminder_grace',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_dues_reminder_expired',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Dues Reminder (expired)',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReminderExpired/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReminderExpired/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_dues_reminder_expired',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_request_received',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Request Received',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstRequestReceived/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstRequestReceived/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_request_received',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_welcome',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Welcome',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstWelcome/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstWelcome/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_welcome',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_notify_new_member_signup',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - New Member Signup',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstNotifyNewMemberSignup/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstNotifyNewMemberSignup/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_notify_new_member_signup',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_notify_uploaded_receipt',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Uploaded Receipt',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstNotifyUploadedReceipt/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstNotifyUploadedReceipt/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_notify_uploaded_receipt',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_payment_receipt',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Payment Receipt',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstPaymentReceipt/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstPaymentReceipt/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_payment_receipt',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_notify_disabled_to_current',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Disabled to Current Membership',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstNotifyDisabledToCurrent/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstNotifyDisabledToCurrent/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_notify_disabled_to_current',
        'is_default' => TRUE,
      ]
    ],
  ],
  [
    'entity' => 'MessageTemplate',
    'name' => 'mayfirst_dues_reduction_follow_up_reminders',
    'update' => 'always',
    'cleanup' => 'always',
    'params' => [
      'version' => 4,
      'values' => [
        'msg_title' => 'May First - Dues Reduction Follow Up Reminders',
        'msg_subject' => trim(file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReductionFollowUpReminders/subject.tpl')),
        'msg_html' => file_get_contents(__DIR__ . '/../templates/MessageTemplate/MayfirstDuesReductionFollowUpReminders/html.tpl'),
        'is_active' => TRUE,
        'workflow_name' => 'mayfirst_dues_reduction_follow_up_reminders',
        'is_default' => TRUE,
      ]
    ],
  ],
];

?>

--- Install table to track membership start date for
--- membership contributions and amount that should be
--- deferred to next year.
CREATE TABLE IF NOT EXISTS `civicrm_mayfirst_contribution` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contribution_id` int(10) unsigned NOT NULL,
  `invoice_start_date` datetime NOT NULL COMMENT 'The start date of the membership for which this contribution pays',
  `deferred_amount` decimal(20,2) NOT NULL DEFAULT 0 COMMENT 'The amount of the contribution that should be deferred to the year after the start date',
  PRIMARY KEY (`id`),
  UNIQUE (`contribution_id`),
  CONSTRAINT `FK_mayfirst_civicrm_contribution_id` FOREIGN KEY (`contribution_id`) REFERENCES `civicrm_contribution` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

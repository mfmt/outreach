<p>Are you sure you want to anonymize the contribution (id: {$contribution_id}) by <em>{$display_name}</em> for <em>${$total_amount}</em> made on <em>{$receive_date}</em>?</p>
<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>

{crmScope extensionKey='mayfirst'}
<p>{ts}Greetings{/ts} {mayfirst.first_name_or_compa}!</p>
        
<p>{ts}You are getting this email because someone (hopefully you) tried to pay your May First dues online but did not have your membership token. So, here it is!{/ts}</p>

<p>
  <strong>{ts}Member Name{/ts}:</strong> {mayfirst.member_name}<br>
  <strong>{ts}Membership Due Date{/ts}:</strong> {mayfirst.membership_renewal_due_date}<br>
  <strong>{ts}Amount Due{/ts}:</strong> ${mayfirst.membership_renewal_amount_localized}  {mayfirst.membership_currency}<br>
  <strong>{ts}Membership Renewal period{/ts}:</strong> {mayfirst.membership_renewal_due_date} - {mayfirst.membership_next_term_end_date}<br>
  <strong>{ts}Payment Link{/ts}:</strong> <a href="{mayfirst.membership_renewal_link_edit_privs}">{mayfirst.membership_renewal_link_edit_privs}</a><br>
  <strong>{ts}Payment Token{/ts}:</strong> {mayfirst.membership_edit_token}<br>
  <strong>{ts}Reference Number{/ts}:</strong> {mayfirst.membership_renewal_reference_id}<br>
</p>

<p><em>{ts}May First Movement Technology is a people of color led, democratic organization helping to build the movement for global transformation and emancipation without borders.{/ts}</em></p>

<p><em>{mayfirst.exchange_rate_explanation}</em></p>
{/crmScope}

{crmScope extensionKey='mayfirst'}
<p>{ts}It's time to ask the member <a href="{mayfirst.admin_edit_link}">{mayfirst.member_name}</a> if they want to continue their dues reduction.{/ts}</a></p>

<p>Their "Reduction Follow Up Date" field has been advanced one year, so you will get this notice again in one year.</p>
{/crmScope}

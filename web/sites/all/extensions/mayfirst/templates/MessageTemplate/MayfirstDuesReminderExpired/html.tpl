{crmScope extensionKey='mayfirst'}
<p>{ts}Greetings{/ts} {mayfirst.first_name_or_compa},</p>
        
<p>{ts}You are receiving this email because your membership is expired and your resources either have been disabled or are scheduled to be disabled.{/ts}</p>

<p>{ts}Please click the link below to pay your dues.{/ts} <strong>{ts}We can
only keep your hosting resources on our servers for 3 months after your grace
period ends. After 3 months we will permanently delete your data to make room for
other members on our collectively owned servers.{/ts}</strong>
</p>

<p>
  <strong>{ts}Member Name{/ts}:</strong> {mayfirst.member_name}<br>
  <strong>{ts}Due Date{/ts}:</strong> {mayfirst.membership_renewal_due_date}<br>
  <strong>{ts}Amount Due{/ts}:</strong> ${mayfirst.membership_renewal_amount_localized} {mayfirst.membership_currency}<br>
  <strong>{ts}Membership renewal term/frequency{/ts}:</strong> {mayfirst.membership_frequency}<br>
  <strong>{ts}Membership Renewal period{/ts}:</strong> {mayfirst.membership_renewal_due_date} - {mayfirst.membership_next_term_end_date}<br>
  <strong>{ts}Payment Link{/ts}:</strong> <a href="{mayfirst.membership_renewal_link_edit_privs}">{ts}Pay your dues online by clicking this link{/ts}</a><br>
  <strong>{ts}Payment Token{/ts}:</strong> {mayfirst.membership_edit_token}<br>
  <strong>{ts}Reference Number{/ts}:</strong> {mayfirst.membership_renewal_reference_id}<br>
</p>

<p>{ts}Your support is critical for our project to continue. It both pays for our organizing work and also the servers, electricity and labor to operate our members&#39; web sites, email accounts, video conferencing and all the other technology services running on our servers.{/ts}</p>

<p>{ts}If you would like to cancel your membership, please email info@mayfirst.org.{/ts}</p>

<p>{ts}Thank you for your support!{/ts}</p>
<p><em>{mayfirst.exchange_rate_explanation}</em></p>
{/crmScope}

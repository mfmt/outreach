{crmScope extensionKey='mayfirst'}
<p>{ts}Greetings{/ts} {mayfirst.first_name_or_compa},</p>
        
<p>{ts}You are receiving this email because your membership is in the grace status. That means you are behind on your dues!{/ts}</p>

{capture assign=captured_membership_auto_renew_notice}{mayfirst.membership_auto_renew_notice}{/capture}

{if $captured_membership_auto_renew_notice eq "" }
<p>{ts}Please click the link below to pay your dues. We allow a 2 month grace period after which your hosting services will be disabled.{/ts}.
{/if}

<p>
  <strong>{ts}Member Name{/ts}:</strong> {mayfirst.member_name}<br>
  <strong>{ts}Due Date{/ts}:</strong> {mayfirst.membership_renewal_due_date}<br>
  <strong>{ts}Amount Due{/ts}:</strong> ${mayfirst.membership_renewal_amount_localized} {mayfirst.membership_currency}<br>
  <strong>{ts}Membership renewal term/frequency{/ts}:</strong> {mayfirst.membership_frequency}<br>
  <strong>{ts}Membership Renewal period{/ts}:</strong> {mayfirst.membership_renewal_due_date} - {mayfirst.membership_next_term_end_date}<br>
{if $captured_membership_auto_renew_notice eq "" }
  <strong>{ts}Payment Link{/ts}:</strong> <a href="{mayfirst.membership_renewal_link_edit_privs}">{ts}Pay your dues online by clicking this link{/ts}</a><br>
{/if}
  <strong>{ts}Payment Token{/ts}:</strong> {mayfirst.membership_edit_token}<br>
  <strong>{ts}Reference Number{/ts}:</strong> {mayfirst.membership_renewal_reference_id}<br>
</p>

{if $captured_membership_auto_renew_notice ne "" }
<p style="color:#dc3545">{mayfirst.membership_auto_renew_notice}</p>
{/if}

<p>{ts}Your support is critical for our project to continue. It both pays for our organizing work and also the servers, electricity and labor to operate our members&#39; web sites, email accounts, video conferencing and all the other technology services running on our servers.{/ts}</p>

<p>{ts}If you have any questions about your membership, what services we offer, or how you can learn more about May First, please reply to this email.{/ts}</p>

<p>{ts}Want to get more involved in building our movements digital autonomy? We need your help! No technology skills are needed. We are looking for folks to join our Engagement and Communications Team and our Technology, Infrastructure and Services Team. More info:{/ts} <a href="https://mayfirst.coop/en/get-involved/">https://mayfirst.coop/en/get-involved/</a>.</p>

<p>{ts}If you would like to cancel your membership, please email info@mayfirst.org.{/ts}</p>

<p>{ts}Thank you for your support!{/ts}</p>
<p><em>{mayfirst.exchange_rate_explanation}</em></p>
{/crmScope}

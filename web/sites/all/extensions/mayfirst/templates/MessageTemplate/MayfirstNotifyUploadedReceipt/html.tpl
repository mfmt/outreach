{crmScope extensionKey='mayfirst'}
<p>{ts}A member has uploaded a receipt:{/ts} {mayfirst.member_name}</p>

<p><a href="{domain.base_url}civicrm/a/#/mayfirst/manual">{ts}Review and update the contribution status{/ts}</a>.
{/crmScope}

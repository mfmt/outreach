{crmScope extensionKey='mayfirst'}
<h2>{ts}May First Member Payment Receipt{/ts}</h2>

<strong>{ts}Member Name{/ts}:</strong> {mayfirst.member_name}<br>
<strong>{ts}Payment Date{/ts}:</strong> {mayfirst.payment_date}<br>
<strong>{ts}Payment Amount{/ts}:</strong> ${mayfirst.payment_amount} {mayfirst.payment_currency}<br>
{capture assign=captured_payment_trxn_id}{mayfirst.payment_trxn_id}{/capture}
{if $captured_payment_trxn_id ne "" }
<strong>{ts}Transaction ID{/ts}:</strong> {mayfirst.payment_trxn_id}<br>
{/if}
{capture assign=captured_payment_check_number}{mayfirst.payment_check_number}{/capture}
{if $captured_payment_check_number ne "" }
<strong>{ts}Check Number{/ts}:</strong> {mayfirst.payment_check_number}<br>
{/if}
<strong>{ts}Reference Number{/ts}:</strong> {mayfirst.membership_renewal_reference_id}<br>
<strong>{ts}New Expiration Date{/ts}:</strong> {mayfirst.membership_renewal_due_date}<br><br>

<p>{ts}Thank you for paying your May First Membership dues! Your membership dues pay for our organizing work as well as the hosting infrastructure our movement depends on.{/ts}</p>

<p><strong>{ts}Please note:{/ts}</strong>{ts}If your membership was disabled due to lack of payment, it will take us up to 24 hours to restore your site.{/ts}</p>

<p><em>{ts}May First Movement Technology is a people of color led, democratic organization helping to build the movement for global transformation and emancipation without borders.{/ts}</em></p>
{/crmScope}

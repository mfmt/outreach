{crmScope extensionKey='mayfirst'}
<p>{ts}Thank you for submitting your May First membership request. We review
each request manually, which can take up to a few days so please be patient. If
you have any questions please feel free to reply to this email.{/ts}</p>
{/crmScope}

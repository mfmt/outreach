{crmScope extensionKey='mayfirst'}
<p>{ts}Greetings{/ts} {mayfirst.first_name_or_compa}!</p>

<p><strong>{ts}Welcome! You are now an official member of May First Movement Technology, a people of color led, democratic organization helping to build the movement for global transformation and emancipation without borders.{/ts}</strong></p>

{ts}This mail contains important information about membership dues, updating your contact information and ensuring your membership remains active.{/ts}

{ts}Your membership is currently in Grace status, but you can pay pending membership dues and ensure your account is current by following the link below to contribute your share to our cooperative project.{/ts}

<p>
  <strong>{ts}Member Name:{/ts}</strong> {mayfirst.member_name}<br>
  <strong>{ts}Amount Due:{/ts}</strong> ${mayfirst.membership_renewal_amount_localized} {mayfirst.membership_currency}<br>
  <strong>{ts}Payment Link:{/ts}</strong> <a href="{mayfirst.membership_renewal_link_edit_privs}">{ts}Click on this link to pay your dues or modify your membership details.{/ts}</a><br>
  <strong>{ts}Payment Token:{/ts}</strong> {mayfirst.membership_edit_token}<br>
</p>

<p>{ts}To keep your membership active and and in good standing, please keep the following information in mind:{/ts}</p>

<ul>
<li>{ts}The primary means of communication with May First is email. Periodically, you will receive both automated and personalized notifications. Please read the received emails carefully, especially if you are the administrative contact for your membership. Ensure that at least one of the referenced email addresses is hosted outside your membership. This way, if your membership is disabled due to non-payment, we will still be able to contact you through another means.{/ts}</li>
<li>{ts}Your membership is considered active from the day we welcome you (that is, today) and can remain in a grace period for up to two months. However, we encourage you to pay for your membership as soon as possible.{/ts}</li>
<li>{ts}After the initial 2 months passes without payment, your membership will expire and we will disable your services, which can be reactivated within 24 hours after you make the payment. While in the disabled state, all information and data stored on the servers will remain even though you will not have access to them.{/ts}</li>
<li>{ts 1='href="https://mayfirst.coop/es/politica-de-privacidad"'}After 5 months passes without payment or communication from you, we will proceed to cancel your membership. When your membership is cancelled, your information will be deleted from for privacy reasons. Please review our <a %1>privacy policy</a>.{/ts}</li>

<p>{ts 1='href="https://mayfirst.coop/en/orientation/"' 2='href="https://mayfirst.coop/en/get-involved/"'}May First is your organization. Please take a moment to <a %1>orient yourself</a> and <a %2>learn how to get involved</a>.{/ts} {ts 1='href="https://mayfirst.coop/en/intentionality/"'}Our <a %1>statement on combatting racism and sexism</a> is particularly important.{/ts} 

{ts 1='href="https://members.mayfirst.org/cp"'}You may also want to book mark our <a %1>control panel site</a> (for adding web sites, email address, etc).{/ts} {ts 1='href="https://help.mayfirst.org/"'}Lastly, our <a %1>support site</a> is the place for finding information about how to use our hosting resources and if you have support questions, please email support@mayfirst.org.{/ts}</p>

<p>{ts}You should receive, via a separate email message, instructions for how to set up  your initial login and password and how to connect to our control panel to use your hosting resources.{/ts}</p>
<p>{ts}Thank you for your support!{/ts}</p>

<p><em>{mayfirst.exchange_rate_explanation}</em></p>

<p><em>{mayfirst.spanish_pronoun_explanation}</em></p>

<p>{ts}Why Join?{/ts} {mayfirst.why_join}</p>
{/crmScope}

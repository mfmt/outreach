<?php

namespace Civi\Mayfirst;
use \CRM_Mayfirst_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * Tests payments submitted through Stripe and Paypal. 
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class CreditCardPaymentTest extends MayfirstBase {

  public function setUp(): void {
    parent::setUp();
    // Avoided undefined index errors.
    $_SERVER['HTTP_REFERER'] = 'foo.bar';
  }

  /**
   * Test making a one time stripe payment.
   *
   */
  public function testStripeOneTimePayment() {
    // Establish the initial end date - which is now.
    $this->assertMembershipEndDate(date('Y-m-d'), 'Before first payment');

    $this->setupStripePaymentProcessor();
    $this->oneTimePayment('stripe');

    $this->assertMembershipEndDate(date('Y-m-d', strtotime("+1 year")), 'First payment');
    $this->assertContributionStatus('Completed');
    $this->assertMembershipStatus('Current');  

  }

  /**
   * Test making a one time paypal payment.
   *
   */
  public function testPayPalOneTimePayment() {
    // Establish the initial end date - which is now.
    $this->assertMembershipEndDate(date('Y-m-d'), 'Before first payment');

    $this->setupPayPalPaymentProcessor();
    $this->oneTimePayment('paypal');

    $this->assertMembershipEndDate(date('Y-m-d', strtotime("+1 year")), 'First payment');
    $this->assertContributionStatus('Completed');
    $this->assertMembershipStatus('Current');  

    // Do it again and see if we get two years now.
    $this->oneTimePayment('paypal');
    $this->assertMembershipEndDate(date('Y-m-d', strtotime("+2 year")), 'Renewal payment');
  }


  /**
   * Test making a recurring stripe payment.
   *
   */
  public function testStripeRecurringPayment() {
    // Still not working
    return TRUE;
    $this->setupStripePaymentProcessor();

    $this->assertMembershipEndDate(date('Y-m-d'), 'Before first payment');
    $this->recurringPayment('stripe');

    civicrm_api3('Job', 'process_paymentprocessor_webhooks', ['delete_old' => '-3 month']);

    $this->assertMembershipEndDate(date('Y-m-d', strtotime("+1 year")));
    $this->assertMembershipStatus('Current', "After processing IPN, membership status is Current.");  
    $this->assertContributionStatus('Completed', "After processing IPN, contribution status is Completed.");
    $this->assertContributionRecurStatus('In Progress', "After submitting recurring payment, contribution recur status is In Progress");

  }

  /**
   * Test making a recurring paypal payment.
   *
   */
  public function testPayPalRecurringPayment() {
    $this->setupPayPalPaymentProcessor();

    $this->recurringPayment('paypal');

    $this->assertMembershipEndDate(date('Y-m-d', strtotime("+1 year")));
    $this->assertMembershipStatus('Current', "After processing IPN, membership status is Current.");  
    $this->assertContributionStatus('Completed', "After processing IPN, contribution status is Completed.");
    $this->assertContributionRecurStatus('In Progress', "After submitting recurring payment, contribution recur status is In Progress");
  }



  
}

<?php

namespace Civi\Mayfirst;
use \CRM_Mayfirst_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * Test Email notifications dealing with membership signups.
 *
 * @group headless
 */
class PaymentTest extends MayfirstBase {

  public function setUp(): void {
    $this->startMonitoringEmail();
    $this->addNotificationGroupContacts();
    parent::setUp();
  }

  /**
   * Add contacts to groups
   *
   * To fully test email notifications we have to add
   * contacts and then add those contacts to the notification
   * group.
   */
  private function addNotificationGroupContacts() {
    $notificationContactId = \Civi\Api4\Contact::create()
      ->addValue('first_name', 'Ranger')
      ->addValue('last_name', 'Tabes')
      ->execute()->first()['id'];

    \Civi\Api4\Email::create()
      ->addValue('email', 'tabes@ranger.net')
      ->addValue('location_type_id:name', 'Work')
      ->addValue('contact_id', $notificationContactId)
      ->execute();

    \Civi\Api4\GroupContact::create()
      ->addValue('group_id:name', 'Receive_New_Member_Notifications')
      ->addValue('contact_id', $notificationContactId)
      ->execute();
  }

  /**
   * Add a spanish contact
   *
   * So we can test the translation of the email.
   */
  private function addSpanishContact() {
    $contactId = \Civi\Api4\Contact::create()
      ->addValue('first_name', 'Charlie')
      ->addValue('last_name', 'Sasquatch')
      ->addValue('preferred_language', 'es_ES')
      ->execute()->first()['id'];

    \Civi\Api4\Email::create()
      ->addValue('email', 'charlie@sas.net')
      ->addValue('location_type_id:name', 'Work')
      ->addValue('contact_id', $contactId)
      ->execute();

    \Civi\Api4\Relationship::create()
      ->addValue('contact_id_a', $contactId)
      ->addValue('contact_id_b', $this->contactId)
      ->addValue('relationship_type_id:name', 'Membership_Admin_Contact_For')
      ->execute();
  }

  public function tearDown(): void {
    $this->stopMonitoringEmail();
    parent::tearDown();
  }

  public function testInitialAndWelcomeEmail() {
    // Ensure the member signing up received a notification
    // that their request is pending.
    $this->checkAllMailLog([
      'Thank you for submitting your May First membership request',
      'crowbar@example.net',
    ]);


    // Ensure that someone in the Notification Group gets an email
    // telling them to finalize it.
    $this->checkAllMailLog([
      'Please review the membership request',
      'tabes@ranger.net'
    ]);

    // Send a welcome message.
    \Civi\Api4\MayfirstMember::Welcome()
      ->setMembershipId($this->membershipId)
      ->setMessage("Welcome Crowbar.")
      ->execute();

    // Ensure our welcome tokens are working.
    $this->checkAllMailLog([
      'We Bare Bears',
      'Welcome Crowbar',
      'crowbar@example.net',
      $this->renewalAmount . ' USD',
      'To be a hero' // Why Join value.
    ]);
  }

  public function testDuesNotification() {
    $this->addSpanishContact();
    \Civi\Api4\MayfirstMember::SendRenewalReminders()->execute();
    $this->checkAllMailLog([
      'Greetings',
      'Your support is critical',
      'membership is in the grace status',
      'Saludos',
      'Si tienes alguna pregunta',
      'de gracia'
    ]);
  }

  /**
   * If a member whose membership is disabled pays, ensure
   * that we get a notification so we can re-enable their hosting
   * orders.
   */
  public function testDisableToCurrentNotification() {
    // Set to disabled.
    \Civi\Api4\Membership::update()
      ->setCheckPermissions(FALSE)
      ->addValue('status_id:name', 'Disabled')
      ->addWhere('id', '=', $this->membershipId)
      ->execute();
    // Trigger auto calculation by setting end date in the future.
    $endDate = date('Ymd', time() + 86400);
    \Civi\Api4\Membership::update()
      ->setCheckPermissions(FALSE)
      ->addValue('end_date', $endDate)
      ->addWhere('id', '=', $this->membershipId)
      ->execute();
    $this->checkAllMailLog([
      'Please re-enable hosting orders for the member',
    ]);
  }
}

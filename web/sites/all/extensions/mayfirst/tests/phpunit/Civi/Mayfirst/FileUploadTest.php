<?php

namespace Civi\Mayfirst;
use \CRM_Mayfirst_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * Tests File upload and notifications. 
 * 
 * Members who pay in MXN can upload a scanned image of the receipt
 * of their membership dues bank transfer. This test ensures that the
 * upload worked and the proper perso is notified about it.
 *
 * @group headless
 */
class FileUploadTest extends MayfirstBase {

  public function setUp(): void {
    $this->currency = 'MXN';
    $this->startMonitoringEmail(); 
    $this->addNotificationGroupContacts();
    parent::setUp();
  }

  public function tearDown(): void {
    $this->stopMonitoringEmail(); 
    parent::tearDown();
  }

  /**
   * Add contacts to groups
   *
   * To fully test email notifications we have to add
   * contacts and then add those contacts to the notification
   * group.
   */
  private function addNotificationGroupContacts() {
    $notificationContactId = \Civi\Api4\Contact::create()
      ->addValue('first_name', 'Ranger')
      ->addValue('last_name', 'Tabes')
      ->execute()->first()['id'];

    \Civi\Api4\Email::create()
      ->addValue('email', 'tabes@ranger.net')
      ->addValue('location_type_id:name', 'Work')
      ->addValue('contact_id', $notificationContactId)
      ->execute();

    \Civi\Api4\GroupContact::create()
      ->addValue('group_id:name', 'Receive_Uploaded_Receipt_Notifications')
      ->addValue('contact_id', $notificationContactId)
      ->execute();
  }

  public function testFileUpload() {
    $_GET['token'] = \Civi\Mayfirst\Utils::getToken($this->membershipId);
    $tmpPath = tempnam('/tmp', 'receipt-test');
    $_FILES['file'] = [
      'type' => 'text/plain',
      'name' => 'test.receipt.txt',
      'tmp_name' => $tmpPath,
    ];

    $this->contributionId = \Civi\Api4\MayfirstMember::UploadReceipt()
      ->execute()->first()['id'];

    // Ensure we have an attachment.
    $contribution = \Civi\Api4\Contribution::get()
      ->addWhere('id', '=', $this->contributionId) 
      ->addSelect(
        'contact_id', 
        'contribution_status_id:name', 
        'Membership_Dues_Info.Transfer_Receipt',
        'currency'
      )
      ->execute()->first();
      
    $this->assertContributionStatus('Pending');
    // The transfer receipt should be a positive integer.
    $this->assertGreaterThan(0, $contribution['Membership_Dues_Info.Transfer_Receipt']);
    $this->assertEquals($contribution['currency'], $this->currency);
    $this->assertMembershipStatus('Grace');

    // Ensure our notify email was sent.
    $this->checkAllMailLog([
      'A member has uploaded',
    ]);
  }


}

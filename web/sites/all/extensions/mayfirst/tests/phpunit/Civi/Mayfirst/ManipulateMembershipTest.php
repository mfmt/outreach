<?php

namespace Civi\Mayfirst;
use \CRM_Mayfirst_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 *
 * Manipulate Membership Tests. 
 *
 * We manipulate memberships and membership payments in a few particular ways.
 *
 * 1. If a membership is expired, CiviCRM will reset the start date to be the
 * date they renew and push the expiration date forward a term. We intervene to
 * keep the old start date and expiration date so they don't get free time.
 *
 * 2. When a membership payment is made we record the start date of the membership
 * in the contribution record for accounting purposes and calculate how much of
 * the payment should be deferred to next year.
 *
 * These changes are implemented via the mayfirst_civicrm_post hook and the 
 * MayfirstMember.submitManualPayment API.
 *
 * @group headless
 *
 */

class ManipulateMembershipTest extends MayfirstBase {

  public function setUp(): void {
    parent::setUp();
    // Avoided undefined index errors.
    $_SERVER['HTTP_REFERER'] = 'foo.bar';
  }

  /**
   * Test recording the membership start date and deferred amount
   * for membership payments. 
   *
   */
  public function testRecordMembershipStartDateAndDeferredAmount() {
    // Change the membership start and end date to yesterday.
    $earlierDate = date('Y-m-d', strtotime("7 days ago"));
    \Civi\Api4\Membership::update()
      ->addValue('start_date', $earlierDate)
      ->addValue('end_date', $earlierDate)
      ->addWhere('id', '=', $this->membershipId)
      ->execute();

    $this->setupStripePaymentProcessor();

    $this->oneTimePayment('stripe'); 

    $contribution = \Civi\Api4\Contribution::get()
      ->addWhere('id', '=', $this->contributionId)
      ->addSelect(
        'receive_date',
        'total_amount',
        'Membership_Dues_Info.Membership_Payment_Deferred',
        'Membership_Dues_Info.Membership_Term_Start_Date'
      )
      ->execute()->first();

    $deferredAmount = \Civi\Mayfirst\Utils::getDeferredAmountForContribution($this->contributionId);
    $invoiceStartDate = substr(\Civi\Mayfirst\Utils::getInvoiceStartDateForContribution($this->contributionId), 0, 10);
    $endDate = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $this->membershipId)
      ->addSelect('end_date')
      ->execute()->first()['end_date'];

    $this->assertEquals(date('Y-m-d'), substr($contribution['receive_date'], 0, 10));
    $expectedDeferred = \Civi\Mayfirst\Utils::getDeferredAmount($earlierDate, $endDate, $contribution['total_amount']);
    $this->assertEquals($earlierDate, $invoiceStartDate, "Contribution custom membership start date is set properly.");
    $this->assertEquals($expectedDeferred, $deferredAmount, "Payment deferred amount is set properly");
  }

  /**
    * Test ensuring that with an expired membership, we don't reset the start
    * date to the current date, but ensure that members have to cover the
    * back pay.. 
    *
   */
  public function testKeepStartDateForExpiredMemberships() {
    // Change the membership start date to 15 months ago
    // and the end date to three months ago, and the status
    // to expired.
    $start = new \DateTimeImmutable();
    $start = $start->sub(new \DateInterval('P15M'));
    $end = new \DateTimeImmutable();
    $end = $end->sub(new \DateInterval('P3M'));

    // After we renew, we should have these values:
    $newStart = $end;
    $newEnd = $newStart->add(new \DateInterval('P1Y'));

    \Civi\Api4\Membership::update()
      ->addValue('start_date', $start->format('Ymd'))
      ->addValue('end_date', $end->format('Ymd'))
      ->addValue('status_id:name', 'Expired')
      ->addWhere('id', '=', $this->membershipId)
      ->execute();

    $this->setupStripePaymentProcessor();

    $this->oneTimePayment('stripe'); 

    $membership = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $this->membershipId)
      ->addSelect(
        'start_date',
        'end_date',
      )
      ->execute()->first();
    
    $this->assertEquals($newStart->format('Y-m-d'), $membership['start_date'], "Ensure start date is kept.");
    if ($newEnd->format('Y-m-d') != $membership['end_date']) {
      // Calendar math is weird. It should be within 5 days? Someons who is smarter
      // will someday make this work properly.
      $allowedMarginDays = 5;
      $actualEnd = new \DateTime($membership['end_date']);
      $interval = $newEnd->diff($actualEnd);
      $daysDiff = $interval->days;
      $this->assertLessThan($allowedMarginDays, $daysDiff, "Ensure end date makes sense based on kept start date.");
    }
  }
}

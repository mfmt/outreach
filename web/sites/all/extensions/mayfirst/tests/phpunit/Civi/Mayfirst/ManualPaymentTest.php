<?php

namespace Civi\Mayfirst;
use \CRM_Mayfirst_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * Tests payments made manually. 
 *
 * @group headless
 */
class ManualPaymentTest extends MayfirstBase {

  /**
   * Test making a one time stripe payment.
   *
   */
  public function testManualPayment() {
    $startDate = date('Y-m-d');
    $numTerms = 1;
    $membershipType = 'Annual';
    // Establish the initial end date - which is now.
    $this->assertMembershipEndDate($startDate, 'Before first payment');
    $endDate = \Civi\Mayfirst\Utils::calculateEndDate($startDate, $membershipType, $numTerms);

    $paymentInstrumentId = \Civi\Api4\OptionValue::get()
      ->addSelect('value')
      ->addWhere('option_group_id:name', '=', 'payment_instrument')
      ->addWhere('name', '=', 'EFT')
      ->execute()->first()['value'];

    $paymentResult = \Civi\Api4\MayfirstMember::SubmitManualPayment()
      ->setMembershipId($this->membershipId)
      ->setContactId($this->contactId)
      ->setTotalAmount("$this->renewalAmount")
      ->setCurrency($this->currency)
      ->setReceiveDate(date('Y-m-d'))
      ->setNumTerms($numTerms)
      ->setEndDate($endDate)
      ->setPaymentInstrumentId($paymentInstrumentId)
      ->execute()->first();

    $this->contributionId = $paymentResult['contribution_id'];
    $this->assertContributionStatus('Completed');
    $this->assertMembershipEndDate(date('Y-m-d', strtotime("+1 year")));
    $this->assertMembershipStatus('Current');  
  }
}

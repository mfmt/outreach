<?php

namespace Civi\Mayfirst;
use \CRM_Mayfirst_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;
use Stripe\StripeObject;

/**
 * Base test class providing helpers for the other test classes. 
 *
 * @group headless
 */
class MayfirstBase extends \PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  protected $contactId;
  protected $membershipId;
  protected $renewalAmount;
  protected $createdTs;
  protected $contributionId;
  protected $invoiceId;
  protected $firstName = 'Crowbar';
  protected $lastName = 'Jones';
  protected $currency = 'USD';
  protected $stripeCustomerId = 'cus_abc';
  protected $paymentProcessorId;
  protected $paymentObject;

  private $originalOutboundOption;

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://docs.civicrm.org/dev/en/latest/testing/phpunit/#civitest
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->install(['mayfirst', 'mjwshared', 'com.drastikbydesign.stripe', 'firewall', 'formprotection'])
      ->callback(function() {
        // Configure form protection. NOTE: formprotection should not prevent membership payments,
        // we are configuring it here so if it does inhibit them, the tests will fail.
        $settings = [
          'formprotection_enable_test' => TRUE,
          'formprotection_enable_firewall_integration' => TRUE,
          'formprotection_enable_floodcontrol' => TRUE,
          'formprotection_floodcontrol_enabled_quickforms' => [ 'CRM_Contribute_Form_Contribution_Main' ],
          'formprotection_floodcontrol_recaptcha' => TRUE,
          'formprotection_enable_recaptcha' => 'v3',
          'formprotection_recaptcha_enabled_quickforms' => [ 'CRM_Contribute_Form_Contribution_Main' ],
        ];
        foreach ($settings as $k => $v) {
          \Civi\Api4\Setting::set()
            ->addValue($k, $v)
            ->execute();
        }
      }, 'setupFormprotection')
      ->apply();
  }

  public function setUp(): void {
    parent::setUp();
    \CRM_Utils_System::flushCache();
    \CRM_Core_ManagedEntities::singleton(TRUE)->reconcile();
    if (!defined('CIVICRM_UF_BASEURL')) {
      define('CIVICRM_UF_BASEURL', 'http://localhost/');
    }
    $this->createdTs = time();
    $this->setupMemberNotificationContact();
    $this->setupMembership();
  }

  private function setupMemberNotificationContact() {
    $contact = \Civi\Api4\Contact::create()
      ->addChain('email', \Civi\Api4\Email::create()
        ->addValue('contact_id', '$id')
        ->addValue('email', 'members@mayfirst.org')
        ->addValue('location_type_id:name', 'Work')
      )
      ->addValue('contact_type', 'Individual')
      ->addValue('first_name', 'May First')
      ->addValue('last_name', 'Member contact')
      ->execute();
  }

  private function setupMembership() {
    $fields = [
      'contact_type' => 'Organization',
      'organization_name' => 'We Bare Bears',
      'first_name' => $this->firstName,
      'last_name' => $this->lastName,
      'email_address' => 'crowbar@example.net',
      'mobile_phone' => '5555555555',
      'city' => 'San Francisco',
      'state_province' => [ 'id' => 1004 ],  // California
      'country' => [ 'id' => 1228 ], // US
      'hosting_benefits' => 1,
      'virtual_private_servers' => 1,
      'cpu' => 1,
      'ram' => 1,
      'ssd' => 10,
      'domain_name' => 'crowbarjones.org',
      'income' => 2,
      'plan' => 2,
      'preferred_language' => 'en_US',
      'why_join' => 'To be a hero',
      'membership_type' => 'Annual',
      'currency' => $this->currency,
      'extra_storage' => 20,
      'dues_reduction_description' => 'We are bears with no jobs',
    ];
    $this->membershipId = \Civi\Api4\MayfirstMember::Save()
      ->setFields($fields)
      ->execute()->first()['id'];

    // Lookup the contact id and renewal amount that was created.
    $membership = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $this->membershipId)
      ->addSelect('contact_id', 'Membership_Details.Renewal_Amount')
      ->execute()->first();
    $this->contactId = $membership['contact_id'];
    $this->renewalAmount = $membership['Membership_Details.Renewal_Amount'];

    // We should start in Requested state.
    $this->assertMembershipStatus('Requested');

    \Civi\Api4\MayfirstMember::activateMembership()
      ->setMembershipId($this->membershipId)
      ->execute();
   
    // Now we should be in grace.
    $this->assertMembershipStatus('Grace');
  }

  protected function setupStripePaymentProcessor() {
    if (!defined('STRIPE_PHPUNIT_TEST')) {
      define('STRIPE_PHPUNIT_TEST', 1);
    }
    $pp = $this->getOrCreateStripePaymentProcessor();
    $this->paymentProcessorId = $pp['id'];
    $this->paymentObject = \Civi\Payment\System::singleton()->getById($pp['id']);
    // Set params, creates stripeClient
    $this->paymentObject->setAPIParams();

    // Create a mock client so we don't verify our payments.
    $stripeClient = $this->createMock('Stripe\\StripeClient');

    // The balance transactions is where Stripe verifies that what we receive
    // via (our fake) webhook notifications are legit. That's what we have to
    // override.
    $stripeClient->balanceTransactions = $this->createMock('Stripe\\Service\\BalanceTransactionService');
    $fakeBalanceTransaction = new \stdClass();
    $fakeBalanceTransaction->id = 'txn_test';
    $fakeBalanceTransaction->fee = $this->renewalAmount * 100 * .029 + .30;
    $fakeBalanceTransaction->amount = $this->renewalAmount * 100;
    $fakeBalanceTransaction->currency = $this->currency;
    $fakeBalanceTransaction->exchange_rate = NULL;
    $fakeBalanceTransaction->object = 'balance_transaction';
    $fakeBalanceTransaction->available_on = time(); 

    $stripeClient->balanceTransactions
      ->method('retrieve')
      ->with($this->equalTo('txn_test'))
      ->willReturn($fakeBalanceTransaction);

    // In addition we call paymentMethods->create which should return
    // a payment method object.
    $stripeClient->paymentMethods = $this->createMock('Stripe\\Service\\PaymentMethodService');
    $fakePaymentMethod = new \stdClass();
    $fakePaymentMethod->id = 'pm_test';

    $stripeClient->paymentMethods
      ->method('create')
      ->willReturn($fakePaymentMethod);

    $fakeCharge = new \stdClass();
    $fakeCharge->id = 'ch_test';
    $fakeCharge->object = 'charge';
    $fakeCharge->captured = TRUE;
    $fakeCharge->status = 'succeeded';
    $fakeCharge->balance_transaction = 'txn_test';
    $fakeCharge->amount = $this->renewalAmount * 100;
    $stripeClient->charges = $this->createMock('Stripe\\Service\\ChargeService');
    $stripeClient->charges
      ->method('retrieve')
      ->with($this->equalTo('ch_test'))
      ->willReturn($fakeCharge);

    $this->paymentObject->setMockStripeClient($stripeClient);

    // Add a customer id
    \Civi\Api4\StripeCustomer::create(FALSE)
      ->addValue('customer_id', $this->stripeCustomerId)
      ->addValue('processor_id', $this->paymentProcessorId)
      ->addValue('contact_id', $this->contactId)
      ->addValue('currency', $this->currency)
      ->execute();
  }

  protected function getOrCreateStripePaymentProcessor(): array {
    $params = [
      'name' => 'Stripe',
      'domain_id' => \CRM_Core_Config::domainID(),
      'payment_processor_type_id:name' => 'Stripe',
      'title' => 'Stripe',
      'is_active' => 1,
      'is_default' => 0,
      'is_test' => 1,
      'is_recur' => 1,
      'user_name' => 'pk_test_123',
      'password' => 'sk_test_123',
      'url_site' => 'https://api.stripe.com/v1',
      'url_recur' => 'https://api.stripe.com/v1',
      'class_name' => 'Payment_Stripe',
      'billing_mode' => 1
    ];
    // First see if it already exists.
    $pp = \Civi\Api4\PaymentProcessor::get()
      ->addWhere('name', '=', 'Stripe')
      ->execute()->first();
    if (!$pp) {
      $pp = \Civi\Api4\PaymentProcessor::create();
      foreach($params as $k => $v) {
        $pp = $pp->addValue($k, $v);
      };
      $pp = $pp->execute()->first();
    }
    return $pp;
  }

  protected function setupPayPalPaymentProcessor() {
    if (!$this->paymentProcessorId) {
      $this->paymentProcessorId = \Civi\Api4\PaymentProcessor::create()
        ->addvalue("name", "PayPal")
        ->addValue("payment_processor_type_id:name", "PayPal_Standard")
        ->addValue("is_test", TRUE)
        ->addValue("user_name", "paypal@example.net")
        ->addValue("url_site", "https://www.sandbox.paypal.com/")
        ->addValue("url_recur", "https://www.sandbox.paypal.com/")
        ->addValue("class_name", "Payment_PayPalImpl")
        ->addValue("billing_mode", 4)
        ->addValue("is_recur", true)
        ->addvalue("payment_type", 1)
        ->addvalue("payment_instrument_id", 1)
        ->execute()->first()['id'];
    }
  }
  public function tearDown(): void {
    parent::tearDown();
    \Civi\Api4\MembershipType::delete()
      ->addWhere('name', '=', 'Annual')
      ->execute();
  }


  protected function assertContributionStatus($wantStatus, $desc = '') {
    $isStatus = \Civi\Api4\Contribution::get()
      ->addWhere('id', '=', $this->contributionId)
      ->addSelect('contribution_status_id:name')
      ->execute()->first()['contribution_status_id:name'];
    $this->assertEquals($wantStatus, $isStatus, $desc);
  }

  protected function assertContributionRecurStatus($wantStatus, $desc = '') {
    $isStatus = \Civi\Api4\ContributionRecur::get()
      ->addWhere('id', '=', $this->contributionRecurId)
      ->addSelect('contribution_status_id:name')
      ->execute()->first()['contribution_status_id:name'];
    $this->assertEquals($wantStatus, $isStatus, $desc);
  }

  protected function assertMembershipStatus($wantStatus, $desc = '') {
    $isStatus = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $this->membershipId)
      ->addSelect('status_id:name')
      ->execute()->first()['status_id:name'];
    $this->assertEquals($wantStatus, $isStatus, $desc);
  }

  protected function assertMembershipEndDate($wantEndDate, $desc = '') {
    $isEndDate = \Civi\Api4\Membership::get()
      ->addWhere('id', '=', $this->membershipId)
      ->addSelect('end_date')
      ->execute()->first()['end_date'];
    $this->assertEquals($wantEndDate, $isEndDate, $desc);
  }

  /**
   * Check contents of mail log.
   *
   * @param array $strings
   *   Strings that should be included.
   * @param array $absentStrings
   *   Strings that should not be included.
   * @param string $prefix
   *
   * @return \ezcMail|string
   */
  protected function checkAllMailLog($strings, $absentStrings = [], $prefix = '') {
    $mails = $this->getAllMessages();
    $mail = implode(',', $mails);
    return $this->checkMailForStrings($strings, $absentStrings, $prefix, $mail);
  }

  /**
   * @param string $type
   *   'raw'|'ezc'.
   *
   * @throws CRM_Core_Exception
   *
   * @return array
   */
  protected function getAllMessages(): array {
    $msgs = [];

    $dao = \CRM_Core_DAO::executeQuery('SELECT headers, body FROM civicrm_mailing_spool ORDER BY id');
    while ($dao->fetch()) {
      $msgs[] = $dao->headers . "\n\n" . $dao->body;
    }
    return $msgs;
  }

   /**
   * @param array $strings
   * @param $absentStrings
   * @param $prefix
   * @param $mail
   * @return mixed
   */
  protected function checkMailForStrings(array $strings, $absentStrings, $prefix, $mail) {
    foreach ($strings as $string) {
      $this->assertStringContainsString($string, $mail, "$string .  not found in  $mail  $prefix");
    }
    foreach ($absentStrings as $string) {
      $this->assertEmpty(strstr($mail, $string), "$string  incorrectly found in $mail $prefix");
    }
    return $mail;
  }

  protected function startMonitoringEmail() {
    // Start tracking email message sent.
    $mailingBackend = \CRM_Core_BAO_Setting::getItem(\CRM_Core_BAO_Setting::MAILING_PREFERENCES_NAME,
      'mailing_backend'
    );
    $this->originalOutboundOption = $mailingBackend['outBound_option'];
    $mailingBackend['outBound_option'] = \CRM_Mailing_Config::OUTBOUND_OPTION_REDIRECT_TO_DB;
    \Civi::settings()->set('mailing_backend', $mailingBackend);
  }

  protected function stopMonitoringEmail() {
   $mailingBackend = \CRM_Core_BAO_Setting::getItem(\CRM_Core_BAO_Setting::MAILING_PREFERENCES_NAME,
      'mailing_backend'
    );
    $mailingBackend['outBound_option'] = $this->originalOutboundOption;
    \Civi::settings()->set('mailing_backend', $mailingBackend);

  }

  protected function generateStripeIPNEvent($type, $recur = NULL) {
    if (is_null($recur)) {
      // Silly json wants null string here.
      $subscription = 'null';
    }
    else {
      $subscription = $recur;
    }
    $created = time();
    $expires = time() + 1000;
    switch($type) {
      case 'checkout.session':
        $json = '{
          "id":"evt_test",
          "object":"event",
          "api_version":"2022-11-15",
          "created":' . $created . ',
          "data":{
            "object":{
              "id": "cs_test_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
              "object":"checkout.session",
              "after_expiration":null,
              "allow_promotion_codes":null,
              "amount_subtotal": ' . $this->renewalAmount * 100 . ',
              "amount_total": ' . $this->renewalAmount * 100 . ',
              "automatic_tax":{
                "enabled":false,
                  "status":null
              },
              "billing_address_collection":null,
              "cancel_url":"",
              "client_reference_id": "'. $this->invoiceId . '",
              "consent":null,
              "consent_collection":null,
              "created":' . $created . ',
              "currency":"usd",
              "currency_conversion":null,
              "custom_fields":[],
              "custom_text":{
              "shipping_address":null,
                "submit":null,
                "terms_of_service_acceptance":null
              },
              "customer":"' . $this->stripeCustomerId . '",
              "customer_creation":null,
              "customer_details":{
                "address":{
                  "city":null,
                    "country":"US",
                    "line1":null,
                    "line2":null,
                    "postal_code":"12345",
                    "state":null
                  },
                 "email":"stripe.buyer@example.net",
                 "name":"' . $this->firstName . ' ' . $this->lastName . '",
                 "phone":null,
                 "tax_exempt":"none",
                 "tax_ids":[]
              },
              "customer_email":null,
              "expires_at":' . $expires . ',
              "invoice":null,
              "invoice_creation":{
                "enabled":false,
                "invoice_data":{
                  "account_tax_ids":null,
                  "custom_fields":null,
                  "description":null,
                  "footer":null,
                  "metadata":[],
                  "rendering_options":null
                }
              },
              "livemode":true,
              "locale":null,
              "metadata":[],
              "mode":"payment",
              "payment_intent":"pi_test",
              "payment_link":null,
              "payment_method_collection":"always",
              "payment_method_configuration_details":null,
              "payment_method_options":[],
              "payment_method_types":["card"],
              "payment_status":"paid",
              "phone_number_collection":{
                "enabled":false
              },
              "recovered_from":null,
              "setup_intent":null,
              "shipping_address_collection":null,
              "shipping_cost":null,
              "shipping_details":null,
              "shipping_options":[],
              "status":"complete",
              "submit_type":null,
              "subscription":' . $subscription . ',
              "success_url":"",
              "total_details":{
                "amount_discount":0,
                "amount_shipping":0,
                "amount_tax":0
              },
              "url":null
            }
          },
          "livemode":false,
          "pending_webhooks":1,
          "request":{
            "id":null,
              "idempotency_key":null
          },
          "type":"checkout.session.completed"
        }';
        break;

      case 'charge.succeeded':
        $json = '{
            "id":"evt_def",
            "object":"event",
            "api_version":"2022-11-15",
            "created":' . $created . ',
            "data":{
              "object": {
                "id": "ch_test",
                "object": "charge",
                "amount": ' . $this->renewalAmount * 100 . ',
                "amount_captured": ' . $this->renewalAmount * 100 . ',
                "amount_refunded": 0,
                "application": null,
                "application_fee": null,
                "application_fee_amount": null,
                "balance_transaction": "txn_test",
                "billing_details": {
                    "address": {
                        "city": null,
                        "country": "US",
                        "line1": null,
                        "line2": null,
                        "postal_code": "12345",
                        "state": null
                    },
                    "email": "stripe.buyer@example.net",
                    "name": "' . $this->firstName . ' ' . $this->lastName . '",
                    "phone": null
                },
                "calculated_statement_descriptor": "Test",
                "captured": true,
                "created": ' . $created . ',
                "currency": "usd",
                "customer": "' . $this->stripeCustomerId . '",
                "description": "test description",
                "destination": null,
                "dispute": null,
                "disputed": false,
                "failure_balance_transaction": null,
                "failure_code": null,
                "failure_message": null,
                "fraud_details": [],
                "invoice": null,
                "livemode": true,
                "metadata": [],
                "on_behalf_of": null,
                "order": null,
                "outcome": {
                    "network_status": "approved_by_network",
                    "reason": null,
                    "risk_level": "normal",
                    "seller_message": "Payment complete.",
                    "type": "authorized"
                },
                "paid": true,
                "payment_intent": "pi_test",
                "payment_method": "pm_test",
                "payment_method_details": {
                    "card": {
                        "amount_authorized": ' . $this->renewalAmount . ',
                        "brand": "mastercard",
                        "checks": {
                            "address_line1_check": null,
                            "address_postal_code_check": "pass",
                            "cvc_check": "pass"
                        },
                        "country": "US",
                        "exp_month": 7,
                        "exp_year": 2084,
                        "extended_authorization": {
                            "status": "disabled"
                        },
                        "fingerprint": "abc",
                        "funding": "credit",
                        "incremental_authorization": {
                            "status": "unavailable"
                        },
                        "installments": null,
                        "last4": "1234",
                        "mandate": null,
                        "multicapture": {
                            "status": "unavailable"
                        },
                        "network": "mastercard",
                        "network_token": {
                            "used": false
                        },
                        "overcapture": {
                            "maximum_amount_capturable": ' . $this->renewalAmount . ',
                            "status": "unavailable"
                        },
                        "three_d_secure": null,
                        "wallet": null
                    },
                    "type": "card"
                },
                "receipt_email": null,
                "receipt_number": null,
                "receipt_url": "",
                "refunded": false,
                "review": null,
                "shipping": null,
                "source": null,
                "source_transfer": null,
                "statement_descriptor": null,
                "statement_descriptor_suffix": null,
                "status": "succeeded",
                "transfer_data": null,
                "transfer_group": null
            }
          },
          "livemode":false,
          "pending_webhooks":1,
          "request":{
            "id":null,
              "idempotency_key":null
          },
          "type":"charge.succeeded"
        }';
        break;
      case 'invoice.paid':
        $json = '{
          "id":"evt_ghi",
          "object":"event",
          "api_version":"2022-11-15",
          "created":' . $created . ',
          "data":{
            "object": {
              "id": "in_abc",
              "object": "invoice",
              "account_country": "US",
              "account_name": "May First Movement Technology",
              "account_tax_ids": null,
              "amount_due": ' . $this->renewalAmount * 100 . ',
              "amount_paid": ' . $this->renewalAmount * 100 . ',
              "amount_remaining": 0,
              "amount_shipping": 0,
              "application": null,
              "application_fee_amount": null,
              "attempt_count": 1,
              "attempted": true,
              "auto_advance": false,
              "automatic_tax": {
                  "enabled": false,
                  "status": null
              },
              "billing_reason": "subscription_create",
              "charge": "ch_test",
              "collection_method": "charge_automatically",
              "created": ' . $created . ',
              "currency": "usd",
              "custom_fields": null,
              "customer": ' . $this->stripeCustomerId . ',
              "customer_address": null,
              "customer_email": "stripe.buyer@example.net",
              "customer_name": "' . $this->firstName . ' ' . $this->lastName . '",
              "customer_phone": null,
              "customer_shipping": null,
              "customer_tax_exempt": "none",
              "customer_tax_ids": [],
              "default_payment_method": null,
              "default_source": null,
              "default_tax_rates": [],
              "description": null,
              "discount": null,
              "discounts": [],
              "due_date": null,
              "effective_at": . ' . $created . ',
              "ending_balance": 0,
              "footer": null,
              "from_invoice": null,
              "hosted_invoice_url": "",
              "invoice_pdf": "",
              "last_finalization_error": null,
              "latest_revision": null,
              "lines": {
                  "object": "list",
                  "data": [
                      {
                          "id": "il_abc",
                          "object": "line_item",
                          "amount": ' . $this->renewalAmount * 100 . ',
                          "amount_excluding_tax": ' . $this->renewalAmount * 100 . ',
                          "currency": "usd",
                          "description": "1 \u00d7 May First Membership Renewal (at $125.00 \/ year)",
                          "discount_amounts": [],
                          "discountable": true,
                          "discounts": [],
                          "livemode": true,
                          "metadata": [],
                          "period": {
                              "end": ' . strval($created + 10000000) . ',
                              "start": ' . $created . '
                          },
                          "plan": {
                              "id": "price_abc",
                              "object": "plan",
                              "active": false,
                              "aggregate_usage": null,
                              "amount": ' . $this->renewalAmount * 100 . ',
                              "amount_decimal": "' . $this->renewalAmount * 100 . '",
                              "billing_scheme": "per_unit",
                              "created": ' . $created . ',
                              "currency": "usd",
                              "interval": "year",
                              "interval_count": 1,
                              "livemode": true,
                              "metadata": [],
                              "nickname": null,
                              "product": "prod_abc",
                              "tiers": null,
                              "tiers_mode": null,
                              "transform_usage": null,
                              "trial_period_days": null,
                              "usage_type": "licensed"
                          },
                          "price": {
                              "id": "price_abc",
                              "object": "price",
                              "active": false,
                              "billing_scheme": "per_unit",
                              "created": ' . $created . ',
                              "currency": "usd",
                              "custom_unit_amount": null,
                              "livemode": true,
                              "lookup_key": null,
                              "metadata": [],
                              "nickname": null,
                              "product": "prod_abc",
                              "recurring": {
                                  "aggregate_usage": null,
                                  "interval": "year",
                                  "interval_count": 1,
                                  "trial_period_days": null,
                                  "usage_type": "licensed"
                              },
                              "tax_behavior": "unspecified",
                              "tiers_mode": null,
                              "transform_quantity": null,
                              "type": "recurring",
                              "unit_amount": ' . $this->renewalAmount * 100 . ',
                              "unit_amount_decimal": "' . $this->renewalAmount . '"
                          },
                          "proration": false,
                          "proration_details": {
                              "credited_items": null
                          },
                          "quantity": 1,
                          "subscription": "' . $subscription . '",
                          "subscription_item": "si_abc",
                          "tax_amounts": [],
                          "tax_rates": [],
                          "type": "subscription",
                          "unit_amount_excluding_tax": "' . $this->renewalAmount * 100 . '"
                      }
                  ],
                  "has_more": false,
                  "total_count": 1,
                  "url": "\/v1\/invoices\/in_abc\/lines"
              },
              "livemode": true,
              "metadata": [],
              "next_payment_attempt": null,
              "number": "8C6C419D-0002",
              "on_behalf_of": null,
              "paid": true,
              "paid_out_of_band": false,
              "payment_intent": "pi_abc",
              "payment_settings": {
                  "default_mandate": null,
                  "payment_method_options": null,
                  "payment_method_types": null
              },
              "period_end": ' . strval($created + 10000) . ',
              "period_start": ' . $created . ',
              "post_payment_credit_notes_amount": 0,
              "pre_payment_credit_notes_amount": 0,
              "quote": null,
              "receipt_number": null,
              "rendering": null,
              "rendering_options": null,
              "shipping_cost": null,
              "shipping_details": null,
              "starting_balance": 0,
              "statement_descriptor": null,
              "status": "paid",
              "status_transitions": {
                  "finalized_at": ' . $created . ',
                  "marked_uncollectible_at": null,
                  "paid_at": ' . $created . ',
                  "voided_at": null
              },
              "subscription": "' . $subscription . '",
              "subscription_details": {
                  "metadata": []
              },
              "subtotal": ' . $created * 100 . ',
              "subtotal_excluding_tax": ' . $created * 100 . ',
              "tax": null,
              "tax_percent": null,
              "test_clock": null,
              "total": 12500,
              "total_discount_amounts": [],
              "total_excluding_tax": ' . $this->renewalAmount * 100 . ',
              "total_tax_amounts": [],
              "transfer_data": null,
              "webhooks_delivered_at": ' . $created . '
            }
          },
          "livemode":false,
          "pending_webhooks":1,
          "request":{
            "id":null,
            "idempotency_key":null
          },
          "type":"invoice.paid" 
        }';
      break;

    }
    return json_decode($json, TRUE);

  }

  protected function generatePayPalIPN($type) {
    switch($type) {
      case 'web_accept':
        return [
          "mc_gross" => $this->renewalAmount,
          "invoice" => $this->invoiceId,
          "protection_eligibility" => "Eligible",
          "payer_id" => "payeridabc",
          "payment_date" => date('H:i:s M d, Y T'),
          "payment_status" => "Completed",
          "charset" => "windows-1252",
          "first_name" => $this->firstName,
          "mc_fee" => number_format($this->renewalAmount * .038, 2),
          "notify_version" => 3.9,
          "custom" => '{"module":"membership","contactID":' . $this->contactId . ',"contributionID":' . $this->contributionId . '}',
          "payer_status" => "verified",
          "business" => "paypal.seller@example.net",
          "quantity" => 1,
          "verify_sign" => "verifysignabc",
          "payer_email" => "paypal.buyer@example.net",
          "txn_id" => "txnabc" . sha1(rand(10000000, getrandmax())),
          "payment_type" => "instant",
          "last_name" => $this->lastName,
          "receiver_email" => "paypal.seller@example.net",
          "payment_fee" => number_format($this->renewalAmount * .038, 2),
          "shipping_dscount" => 0.00,
          "receiver_id" => "receiveabc",
          "insurance_amount" => 0.00,
          "txn_type" => "web_accept",
          "item_name" => "abc May First Membership Renewal",
          "discount" => 0.00,
          "mc_currency" => "USD",
          "item_number" => NULL,
          "residence_country" => "US",
          "test_ipn" => 1,
          "shipping_method" => "Default",
          "transaction_subject" => NULL,
          "payment_gross" => $this->renewalAmount,
          "ipn_track_id" => "ipntrackabc",
          "IDS_request_uri" => "/civicrm/payment/ipn/" . $this->paymentProcessorId,
          "IDS_user_agent" => "PayPal IPN ( https://www.paypal.com/ipn )",
          'processor_id' => $this->paymentProcessorId,
        ];
      case 'subscr_signup':
        return [ 
          'txn_type' => 'subscr_signup',
          'subscr_id' => 'subscriber_idabc',
          'last_name' => $this->lastName,
          'residence_country' => 'US',
          'mc_currency' => 'USD',
          'item_name' => $this->contactId .  '-' . $this->contributionRecurId . '-May First Membership Renewal',
          "business" => "paypal.seller@example.net",
          'amount3' => $this->renewalAmount,
          'recurring' => '1',
          'verify_sign' => 'abc',
          'payer_status' => 'verified',
          'test_ipn' => '1',
          "payer_email" => "paypal.buyer@example.net",
          'first_name' => $this->firstName,
          "receiver_email" => "paypal.seller@example.net",
          'payer_id' => 'payeridabc',
          "invoice" => $this->invoiceId,
          'reattempt' => '1',
          "subscr_date" => date('H:i:s M d, Y T'),
          "custom" => '{"module":"membership","contactID":' . $this->contactId . ',"contributionID":' . $this->contributionId . '}',
          'charset' => 'windows-1252',
          'notify_version' => '3.9',
          'period3' => '1 Y',
          'mc_amount3' => $this->renewalAmount,
          'ipn_track_id' => 'ipntrackabc',
          "IDS_request_uri" => "/civicrm/payment/ipn/" . $this->paymentProcessorId,
          'IDS_user_agent' => 'PayPal IPN ( https://www.paypal.com/ipn )',
          'processor_id' => $this->paymentProcessorId,
        ];
      case 'subscr_payment':
        return [
          'transaction_subject' => $this->contactId .  '-' . $this->contributionRecurId . '-May First Membership Renewal',
          "payment_date" => date('H:i:s M d, Y T'),
          'txn_type' => 'subscr_payment',
          'subscr_id' => 'subscriber_idabc',
          'last_name' => $this->lastName,
          'residence_country' => 'US',
          'item_name' => $this->contactId .  '-' . $this->contributionRecurId . '-May First Membership Renewal',
          'payment_gross' => $this->renewalAmount,
          'mc_currency' => 'USD',
          "business" => "paypal.seller@example.net",
          'payment_type' => 'instant',
          'protection_eligibility' => 'Eligible',
          'verify_sign' => 'verify_signabc',
          'payer_status' => 'verified',
          'test_ipn' => '1',
          "payer_email" => "paypal.buyer@example.net",
          'txn_id' => $this->invoiceId,
          "receiver_email" => "paypal.seller@example.net",
          'first_name' => 'Test',
          'invoice' => $this->invoiceId,
          'payer_id' => 'payer_idabc',
          'receiver_id' => 'receive_idabc',
          'payment_status' => 'Completed',
          "payment_fee" => number_format($this->renewalAmount * .038, 2),
          "mc_fee" => number_format($this->renewalAmount * .038, 2),
          'mc_gross' => $this->renewalAmount,
          "custom" => '{"module":"membership","contactID":' . $this->contactId . ',"contributionID":' . $this->contributionId . '}',
          'charset' => 'windows-1252',
          'notify_version' => '3.9',
          'ipn_track_id' => 'ipn_track_idabc',
          "IDS_request_uri" => "/civicrm/payment/ipn/" . $this->paymentProcessorId,
          'IDS_user_agent' => 'PayPal IPN ( https://www.paypal.com/ipn )',
          'processor_id' => $this->paymentProcessorId,
        ];
    }
  }

  protected function buildStripeIPNObject($event) {
    //$rawData = file_get_contents("php://input");
    //$event = json_decode($rawData, TRUE);
    $ipnClass = new \CRM_Core_Payment_StripeIPN($this->paymentObject);
    $id = $event['id'] ?? NULL;
    $type = $event['type'] ?? NULL;
    $rawData = $event['data'] ?? NULL;
    $ipnClass->setEventID($id);
    $ipnClass->setEventType($type);
    $data = StripeObject::constructFrom($rawData);
    $ipnClass->setData($data);
    $ipnClass->setVerifyData(FALSE);
    return $ipnClass;
  }

  /**
   * Making a one time payment.
   *
   * @var $processor - either paypal or stripe
   *
   */
  protected function oneTimePayment($processor) {
    $token = \Civi\Mayfirst\Utils::getToken($this->membershipId);
    $result = \Civi\Api4\MayfirstMember::SubmitPayment()
      ->setToken($token)
      ->setPaymentProcessorId($this->paymentProcessorId)
      ->setTestMode(1)
      ->setAutomaticRenewal(0)
      ->execute()->first();

    $this->contributionId = $result['contribution_id'];
    $this->invoiceId = $result['invoice_id'];
    
    // Simulate confirmation of payment.
    $this->oneTimeIPN($processor);

  }

  /**
   * Similuate the payment processor notifying us of the one time
   * payment being successfully processed.
   */
  protected function oneTimeIPN($processor) {
    // The user navigates to Pay Pal to complete the transactin
    // so we have no way to know whether it went through or not
    // until we get the Instant Payment Notification (IPN). Here
    // we simulate receiving it.
    if ($processor == 'paypal') {
      $ipn = $this->generatePayPalIPN('web_accept');
      $ipnClass = new \CRM_Core_Payment_PayPalIPN($ipn);
      $ipnClass->main();
    }
    else if ($processor == 'stripe') {
      $event = $this->generateStripeIPNEvent('checkout.session');
      $ipnClass = $this->buildStripeIPNObject($event);
      $ipnClass->onReceiveWebhook();

      $event = $this->generateStripeIPNEvent('charge.succeeded');
      $ipnClass = $this->buildStripeIPNObject($event);
      $ipnClass->onReceiveWebhook();

    }
  }

  /**
   * Test making a recurring payment.
   *
   */
  protected function recurringPayment($processor) {
    $token = \Civi\Mayfirst\Utils::getToken($this->membershipId);
    $result = \Civi\Api4\MayfirstMember::SubmitPayment()
      ->setToken($token)
      ->setPaymentProcessorId($this->paymentProcessorId)
      ->setTestMode(1)
      ->setAutomaticRenewal(1)
      ->execute()->first();

    $this->contributionId = $result['contribution_id'];
    $this->invoiceId = $result['invoice_id'];
    $this->contributionRecurId = $result['contribution_recur_id'];

    // Simulate response form payment processor.
    $this->recurringIPN($processor);
  }

  protected function recurringIPN($processor) {
    if ($processor == 'paypal') {
      $ipn = $this->generatePayPalIPN('subscr_signup');
      $ipnClass = new \CRM_Core_Payment_PayPalIPN($ipn);
      $ipnClass->main();
      $ipn = $this->generatePayPalIPN('subscr_payment');
      $ipnClass = new \CRM_Core_Payment_PayPalIPN($ipn);
      $ipnClass->main();
    }
    else {
      $recur = 'sub_abc';
      $event = $this->generateStripeIPNEvent('checkout.session', $recur);
      $ipnClass = $this->buildStripeIPNObject($event);
      $ipnClass->onReceiveWebhook();

      $eventInvoicePaid = $this->generateStripeIPNEvent('invoice.paid');
      $ipnClass = $this->buildStripeIPNObject($eventInvoicePaid);
      $ipnClass->onReceiveWebhook();

    }
  }
}

